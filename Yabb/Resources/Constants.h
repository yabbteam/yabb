

#define APPLE_STORE_VERSION



typedef enum V_ACCOUNT_TYPE {
	ACCOUNT_DEFAULT = -1,
	ACCOUNT_SIP=0,
    ACCOUNT_GSM=1
} ACCOUNTTYPE;

enum {
	kSipFieldDomain   = 0,  
	kSipFieldUserName,
	kSipFieldPassword,
	kSipProxyServer,
	kSipFieldRegister,
	kSipTempRegID
};

#define KEEP_ALIVE_INTERVAL 600
#define INCOMING_CALL_MSG @"kIncomingCall"


#define SIP_USERNAME_KEY	@"kSipUsername"
#define SIP_PASSWORD_KEY	@"kSipPassword"
#define SIP_REGISTER_KEY	@"kSipRegister"

#define GSM_LAST_NUM_KEY    @"kGsmLastNum"
#define GSM_PINLESS_KEY     @"kGsmPinless"
#define GSM_CITY_KEY        @"kGsmCity"
#define GSM_COUNTRY_KEY     @"kGsmCountry"
#define GSM_IDD_KEY         @"kGsmIDD"
#define GSM_CITYPH_KEY      @"kGsmCityPh"

#define PIN_PREV_BAL_KEY    @"kPinPrevBal"
#define PIN_PREV_TIM_KEY    @"kPinPrevTim"
#define PIN_CARD_KEY		@"kPinCard"
#define PIN_SERIAL_KEY      @"kPinSerial"
#define PIN_DOMAIN_KEY      @"kPinDomain"



// Configuration file
#define APP_SIPCODEC_KEY			@"SIPCODEC"
#define APP_SIPCODEC_VALUE			@"0"
#define APP_SIPSTUN_KEY				@"SIPSTUN"
#define APP_SIPSTUN_VALUE			@"0"
#define APP_SIPSTUNSRV_KEY			@"SIPSTUNSRV"
#define APP_SIPSTUNSRV_VALUE		@""
#define APP_DEFAULT_SIP_KEY			@"DEFAULTSIP"
#define APP_DEFAULT_SIP_VALUE		@"1"
#define APP_NULL_VALUE				@""
#define APP_SIPPREFIX_KEY			@"SIPPREFIX"
#define APP_SIP_OUT_PROXY_KEY		@"SIPOUTPROXY"
#define APP_SIP_RTP_PORT_KEY		@"SIPRTPPORT"
#define APP_SIP_RTP_PORT_VALUE		@"4000"


#define LEFT_COLUMN_OFFSET		10
#define LEFT_COLUMN_WIDTH		80

#define UPPER_ROW_TOP			10

#define CELL_HEIGHT				38

#define kSwitchButtonWidth		94.0
#define kSwitchButtonHeight		27.0

#define mSegmentedControlHeight 30.0

#define mLabelHeight            25.0
#define mLeftMargin             20.0 
#define mRightMargin            20.0
#define mTweenMargin            20.0

#define kSegmentedControlHeight 27.0
#define kSegmentedControlWidth  94.0

#define REG_ID_KEY	@"kTempRegID"
#define kTempRegID				4

#define kPhotoRowHeight			60.0

#define kCellHeight				25.0
#define kTextFieldWidth			100.0	// initial width, but the table cell will dictact the actual width

// padding for margins
#define kLeftMargin				20.0
#define kTopMargin				20.0
#define kRightMargin			20.0
#define kBottomMargin			20.0
#define kTweenMargin			10.0

// control dimensions
#define kStdButtonWidth			106.0
#define kStdButtonHeight		40.0
#define kSegmentedControlHeight 27.0
#define kSegmentedControlWidth  94.0
#define kPageControlHeight		20.0
#define kPageControlWidth		160.0
#define kSliderHeight			7.0
#define kSwitchButtonWidth		94.0
#define kSwitchButtonHeight		27.0
#define kTextFieldHeight		30.0
#define kSearchBarHeight		40.0
#define kLabelHeight			20.0
#define kProgressIndicatorSize	40.0
#define kToolbarHeight			40.0
#define kUIProgressBarWidth		160.0
#define kUIProgressBarHeight	24.0

// specific font metrics used in our text fields and text views
#define kFontName				@"Arial"
#define kTextFieldFontSize		18.0
#define kTextViewFontSize		18.0

// UITableView row heights
#define kUIRowHeight			50.0
#define kUIRowLabelHeight		22.0

// table view cell content offsets
#define kCellLeftOffset			8.0
#define kCellTopOffset			12.0

// Photo Height
#define kPhotoRowHeight			60.0

// activity 
#define ACTIVITYWHEEL_X         110
#define ACTIVITYWHEEL_Y         160
#define ACTIVITYWHEEL_WEIGHT    100
#define ACTIVITYWHEEL_HIGHT     100
#define kOFFSET_FOR_KEYBOARD    50

#define NETWORK_WIFI            0
#define NETWORK_EDGE            1
#define NETWORK_3G              2

// http url
/*
#define HTTP_URL                @"http://www.adoreinfotech.com"
#define VERSION                 @"Version 1.0"
#define WEBSITE                 @"http://www.adoreinfotech.com"
#define SUPPORT                 @"support@adoreinfotech.com"
#define SALES                   @"sales@adoreinfotech .com"
*/
 
// recent calls
#define RECENT_CALL_NAME_KEY    @"RECENT_CALL_NAME_KEY"
#define RECENT_CALL_NUM_KEY     @"RECENT_CALL_NUM_KEY"
#define RECENT_CALL_TYPE_KEY	@"RECENT_CALL_TYPE_KEY"
#define RECENT_SIP_TYPE         @"RECENT_CALL_SIP"
#define RECENT_CALL_DIR_KEY     @"RECENT_CALL_DIR_KEY"
#define RECENT_CALL_DATE_KEY	@"RECENT_CALL_DATE_KEY"
