//
//  speedsipAppDelegate.m
//  speedsip
//
//  Created by adore on 09/07/10.
//  Modified by denebtech, 10.aug.11
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//
#import "Constants.h"
#import "AppDelegate.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "RegisterViewController.h"
#import "ConnectionQueue.h"
#import "Notifications.h"
#import "AddressCollector.h"
#import "ShareImage.h"
#import "ChatStorage.h"
#import "Settings.h"
#import "SoundManager.h"
#import "TimeBomb.h"
#import "AvatarManager.h"

#import <AddressBook/AddressBook.h>
#import <FacebookSDK/FacebookSDK.h>
#import <TestFlight.h>

#import <FontasticIcons.h>
#import <FIFontAwesomeIcon.h>

#import "JabberLayer.h"
#import <DDLog.h>
#import <DDTTYLogger.h>

#import <AVFoundation/AVFoundation.h>

AppDelegate *app = nil;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void MyAddressBookExternalChangeCallback (
                                          ABAddressBookRef addressBook,
                                          CFDictionaryRef info,
                                          void *context
                                          )
{
    NSLog(@"contact book was changed");
    
    AppDelegate* delegate = (__bridge AppDelegate*)context;
    if (delegate.lastSyncedAbookTime == nil) {
        delegate.lastSyncedAbookTime = [[NSDate alloc] init];
    }
    else {
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:delegate.lastSyncedAbookTime];
        if (interval < 20.0f) {
            NSLog(@"callback is invalid, so we ignore this callback!");
            return;
        }
        else {
            delegate.lastSyncedAbookTime = [[NSDate alloc] init];
        }
    }
    
    delegate.needToSyncAbook = YES;
}

@implementation NavigationWrapperViewController

@synthesize navigationController;

-(id)initWithNavControllerWithSubViewController:(UIViewController *)subviewcontroller {
	if (self = [super init]) {
        //subviewcontroller.hidesBottomBarWhenPushed = YES;
		self->navigationController = [[UINavigationController alloc] initWithRootViewController:subviewcontroller];
	}
    return self;
}

-(void)loadView {
	self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
	[self.view setBackgroundColor:[UIColor blueColor]];
	if (self->navigationController != nil) {
		[self.view addSubview:self->navigationController.view];
		//self.view = self->navController.view; either line doesn't work.
	}
}

@end

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize window;
@synthesize tabBarController;
@synthesize sipAccount;
@synthesize strname;
@synthesize recentFile;
@synthesize recentCalls;

@synthesize contactno;
@synthesize register_view_controller;
@synthesize recentNavController;
@synthesize contactsNavController;
@synthesize settingsNavController;
@synthesize acc;

@synthesize logFile;
@synthesize configFile;
@synthesize configList;


@synthesize isMute;
@synthesize isHold;


@synthesize currentNetwork;
@synthesize vibrateTimer;


@synthesize ab;
@synthesize DID;
@synthesize IDD;
@synthesize need_promo = _need_promo;
@synthesize needGoRoomId = _needGoRoomId;


- (void)registerGsmAccount: (NSDictionary *) dict
{
    char c;
    int i,j;
    static char buf2[128];
    
    for (i=0,j=0; i<[[dict objectForKey:GSM_CITYPH_KEY] length]; i++)
    {
        c = [[dict objectForKey:GSM_CITYPH_KEY] characterAtIndex:i];
        if ((c>='0' && c<='9') || c=='+')
            buf2[j++] = c;
    }
    buf2[j] = 0;

    app.IDD = [dict objectForKey:GSM_IDD_KEY];
    
    
    NSString * pinless = [dict objectForKey:GSM_PINLESS_KEY];
    
    if (pinless && [pinless isEqualToString:@"ON"])
        app.DID = [[NSString alloc] initWithFormat:@"%@,,",[NSString stringWithUTF8String:buf2]];        
    else
        app.DID = [[NSString alloc] initWithFormat:@"%@,,%@,,",[NSString stringWithUTF8String:buf2],[dict objectForKey:SIP_USERNAME_KEY]];        
    
}


- (void)saveFavNum:(NSString*)num_ withAlert:(BOOL)a
{
    if([num_ isEqualToString:@""])
        return;
    
    if([[app recentCalls] count])
    for(int r=0;r<[[app recentCalls] count];r++)
    if([[[[app recentCalls] objectAtIndex:r] objectForKey:RECENT_CALL_NUM_KEY] isEqual:num_])
        return;

    NSString *nam = [app.contactno isEqualToString:num_] ? app.strname : @"";        
        
    NSLog(@"saveFav#: num=%@ // #%@ name=%@ // nam=%@",num_,app.contactno,app.strname,nam);
    
    NSMutableDictionary *recent;    
    recent =  [[NSMutableDictionary  alloc] initWithObjectsAndKeys :
               nam,           RECENT_CALL_NAME_KEY,
               num_,          RECENT_CALL_NUM_KEY,
               nil];
    
    [[app recentCalls] addObject: recent];
    
    if(a)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Favorites", @"Favorites")
                                                        message:NSLocalizedString(@"The number has been added", @"The number has been added")
                                                       delegate:nil 
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }

    [app saveFavs];
}


- (void)saveFavs
{
    NSLog(@"saveFavs: a writting array: %@",recentCalls);
    [[app recentCalls] writeToFile:[app recentFile] atomically:YES];
}


- (void) registerSipAccount: (NSDictionary *) dict
{
    if (dict)
    {
//      unused (c) sdg
//      [dict setValue:@"0" forKey:SIP_REGISTER_KEY];
        [self registerGsmAccount:dict];
    }
}	



// unused (c) sdg
- (void) unregisterSipAccount
{
    
    app.IDD = nil;
    app.DID = nil;
    
//    [app.dialNavController setStatus:@""];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_pass"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_phone"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pin"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [sipAccount removeAllObjects];
}

- (void)readConfig
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    configFile = [[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"config.plist"]];
	sipAccount = [[NSMutableArray alloc] initWithContentsOfFile:configFile];
	if (!sipAccount)
		sipAccount = [[NSMutableArray alloc] initWithCapacity:0];
}

//-(void)viewWillAppear:(BOOL)animated { 
//	[self viewWillAppear:animated];
//    [settingsNavController viewWillAppear:animated];
//}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    if ([RegisterViewController isActivated] == YES)
        [[ChatStorage sharedStorage] appDidLaunch];
    
    NSLog(@"Did become active");
    
    self.isForeground = YES;
    self.isTempAlive = NO;
    
    if (app.chatRoomsViewController.isHaveAppeared && [app.chatRoomsViewController becomeActiveProcess]) {
        _needGoRoomId = nil;
        return;
    }
    
    if (_needGoRoomId != nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:@{@"force":@YES}];
        [self jumpToNewChatRoom];
    }
    _needGoRoomId = nil;
    
    if ([[JabberLayer SharedInstance] isConnectedServer]) {
        [[JabberLayer SharedInstance] sayOnline];
        if ([[AddressCollector sharedCollector] isNeedToUploadAbook])
            [[AddressCollector sharedCollector] Upload_To_Server:NO];
    }

    if (self.needToSyncAbook) {
        self.needToSyncAbook = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changed_contactbook" object:nil];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [[Notifications sharedNotifications] didRegistered:deviceToken];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    [[Notifications sharedNotifications] didFailed:error];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    if (self.gettingAvatarFromFacebook) {
        self.gettingAvatarFromFacebook = NO;
        if (FBSession.activeSession != nil) {
            return [FBSession.activeSession handleOpenURL:url];
        }
        return NO;
    }
    
    Facebook * facebook = [ShareImage SharedImage].facebook;
    return [facebook handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    
    if (self.gettingAvatarFromFacebook) {
        self.gettingAvatarFromFacebook = NO;
        if (FBSession.activeSession != nil) {
            return [FBSession.activeSession handleOpenURL:url];
        }
        return NO;
    }
    
    Facebook * facebook = [ShareImage SharedImage].facebook;
    return [facebook handleOpenURL:url];
}

- (void) initContacts {
    contacts = [[Contacts alloc] init];
    
    [[AddressCollector sharedCollector] GetPeopleWithNormal:NO];
    
    ABAddressBookRef notificationAddressBook = ABAddressBookCreate();
    ABAddressBookRegisterExternalChangeCallback(notificationAddressBook, MyAddressBookExternalChangeCallback, (__bridge void *)(self));
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [JabberLayer SharedInstance];
    [SoundManager sharedInstance];
    [TimeBomb sharedInstance];
    [AvatarManager sharedInstance];
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [self customizeAppearance];
    
    //NSDate * date = [NSDate date];
    
	app = self;
    
    _need_promo = YES;
    _needGoRoomId = nil;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    [self readConfig];
	
    recentFile = [[NSString alloc] initWithString: [documentsDirectory stringByAppendingPathComponent:@"history.plist"]];
	recentCalls = [[NSMutableArray alloc] initWithContentsOfFile:recentFile];
	if (!recentCalls) {
		recentCalls = [[NSMutableArray alloc] initWithCapacity: 20];
    } else {
        while([recentCalls count] >= 20) {
            [recentCalls removeObjectAtIndex:0];
        }
    }
    
    // start: main tab view controller
    tabBarController = [[TabBarViewController alloc] init];
    tabBarController.delegate = self;
    
	NSMutableArray *localViewControllersArray = [[NSMutableArray alloc] initWithCapacity:6];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.21 green:0.7 blue:0.95 alpha:1.0]];
        [[tabBarController tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"tab_item_sel_bg.png"]];
    }
    
    // Chat
    UINavigationController *chatRoomsNavController = [[UINavigationController alloc] initWithRootViewController:self.chatRoomsViewController];
    chatRoomsNavController.tabBarItem.image = [UIImage imageNamed:@"more_invite.png"];
    chatRoomsNavController.tabBarItem.title = NSLocalizedString(@"Chat", @"Chat");
    [localViewControllersArray addObject:chatRoomsNavController];
    
    // Contacts
    NSArray * nib2 = [[NSBundle mainBundle] loadNibNamed:@"MyPeoplePicker" owner:self options:nil];
    contactsNavController = [nib2 objectAtIndex:0];
	contactsNavController.tabBarItem.image = [UIImage imageNamed:@"tab_contacts.png"];
	contactsNavController.tabBarItem.title = NSLocalizedString(@"Contacts", @"Contacts");
    [localViewControllersArray addObject:contactsNavController];
    
	// Settings
    
	acc = [[Accounts alloc] initWithStyle:UITableViewStyleGrouped];
    /*
     settingsNavController = [[NavigationWrapperViewController alloc] initWithNavControllerWithSubViewController:acc];
     settingsNavController.tabBarItem.image = [UIImage imageNamed:@"tab_settings.png"];
     settingsNavController.tabBarItem.title = @"Settings";
     [settingsNavController.view setContentMode:UIViewContentModeTopLeft];
     [settingsNavController.view setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
     [localViewControllersArray addObject:settingsNavController];
     */
	// About
	ab = [[About alloc] init];
    
    UINavigationController  *abNavigationController = [[UINavigationController alloc] initWithRootViewController:ab];
    abNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:0];
    
    //[abNavigationController.navigationBar setBarStyle:UIBarStyleBlack];
    //[abNavigationController.navigationBar setTintColor:[UIColor darkGrayColor]];
    
    [localViewControllersArray addObject:abNavigationController];
    
	tabBarController.viewControllers = localViewControllersArray;
    
    
    //    NSMutableArray *newViewControllers = [NSMutableArray arrayWithArray:tabBarController.viewControllers];
    //    [newViewControllers removeObjectAtIndex:0];
    //    [tabBarController setViewControllers:newViewControllers];
    
    
    //    [[tabBarController.tabBar.items objectAtIndex:1] setBadgeValue:@"100"];
    
    register_view_controller = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    
    //    UINavigationController * nav_controller = [[UINavigationController alloc] initWithRootViewController:register_view_controller];
    
    [window setRootViewController:tabBarController];
    [window makeKeyAndVisible];
    
    //	[window addSubview:tabBarController.view];
    
    // stop: main tab view controller
    
    DID = @"";
    
    if ([self initiated])
    {
        [app registerSipAccount: [sipAccount objectAtIndex: 0]];
	}
    
    
    [app.tabBarController setHidesBottomBarWhenPushed:YES];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"force":@YES}];
    
    //    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970] - [date timeIntervalSince1970];
    //date = [NSDate date];
    
    //    NSTimeInterval timer2 = [[NSDate date] timeIntervalSince1970] - [date timeIntervalSince1970];
    
    /*NSArray * t_array = */
    
    /*
     UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Intervals" message:[NSString stringWithFormat:@"Interfaces: %.02f s.\nInits: %.02f s.\n Get access: %.02f s.\n Get contacts: %.02f s.\n Sort contacts: %.02f s.", timer, timer2, [[t_array objectAtIndex:0] floatValue], [[t_array objectAtIndex:1] floatValue], [[t_array objectAtIndex:2] floatValue]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
     
     [alert show];
     [alert release];*/
    
    [self initContacts];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    [session setActive:YES error:nil];
    
    [TestFlight takeOff:@"6e12f60c-3a62-46ad-b3af-7c26eb04d7a6"];
    
    if (launchOptions) {
        NSLog(@"launchOptions: %@", launchOptions);
    }
    
#if DV_FLOATING_WINDOW_ENABLE
    
    DVWindowActivationTap(3);
    //DVWindowShow();
    
    CGRect dvFrame = DVConfigFrameGet();
    dvFrame.origin.x = 00.0;
    dvFrame.origin.y = 100.0;
    dvFrame.size.width = 320;
    dvFrame.size.height = 320;
    DVConfigFrameSet(dvFrame);
    
#endif
    
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    [[NSUserDefaults standardUserDefaults] setObject:build forKey:@"prev_run_version"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self syncWithNonXmppVersion];
    
    return YES;
}

- (void)customizeAppearance
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UIApplication sharedApplication] keyWindow].tintColor = [UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0];
    }
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], UITextAttributeTextColor, nil];
        [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
        
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.98 green:0.71 blue:0.2 alpha:1.0]];
        [[UINavigationBar appearance] setBarTintColor:[UIColor darkGrayColor]];
        [[UINavigationBar appearance] setBackgroundColor:[UIColor blackColor]];
    } else {
        [[UINavigationBar appearance] setTintColor:[UIColor darkGrayColor]];
    }
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]];
    [[UISegmentedControl appearance] setTintColor:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UITabBar appearance] setBarStyle:UIBarStyleBlack];
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]];
        [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
        [[UITabBar appearance] setBackgroundColor:[UIColor blackColor]];
    }

    [[UIToolbar appearance] setBarStyle:UIBarStyleBlack];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UIToolbar appearance] setTintColor:[UIColor lightGrayColor]];
        [[UIToolbar appearance] setBarTintColor:[UIColor darkGrayColor]];
        [[UIToolbar appearance] setBackgroundColor:[UIColor blackColor]];
    } else {
        [[UIToolbar appearance] setTintColor:[UIColor darkGrayColor]];
    }
    
    [[UISearchBar appearance] setBarStyle:UIBarStyleBlack];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                      [UIColor blueColor],
                                                                                                      UITextAttributeTextColor,
                                                                                                      [UIColor clearColor],
                                                                                                      UITextAttributeTextShadowColor,
                                                                                                      [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],
                                                                                                      UITextAttributeTextShadowOffset,
                                                                                                      nil]
                                                                                            forState:UIControlStateNormal];
        [[UISearchBar appearance] setBarTintColor:[UIColor clearColor]];
        [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
        [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
        [[UISearchBar appearance] setImage:[[FIFontAwesomeIcon searchIcon] imageWithBounds:CGRectMake(0, 0, 16, 16) color:[UIColor lightGrayColor]] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    } else {
        [[UISearchBar appearance] setTintColor:[UIColor darkGrayColor]];
    }
        
    [[UITableViewHeaderFooterView appearance] setTintColor:[UIColor darkGrayColor]];

    [[UITableView appearance] setBackgroundColor:DEFAULT_BACK_COLOR];
    [[UITableView appearance] setBackgroundView:nil];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UITableView appearance] setSectionIndexColor:[UIColor blackColor]];
        [[UITableView appearance] setSectionIndexTrackingBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
        [[UITableView appearance] setSectionIndexBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
    }
    
}

/*
 * Last chance to send pending messages
 */
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    self.needGoRoomId = nil;
    self.isTempAlive = YES;
    
    __block UIBackgroundTaskIdentifier completionTask = [application beginBackgroundTaskWithExpirationHandler:^{
        [application endBackgroundTask:completionTask];
        completionTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1200 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        if (completionTask != UIBackgroundTaskInvalid) {
            [[JabberLayer SharedInstance] sayOffline];
            
            NSLog(@"Entering background...really I was killed");
            
            [application endBackgroundTask:completionTask];
            completionTask = UIBackgroundTaskInvalid;
            self.isTempAlive = NO;
        }
    });
    
    [[ConnectionQueue sharedQueue] forceCycle];
    NSLog(@"Entering background... but i'm still alive");
    
    self.isForeground = NO;
    [[JabberLayer SharedInstance] sayAway];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"userInfo: %@", userInfo);
    NSString* roomIDStr = [userInfo objectForKey:@"chatid"];
    if (roomIDStr == nil || [roomIDStr length] == 0)
        _needGoRoomId = nil;
    else
        _needGoRoomId = [[JabberLayer SharedInstance] getRoomIdFromJID:roomIDStr];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"userInfo: %@", notification.userInfo);
    NSDictionary* userInfo = notification.userInfo;
    NSString* roomIDStr = [userInfo objectForKey:@"chatid"];
    if (roomIDStr == nil || [roomIDStr length] == 0)
        _needGoRoomId = nil;
    else
        _needGoRoomId = [[JabberLayer SharedInstance] getRoomIdFromJID:roomIDStr];
}


/*
-(void)makeTabBarHidden:(BOOL)hide {
    
    
	// Custom code to hide TabBar
	if ( [app.tabBarController.view.subviews count] > 2 ) {
		return;
	}
    
	UIView *contentView;
    
	if ( [[app.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
		contentView = [app.tabBarController.view.subviews objectAtIndex:1];
	} else {
		contentView = [app.tabBarController.view.subviews objectAtIndex:0];
	}
    
	if (hide) {
		contentView.frame = app.tabBarController.view.bounds;
	}
	else {
		contentView.frame = CGRectMake(app.tabBarController.view.bounds.origin.x,
                                       app.tabBarController.view.bounds.origin.y,
                                       app.tabBarController.view.bounds.size.width,
                                       app.tabBarController.view.bounds.size.height - app.tabBarController.tabBar.frame.size.height);
	}
    
	app.tabBarController.tabBar.hidden = hide;
}
*/

-(int)initiated
{
    if ([sipAccount count])
        if (![[[sipAccount objectAtIndex: 0] objectForKey:SIP_USERNAME_KEY] isEqual:@""] && ![[[sipAccount objectAtIndex: 0] objectForKey:GSM_CITYPH_KEY] isEqual:@""])
            return 1;
    
    return 0;
}

/*
 // Optional UITabBarControllerDelegate method
 - (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
 }
 */

/*
 // Optional UITabBarControllerDelegate method
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
 }
 */

// people picker
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker 
{   
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
	  shouldContinueAfterSelectingPerson:(ABRecordRef)person 
								property:(ABPropertyID)property 
							  identifier:(ABMultiValueIdentifier)identifier
{ 
	return NO; 
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
	  shouldContinueAfterSelectingPerson:(ABRecordRef)person
{ 
		return YES; 
}

//- (void)ContactsBook:(id)sender
//{
//	ABNewPersonViewController *abNewPersonViewController;
//	
//	abNewPersonViewController = [[ABNewPersonViewController alloc] init];
//	abNewPersonViewController.newPersonViewDelegate = self;
//	
//	[[app contactsNavController] presentModalViewController:abNewPersonViewController animated:YES];
//	[abNewPersonViewController release];
//}
//
//
//
//- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController
//       didCompleteWithNewPerson:(ABRecordRef)person
//{
//}
//

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
	//	contactsNavController.navigationBarHidden=YES;
	UIView *custom = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
	UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:custom];
	[viewController.navigationItem setRightBarButtonItem:btn animated:NO];
	// add our custom button to show our modal view controller
#if 0
	if (navigationController ==[app contactsNavController])
    {
		UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
		UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
		[modalViewButton addTarget:self action:@selector(ContactsBook:) forControlEvents:UIControlEventTouchUpInside];
		[viewController.navigationItem setRightBarButtonItem:modalButton animated:NO];
		[modalButton release];
		[modalViewButton release];
	}
#endif	
}

//- (BOOL) isNetworkTypeWiFi
//{
//	static BOOL checkNetwork = YES;
//	static Boolean iscell; 
//	
//	SCNetworkReachabilityFlags	flags = 0;
//	Boolean success;
//	const char *host_name = "www.google.com";
//
//    // Since checking the reachability of a host can be expensive, cache the result and perform the reachability check once.
//	if (checkNetwork)
//    {
//        checkNetwork = NO;
//		SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
//		success = SCNetworkReachabilityGetFlags(reachability, &flags);
//		iscell = success && (flags & kSCNetworkReachabilityFlagsIsWWAN);
//		CFRelease(reachability);
//	}
//	
//    currentNetwork = (iscell) ? NETWORK_3G : NETWORK_WIFI;
//	return !iscell;
//}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    if (viewController == contactsNavController) {
        
        [contactsNavController popToRootViewControllerAnimated:NO];
    }
    
    return YES;
}

- (void)setNewMinuteTimer:(int) interval
{
	if (vibrateTimer && [vibrateTimer isValid])
    {
		[vibrateTimer invalidate];
		vibrateTimer = NULL;
	}
	
	if (interval > 0)
    {
		NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval: interval
														  target: self
														selector: @selector(handleTimer:)
														userInfo: nil
														 repeats: YES];
		vibrateTimer = timer;
	}
}

- (UIViewController *)chatRoomsViewController
{
    if (!_chatRoomsViewController) {
        _chatRoomsViewController = [[ChatRoomsViewController alloc] initWithNibName:@"ChatRoomsViewController" bundle:nil];
    }
    return _chatRoomsViewController;
}

- (void) jumpToNewChatRoom {
    if (self.needGoRoomId < 0)
        return;
    
    if (self.tabBarController.selectedViewController != nil) {
#ifdef APP_TARGET_Yabb_Dialer
        UINavigationController* chatroomNavi = [self.tabBarController.viewControllers  objectAtIndex:1];
#else
        UINavigationController* chatroomNavi = [self.tabBarController.viewControllers  objectAtIndex:0];
#endif
        UINavigationController* selectedNavi = (UINavigationController*)[self.tabBarController selectedViewController];
        if (selectedNavi == chatroomNavi) {
            if (self.chatRoomsViewController.presentedViewController != nil)
            {
                [self.chatRoomsViewController dismissViewControllerAnimated:NO completion:^() {
                    NSDictionary * chat_room = [[ChatStorage sharedStorage] getChatRoom:self.needGoRoomId];
                    [self.chatRoomsViewController Move_To_Chat_Board:chat_room showKeyboard:NO anim:NO];
                }];
            }
            else {
                NSDictionary * chat_room = [[ChatStorage sharedStorage] getChatRoom:self.needGoRoomId];
                [self.chatRoomsViewController Move_To_Chat_Board:chat_room showKeyboard:NO anim:NO];
            }
        }
        else
        {
            // Does not push twice!
            NSArray * controllers = selectedNavi.viewControllers;
            for (id obj in controllers) {
                if ([obj isKindOfClass:[MyPeoplePicker class]] || [obj isKindOfClass:[About class]]) {
                    UIViewController* viewController = (UIViewController*)obj;
                    if (viewController.presentedViewController != nil)
                    {
                        [viewController dismissViewControllerAnimated:NO completion:^() {
                            [selectedNavi popToViewController:obj animated:YES];
                        }];
                    }
                    else
                        [selectedNavi popToViewController:obj animated:YES];
                }
            }
            
#ifdef APP_TARGET_Yabb_Dialer
            self.tabBarController.selectedIndex = 1;
#else
            self.tabBarController.selectedIndex = 0;
#endif
            
            NSDictionary * chat_room = [[ChatStorage sharedStorage] getChatRoom:self.needGoRoomId];
            [self.chatRoomsViewController Move_To_Chat_Board:chat_room showKeyboard:NO anim:NO];
        }
        
    }
}

- (void) syncWithNonXmppVersion {

    NSString* userPhone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    if (userPhone && ![[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]) {
        [[NSUserDefaults standardUserDefaults] setObject:userPhone forKey:@"user_id"];
    }
    
    NSString* userPin = [[NSUserDefaults standardUserDefaults] objectForKey:@"pin"];
    if (userPin && ![[NSUserDefaults standardUserDefaults] objectForKey:@"user_pin"]) {
        [[NSUserDefaults standardUserDefaults] setObject:userPin forKey:@"user_pin"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
