//
//  RecipientView.h
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import <UIKit/UIKit.h>

@protocol RecipientDelegate;

@interface RecipientView : UIView {
    
    id<RecipientDelegate> __weak _recipient_delegate;
    BOOL _selected;
}

@property (nonatomic, weak) IBOutlet id<RecipientDelegate> recipient_delegate;
@property (nonatomic, assign) BOOL selected;

- (int) setText:(NSString *) text atPosition:(CGPoint)position;
- (void) didActive;

@end

@protocol RecipientDelegate <NSObject>

- (void) didTouch;

@end
