//
//  CreateChatRoomViewController.h
//  speedsip
//
//  Created by denebtech on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeoplePickerViewController.h"
#import "BaseViewController.h"

@protocol CreateChatDelegate;


@interface CreateChatRoomViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, PeoplePickerDelegate> {
    
    IBOutlet UITableView * invate_table;
    UITextField * chat_name_field;
    
    NSMutableArray * invated_array;
}

@property (nonatomic, weak) IBOutlet id<CreateChatDelegate> delegate;

@end

@protocol CreateChatDelegate <NSObject>

@required
- (void) didCreateNewRoom:(NSString *)room_name withPersons:(NSArray *)invated_persons;

@end
