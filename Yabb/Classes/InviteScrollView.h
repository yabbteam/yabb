//
//  InviteScrollView.h
//  Baycall
//
//  Created by denebtech on 29.10.12.
//
//

#import <UIKit/UIKit.h>
#import "InviteView.h"
#import "RecipientView.h"

#define MAX_RECIPIENTS      40
#define RECIPIENT_SPACE     1

@interface InviteScrollView : UIScrollView <InviteViewDelegate, RecipientDelegate> {
    
@public
    IBOutlet UILabel * hint_label;
    IBOutlet UILabel * counter_label;
    
    IBOutlet InviteView * invite_view;
    IBOutlet UITextField * text_field;
    
    CGPoint _next_position;
    
    BOOL is_active;
}

@property (nonatomic, assign) CGPoint next_position;


- (void) Update_Height;
- (void) Set_Invite_Height;
- (void) Add_New_Person:(NSString *)name withIndex:(int)index withPhone:(NSString *)phone;
- (void) Remove_Person:(int)index;
- (void) Rebuild_Persons;
- (BOOL) Remove_Selected;
- (void) resetActive;

- (void) Text_Field_Position;
- (void) Text_Field_Hide;
- (void) Keyboard_Hide;

@end
