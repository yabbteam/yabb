//
//  ImageDownloadViewController.m
//  speedsip
//
//  Created by Tolian T on 06.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImageDownloadViewController.h"
#import "ChatStorage.h"
#import "ConnectionQueue.h"
#import "NSData+Base64.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import "Alert.h"
#import "AppDelegate.h"
//#import "ChatRoomsNavController.h"
#import "AddressCollector.h"
#import "SettingsManager.h"
#import "TimeBomb.h"

#define ACTION_SHEET_GALLERY 2
#define ACTION_SHEET_DELETE 3

@interface ImageDownloadViewController ()

@end

@implementation ImageDownloadViewController

@synthesize server_id = _server_id;
@synthesize chat_id = _chat_id;

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setWantsFullScreenLayout:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([[ChatStorage sharedStorage] isOwnMessage:self.server_id] || ![[TimeBomb sharedInstance] isTimeBombMessage:self.server_id]) {
        array_server_ids = [[ChatStorage sharedStorage] getImagesServerId:self.chat_id];
        nTimerCount = -1;
    }
    else {
        array_server_ids = @[@{@"server_id":self.server_id}];
        nTimerCount = [[TimeBomb sharedInstance] getMessageTimeout:self.server_id];
    }

    page_control.numberOfPages = [array_server_ids count];
    page_control.currentPage = [self getCurrentImageIndex];
    if (page_control.numberOfPages <= 1)
        page_control.hidden = YES;

    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onLeftSwipe:)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [source_view addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRightSwipe:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [source_view addGestureRecognizer:rightSwipe];
    
    self.labelCountDown = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    self.labelCountDown.textColor = [UIColor whiteColor];
    self.labelCountDown.font = [UIFont boldSystemFontOfSize:30];
    self.labelCountDown.backgroundColor = [UIColor clearColor];
    
    source_view.photoViewDelegate = self;
    source_view.backgroundColor = [UIColor blackColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadProgressUpdated:) name:@"download_progress_updated" object:nil];
    labelProgress.hidden = YES;
    sizeDownloaded = 0;
    
    isPlayingVideo = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) didVideoDone:(NSURL *)videoURL {
    
    if (videoURL != nil) {
        [activity stopAnimating];
        labelProgress.hidden = YES;
        self.view.userInteractionEnabled = YES;
        
        [source_view prepareForReuse];
        [source_view displayImage:currentImageThumb];
        buttonVideoPlay.hidden = NO;
        buttonVideoPlay.enabled = YES;
        
        currentMediaURL = videoURL;
        
        [tool_bar setHidden:NO];
        tool_bar.alpha = 1.0f;
        
        source_view.maximumZoomScale = source_view.minimumZoomScale;
    }
    else {
        NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
        
        NSString * name = [NSString stringWithFormat:@"video_%@", [[ChatStorage sharedStorage] generateRandom:16]];
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:user_phone, @"phone", /*user_pass, @"pass", */_server_id, @"mid", @"getmedia", @"cmd", name, @"dest_name", @YES, @"isVideo", nil];
        
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"download_video" silent:YES];
        
        labelProgress.hidden = NO;
        sizeDownloaded = 0;
        [self updateDownloadProgressText:0];
        
        NSLog(@"Image not exist!");
    }
}

- (void) didVideoFailed {
    
    NSLog(@"Video Failed!");
    
    [activity stopAnimating];
    self.view.userInteractionEnabled = YES;
    labelProgress.hidden = YES;
}

- (void) didImageDone:(UIImage *)image {
    
    if (image == nil) { // Image does not exists
        NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
//        NSString * user_pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];
        
        NSString * name = [NSString stringWithFormat:@"image_%@", [[ChatStorage sharedStorage] generateRandom:16]];
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:user_phone, @"phone", /*user_pass, @"pass", */_server_id, @"mid", @"getmedia", @"cmd", name, @"dest_name", nil];
        
        sizeDownloaded = 0;
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"download_image" silent:YES];
        
        NSLog(@"Image not exist!");
    }
    else {
        
        // Show image

        [activity stopAnimating];
        self.view.userInteractionEnabled = YES;
        labelProgress.hidden = YES;
        
        [ShareImage SharedImage].image = image;
        [ShareImage SharedImage].message_id = _server_id;
        [ShareImage SharedImage].chat_id = _chat_id;
        
        [source_view prepareForReuse];
        [source_view displayImage:image];
        source_view.scrollEnabled = YES;
        
        [source_view setAlpha:0.4];
        
        [tool_bar setHidden:NO];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.4];

        [source_view setAlpha:1.0];
        [tool_bar setAlpha:1.0];

        [UIView commitAnimations];

        NSLog(@"Image done!");

        if (nTimerCount > 0) {
            UIBarButtonItem *barItemCountDown = [[UIBarButtonItem alloc] initWithCustomView:self.labelCountDown];
            self.navigationItem.rightBarButtonItem = barItemCountDown;
            
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
            nTimerCount++;
            [self timerTick];

            itemAction.enabled = NO;
            itemDelete.enabled = NO;
        }
    }
}

- (void) didImageFailed {
    
    NSLog(@"Image Failed!");
    
    [activity stopAnimating];
    self.view.userInteractionEnabled = YES;
    labelProgress.hidden = YES;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void) viewWillAppear:(BOOL)animated {

    [tool_bar setHidden:YES];
    [tool_bar setAlpha:0.0];
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
    
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];

    int nPage = [self getCurrentImageIndex];
    [self gotoNewPage:nPage];

    if (!isPlayingVideo) {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(rotationChanged:)
                                                     name:@"UIDeviceOrientationDidChangeNotification"
                                                   object:nil];
    }
    isPlayingVideo = NO;
}

- (void) viewWillDisappear:(BOOL)animated {

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:animated];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
}

- (void) viewDidDisappear:(BOOL)animated {
    
    if (!isPlayingVideo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    }
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"download_image"]) {
        
        NSDictionary * data_dict = [dict objectForKey:@"data"];
        if (data_dict == nil) {
            [Alert show:NSLocalizedString(@"Image error!", @"Image error!") withMessage:NSLocalizedString(@"Empty dictionary.", @"Empty dictionary.")];
            [self didImageFailed];
            return;
        }

        NSString* name = [data_dict objectForKey:@"dest_name"];
        NSString* image_path = [[ChatStorage sharedStorage] getMediaSavePath:name];
        UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfFile:image_path]];
        
        if (![[TimeBomb sharedInstance] isTimeBombMessage:_server_id]) {
            
            NSString* sid = _server_id;
            
            [[ChatStorage sharedStorage] setMediaName:name forServerId:_server_id];
            [[ChatStorage sharedStorage] getImage:sid target:self done:@selector(didImageDone:) failed:@selector(didImageFailed)];
            
            if ([[SettingsManager sharedInstance] saveToGallery]) {
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:[image imageOrientation] completionBlock:nil];
            }
        }
        else {
            [[TimeBomb sharedInstance] start:_server_id];
            [self didImageDone:image];
        }
    }
    else if ([connection_name isEqualToString:@"download_video"]) {
        NSDictionary * data_dict = [dict objectForKey:@"data"];
        if (data_dict == nil) {
            [self didConnectionFailed:connection_name];
        }
        
        NSString* name = [data_dict objectForKey:@"dest_name"];
        if (name == nil) {
            [self didConnectionFailed:connection_name];
            return;
        }
        
        [[ChatStorage sharedStorage] setMediaName:name forServerId:_server_id];
        [[ChatStorage sharedStorage] getVideo:_server_id target:self done:@selector(didVideoDone:) failed:@selector(didVideoFailed)];
    }
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    [Alert show:NSLocalizedString(@"Error", @"Error") withMessage:NSLocalizedString(@"Can not download media.", @"Can not download media.")];
    [self didImageFailed];
}

#pragma mark - 
#pragma mark Buttons

- (IBAction) shareButton:(id)sender {
    
    disableRotation = YES;
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    if (!_is_video) {
        
        [ShareImage SharedImage].root_controller = self;
        [ShareImage SharedImage].share_delegate = self;
        [ShareImage SharedImage].isCanSaveGallery = !self.is_owner;
        
        [[ShareImage SharedImage] showInView:[self.view window]];
    }
    else {
        UIActionSheet * actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
        
        int index = [actSheet addButtonWithTitle:NSLocalizedString(@"Save to Gallery", @"Save to Gallery")];
        [actSheet setDestructiveButtonIndex:index];
        actSheet.tag = ACTION_SHEET_GALLERY;
        
        index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
        [actSheet setCancelButtonIndex:index];
        
        [actSheet showInView:[self.view window]];
    }
    
}

- (IBAction) deleteButton:(id)sender {

    disableRotation = YES;
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    UIActionSheet * actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    int index = [actSheet addButtonWithTitle:NSLocalizedString(@"Delete image", @"Delete image")];
    [actSheet setDestructiveButtonIndex:index];
    actSheet.tag = ACTION_SHEET_DELETE;
    
    index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:index];

    [actSheet showInView:[self.view window]];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == ACTION_SHEET_DELETE) {
        if (buttonIndex == 0) {     // Delete image
            [[ChatStorage sharedStorage] deleteMessage:_server_id];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (actionSheet.tag == ACTION_SHEET_GALLERY) {
        if (buttonIndex == 0) {     // Save Movie
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum ([currentMediaURL path])) {
                UISaveVideoAtPathToSavedPhotosAlbum ([currentMediaURL path], nil, nil, nil);
            }
        }
    }
    disableRotation = NO;
}

#pragma mark -
#pragma mark Share Image Delegate

- (void) ForwardToFriend {
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];

    [people_picker setShort_version:YES];
    [people_picker setFriends_only:YES];
    [people_picker setDelegate:self];
    
    [self.navigationController pushViewController:people_picker animated:YES];
}

- (void) SelectActionSheet {
    disableRotation = NO;
}


- (void) didSelectPerson:(int)index fromViewController:(UIViewController *)controller {

    [self.navigationController popToViewController:self.source_controller animated:YES];
        
    NSString* send_to_chat = [app.chatRoomsViewController Send_Private_Message:index withKeyboard:NO];
    
    if (send_to_chat) {
        
        [app.chatRoomsViewController.chat_board setSend_to_chat:send_to_chat];
        [app.chatRoomsViewController.chat_board Pick_Image:[ShareImage SharedImage].image message:nil assetReference:@"asset" isWink:NO];
        [app.chatRoomsViewController setChat_rooms_delegate:nil];
    }
    else {
        [app.chatRoomsViewController setChat_rooms_delegate:self];
    }
}

- (void) didCreateRoom:(NSString*) room_id {
    
    [app.chatRoomsViewController.chat_board setSend_to_chat:room_id];
    
    [app.chatRoomsViewController.chat_board Pick_Image:[ShareImage SharedImage].image message:nil assetReference:@"asset" isWink:NO];
    
}

- (int) getCurrentImageIndex {
    if (array_server_ids == nil || [array_server_ids count] == 0)
        return -1;
    
    int index = -1;
    for (int i = 0; i < [array_server_ids count]; i++) {
        NSString* sid = [[array_server_ids objectAtIndex:i] objectForKey:@"server_id"];
        if ([self.server_id isEqualToString:sid]) {
            index = i;
            break;
        }
    }
    
    return index;
}

- (void) gotoNewPage:(int) page {
    UIView * view = (UIView *)[self.view viewWithTag:100];
    if (view)
        [view removeFromSuperview];
    
    if (page < 0)
        return;

    [activity startAnimating];
    self.view.userInteractionEnabled = NO;
    NSLog(@"Get image");
    
    NSString *sid = [[array_server_ids objectAtIndex:page] objectForKey:@"server_id"];
    
    _server_id = sid;
    
    NSDictionary* dict = [[ChatStorage sharedStorage] getRecordByServerId:sid];
    NSString* thumbStr = [dict objectForKey:@"media_thumb"];
    NSData* dataThumb = [NSData dataFromBase64String:thumbStr];
    currentImageThumb = [UIImage imageWithData:dataThumb];
    currentMediaURL = nil;
    
    [source_view prepareForReuse];
    _is_video = [[ChatStorage sharedStorage] isRecordVideoByServerId:sid];
    if (_is_video) {
        buttonVideoPlay.hidden = YES;
        buttonVideoPlay.enabled = NO;
        [[ChatStorage sharedStorage] getVideo:_server_id target:self done:@selector(didVideoDone:) failed:@selector(didVideoFailed)];
    }
    else {
        [[ChatStorage sharedStorage] getImage:_server_id target:self done:@selector(didImageDone:) failed:@selector(didImageFailed)];
        buttonVideoPlay.hidden = YES;
    }
    
    page_control.currentPage = [self getCurrentImageIndex];
}

- (void) onRightSwipe:(UISwipeGestureRecognizer*) recoginzer {

    int nPage = [self getCurrentImageIndex];
    
    nPage --;
    if (nPage < 0)
        return;
    
    [self gotoNewPage:nPage];
}

- (void) onLeftSwipe:(UISwipeGestureRecognizer*) recoginzer {

    int nPage = [self getCurrentImageIndex];
    int nPageCount = (int)[array_server_ids count];
    
    nPage ++;
    if (nPage > nPageCount-1)
        return;
    
    [self gotoNewPage:nPage];
}

- (void) timerTick {
    
    --nTimerCount;
    if (nTimerCount < 0)
        nTimerCount = 0;
    self.labelCountDown.text = [NSString stringWithFormat:@"%d", nTimerCount];
    
    self.labelCountDown.alpha = 1.0f;
    self.labelCountDown.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);

    [UIView animateWithDuration:0.8f
                          delay:0.0f
                        options:UIViewAnimationCurveEaseOut
                     animations:^{
                         self.labelCountDown.alpha = 0;
                         self.labelCountDown.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5, 0.5);
                     }
                     completion:^(BOOL finished) {
                         if (nTimerCount <= 0) {
                             [self.navigationController popToViewController:self.source_controller animated:YES];
                             [self.labelCountDown removeFromSuperview];
                             self.labelCountDown = nil;
                         }
                     }
     
     ];
}

-(void)rotationChanged:(NSNotification *)notification{
    
    if (disableRotation)
        return;
    
    NSInteger orientation = [[UIDevice currentDevice] orientation];
    
    if (_orientation == orientation || orientation > UIDeviceOrientationLandscapeRight)
        return;
    
    [self bk_performBlock:^(id obj) {
        _orientation = orientation;
        [self changeWindowOrientation:_orientation animated:YES];
        
    } afterDelay:0.15f];
}

- (void) changeWindowOrientation:(NSInteger)orientation animated:(BOOL) animated {
    
    float duration = 0.0f;
    if (animated)
        duration = 0.4f;
    
    CGRect rtScreen = [UIScreen mainScreen].bounds;
    UIWindow *_window = [[[UIApplication sharedApplication] delegate] window];
    _window.backgroundColor = [UIColor blackColor];
    
    switch (orientation) {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:duration];
            
            [_window setTransform:CGAffineTransformMakeRotation (0)];
            [_window setFrame:rtScreen];
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:animated];
            
            [UIView commitAnimations];
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            [UIView beginAnimations:@"rotate_screen" context:nil];
            [UIView setAnimationDuration:duration];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationWillStartSelector:@selector(animationWillStart:context:)];
            
            [_window setTransform:CGAffineTransformMakeRotation (M_PI / 2)];
            [_window setFrame:rtScreen];
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:animated];
            
            [UIView commitAnimations];
            break;
            
        case UIDeviceOrientationLandscapeRight:
            [UIView beginAnimations:@"rotate_screen" context:nil];
            [UIView setAnimationDuration:duration];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationWillStartSelector:@selector(animationWillStart:context:)];
            [_window setTransform:CGAffineTransformMakeRotation (- M_PI / 2)];
            [_window setFrame:rtScreen];
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft animated:animated];
            
            [UIView commitAnimations];
            break;
        default:
            break;
    }
    
    _myRect = self.view.frame;
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)
            _myRect.origin.y += 12;
    }
}

- (void) animationWillStart:(NSString *)animationID context:(void *)context {
    
    self.view.frame = _myRect;
}

#pragma mark - PZPhotoViewDelegate
#pragma mark -

- (void)photoViewDidSingleTap:(PZPhotoView *)photoView {

    isTaped = !isTaped;
    if (isTaped) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             CGRect rt = tool_bar.frame;
                             CGRect rtScrollView = source_view.frame;
                             
                             rt.origin.y += tool_bar.frame.size.height;
                             rtScrollView.size.height += tool_bar.frame.size.height;
                             
                             tool_bar.frame = rt;
                             source_view.frame = rtScrollView;
                             buttonVideoPlay.center = source_view.center;
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 if (_is_video)
                                     source_view.maximumZoomScale = source_view.minimumZoomScale;
                             }
                         }];
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             CGRect rt = tool_bar.frame;
                             CGRect rtScrollView = source_view.frame;
                             
                             rt.origin.y -= tool_bar.frame.size.height;
                             rtScrollView.size.height -= tool_bar.frame.size.height;
                             
                             tool_bar.frame = rt;
                             source_view.frame = rtScrollView;
                             buttonVideoPlay.center = source_view.center;
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 if (_is_video)
                                     source_view.maximumZoomScale = source_view.minimumZoomScale;
                             }
                         }
         ];
    }

}

- (void)photoViewDidDoubleTap:(PZPhotoView *)photoView {
    // do nothing
}

- (void)photoViewDidTwoFingerTap:(PZPhotoView *)photoView {
    // do nothing
}

- (void)photoViewDidDoubleTwoFingerTap:(PZPhotoView *)photoView {
//    [self logLayout];
}

- (IBAction) onVideoPlayClicked:(id)sender {
    
    isPlayingVideo = YES;
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    moviePlayController = [[MPMoviePlayerViewController alloc] initWithContentURL:currentMediaURL];
    moviePlayController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [moviePlayController.moviePlayer prepareToPlay];
    
    // Remove the movie player view controller from the "playback did finish" notification observers
    [[NSNotificationCenter defaultCenter] removeObserver:moviePlayController
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayController.moviePlayer];
    
    // Register this class as an observer instead
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayController.moviePlayer];

    [self presentViewController:moviePlayController animated:YES completion:^{
        [moviePlayController.moviePlayer play];
    }];
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    // Obtain the reason why the movie playback finished
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    // Dismiss the view controller ONLY when the reason is not "playback ended"
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded)
    {
        MPMoviePlayerController *moviePlayer = [aNotification object];
        
        // Remove this class from the observers
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        
        // Dismiss the view controller
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }
}

- (void) downloadProgressUpdated:(NSNotification*)notification {
    NSDictionary* dict = notification.object;
    unsigned long long size = [[dict objectForKey:@"size"] unsignedLongLongValue];
    unsigned long long total = [[dict objectForKey:@"total"] unsignedLongLongValue];
    sizeDownloaded += size;
    [self updateDownloadProgressText:total];
}

- (void) updateDownloadProgressText:(unsigned long long)total {
    
    if (sizeDownloaded == 0 || total == 0) {
        [labelProgress setText:@"(0%)"];
        return;
    }
    
    int value = sizeDownloaded * 100 / total;
    [labelProgress setText:[NSString stringWithFormat:@"(%d%%)", value]];
}

@end
