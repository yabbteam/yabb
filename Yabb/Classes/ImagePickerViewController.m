//
//  ImagePickerViewController.m
//  speedsip
//
//  Created by denebtech on 29.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImagePickerViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface ImagePickerViewController ()

@end

@implementation ImagePickerViewController

- (id) initWithType:(UIImagePickerControllerSourceType)type{
    
    self = [super init];
    if (self) {
        self.delegate = self;
        self.image_picker_delegate = nil;
        self.sourceType = type;
    }
    
    return self;
}

- (id) initWithPhotoMovie {
    
    self = [super init];
    if (self) {
        self.delegate = self;
        self.image_picker_delegate = nil;
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie];
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

#pragma mark -
#pragma mark Image Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];
        
        if (_image_picker_delegate && [_image_picker_delegate respondsToSelector:@selector(Pick_Video:)]) {
            self.resultMoviePath = moviePath;
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.resultMoviePath != nil)
                [_image_picker_delegate performSelector:@selector(Pick_Video:) withObject:self.resultMoviePath];
        }];
    }
    else {
        if (_image_picker_delegate && [_image_picker_delegate respondsToSelector:@selector(Pick_Image:assetReference:)]) {
            
            UIImage * image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
            self.result = image;
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.result != nil)
                [_image_picker_delegate performSelector:@selector(Pick_Image:assetReference:) withObject:self.result withObject:@""/*[url absoluteString]*/ ];
        }];
    }
}

@end
