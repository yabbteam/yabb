//
//  AboutViewController.m
//  Baycall
//
//  Created by admin on 07.05.13.
//
//

#import "AboutViewController.h"
#import "ChatStorage.h"
#import "JabberLayer.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (IS_IPHONE_5) {
        [back_image setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
        [version_label setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
    }
    
    [web_view loadHTMLString:@"<html><head></head><body style=\"margin:0 auto;text-align:center;background-color: transparent;\"><a href=\"http://www.yabb.com/terms-and-privacy\" aligne=center><font color=gray size=-1>Terms and Privacy</font></a></body></html>" baseURL:nil];
    [web_view setOpaque:NO];
    [web_view setBackgroundColor:[UIColor clearColor]];
    [web_view setDelegate:self];
    
    NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [info objectForKey:@"CFBundleVersion"];
    version_label.text = [NSString stringWithFormat:@"%@ (%@)", version, build];
    myPhone.text = [NSString stringWithFormat:@"%@:%@", NSLocalizedString(@"Registered Number", @"Registered Number"),
                    [[JabberLayer SharedInstance] getMyPhoneNo]];
}

-(BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
}

@end
