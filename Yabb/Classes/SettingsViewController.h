//
//  SettingsViewController.h
//  Yabb
//
//  Created by denebtech on 10/10/13.
//
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface SettingsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@end
