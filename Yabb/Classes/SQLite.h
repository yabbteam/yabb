//
//  SQLite.h
//  WeatherNow
//
//  Created by Tolian T on 11.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface SQLite : NSObject {
    
    sqlite3 *contactDB;

}

+ (SQLite *) shared_sql;
- (int) Open_DB:(NSString *)db_name;
- (void) Close_DB;
- (int) Delete_DB:(NSString *)db_name;

- (void) Exec:(NSString *)exec_str;
- (NSArray *) Query:(NSString *)query_str;

@end
