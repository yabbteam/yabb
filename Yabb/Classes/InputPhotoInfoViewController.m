//
//  InputPhotoInfoViewController.m
//  Yabb
//
//  Created by denebtech on 3/15/14.
//
//

#import "InputPhotoInfoViewController.h"
#import "UIPlaceHolderTextView.h"

@interface InputPhotoInfoViewController ()

@end

@implementation InputPhotoInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.textView.placeholder = NSLocalizedString(@"Add Comment of This Photo", @"Add Comment of This Photo");
    self.photoView.image = self.image;
    self.imageVideoPlay.center = self.photoView.center;
    if (self.isVideo)
        self.imageVideoPlay.hidden = NO;
    else
        self.imageVideoPlay.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (IBAction)onOKClicked:(id)sender {

    [self dismissViewControllerAnimated:YES completion:^ {
        if (self.delegate != nil)
            [self.delegate onSelectedComment:self.image comment:self.textView.text other:self.otherParam];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
    }];
}

- (IBAction)onCancelClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^ {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
    }];
}

@end
