//
//  ShareImage.m
//  speedsip
//
//  Created by denebtech on 04.09.12.
//
//

#import "ShareImage.h"
#import <Twitter/Twitter.h>
#import "AppDelegate.h"

static ShareImage * share_image = nil;



@implementation ShareImage

@synthesize root_controller = _root_controller;
@synthesize image = _image;
@synthesize facebook;
@synthesize isConnected;
@synthesize share_delegate = _share_delegate;
@synthesize message_id = _message_id;
@synthesize chat_id = _chat_id;

+ (ShareImage *) SharedImage {
    
    if (share_image == nil) {
        
        share_image = [[ShareImage alloc] init];
    }
    
    return share_image;
}

- (void) showInView:(UIView *) view {
    actSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Share image", @"Share image") delegate:share_image cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Send by Email",@"Send by Email")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Share on Facebook", @"Share on Facebook")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Share on Twitter", @"Share on Twitter")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Forward to Friend", @"Forward to Friend")];
    if (self.isCanSaveGallery)
        [actSheet addButtonWithTitle:NSLocalizedString(@"Save to Gallery", @"Save to Gallery")];
    
    int cancel_index = [share_image->actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:cancel_index];
    
    [actSheet showInView:view];
}


#define kAPIGraphUserPhotosPost 0
#define kAPIGraphPhotoData      1
#define kDialogFeedUser         2

#pragma mark -
#pragma mark Share

- (void) Send_By_Email {
    
    MFMailComposeViewController *tempMailCompose = [[MFMailComposeViewController alloc] init];
    
	tempMailCompose.mailComposeDelegate = self;
    
	[tempMailCompose setSubject:NSLocalizedString(@"Yabb photo", @"Yabb photo")];
    
    NSData *data = UIImagePNGRepresentation(_image);
    [tempMailCompose addAttachmentData:data
                       mimeType:@"image/png"
                       fileName:@"Yabb_Photo.png"];
    
    [_root_controller presentModalViewController:tempMailCompose animated:YES];
}
#import <CommonCrypto/CommonDigest.h>

- (NSString *)MD5String:(NSString *) encode {
    const char *cstr = [encode UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}
- (void) Upload_Facebook_Image {
    /*
    currentAPICall = kAPIGraphUserPhotosPost;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   _image, @"picture",
                                   nil];
    
    [facebook requestWithGraphPath:@"me/photos"
                              andParams:params
                          andHttpMethod:@"POST"
                            andDelegate:self];
    
    */
    [app.acc showActivityViewer:NSLocalizedString(@"Share on Facebook", @"Share on Facebook")];
    

    NSString * hash = [NSString stringWithFormat:@"FB%@!n@!!!", _message_id, _chat_id];
    
    NSString* encrypted = [self MD5String:hash];
    
    
//    NSString *imageLink = @"http://api.yabb.com/api.php?cmd=fbimage&mid=4133&hash=dd1a854e354b2864b04f28caac2fff84";
    NSString * imageLink = [NSString stringWithFormat:@"http://api.yabb.com/api.php?cmd=fbimage&mid=%@&hash=%@", _message_id, encrypted];
    
    NSMutableDictionary* dialogParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         @"243465279150608", @"app_id",
                                         facebook.accessToken, @"access_token",
                                         imageLink, @"link",
                                         @"Yabb Messenger", @"name",
                                         nil];
    
    [facebook dialog:@"feed"
           andParams:dialogParams
         andDelegate:self];
}

- (void) Share_On_Facebook {
    
    // iOS 6.0 or greater
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        NSString *shareText = @"Yabb Messenger";
        [mySLComposerSheet setInitialText:shareText];
        
        [mySLComposerSheet addImage:_image];
        
        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://yabb.com"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                default:
                    break;
            }
        }];
        
        [app.tabBarController presentViewController:mySLComposerSheet animated:YES completion:nil];
        
        return;
    }
    
    // iOS 5.0
    if (facebook == nil) {
        facebook = [[Facebook alloc]
                     initWithAppId:FBSession.activeSession.appID
                     andDelegate:self];
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
            facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
            facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
        }
    }    
    
    if ([facebook isSessionValid]) {
        [self Upload_Facebook_Image];

    }
    else {
        [facebook authorize:nil];
    }
}


- (void) Share_On_Twitter {
    
    if (_root_controller == nil || _image == nil)
        return;
    
    TWTweetComposeViewController * twitter_controller = [[TWTweetComposeViewController alloc] init];
    
    [twitter_controller addImage:_image];
        
    [_root_controller presentViewController:twitter_controller animated:YES completion:nil];
    
    NSLog(@"Share on Twitter");
}


#pragma mark -
#pragma mark Action Sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (_share_delegate && [_share_delegate respondsToSelector:@selector(SelectActionSheet)])
        [_share_delegate SelectActionSheet];
    
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;
    
    switch (buttonIndex) {
            
        case 0:
            [self Send_By_Email];
        break;
            
        case 1:
            [self Share_On_Facebook];
        break;
            
        case 2:
            [self Share_On_Twitter];
        break;
            
        case 3:
            
            if (_share_delegate && [_share_delegate respondsToSelector:@selector(ForwardToFriend)])
                [_share_delegate ForwardToFriend];
            
            break;
        case 4:
            [self saveToGallery];
            break;
            
    }
}


- (void)storeAuthData:(NSString *)accessToken expiresAt:(NSDate *)expiresAt {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:expiresAt forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
}

- (void)dialogDidComplete:(FBDialog *)dialog {
    
    [app.acc hideActivityViewer];

    return;
}

- (void) dialogDidNotComplete:(FBDialog *)dialog {
    [app.acc hideActivityViewer];
    
    return;
}

- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error {

    [app.acc hideActivityViewer];

    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Dialog Exception", @"Dialog Exception")
                              message:[error localizedDescription]
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil,
                              nil];
    [alertView show];
}

- (void) saveToGallery {
    if (_image)
        UIImageWriteToSavedPhotosAlbum(_image, nil, nil, nil);
}

#pragma mark - 
#pragma FB Session Delegate

-(void)fbDidLogin{
    
    [self storeAuthData:[facebook accessToken] expiresAt:[facebook expirationDate]];

    [self Upload_Facebook_Image];
}

-(void)fbDidNotLogin:(BOOL)cancelled{
    // Keep this for testing purposes.
    //NSLog(@"Did not login");
    
//    UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Login cancelled." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [al show];
//    [al release];
    return;
}

-(void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt {
    NSLog(@"token extended");
    [self storeAuthData:accessToken expiresAt:expiresAt];
}



-(void)fbDidLogout {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    [defaults synchronize];


}

- (void)fbSessionInvalidated {
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Auth Exception", @"Auth Exception")
                              message:NSLocalizedString(@"Your session has expired.", @"Your session has expired.")
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil,
                              nil];
    [alertView show];
    [self fbDidLogout];
}

#pragma mark -
#pragma mark FB Request Delegate

- (void)request:(FBRequest *)request didLoad:(id)result {
    
    [app.acc hideActivityViewer];
    [app.acc showActivityViewer:NSLocalizedString(@"Done", @"Done")];
    
    [app.acc performSelector:@selector(hideActivityViewer) withObject:nil afterDelay:1.0];     
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {

    [app.acc hideActivityViewer];
    [app.acc showActivityViewer:NSLocalizedString(@"Failed", @"Failed")];
    
    [app.acc performSelector:@selector(hideActivityViewer) withObject:nil afterDelay:1.0];

    UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Facebook error", @"Facebook error") message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    [al show];
    
}

#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    
    [controller dismissModalViewControllerAnimated:YES];
}


@end
