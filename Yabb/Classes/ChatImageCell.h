//
//  ChatImageCell.h
//  speedsip
//
//  Created by denebtech on 29.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"
#import "TTTAttributedLabel.h"
#import "DDProgressView.h"

@interface ChatImageCell : ChatCell<TTTAttributedLabelDelegate> {

    BOOL    isSeen;
    int     media_size;
    int     message_type;
}

//@property (nonatomic, retain) NSNumber * server_id;
@property (nonatomic, weak) id cell_delegate;
@property (nonatomic, weak) IBOutlet UIImageView * bubble;
@property (nonatomic, weak) IBOutlet UIImageView * thumb;
@property (nonatomic, weak) IBOutlet UIButton * thumb_button;
@property (nonatomic, weak) IBOutlet TTTAttributedLabel * labelComment;
@property (nonatomic, weak) IBOutlet UIImageView * iconVideoPlay;
@property (nonatomic, weak) IBOutlet DDProgressView * progressUploadingVideo;
@property (nonatomic, weak) IBOutlet UILabel * labelVideoSize;

- (void) setData:(NSDictionary *)dict editMode:(BOOL)edit;
//- (void) setContactName:(NSString *)name;
- (void) setButton:(int) status;
- (void) setMediaSize:(int)size;
- (void) setMessageType:(int)type;

@end
