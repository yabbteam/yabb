//
//  CodecViewCell.m
//  speedsip
//
//  Created by adore on 10/07/10.
//  Modified by sdg, 10.aug.11
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CodecViewCell.h"
#import "Constants.h"



@implementation CodecViewCell
@synthesize nameLabel;


- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleGray;

        nameLabel = [[UILabel alloc] initWithFrame:frame];               
		nameLabel.backgroundColor = [UIColor clearColor];
		nameLabel.opaque = NO;
		nameLabel.textColor = [UIColor blackColor];
		nameLabel.highlightedTextColor = [UIColor blackColor];
		nameLabel.font = [UIFont boldSystemFontOfSize:18];
        [self.contentView addSubview:nameLabel];
    }
    return self;
}

/*
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{	
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        //sdg.opt
		//nameLabel = [[UILabel alloc] initWithStyle:style];
        nameLabel = [UILabel alloc];
        
		nameLabel.backgroundColor = [UIColor clearColor];
		nameLabel.opaque = NO;
		nameLabel.textColor = [UIColor blackColor];
		nameLabel.highlightedTextColor = [UIColor blackColor];
		nameLabel.font = [UIFont boldSystemFontOfSize:18];
		[self.contentView addSubview:nameLabel];
    }
    return self;
}
*/



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{	
    [super setSelected:selected animated:animated];	
    // Configure the view for the selected state
}



- (void)dealloc
{
    [super dealloc];
    [nameLabel release];
}



- (void)layoutSubviews
{	
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    CGRect frame = CGRectMake(contentRect.origin.x + 8.0, 12.0, contentRect.size.width, kCellHeight);
	nameLabel.frame = frame;
}



@end
