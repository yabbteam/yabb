//
//  CreateChatRoomViewController.m
//  speedsip
//
//  Created by denebtech on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateChatRoomViewController.h"
#import "ChatRoomNameCell.h"
#import "PeoplePickerViewController.h"
#import "AddressCollector.h"
#import "Friends.h"

@interface CreateChatRoomViewController ()

@end

@implementation CreateChatRoomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self->invated_array = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (void) viewControllerDidBecomeActive {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [invate_table setEditing:YES];
    
    UIBarButtonItem * bar_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(Done_Button:)];
    self.navigationItem.rightBarButtonItem = bar_button;
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewControllerDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {

    self.navigationItem.title = NSLocalizedString(@"Create chat", @"Create chat");
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void) Invate_Friend {
    
    if ([[Friends SharedFriends] numberOfFriends] == 0) {
        
        UIAlertView * alert_view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:NSLocalizedString(@"There are no friends at your contact list who using this applications.", @"There are no friends at your contact list who using this applications.") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil];
        [alert_view show];
        
        return;
    }
    
    PeoplePickerViewController * view_controller = [[PeoplePickerViewController alloc] init];

    [view_controller setShort_version:YES];
    [view_controller setFriends_only:YES];
    [view_controller setDelegate:self];
    
    [self.navigationController pushViewController:view_controller animated:YES];
    
}

- (IBAction) Done_Button:(id)sender {
    
    if (invated_array == nil || [invated_array count] == 0)
        return;
    
    NSString * room_name;
    if ([invated_array count] == 1) {
        room_name = [[invated_array objectAtIndex:0] objectForKey:@"person_name"];
    }
    else {
        room_name = [NSString stringWithString:[chat_name_field text]];
        if ([room_name isEqualToString:@""]) {
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Chat name", @"Chat name") message:NSLocalizedString(@"Please enter chat name", @"Please enter chat name") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil];
            [alert show];
            
            return;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(didCreateNewRoom:withPersons:)]) {
        [chat_name_field resignFirstResponder];
        [_delegate didCreateNewRoom:room_name withPersons:invated_array];
    }
}

#pragma mark -
#pragma mark Table Delegates


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2)
        [self Invate_Friend];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        
        case 0:
            return ([invated_array count] >= 2)?1:0;
        break;
        
        case 1:
            return [invated_array count];
        break;
        
        case 2:
            return 1;
        break;
    }
    
    return 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return (indexPath.section == 1)?YES:NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    int row = indexPath.row;
    UITableViewCell * cell = nil;
    
    switch (section) {

        case 0: {
            ChatRoomNameCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRoomNameCellId"];
            
            if (cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomNameCell" owner:self options:nil];
                cell = (ChatRoomNameCell *)[nib objectAtIndex:0];
            }
            
            chat_name_field = cell.chat_name_field;
            [chat_name_field setDelegate:self];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return cell;
        }
        break;
        
        case 1: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCellId"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendCellId"];
            
            NSDictionary * person = [invated_array objectAtIndex:row];
            [cell.textLabel setText:[person objectForKey:@"person_name"]];
        break;
        
        }
        case 2:
            cell = [tableView dequeueReusableCellWithIdentifier:@"InvateCellId"];

            if (cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InvateFriendCell" owner:self options:nil];
                cell = (UITableViewCell *)[nib objectAtIndex:0];
            }
        break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [invated_array removeObjectAtIndex:indexPath.row];
        
        if ([invated_array count] == 1)
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, [NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationLeft];
        else
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        if ([invated_array count] == 0)
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
}

#pragma mark -
#pragma mark Chat Name Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [chat_name_field resignFirstResponder];
    
    return YES;
}

#pragma mark - 
#pragma mark People Picker Delegate


- (BOOL) isInvatedPerson:(int)index {
    
    NSString * name = [[AddressCollector sharedCollector] Get_Name_By_Index:index];

    for (NSDictionary * person in invated_array) {
        
        NSString * person_name = [person objectForKey:@"person_name"];
        
        if ([person_name isEqualToString:name])
            return YES;
        
    }
    
    return NO;
}

- (void) didSelectPerson:(int)index fromViewController:(UIViewController*) controller {
    
    if ([self isInvatedPerson:index]) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Invate", @"Invate") message:NSLocalizedString(@"This contact already invated.", @"This contact already invated.") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    NSString * person_name = [[AddressCollector sharedCollector] Get_Name_By_Index:index];
    NSArray * phones = [[AddressCollector sharedCollector] Get_Phones_By_Index:index];
    
    NSDictionary * dictionary = [NSDictionary dictionaryWithObjectsAndKeys:person_name, @"person_name", phones, @"phones", nil];
    
    [invated_array addObject:dictionary];
    
    if ([invated_array count] == 2)
        [invate_table insertRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], [NSIndexPath indexPathForRow:0 inSection:1], nil] withRowAnimation:UITableViewRowAnimationTop];
    else
        [invate_table insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationTop];
    
    [invate_table reloadData];
    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];

    [self.navigationController popViewControllerAnimated:YES];
}

@end
