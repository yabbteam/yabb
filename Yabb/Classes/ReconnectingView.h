//
//  ReconnectingView.h
//  Yabb
//
//  Created by denebtech
//
//

#import <UIKit/UIKit.h>

@interface ReconnectingView : UIView

@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) UIActivityIndicatorView* indicator;

@end
