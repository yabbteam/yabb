//
//  ChatStatusViewController.h
//  speedsip
//
//  Created by denebtech on 02.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ChatStatusViewController : BaseViewController {
    
    IBOutlet UIView * back_view;
    IBOutlet UITextView * text_view;
}

+ (void)setStatus:(NSString *)user_status;
+ (NSString *)getStatus;
+ (void) sendUserStatus;

@end
