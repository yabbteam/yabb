//
//  ChatStatusViewController.m
//  speedsip
//
//  Created by Anatoly T on 02.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatStatusViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ConnectionQueue.h"
#import "ChatStorage.h"
#import "MainsipphoneAppDelegate.h"

@interface ChatStatusViewController ()

@end

@implementation ChatStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    text_view.layer.cornerRadius = 8.0;
    text_view.clipsToBounds = YES;

    back_view.layer.cornerRadius = 8.0;
    back_view.clipsToBounds = YES;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated {

    [text_view setText:[ChatStatusViewController getStatus]];
    
    [text_view becomeFirstResponder];
    
    UIBarButtonItem * save_button = [[UIBarButtonItem alloc] 
                                   initWithTitle: @"Save Status" 
                                   style: UIBarButtonItemStyleDone
                                   target: self action: @selector(saveUserStatus)];
    
    [self.navigationItem setRightBarButtonItem:save_button];    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    return;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

+ (void) sendUserStatus {
    
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    NSString * user_status = [ChatStatusViewController getStatus];  // correct way to get user status
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"profile", @"cmd", user_phone, @"phone", [[ChatStorage sharedStorage] encodeString:user_status], @"origin", nil];
    
    [[ConnectionQueue sharedQueue] Add_Connection:app.chat withParams:params withName:@"set_user_status" silent:NO];
}

- (void) saveUserStatus {
    
    if (text_view.text) {
        [ChatStatusViewController setStatus:text_view.text];
        [ChatStatusViewController sendUserStatus];
    }
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

+ (void)setStatus:(NSString *)user_status {
    
    [[NSUserDefaults standardUserDefaults] setObject:user_status forKey:@"user_status"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getStatus {
    
    NSString * user_status = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_status"];
    
    return user_status?user_status:@"";
}


@end
