//
//  PickerCell.m
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import "PickerCell.h"

@implementation PickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) enableSelector:(BOOL)active {
    
    [self.user_selector setHidden:NO];
    [self.user_selector setImage:[UIImage imageNamed:active?@"IsSelected.png":@"NotSelected.png"]];

    if ([self.origin_label.text isEqualToString:@""]) {
        [self.person_label setFrame:CGRectMake(40+40, 11, 220, 22)];
    }
    else {
        [self.person_label setFrame:CGRectMake(40+40, 2, 220, 22)];
        [self.origin_label setFrame:CGRectMake(40+40, 21, 250, 20)];
    }
    [self.person_photo setFrame:CGRectMake(40, self.person_photo.frame.origin.y,
                                           self.person_photo.frame.size.width, self.person_photo.frame.size.height)];
    CALayer * l = [self.person_photo layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
}

- (void) setOriginText:(NSString *)text {
    
    [self.origin_label setText:text];
    [self.origin_label setHidden:NO];

    if (text == nil || [text isEqualToString:@""]) {
        [self resetOriginText];
    }
    else {
        [self.person_label setFrame:CGRectMake(10+40, 2, 220, 22)];
        [self.origin_label setFrame:CGRectMake(10+40, 21, 220, 20)];
    }
    CALayer * l = [self.person_photo layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
}

- (void) resetOriginText {
    
    [self.origin_label setHidden:YES];
    [self.origin_label setText:@""];

    [self.person_label setFrame:CGRectMake(10+40, 11, 220, 22)];
}


@end
