//
//  CommonUtil.h
//  Yabb
//
//  Created by lion on 11/27/13.
//
//

#import <Foundation/Foundation.h>

@interface CommonUtil : NSObject

+ (CGSize) getTextSize:(NSString*)text font:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode;
+ (CGSize) getAttributedLabelSize:(NSString*)text font:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode;
+ (UIImage*)imageByScalingToSize:(CGSize)targetSize cgimage:(CGImageRef)cgimage orientation:(int)orientation;
+ (UIImage *)fixOrientation:(UIImage*) image;
+ (UIImage*) drawTextOnImage:(NSString*) text
                     inImage:(UIImage*)  image
                       color:(UIColor*)   color
                    fontSize:(int)fontSize;
+ (UIImage*) drawIconOnImage:(UIImage*) icon inImage:(UIImage*)  image;
+ (UIImage*) getNoAvatarImage;
+ (UIImage*) getMovieScreenShot:(NSString*)filePath;

@end
