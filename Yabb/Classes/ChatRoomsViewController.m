//
//  ChatRoomsViewController.m
//  Baycall
//
//  Created by denebtech on 14.03.13.
//
//

#import "ChatRoomsViewController.h"
#import "ChatRoomCell.h"
#import "ConnectionQueue.h"
#import "AppDelegate.h"
#import "ChatStorage.h"
#import "AddressCollector.h"
#import "CustomBadge.h"
#import "ChatStatusViewController.h"
#import "ChatRoomStatusCell.h"
#import "Friends.h"
#import "PeopleMultiselectorViewController.h"
#import "InvitedFriends.h"
#import "BroadcastViewController.h"
#import "Settings.h"
#import "SoundManager.h"
#import "LafUtil.h"
#import "JabberLayer.h"
#import "ServerStateView.h"
#import "SettingsManager.h"
#import "PhotoEditViewController.h"
#import "PopUpMessageView.h"
#import "AvatarManager.h"
#import "ReconnectingView.h"

#define BROADCAST_IDX       0
#define STATUS_IDX          1
#define CHAT_ROOM_IDX       2
#define LAST_IDX            CHAT_ROOM_IDX

@interface ChatRoomsViewController () {

    __strong ReconnectingView*   reconnectingView;
    __strong UIView*             titleView;
}

@end

@implementation ChatRoomsViewController

/*- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.table_editing = NO;
        self.room_phones = [[NSMutableArray alloc] init];
        self.room_name = [[NSMutableString alloc] initWithString:@""];
        self.chat_room_sorted = [[NSMutableArray alloc] init];
    }
    
    return self;
} */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    reconnectingView = [[ReconnectingView alloc] initWithFrame:CGRectMake(0, 0, 160, 40)];
    
    self.table_editing = NO;
    self.room_phones = [[NSMutableArray alloc] init];
    self.room_name = [[NSMutableString alloc] initWithString:@""];
    self.chat_room_sorted = [[NSMutableArray alloc] init];

    self.chat_board = [[ChatBoardViewController alloc] initWithNibName:@"ChatBoardViewController" bundle:nil];
    
    [[ChatStorage sharedStorage] initImageMessageState];
    
    [self customizeAppearance];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddedMessages:) name:@"added_messages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChangedContactbook:) name:@"changed_contactbook" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ConflictedAccount:) name:@"conflict_account" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tryToLogin) name:@"request_login" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoRegisterScreen) name:@"request_register" object:nil];

    popups = [[NSMutableDictionary alloc] init];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void)customizeAppearance
{
    [self customizeNavigationBar];
}

- (void)customizeNavigationBar
{
    self.navigationItem.title = NSLocalizedString(@"Chats", @"Chats");
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", @"Edit") style:UIBarButtonItemStyleBordered target:self action:@selector(editRoomsAction:)];
    
    ServerStateView* serverStateView = [[ServerStateView alloc] init];
    UIBarButtonItem *serveState = [[UIBarButtonItem alloc] initWithCustomView:serverStateView];
    NSArray* arrayItem = [NSArray arrayWithObjects:serveState, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(composeAction:)], nil];
    
    [self.navigationItem setRightBarButtonItems:arrayItem];
}

- (void) importRooms {
    
    if (self.chat_room_list)
        self.chat_room_list = nil;
    
    NSArray * array = [[ChatStorage sharedStorage] getChatRooms];
    if (array)
        self.chat_room_list = [[NSArray alloc] initWithArray:array];
    
    [self sortByFilter:@""];
}

- (void)gotoRegisterScreen
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_pass"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[ChatStorage sharedStorage] clearAccount];
    
    app.isRegistering = YES;
    [app.register_view_controller clearSelection];
//        [app.tabBarController.selectedViewController presentModalViewController:app.register_view_controller animated:NO];
    
    
//        [app.tabBarController.selectedViewController presentModalViewController:nav animated:NO];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:app.register_view_controller];
    
    [app.register_view_controller setTitle:NSLocalizedString(@"Phone Number", @"Phone Number")];
    
    NSLog(@"app.tabBarController.selectedViewController: %@", app.tabBarController.selectedViewController);
    NSLog(@"app.tabBarController: %@", app.tabBarController);
    NSLog(@"app: %@", app);
    
    [app.tabBarController.selectedViewController presentViewController:nav animated:NO completion:^{}];
    
//        [app.tabBarController presentViewController:app.register_view_controller animated:YES completion:nil];
    
//        [app.tabBarController.selectedViewController presentViewController:nav animated:YES completion:nil];
}

- (void)sendSoundSettingToServer
{
    //send sound setting to server
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    
    NSString* soundSettingValue;
    NSString* groupSoundSettingValue;
    if ([[SettingsManager sharedInstance] enableSounds])
        soundSettingValue = @"0";
    else
        soundSettingValue = @"1";
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"profile", @"cmd", user_phone, @"phone", soundSettingValue, @"nosound", @"0", @"nosoundgroup", nil];
    [[ConnectionQueue sharedQueue] Add_Connection:app.chatRoomsViewController withParams:params withName:@"set_sound_setting" silent:YES];
}

- (void)tryToLogin
{
    self.view.userInteractionEnabled = NO;
    
    [app.acc showActivityViewer:NSLocalizedString(@"Loading...\n Please be patient", @"Loading...\n Please be patient")];

//    if (self.chat_board.view.superview == nil) {
//        [self bk_performBlock:^(id obj) {
//            [self.view addSubview:self.chat_board.view];
//            self.chat_board.view.hidden = YES;
//            self.chat_board.view.tag = 6666;
//        } afterDelay:0.1f];
//    }

    [self performLogin];
}

- (void) performLogin {
    
    NSString * userid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
    NSString * userpass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];
    
    [[JabberLayer SharedInstance] setLoggedinDelegate:self];
    BOOL result = [[JabberLayer SharedInstance] login:userid password:userpass];
    if (result != YES) {
        [app.acc hideActivityViewer];
        [self performSelector:@selector(gotoRegisterScreen) withObject:nil afterDelay:0.1f];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [self UpdateBadgeNumbers];
    [self.chat_rooms setContentOffset:CGPointMake(0, 44)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedChatRoomInfo:) name:@"update_chatroom_info" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedChatRoomInfo:) name:@"update_friends" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedReconnecting:) name:SERVER_STATE_RECONNECTING object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedReconnected:) name:SERVER_STATE_RECONNECTED object:nil];
 
    if (self.isHaveAppeared == NO) {
        [self becomeActiveProcess];
        self.isHaveAppeared = YES;
    }
    
    if ([[JabberLayer SharedInstance] isReconnecting]) {
        [self UpdatedReconnecting:nil];
    }
    else {
        [self UpdatedReconnected:nil];
    }
}

- (BOOL) becomeActiveProcess{
    
    if ([[JabberLayer SharedInstance] isLoggingProgress] || app.isRegistering)
        return NO;
    
    if ([RegisterViewController isActivated] == NO) {
        [self gotoRegisterScreen];
        return YES;
    }
    
    if ([[JabberLayer SharedInstance] isLoggingInitState]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"request_login" object:nil];
        return YES;
    }
    
    return NO;
}

- (void) viewDidAppear:(BOOL)animated {
    
    [self.chat_rooms setContentOffset:CGPointMake(0, 44)];
    
    if (self.needGoToRoom != nil) {
        
        [self bk_performBlock:^(id sender) {
            [self gotoOtherRoomOnChatBoard:self.needGoToRoom withKeyboard:YES];
            self.needGoToRoom = nil;
        }afterDelay:0.2f];
    }
    
    isAppear = YES;
}

- (void) viewWillDisappear:(BOOL)animated {
    
    isAppear = NO;
    
    if (self.table_editing)
        [self composeAction:nil];
    
    [self removeAllPopups];
}

- (void) viewDidDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_chatroom_info" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_friends" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SERVER_STATE_RECONNECTING object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SERVER_STATE_RECONNECTED object:nil];
}

- (void)editRoomsAction:(id)sender
{
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    self.table_editing = self.table_editing ? NO : YES;
    [self.chat_rooms setEditing:self.table_editing animated:YES];
    
    if (self.table_editing) {
        [self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Done", @"Done")];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    } else {
        [self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Edit", @"Edit")];
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}

- (void)composeAction:(id)sender
{
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:people_picker];
    UIBarButtonItem * cancel_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissComposeController:)];
    [people_picker.navigationItem setRightBarButtonItem:cancel_button];
    [people_picker setShort_version:YES];
    [people_picker setFriends_only:YES];
    [people_picker setDelegate:self];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void) dismissComposeController:(id)sender {
    [_chat_board forwardCancel];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) didSelectPerson:(int)index fromViewController:(UIViewController *)controller {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    [self Send_Private_Message:index withKeyboard:YES];
}

- (void) Flashing_Badge:(NSNumber *)first_time {
    
    static BOOL working = NO;
    static int counter = 0;
    
    bool need_restart = NO;
    
    if ([first_time boolValue] == YES) {
        
        counter = 0;
        if (working == YES)
            return;
    }
    
    working = YES;
    
    counter++;
    if (counter < 10)
        need_restart = YES;
    else
        counter = 0;
    
    int messages = [[ChatStorage sharedStorage] countNewMessages];
    
#if APP_TARGET_Yabb_Dialer
    UITabBarItem * item = [self.tabBarController.tabBar.items objectAtIndex:1];
#else
    UITabBarItem * item = [self.tabBarController.tabBar.items objectAtIndex:0];
#endif
    if (item.badgeValue == nil || need_restart == NO) {
        [item setBadgeValue:(messages?[NSString stringWithFormat:@"%d", messages]:nil) ];
        
        if (need_restart)
            [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:NO] afterDelay:1.0];
        else
            working = NO;
    }
    else {
        [item setBadgeValue:nil];
        [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.5];
    }
}

- (void) UpdateBadgeNumbers {
    
    int prev_numbers = [[UIApplication sharedApplication] applicationIconBadgeNumber];
    int messages = [[ChatStorage sharedStorage] countNewMessages];
    
    NSString * nums = [NSString stringWithFormat:@"%d", messages];
    
    //    NSLog(@"Bage nums: %@", nums);
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:messages];
    
    [self importRooms];
    [_chat_rooms reloadData];
    
    if (prev_numbers < messages) {
        [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:YES]];
    } else

#if APP_TARGET_Yabb_Dialer
        [[self.tabBarController.tabBar.items objectAtIndex:1] setBadgeValue:([nums isEqualToString:@"0"]?nil:nums)];
#else
    [[self.tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:([nums isEqualToString:@"0"]?nil:nums)];
#endif
}

- (void) processPostData {
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    
    [params setObject:@"post" forKey:@"cmd"];
    [params setObject:phone forKey:@"phone"];

    // Compose new local messages
    NSArray * messages_array = [[ChatStorage sharedStorage] getLocalMessages];
    
    if (messages_array != nil && [messages_array count] != 0) {
        
        int tr = 0;
        if ([messages_array count] >= 2) {
            tr++;
        }
        
        for (NSDictionary * message in messages_array) {
            [[JabberLayer SharedInstance] sendMessage:message];
        }
    }
    
    // set delivered state
    NSArray * delivered_array = [[ChatStorage sharedStorage] getDeliveredMessages];
    [[ChatStorage sharedStorage] resetDeliveredMessages];
    if (delivered_array != nil && [delivered_array count] != 0) {

        for (NSDictionary * dict in delivered_array) {
            NSString * message_id = [dict objectForKey:@"server_id"];
            [[JabberLayer SharedInstance] sendMessageSeen:message_id];
        }
    }
        
}

- (void) Move_To_Chat_Board:(NSDictionary *)room_dict showKeyboard:(BOOL)keyboard anim:(BOOL) isAnim  {
    
    if (room_dict == nil)
        return;
    
    //long long chat_id = [[room_dict objectForKey:@"room_id"] longLongValue];
    //NSString * chat_name = [room_dict objectForKey:@"room_name"];
    
    //[_chat_board setChatId:chat_id];
    //[_chat_board setTitle:chat_name];
    
    _chat_board.roomInfo = room_dict;
    [_chat_board onInitialUpdate];
    
    if (self.navigationController.visibleViewController != _chat_board) {
        
        // Does not push twice!
        NSArray * controllers = self.navigationController.viewControllers;
        BOOL has_chat_controller = NO;
        for (id obj in controllers) {
            if ([obj isKindOfClass:[ChatBoardViewController class]]) {
                [self.navigationController popToViewController:obj animated:isAnim];
                has_chat_controller = YES;
            }
        }
        
        // Not pushed? Do it!
        if (has_chat_controller == NO)
            [self.navigationController pushViewController:_chat_board animated:isAnim];
   }
    else
        [_chat_board viewWillAppear:NO];
    
    [_chat_board.board_table Scroll_To_Bottom:NO];
    
    
    if (keyboard)
        _chat_board.shouldShowKeyboard = YES;
    
}

- (NSString*) Send_Private_Message:(int)person_index withKeyboard:(BOOL)keyboard {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return nil;
    
    NSArray * phones = [[AddressCollector sharedCollector] Get_Phones_By_Index:person_index];
    
    if (phones == nil)
        return nil;
    
    NSLog(@"Send message!!!");
    
    for (NSString * phone in phones) {
        
        NSString* chat_id = [[ChatStorage sharedStorage] getPrivateRoomByPhone:phone];
        if (chat_id != nil) {
            
#if APP_TARGET_Yabb_Dialer
            [self.navigationController.tabBarController setSelectedIndex:1];
#else
            [self.navigationController.tabBarController setSelectedIndex:0];
#endif
            
//            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:chat_id, @"room_id", @"Chat", @"name", nil];
//            [self Move_To_Chat_Board:dict showKeyboard:keyboard anim:!keyboard];
            
            NSDictionary* dict = nil;

            for (NSDictionary* this_dict in self.chat_room_list) {
                NSString* this_chat_id = [this_dict objectForKey:@"room_id"];
                if ([this_chat_id isEqualToString:chat_id]) {
                    dict = this_dict;
                    break;
                }
            }
            
            if (dict)
                [self Move_To_Chat_Board:dict showKeyboard:keyboard anim:!keyboard];
            
            return chat_id;
        }
    }
    
    // Chat does not exist (need create)
    NSDictionary * dictionary = @{@"person_name":self.room_name, @"phones":phones};
    
    [self didCreateNewRoom:[[AddressCollector sharedCollector] Get_Name_By_Index:person_index] withPersons:[NSArray arrayWithObject:dictionary]];
    
    return 0;
}

- (void) gotoOtherRoomOnChatBoard:(NSString*)room_id withKeyboard:(BOOL)keyboard {
    
    NSDictionary* dict = nil;
    
    for (NSDictionary* this_dict in self.chat_room_list) {
        NSString* this_chat_id = [this_dict objectForKey:@"room_id"];
        if ([this_chat_id isEqualToString:room_id]) {
            dict = this_dict;
            break;
        }
    }
    
    if (dict) {
        [self Move_To_Chat_Board:dict showKeyboard:keyboard anim:!keyboard];
    }
}

#pragma mark -
#pragma mark Chat Room Table Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    
    if (self.has_filter)
        section = CHAT_ROOM_IDX;
        
    
    if (section == STATUS_IDX) {
        return 44;
    }
    else if (section == BROADCAST_IDX) {
        return 54;
    }
    
    NSDictionary * room_dict = [(self.has_filter?self.chat_room_sorted:self.chat_room_list) objectAtIndex:indexPath.row];
    BOOL private = [[room_dict objectForKey:@"private"] boolValue];
    
    return private?58:72;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    
    if (self.has_filter)
        section = CHAT_ROOM_IDX;
    
    if (self.table_editing)
        [self editRoomsAction:nil];
    
    if (section == STATUS_IDX) {
//        ChatStatusViewController * status_controller = [[ChatStatusViewController alloc] init];
//        [self.navigationController pushViewController:status_controller animated:YES];
        ChatRoomStatusCell* cell = (ChatRoomStatusCell*)[tableView cellForRowAtIndexPath:indexPath];
        [cell setEditing:YES];
    }
    else if (section == CHAT_ROOM_IDX) {
        [self.searchDisplayController.searchBar resignFirstResponder];
        [self Move_To_Chat_Board:[(self.has_filter?self.chat_room_sorted:self.chat_room_list) objectAtIndex:indexPath.row] showKeyboard:NO anim:YES];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    [tableView setBackgroundColor:DEFAULT_BACK_COLOR];
    [tableView setSeparatorColor:[UIColor lightGrayColor]];
    
    return self.has_filter?1:3;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.has_filter || indexPath.section == CHAT_ROOM_IDX)
        return YES;
    
    return NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.has_filter || section == CHAT_ROOM_IDX) {
        int num_of_rooms = (self.has_filter?self.chat_room_sorted:self.chat_room_list)?[(self.has_filter?self.chat_room_sorted:self.chat_room_list) count]:0;
        
        if (num_of_rooms == 0 && self.table_editing) {
            [self editRoomsAction:self];
        }
        
        return num_of_rooms;
    }
    
    return 1;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.length  > 0) {
            NSDictionary * chat = [(self.has_filter?self.chat_room_sorted:self.chat_room_list) objectAtIndex:indexPath.row];
            [[ChatStorage sharedStorage] deleteChatRoom:[chat objectForKey:@"room_id"]];
            [self importRooms];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self UpdateBadgeNumbers];
        }
    }
}

- (void) TapBroadcast {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
    self.multiselector_controller = [[PeopleMultiselectorViewController alloc] initWithRootViewController:people_picker rightButtonText:NSLocalizedString(@"Next", @"Next")];
    
    [people_picker setDelegate:self.multiselector_controller];
    [self.multiselector_controller setRoot_controller:self];
    [self.multiselector_controller setMultiselector_delegate:self];
    
    [self presentViewController:self.multiselector_controller animated:YES completion:^{}];
    
    NSLog(@"Broadcast");
}

- (void) TapGroupMessage {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;

    CreateGroupChatViewController * createRoomController = [[CreateGroupChatViewController alloc] initWithNibName:@"CreateGroupChatViewController" bundle:nil];
    createRoomController.delegate = self;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:createRoomController];
    [self presentViewController:nav animated:YES completion:^{}];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    int row = indexPath.row;
    
    if (self.has_filter)
        section = CHAT_ROOM_IDX;
    
    if (section == BROADCAST_IDX) {
        
        UITableViewCell * cell = [self.chat_rooms dequeueReusableCellWithIdentifier:@"BroadcastCellId"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomCell" owner:self options:nil];
            cell = (ChatRoomCell *)[nib objectAtIndex:1];
            
            UIButton * broadcast_button = (UIButton *)[cell viewWithTag:10];
            [broadcast_button addTarget:self action:@selector(TapBroadcast) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton * group_button = (UIButton *)[cell viewWithTag:20];
            [group_button addTarget:self action:@selector(TapGroupMessage) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        return cell;
    }
    else if (section == STATUS_IDX) {
        
        ChatRoomStatusCell * cell = [self.chat_rooms dequeueReusableCellWithIdentifier:@"ChatRoomStatusId"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomStatusCell" owner:self options:nil];
            cell = (ChatRoomStatusCell *)[nib objectAtIndex:0];
        }
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tableview_item_indicator.png"]];
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        [cell updateStatus];
        
        return cell;
    }
    else if (section == CHAT_ROOM_IDX) {
        
        ChatRoomCell * cell = [self.chat_rooms dequeueReusableCellWithIdentifier:@"ChatRoomCellId"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomCell" owner:self options:nil];
            cell = (ChatRoomCell *)[nib objectAtIndex:0];
        }
        
        if (row < [(self.has_filter?self.chat_room_sorted:self.chat_room_list) count]) {
            NSDictionary * room_dict = [(self.has_filter?self.chat_room_sorted:self.chat_room_list) objectAtIndex:row];
            [cell setRoomName:room_dict?[room_dict objectForKey:@"room_name"]:@""];
            
            NSString* chat_id = [room_dict objectForKey:@"room_id"];
            BOOL private = [[room_dict objectForKey:@"private"] boolValue];
            NSString * badge_str = [NSString stringWithFormat:@"%d", [[ChatStorage sharedStorage] countRoomNewMessages:chat_id]];
            
            [cell setLastMessage:chat_id];
            [cell setRoomType:private];
            
            //            CustomBadge * people_badge = (CustomBadge *)[cell viewWithTag:10];
            
            NSString * phones_str = [room_dict objectForKey:@"phones"];
            NSArray * phones = [phones_str componentsSeparatedByString:@":"];
            
            if (!private) {
                UIImage* image = [[AvatarManager sharedInstance] getAvatarWithPhone:chat_id];
                if (image == nil)
                    image = [UIImage imageNamed:@"placeholderGroup_37.png"];
                [cell setRoomPhoto:image];
                [cell setPhoto:phones];
            }
            else {
                UIImage * image = [[AvatarManager sharedInstance] getAvatarWithPhone:[phones objectAtIndex:0]];
                if (image == nil)
                    image = [UIImage imageNamed:@"placeholderPerson_52.png"];
                [cell setRoomPhoto:image];
                [cell setPhoto:nil];
            }
            
            /*
             if (private) {
             if (people_badge)
             [people_badge removeFromSuperview];
             
             }
             else {
             
             if (people_badge == nil) {
             people_badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"1"]];
             [people_badge setTag:10];
             [people_badge setBadgeInsetColor:[UIColor darkGrayColor]];
             [cell addSubview:people_badge];
             }
             
             [people_badge setBadgeText:[NSString stringWithFormat:@"%d", [phones count]]];
             
             CGRect rect = people_badge.frame;
             rect.origin.x = -2;
             rect.origin.y = -2;
             [people_badge setFrame:rect];
             
             }*/
            
            CustomBadge * prev_badge = (CustomBadge *)[cell viewWithTag:20];
            if (prev_badge)
                [prev_badge removeFromSuperview];
            
            if ([badge_str isEqualToString:@"0"] == NO) {
                
                CustomBadge * badge = [CustomBadge customBadgeWithString:badge_str];
                [cell addSubview:badge];
                [badge setTag:20];
                
                CGRect rect = [badge frame];
                
                rect.origin.x = 290 - rect.size.width;
                rect.origin.y = (cell.frame.size.height - rect.size.height)/2;
                
                [badge setFrame:rect];
            }
        }
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tableview_item_indicator.png"]];
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        return cell;
    }
    
    return nil;
}

#pragma mark -
#pragma mark People Picker Delegate

- (void) didDoneButton:(NSString *)abook
{
    BroadcastViewController * b_controller = [[BroadcastViewController alloc] init];
    [b_controller setArray:[NSArray arrayWithArray:[[InvitedFriends SharedFriends] InvitedArray]]];
    [self.navigationController pushViewController:b_controller animated:YES];
    [self.multiselector_controller dismissViewControllerAnimated:YES completion:^{}];
    return;
    
    /*
     NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
     if (phone == nil)
     return;
     
     
     NSMutableDictionary * params = [NSMutableDictionary dictionary];
     
     [params setObject:@"addchat" forKey:@"cmd"];
     [params setObject:phone forKey:@"phone"];
     
     int counter = 0;
     for (NSDictionary * dict in [[InvitedFriends SharedFriends] InvitedArray]) {
     
     NSString * person = [dict objectForKey:@"phone"];
     NSString * name = [dict objectForKey:@"name"];
     if (phone == nil || name == nil)
     continue;
     
     
     NSString * name_key = [NSString stringWithFormat:@"chats[chat_%d][name]", counter];
     NSString * abook_key = [NSString stringWithFormat:@"chats[chat_%d][abook]", counter];
     
     [params setObject:[[ChatStorage sharedStorage] encodeString:name] forKey:name_key];
     [params setObject:[[AddressCollector sharedCollector] packPhoneNumber:person] forKey:abook_key];
     
     counter++;
     }
     [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"addchats" silent:NO];
     
     NSLog(@"Add new chat: %@", params);
     */
}

- (NSString*) getDoneButtonText {
    
    return NSLocalizedString(@"Next", @"Next");
}

#pragma mark -
#pragma Create New Room Delegate

- (void) didCreateNewRoom:(NSString *)room withPersons:(NSArray *)invated_persons {
    
    [self.room_name setString:room];
    
    NSLog(@"Create room: %@", self.room_name);
    
    //    NSString * pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    
    if (phone == nil)
        return;
    
    [self.room_phones removeAllObjects];
    
    NSMutableString * abook = [[NSMutableString alloc] init];
    for (NSDictionary * person in invated_persons) {
        
        NSArray * phones = [person objectForKey:@"phones"];
        for (NSString * phone in phones) {
            
            if ([[Friends SharedFriends] isFriendPhone:phone]) {
                //            if ([[AddressCollector sharedCollector] isPhoneFriend:phone]) {
                
                //                long long v = [phone longLongValue];
                //[abook appendFormat:@"%013qx", v];
                
                [abook appendString:[[AddressCollector sharedCollector] packPhoneNumber:phone]];
                
                
                [self.room_phones addObject:phone];
                
                break;
            }
        }
    }
    
    if ([abook isEqualToString:@""])
        return;
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone",
                             [[ChatStorage sharedStorage] encodeString:self.room_name], @"name", /*abook*/self.room_phones, @"abook", nil];
    
    NSLog(@"Add new chat: %@", params);
    
    [app.acc showActivityViewer:NSLocalizedString(@"Creating...", @"Creating...")];
    
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"create_new_room" silent:NO];
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([[dict objectForKey:@"errorcode"] intValue] == -2) {

        // Epic Fail
        [[ChatStorage sharedStorage] resetAccount];

        [app.register_view_controller clearSelection];
        [app.tabBarController.selectedViewController presentViewController:app.register_view_controller animated:NO completion:^{}];
        
        return;
    }
    
    if ([connection_name isEqualToString:@"set_user_status"])
    {
        [_chat_rooms reloadData];
    }
    else if ([connection_name isEqualToString:@"create_new_room"])
    {
        // Create new room
        
        [app.acc hideActivityViewer];
        
        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1)
            return;
        
        NSString * number_id = [[dict objectForKey:@"data"] objectForKey:@"chatid"];
        if (number_id == nil)
            return;
        
        NSString* chat_id = number_id;
        
        // Add new room
        NSString * created_name = [[ChatStorage sharedStorage] addChatRoom:self.room_name withChatId:chat_id withPhones:self.room_phones];
        
        if (created_name == nil) {
            [self UpdateBadgeNumbers];
        }
        
        bool keyboard = YES;
        
        if (_chat_rooms_delegate && [_chat_rooms_delegate respondsToSelector:@selector(didCreateRoom:)]) {
            
            keyboard = NO;
            [_chat_rooms_delegate didCreateRoom:chat_id];
        }
//        else {
            
            if (created_name == nil) {
                [self.navigationController popViewControllerAnimated:NO];                
            }
            
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:chat_id, @"room_id", self.room_name, @"room_name", nil];
        
#if APP_TARGET_Yabb_Dialer
            [self.navigationController.tabBarController setSelectedIndex:1];
#else
            [self.navigationController.tabBarController setSelectedIndex:0];
#endif
        
        [self Move_To_Chat_Board:dict showKeyboard:keyboard anim:!keyboard];
//        }
    }
    else if ([connection_name isEqualToString:@"lazy_chat_connection"]) {
        
        NSDictionary * data = [dict objectForKey:@"data"];
        //NSLog(@"Update: %@", data);
        
        [[ChatStorage sharedStorage] syncMessages:data];
    }
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"create_new_room"])
        [app.acc hideActivityViewer];
}


- (void) sortByFilter:(NSString *)filter {

    [self.chat_room_sorted removeAllObjects];
    
    if (filter == nil || [filter isEqualToString:@""]) {
        self.has_filter = NO;
        return;
    }
    else {
        self.has_filter = YES;
    }
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    
    for (NSDictionary * room in self.chat_room_list) {
        
        // Compare room name
        NSString * rn = [room objectForKey:@"room_name"];
        
        if (rn && [rn isEqualToString:@""] == NO) {
            NSRange range = [[rn lowercaseString] rangeOfString:filter];
            
            if (range.length) {
                [self.chat_room_sorted addObject:room];
                continue;
            }
        }
        
        // Search by senders
        BOOL found = NO;
        NSString * phones_str = [room objectForKey:@"phones"];
        NSArray * phones = [phones_str componentsSeparatedByString:@":"];
        for (int i = 0; i < [phones count]; i++) {
            NSString * person_name = [[Friends SharedFriends] nameByPhone:[phones objectAtIndex:i]];
            if (person_name) {
                NSRange range = [[person_name lowercaseString] rangeOfString:filter];
                if (range.length) {
                    [self.chat_room_sorted addObject:room];
                    found = YES;
                    break;
                }
            }
        }
        if (found) {
            continue;
        }
        
        // Compare messages
        NSString* room_id = [room objectForKey:@"room_id"];
        NSArray * messages = [[ChatStorage sharedStorage] getAllRecords:room_id];
        for (NSDictionary * dict in messages) {
            NSString * original_msg = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"original_message"]];
            NSRange range = [[original_msg lowercaseString] rangeOfString:filter];
            if (range.length) {
                [self.chat_room_sorted addObject:room];
                break;
            }
        }
    }

    NSLog(@"Sort time: %.2f", [[NSDate date] timeIntervalSince1970] - time );
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar setText:@""];
    [self sortByFilter:@""];
    [searchBar resignFirstResponder];
    [_chat_rooms reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {

    [controller.searchResultsTableView setBackgroundColor:DEFAULT_BACK_COLOR];
    [controller.searchResultsTableView setSeparatorColor:[UIColor lightGrayColor]];

    [self sortByFilter:[searchString lowercaseString]];
    
    return YES;
}

- (void) loginCompleted:(NSNumber*) success needRegister:(NSNumber*)needRegister {
    
//    if (self.chat_board.view.tag == 6666) {
//        [self.chat_board.view removeFromSuperview];
//        self.chat_board.view.hidden = NO;
//        self.chat_board.view.tag = 0;
//    }

    if ([success boolValue]) {
        [self sendSoundSettingToServer];
    }
    
    [[JabberLayer SharedInstance] setLoggedinDelegate:nil];
    if (![success boolValue]) {
        if ([needRegister boolValue]) {
            
            self.view.userInteractionEnabled = YES;
            [app.acc hideActivityViewer];
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Registration failed. Check internet connection and try again", @"Registration failed. Check internet connection and try again") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            alert.tag = 334;
            [alert show];
        }
        else {
            [self performSelector:@selector(performLogin) withObject:nil afterDelay:2.0];
        }
    }
    else {
        //success
        self.view.userInteractionEnabled = YES;
        [app.acc hideActivityViewer];
    }
}

- (void)UpdatedChatRoomInfo:(NSNotification*) notification {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(UpdateBadgeNumbers) object:nil];
    
    NSDictionary* param = notification.object;
    if (param && [param objectForKey:@"force"]) {
        [self UpdateBadgeNumbers];
        return;
    }
    
    [self performSelector:@selector(UpdateBadgeNumbers) withObject:nil afterDelay:1.0f];
}

- (void) AddedMessages:(NSNotification*) notification {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(AddedMessagesProcess) object:nil];
    [self performSelector:@selector(AddedMessagesProcess) withObject:nil afterDelay:1.0f];
}

- (void) AddedMessagesProcess {
    
    int prev_numbers = [[UIApplication sharedApplication] applicationIconBadgeNumber];
    int messages = [[ChatStorage sharedStorage] countNewMessages];
    if (prev_numbers < messages) {
        [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:YES]];
        
        if (app.isForeground) {
            int enabledSoundNewMessages = [[ChatStorage sharedStorage] countNewMessagesWithEnableSound];
            int enabledVibrateNewMessages = [[ChatStorage sharedStorage] countNewMessagesWithEnableVibrate];
            if (enabledSoundNewMessages > 0)
                [[SoundManager sharedInstance] playMessageReceivedSound];
            if (enabledVibrateNewMessages > 0)
                [[SoundManager sharedInstance] playMessageReceivedVibrate];
        }
    }
    
    if (messages > 0) {
        NSDictionary* lastMsg = [[ChatStorage sharedStorage] getLastMessage:nil];
        [self onNewMessageReceived:[lastMsg objectForKey:@"server_id"]];
    }
    
    if (app.isForeground && self.navigationController.visibleViewController == _chat_board) {
        [[ChatStorage sharedStorage] markRoomRead:[_chat_board getChatId]];
    }
    
    [self UpdateBadgeNumbers];
}


- (void) ChangedContactbook:(NSNotification*) notification {
    
    if ([[JabberLayer SharedInstance] isLoggingProgress])
        return;
    
    if (![[AddressCollector sharedCollector] isContactBookChanged])
        return;

    [app.acc showActivityViewer:NSLocalizedString(@"Contacts changed. Updating Yabb", @"Contacts changed. Updating Yabb")];
    [[JabberLayer SharedInstance] setLoggedinDelegate:self];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[AddressCollector sharedCollector] GetPeopleWithNormal:YES];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[AddressCollector sharedCollector] GetPeopleSynchronize];
            [[AddressCollector sharedCollector] remakeAllPeopleByFriends:nil];
            [app->contacts Sort_By_Filter:@"" friendsOnly:app->contacts.friends_only];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_contactlist" object:nil];
        });
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[AddressCollector sharedCollector] setConnectionDoneDeleagate:[JabberLayer SharedInstance]];
            [[AddressCollector sharedCollector] Upload_To_Server:YES];
        });
    });
}

- (void) ConflictedAccount:(NSNotification*) notification {
 
    [self gotoRegisterScreen];
}

- (UILocalNotification *)makeNotification:(NSString*)friendName text:(NSString*)text jid:(NSString*)jid chatid:(NSString*)chatId isPhoto:(BOOL) isPhoto {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [[NSDate date] dateByAddingTimeInterval:1.0f];
    if ([[SettingsManager sharedInstance] enableSounds] && [[ChatStorage sharedStorage] getChatSoundSetting:chatId])
        notification.soundName = @"message_received_notification.caf";
    if (isPhoto)
        notification.alertBody = [NSString stringWithFormat:@"%@ %@", friendName, text];
    else
        notification.alertBody = [NSString stringWithFormat:@"%@ : %@", friendName, text];
    notification.userInfo = @{@"chatid":jid};
    return notification;
}

- (void) onNewMessageReceived:(NSString*) msgId {
    
    if (!isAppear && !app.isTempAlive)
        return;
    
    NSDictionary* msgDict = [[ChatStorage sharedStorage] getRecordByServerId:msgId];
    if (msgDict == nil) {
        NSLog(@"error - ChatBoardViewController - onNewMessageReceived");
        return;
    }
    
    NSString* chatId = [msgDict objectForKey:@"room_id"];
    NSString* phoneNo = [msgDict objectForKey:@"phone"];
    NSDictionary * last_message = [[ChatStorage sharedStorage] getLastMessage:chatId];
    
    NSString* friendName = [[AddressCollector sharedCollector] Get_Name_By_Phone:phoneNo withPlus:YES];
    NSString* text = [last_message objectForKey:@"text"];
    UIImage* friendPhoto = [[AvatarManager sharedInstance] getAvatarWithPhone:phoneNo];
    BOOL isPhoto = [last_message objectForKey:@"is_photo"] != nil;
    
    if (app.isTempAlive)
    {
        UILocalNotification *notification;
        NSString* jid = [[JabberLayer SharedInstance] getServerChatID:chatId phone:phoneNo];
        notification = [self makeNotification:friendName text:text jid:jid chatid:chatId isPhoto:isPhoto];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        return;
    }
    
    PopUpMessageView* popup = [popups objectForKey:chatId];
    if (popup == nil) {
        popup = [[PopUpMessageView alloc] initDefault:self];
        UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
        [window addSubview:popup];//[self.view addSubview:popup];
        [popups setObject:popup forKey:chatId];
        [popup setInfo:friendPhoto name:friendName text:text chatid:chatId];
    }
    else {
        [popup setText:text];
    }
}

- (void) removeAllPopups {
    
    for (NSString* key in [popups allKeys]) {
        PopUpMessageView* view = [popups objectForKey:key];
        [popups removeObjectForKey:key];
        [view removeFromSuperview];
    }
}

- (void) gotoOtherRoom:(NSString*) chatid {
    
    [self gotoOtherRoomOnChatBoard:chatid withKeyboard:YES];
}


#pragma mark -
#pragma mark UIAlertViewDelegate Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 333) {
        [self tryToLogin];
    }
    
    if (alertView.tag == 334) {
        [self gotoRegisterScreen];
    }
}

-(void) UpdatedReconnecting:(NSNotification*) notification {

    titleView = self.navigationItem.titleView;
    self.navigationItem.titleView = reconnectingView;
}

-(void) UpdatedReconnected:(NSNotification*) notification {

    self.navigationItem.titleView = titleView;
    titleView = nil;
}

- (void) didCreateNewRoom:(NSString *)room_subject withRoomPhoto:(UIImage*) room_photo isPublic:(BOOL)isPublic {
    
     PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
     self.multiselector_controller = [[PeopleMultiselectorViewController alloc] initWithRootViewController:people_picker rightButtonText:nil];
     
     [people_picker setDelegate:self.multiselector_controller];
     [self.multiselector_controller setRoot_controller:self];
//    [multiselector_controller setMultiselector_delegate:self];
    if (room_subject == nil)
        room_subject = @"";
    
    if (room_photo == nil)
        self.multiselector_controller.groupParam = @{@"subject":room_subject, @"is_public":[NSNumber numberWithBool:isPublic]};
    else
        self.multiselector_controller.groupParam = @{@"subject":room_subject, @"photo":room_photo, @"is_public":[NSNumber numberWithBool:isPublic]};
    
     [self presentViewController:self.multiselector_controller animated:YES completion:^{}];
}


@end
