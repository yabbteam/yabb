//
//  PeopleMultiselectorViewController.m
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import "AppDelegate.h"
#import "PeopleMultiselectorViewController.h"
#import "PickerCell.h"
//#import "InviteCell.h"
#import "InvitedFriends.h"
#import "ChatStorage.h"
#import "ConnectionQueue.h"
#import "ChatRoomsViewController.h"
#import "AddressCollector.h"

@interface PeopleMultiselectorViewController()

@end

@implementation PeopleMultiselectorViewController

@synthesize multiselector_delegate = _multiselector_delegate;

- (id)initWithRootViewController:(UIViewController *)rootViewController rightButtonText:(NSString*) rtext {
    
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        
        people_picker = (PeoplePickerViewController *)rootViewController;
        
        [people_picker setFriends_only:YES];
        [people_picker setShort_version:YES];
        [people_picker setInvite_mode:YES];

        // Cancel Button
        UIBarButtonItem * left_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didCancelButton)];
        [people_picker.navigationItem setLeftBarButtonItem:left_button];

        // Done Button
        if (rtext == nil)
            done_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didDoneButton)];
        else {
            done_button = [[UIBarButtonItem alloc] initWithTitle:rtext style:UIBarButtonItemStylePlain target:self action:@selector(didDoneButton)];
        }
        [done_button setEnabled:NO];
        [people_picker.navigationItem setRightBarButtonItem:done_button];
        
        room_phones = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];

    [[InvitedFriends SharedFriends] removeAll];
}

#pragma mark -
#pragma mark Buttons delegate


- (void) textFieldDidChange {

    InviteScrollView * invite_scroll_view = people_picker.invite_scroll_view;
    
    NSString * search_str = [invite_scroll_view->text_field.text isEqualToString:@""]?invite_scroll_view->text_field.text:[invite_scroll_view->text_field.text substringFromIndex:1];
    
    [invite_scroll_view Remove_Selected];
    
    [people_picker.contacts Sort_By_Filter:search_str friendsOnly:YES];
    
    [invite_scroll_view Update_Height];
    [invite_scroll_view Text_Field_Position];
    [invite_scroll_view Set_Invite_Height];
    
    [people_picker.table_view_without_search reloadData];
    [invite_scroll_view->text_field becomeFirstResponder];
}

- (void) updateDoneButton {
        
    if ([[InvitedFriends SharedFriends] Count])
        [done_button setEnabled:YES];
    else
        [done_button setEnabled:NO];
    
}

- (void) didCancelButton {
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void) didDoneButton {
    
    NSMutableArray* abook = [[NSMutableArray alloc] init];
    for (NSDictionary * dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        NSString * phone = [dict objectForKey:@"phone"];
        NSString * name = [dict objectForKey:@"name"];
        if (phone == nil || name == nil)
            continue;
        
        [room_phones addObject:phone];
        
        [abook addObject:phone];
    }

    // Invate for exist room
    if (_multiselector_delegate && [_multiselector_delegate respondsToSelector:@selector(didDoneButton:)]) {
        
        [_multiselector_delegate didDoneButton:abook];
        
        return;
    }
    
//    [room_name setString:@""];
//    [room_phones removeAllObjects];
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    if (phone == nil)
        return;
    
    UIImage* photo = [self.groupParam objectForKey:@"photo"];
    NSString* subject = [self.groupParam objectForKey:@"subject"];
    NSNumber* isPublic = [self.groupParam objectForKey:@"is_public"];
    
    NSDictionary * params;
    
    if (photo == nil) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone",
                  abook, @"abookg", isPublic, @"is_public", subject, @"subject", nil];
    }
    else {
        params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone",
                  abook, @"abookg", isPublic, @"is_public", photo, @"photo", subject, @"subject", nil];
    }
    
    NSLog(@"Add new chat: %@", params);
    
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"create_new_room" silent:NO];
    
    [app.acc showActivityViewer:NSLocalizedString(@"Creating...", @"Creating...")];
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"create_new_room"]) {     // Create new room
        
        [app.acc hideActivityViewer];
        
        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1)
            return;
        
        NSString* number_id = [[dict objectForKey:@"data"] objectForKey:@"chatid"];
        if (number_id == nil)
            return;
        
        NSString* chat_id = number_id;
        
        ChatRoomsViewController * room_controller = app.chatRoomsViewController;
        
        NSString* receivedName = [[dict objectForKey:@"data"] objectForKey:@"room_name"];
        
        // Add new room
        NSString * created_name = [[ChatStorage sharedStorage] addChatRoom:receivedName withChatId:chat_id withPhones:room_phones];
        
        [room_controller importRooms];
        [self dismissModalViewControllerAnimated:YES];
        
        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:chat_id, @"room_id", (created_name?created_name:receivedName), @"room_name", nil];
        
        [room_controller Move_To_Chat_Board:dict showKeyboard:NO anim:YES];
    }
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"create_new_room"])
        [app.acc hideActivityViewer];
}

#pragma mark - 
#pragma mark People Picker Delegates

- (void) didSelectCell:(NSIndexPath *)indexPath withIndex:(int)index withName:(NSString *)name withPhone:(NSString *)phone selected:(BOOL)selected {
    
    if (people_picker == nil)
        return;
    
    if (selected)
        [people_picker.invite_scroll_view Add_New_Person:name withIndex:index withPhone:phone];
    else
        [people_picker.invite_scroll_view Remove_Person:index];

    [people_picker.invite_scroll_view Rebuild_Persons];
    [people_picker.invite_scroll_view Update_Height];
    [people_picker.invite_scroll_view Set_Invite_Height];
    
    [self updateDoneButton];
}

- (BOOL) isPreselectedCell:(NSString *)phone {

    for (NSString * pre_phone in self.preselected_phones) {
        
        if ([pre_phone isEqualToString:phone])
            return YES;
        
    }
    
    return NO;
}

@end
