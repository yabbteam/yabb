//
//  ChatImageCell.m
//  speedsip
//
//  Created by denebtech on 29.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatImageCell.h"
#import "NSData+Base64.h"
#import "ChatStorage.h"
#import "JabberLayer.h"
#import "CommonUtil.h"
#import "TimeBomb.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define SIZE_BUFSZ 7
static char const SIZE_PREFIXES[] = "kMGTPEZY";

void format_size(char buf[SIZE_BUFSZ], uint64_t sz)
{
    int pfx = 0;
    unsigned int m, n, rem, hrem;
    uint64_t a;
    if (sz <= 0) {
        memcpy(buf, "0 B", 3);
        return;
    }
    a = sz;
    if (a < 1000) {
        n = a;
        snprintf(buf, SIZE_BUFSZ, "%u B", n);
        return;
    }
    for (pfx = 0, hrem = 0; ; pfx++) {
        rem = a % 1000ULL;
        a = a / 1000ULL;
        if (!SIZE_PREFIXES[pfx + 1] || a < 1000ULL)
            break;
        hrem |= rem;
    }
    n = a;
    if (n < 10) {
        if (rem >= 950) {
            buf[0] = '1';
            buf[1] = '0';
            buf[2] = ' ';
            buf[3] = SIZE_PREFIXES[pfx];
            buf[4] = 'B';
            buf[5] = '\0';
            return;
        } else {
            m = rem / 100;
            rem = rem % 100;
            if (rem > 50 || (rem == 50 && ((m & 1) || hrem)))
                m++;
            snprintf(buf, SIZE_BUFSZ,
                     "%u.%u %cB", n, m, SIZE_PREFIXES[pfx]);
        }
    } else {
        if (rem > 500 || (rem == 500 && ((n & 1) || hrem)))
            n++;
        if (n >= 1000 && SIZE_PREFIXES[pfx + 1]) {
            buf[0] = '1';
            buf[1] = '.';
            buf[2] = '0';
            buf[3] = ' ';
            buf[4] = SIZE_PREFIXES[pfx+1];
            buf[5] = 'B';
            buf[6] = '\0';
        } else {
            snprintf(buf, SIZE_BUFSZ,
                     "%u %cB", n, SIZE_PREFIXES[pfx]);
        }
    }
}

@implementation ChatImageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(NSDictionary *)dict editMode:(BOOL)edit {
    
    NSString * encoded_image = [dict objectForKey:@"media_thumb"];
    NSData * image_data = [NSData dataFromBase64String:encoded_image];
    UIImage * image = [UIImage imageWithData:image_data];
    NSString * comment = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"original_message"]];

    CGSize size = image.size;
    
    [self.thumb setImage:image];
    int message_y = _is_private?0:10;

    CGSize thumb_size = [ChatStorage getThumbSize:size];
    
    int photo_width = person_photo.frame.size.width + 4;
    
    self.labelComment.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.labelComment.delegate = self;
    self.labelComment.font = [UIFont systemFontOfSize:IMAGE_COMMENT_FONT_SIZE];
    self.labelComment.text = comment;
    
    CGSize labelSize = [self getCommentTextSize:comment width:thumb_size.width-10];
    if (labelSize.width > 0 && labelSize.height > 0) {
        labelSize.width -= 4;
    }
    
    CGSize whole_size = thumb_size;
    whole_size.height += labelSize.height;
    
    if (_is_owner) {
        UIImage * bubble_image = [[UIImage imageNamed:@"ChatBubbleGreen.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:8];
        
        [self.bubble setFrame:CGRectMake(self.parent.frame.size.width - thumb_size.width - photo_width, 0, thumb_size.width+1, whole_size.height)];
        [self.bubble setImage:bubble_image];

        [self.thumb setFrame:CGRectMake(self.parent.frame.size.width - thumb_size.width + 5 - photo_width, 5, thumb_size.width - 11, thumb_size.height - 12)];
        [self.thumb_button setFrame:CGRectMake(self.parent.frame.size.width - thumb_size.width + 5- photo_width, 5, thumb_size.width - 11, thumb_size.height - 12)];
        
        [self.labelComment setFrame:CGRectMake(self.thumb.frame.origin.x+2, self.thumb.frame.origin.y+self.thumb.frame.size.height,
                                               whole_size.width-10, labelSize.height)];
        
        [msg_status setFrame:CGRectMake(self.parent.frame.size.width - whole_size.width + 8 - photo_width, whole_size.height-2, 52, 11)];
        [msg_time setFrame:CGRectMake(self.parent.frame.size.width-whole_size.width - msg_time.frame.size.width - 1 - photo_width, (whole_size.height)/2.0 - 11, msg_time.frame.size.width, msg_time.frame.size.height)];
        
        [selector setFrame:CGRectMake(selector.frame.origin.x - photo_width, (whole_size.height)/2.0 - 15, selector.frame.size.width, selector.frame.size.height)];

        [btnOperation setFrame:CGRectMake(self.parent.frame.size.width-whole_size.width - btnOperation.frame.size.width - 5 - photo_width, 0, btnOperation.frame.size.width, btnOperation.frame.size.height)];
        btnOperation.frame = CGRectMake(btnOperation.frame.origin.x - photo_width, btnOperation.frame.origin.y, 30, 30);
        
        //set person photo
        [person_photo setFrame:CGRectMake(self.parent.frame.size.width - person_photo.frame.size.width - 2,
                                          self.bubble.frame.origin.y+self.bubble.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        UIImage* myImage = [self getMyPhoto];
        if (myImage != nil)
            [person_photo setImage:myImage];
    }
    else {
        UIImage * bubble_image = [[UIImage imageNamed:@"ChatBubbleGray.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:8];
        
        [self.bubble setFrame:CGRectMake(photo_width+(edit?40:0), message_y + 2, thumb_size.width+1, whole_size.height)];
        [self.bubble setImage:bubble_image];
        
        [self.thumb setFrame:CGRectMake(photo_width+7 + (edit?40:0), message_y + 7, thumb_size.width - 11, thumb_size.height - 12)];
        [self.thumb_button setFrame:CGRectMake(photo_width+7 + (edit?40:0), message_y + 7, thumb_size.width - 11, thumb_size.height - 12)];
        
        [self.labelComment setFrame:CGRectMake(self.thumb.frame.origin.x+2, self.thumb.frame.origin.y+self.thumb.frame.size.height,
                                               whole_size.width-10, labelSize.height)];
        
        [msg_time setFrame:CGRectMake(photo_width+whole_size.width+2 + (edit?40:0), (whole_size.height)/2.0 + message_y - 10, msg_time.frame.size.width, msg_time.frame.size.height)];
        [selector setFrame:CGRectMake(photo_width+selector.frame.origin.x, (whole_size.height)/2.0 + message_y - 14, selector.frame.size.width, selector.frame.size.height)];

        CGRect rect = person_name.frame;
        rect.origin.x  = photo_width+(edit?40:0);
        [person_name setFrame:rect];

        //person photo
        [person_photo setFrame:CGRectMake((edit?40:0)+2,
                                          self.bubble.frame.origin.y+self.bubble.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        UIImage* photo = self.personPhoto;
        if (photo != nil)
            [person_photo setImage:photo];
    }
    
    if (message_type == 2) { //video
        self.iconVideoPlay.hidden = NO;
        self.iconVideoPlay.center = self.thumb.center;
        
        CGRect rtProgress = CGRectMake(self.thumb.frame.origin.x, self.thumb.frame.origin.y+self.thumb.frame.size.height-16.0+2, self.thumb.frame.size.width, self.progressUploadingVideo.frame.size.height);
        self.progressUploadingVideo.frame = rtProgress;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onChangedUploadingProgress:)
                                                     name:@"upload_progress_updated" object:nil];
    }
    else {
        self.iconVideoPlay.hidden = YES;
        self.progressUploadingVideo.hidden = YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"upload_progress_updated" object:nil];
    }
    
    if (message_type == 2 && !_is_owner && media_size > 0) {
        self.labelVideoSize.hidden = NO;
        
        CGPoint pt;
        pt.x = self.iconVideoPlay.center.x;
        pt.y = self.iconVideoPlay.center.y + self.iconVideoPlay.bounds.size.height/2 + self.labelVideoSize.frame.size.height/2 + 6.0f;
        self.labelVideoSize.center = pt;
        
        CALayer * l = [self.labelVideoSize layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:8.0];
        
        char sss[100];
        format_size(sss, media_size);
        self.labelVideoSize.text = [NSString stringWithUTF8String:sss];
    }
    else {
        self.labelVideoSize.hidden = YES;
    }
    
    CALayer * l = [self.thumb layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];

    UIImageView * closed = (UIImageView*)[self.thumb viewWithTag:555];
    UILabel * winkTimeLabel = (UILabel*)[self.thumb viewWithTag:556];
    if (_is_owner && [[TimeBomb sharedInstance] isTimeBombMessage:self.server_id]) {
        
        if (closed == nil) {
            closed = [[UIImageView alloc] initWithFrame:CGRectMake(self.thumb.frame.size.width-40, 0, 40, 40)];
            closed.tag = 555;
            [self.thumb addSubview:closed];
            
            CALayer * l = [closed layer];
            [l setMasksToBounds:YES];
            [l setCornerRadius:2.0];
        }
        
        if (winkTimeLabel == nil) {
            winkTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            winkTimeLabel.backgroundColor = [UIColor whiteColor];
            winkTimeLabel.textAlignment = NSTextAlignmentCenter;
            winkTimeLabel.textColor = [UIColor darkGrayColor];
            winkTimeLabel.tag = 556;
            winkTimeLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0f];

            CALayer * l = [winkTimeLabel layer];
            [l setMasksToBounds:YES];
            [l setCornerRadius:2.0];

            [self.thumb addSubview:winkTimeLabel];
        }
        
        closed.hidden = NO;
        closed.frame = CGRectMake(self.thumb.frame.size.width-40, 0, 40, 40);
        if (isSeen) {
            closed.image = [UIImage imageNamed:@"eye_closed.png"];
            winkTimeLabel.text = @"0";
        }
        else {
            closed.image = [UIImage imageNamed:@"eye_opened.png"];
            winkTimeLabel.text = [NSString stringWithFormat:@"%d", [[TimeBomb sharedInstance] getMessageTimeout:self.server_id]];
        }
    }
    else {
        if (closed != nil)
            [closed removeFromSuperview];

        if (winkTimeLabel != nil)
            [winkTimeLabel removeFromSuperview];
    }
}

- (void) setMessageStatus:(int)status delivered:(BOOL)delivered {
    
    [super setMessageStatus:status delivered:delivered];
    
    if (delivered)
        isSeen = YES;
    else
        isSeen = NO;
    
    if (status == MSG_STATUS_PENDING || status == MSG_STATUS_IMAGE_UPLOADING) {
        self.progressUploadingVideo.hidden = NO;
        self.progressUploadingVideo.progress = 0.0f;

        self.progressUploadingVideo.backgroundColor = [UIColor clearColor];
        self.progressUploadingVideo.preferredFrameHeight = 16.0f;
        self.progressUploadingVideo.innerColor = [UIColor blueColor];
        self.progressUploadingVideo.outerColor = [UIColor clearColor];
        self.progressUploadingVideo.emptyColor = [UIColor lightGrayColor];
    }
    else {
        self.progressUploadingVideo.hidden = YES;
    }
}

- (void) setButton:(int) status {
    
    [btnOperation removeTarget:self action:@selector(onCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnOperation removeTarget:self action:@selector(onResendClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (status == MSG_STATUS_PENDING || status == MSG_STATUS_IMAGE_UPLOADING) {
        //sending -> show cancel
        btnOperation.hidden = NO;
        [btnOperation setTitle:@"" forState:UIControlStateNormal];
        [btnOperation setBackgroundImage:[UIImage imageNamed:@"btn_cancel.png"] forState:UIControlStateNormal];
        [btnOperation addTarget:self action:@selector(onCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (status == MSG_STATUS_SENT || status == MSG_STATUS_CONFIRMED) {
        //sent
        btnOperation.hidden = YES;
    }
    else if (status == MSG_STATUS_SEND_FAILED || status == MSG_STATUS_SEND_CANCELLED) {
        //failed -> re-upload
        btnOperation.hidden = NO;
        [btnOperation setTitle:@"" forState:UIControlStateNormal];
        [btnOperation setBackgroundImage:[UIImage imageNamed:@"btn_reload.png"] forState:UIControlStateNormal];
        [btnOperation addTarget:self action:@selector(onResendClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void) onCancelClicked:(UIButton*)sender {
    
    [[JabberLayer SharedInstance] cancelUpload:_server_id];
    
    if (_cell_delegate && [_cell_delegate respondsToSelector:@selector(refreshTable:)]) {
        [_cell_delegate performSelector:@selector(refreshTable:) withObject:_server_id];
    }
}

- (void) onResendClicked:(UIButton*)sender {
    if (_cell_delegate && [_cell_delegate respondsToSelector:@selector(didClickImage:)]) {
        [_cell_delegate performSelector:@selector(resendMedia:) withObject:_server_id];
    }
}

- (CGSize) getCommentTextSize:(NSString*) comment width:(int)width {
    
    if (comment == nil || comment.length == 0)
        return CGSizeZero;
    
    CGSize theSize = [CommonUtil getAttributedLabelSize:comment font:self.labelComment.font constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    return theSize;
}

#pragma mark Button Delegate
- (IBAction)click:(id)sender {
    
    if (_cell_delegate && [_cell_delegate respondsToSelector:@selector(didClickImage:)]) {
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:_server_id, @"server_id",
                              [NSNumber numberWithBool:_is_owner], @"owner", [NSNumber numberWithInt:message_type], @"type", nil];
        [_cell_delegate performSelector:@selector(didClickImage:) withObject:dict];
    }
    
}

#pragma mark -
#pragma mark TTTAttributedLabelDelegate methods

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    if (url != nil)
        [[UIApplication sharedApplication] openURL:url];
}

- (void) onChangedUploadingProgress:(NSNotification*) notification {

    NSDictionary* dict = notification.object;
    NSString* msgid = [dict objectForKey:@"local_id"];
    if ([_server_id isEqualToString:msgid]) {
        float value = [[dict objectForKey:@"progress"] floatValue];
        [self.progressUploadingVideo setProgress:value];
    }
}

- (void) setMediaSize:(int)size {
    
    media_size = size;
}

- (void) setMessageType:(int)type {
    
    message_type = type;
}

@end
