//
//  ChatRoomStatusCell.h
//  speedsip
//
//  Created by denebtech on 02.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRoomStatusCell : UITableViewCell<UITextFieldDelegate>  {
    
    IBOutlet UITextField * editView;
}

- (void)updateStatus;
- (void)setEditing:(BOOL)editing;

@end
