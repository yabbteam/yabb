//
//  ChatBoardViewController.h
//  speedsip
//
//  Created by denebtech on 14.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatBoardTable.h"
#import "EmotionsView.h"
#import "ImagePickerViewController.h"
#import "ChatTextView.h"
#import "DeleteButton.h"
#import "TypingView.h"
#import "CustomBadge.h"
#import "BaseViewController.h"
#import "PeoplePickerViewController.h"
#import <WSAssetPicker.h>
#import "PhotoWinkViewController.h"
#import "InputPhotoInfoViewController.h"
#import "VPImageCropperViewController.h"
#import <AddressBookUI/ABPeoplePickerNavigationController.h>
#import <AddressBookUI/ABNewPersonViewController.h>
#import "CTAssetsPickerController.h"

#define MAX_DIMENSION   800.0f
#define RAD(a)          (a * M_PI/180.0f)


#define BOTTOM_EMPTY    0
#define BOTTOM_KEYBOARD 1
#define BOTTOM_EMOTIONS 2

@protocol ChatRoomsViewControllerDelegate;


@interface ChatBoardViewController : BaseViewController <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, PeoplePickerDelegate, EmotionsDelegate, ChatBoardDelegate, ChatRoomsViewControllerDelegate, UIActionSheetDelegate, UIScrollViewDelegate, WSAssetPickerControllerDelegate, PhotoWinkViewControllerDelegate, InputPhotoInfoViewControllerDelegate, VPImageCropperDelegate, UIAlertViewDelegate, ABPeoplePickerNavigationControllerDelegate,
    ABNewPersonViewControllerDelegate, CTAssetsPickerControllerDelegate> {
    
    IBOutlet ChatBoardTable * board_table;
    IBOutlet EmotionsView * emotions_view;
    IBOutlet UIView * tool_view;
    
    IBOutlet UIButton * bar_button;
    IBOutlet UIImageView * bar_button_image;
    IBOutlet ChatTextView * chat_text_view;
    
    IBOutlet UIToolbar * delete_toolbar;
    IBOutlet DeleteButton * delete_button;
    IBOutlet TypingView * typing_view;
    IBOutlet UIView * bottom_view;
    IBOutlet UIImageView * background_image;
    
    NSString* chat_id;
    NSString* _send_to_chat;
    
    CustomBadge * custom_badge;
    
    int bottom_type;
    
    BOOL view_is_up;
    
    CGFloat keyboardHeight;
    CGFloat prevKeyboardY;
    NSMutableDictionary*   _dictForward;
    
    BOOL    isAppear;
    int    _orientation;
    BOOL    _bottom_type;
    BOOL    _isGroupChat;
    
    CGRect  _myRect;
    CGRect  _tableRect;
    
    NSMutableArray*     arrayAboutUploadImages;
    
    NSTimeInterval      lastTimeSentComposing;
    
    NSArray*            backupRightItems;
    
    NSMutableDictionary*    popups;
    
    BOOL                disableAutoRotate;
    BOOL                manualViewOperation;
    
    NSString*           selectPhotoTargetPhoneNo;
    NSTimer*            seenTimer;
}

- (void) setChatId:(NSString*)value;
- (NSString*) getChatId;
- (NSString*) Pick_Image:(UIImage *)image message:(NSString*)message assetReference:(NSString *)asset isWink:(BOOL)isWink;
- (void) Pick_Image:(UIImage *)image assetReference:(NSString *)asset;
- (void) setTyping:(NSString*)typing_chat_id withString:(NSString *)str;
- (void) clearTyping;
- (void) forwardProcess;
- (void) forwardCancel;
- (void) resendMedia:(NSString*) msgId;
- (void) onNewMessageReceived:(NSString*) msgId;
- (void) gotoOtherRoom:(NSString*) chatid;
- (void) onInitialUpdate;

@property (nonatomic, strong) ChatBoardTable * board_table;
@property (nonatomic, strong) NSString* send_to_chat;
@property (nonatomic, strong) NSMutableDictionary*   dictForward;
@property (nonatomic, assign) BOOL    isGroupChat;
@property (nonatomic, strong) ALAssetsLibrary*  assetsLibrary;

@property (nonatomic, strong) NSDictionary *roomInfo;
@property (nonatomic, strong) PhotoWinkViewController* photoWinkController;
@property (nonatomic, strong) InputPhotoInfoViewController* inputPhotoInfoController;
@property (nonatomic, assign) BOOL shouldShowKeyboard;

@end
