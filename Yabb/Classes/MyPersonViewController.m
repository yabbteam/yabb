//
//  MyPersonViewController.m
//  speedsip
//
//  Created by denebtech on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyPersonViewController.h"
#import "AddressCollector.h"
#import "PersonNameCell.h"
#import "PhoneCell.h"
#import "AppDelegate.h"
#import "Friends.h"
#import <QuartzCore/QuartzCore.h>
#import "InvitedFriends.h"
#import "Settings.h"
#import "AvatarManager.h"
#import "JabberLayer.h"

@interface MyPersonViewController ()
{
    UIColor*    _colorRawNavigationBar;
}

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyPersonViewController {
    
    NSString* __name1;
    NSString* __name2;
    NSString* __nickname;
    NSString* __email;
    NSArray* __phones;
    BOOL    __isFriend;
    UIImage* __photo;
}

@synthesize person_id;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeApperance];
}

- (void)customizeApperance
{
//    [self.tableView setBackgroundView:nil];
//    [self.tableView setBackgroundColor:DEFAULT_BACK_COLOR];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

#pragma mark -
#pragma mark Invate Friend

- (void) invateSMS {
    
    // Show phone selectors (for others)
    if (__phones && [__phones count]) {
        
        UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Invite friend", @"Invite friend")
                                                              delegate:self 
                                                     cancelButtonTitle:nil 
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:nil];
        
        for (NSDictionary * phone in __phones)
            [actSheet addButtonWithTitle:[phone objectForKey:@"number"]];                    
        
        int cancel_index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
        [actSheet setCancelButtonIndex:cancel_index];
        
        [actSheet showInView:[self.view window]];
    }

    
}

- (void) invateEmail {
    
    if (__email == nil)
        return;
    
    NSString *subj = NSLocalizedString(@"Look at iPhone Yabb", @"Look at iPhone Yabb");
    NSString *body = NSLocalizedString(@"Hey mate!\nHow are you doing? See what I've found and want to share with you - Yabb - it is free, overseas calls are cheap. Try it!\n\n", @"Hey mate!");
    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:?subject=%@&body=%@",
    //                                                        [subj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
    //                                                        [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[NSArray arrayWithObject:__email]];
    [controller setSubject:subj];
    [controller setMessageBody:body isHTML:NO]; 
    if (controller)
        [self presentViewController:controller animated:YES completion:^{}];
}

#pragma mark -
#pragma mark Table Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 1) {   //  *** Make a call ***
        
        /*
        NSArray * phones = [[AddressCollector sharedCollector] Get_Localized_Phones:[person_id intValue] ];
        if (phones && [phones count]) {
            
            NSString * phone = [[phones objectAtIndex:indexPath.row] objectForKey:@"number"];
            
            [app.dialNavController.DisplayNumber setText:[[AddressCollector sharedCollector] clearedPhoneNumberLeft:phone] ];
            self.navigationController.tabBarController.selectedIndex = 0;
        }*/
        
    }
    else if (indexPath.section == 2) {

//        BOOL is_friend = [[AddressCollector sharedCollector] isMyFriend:[person_id intValue]];
        BOOL is_friend = __isFriend;
        
        if (is_friend && [person_id intValue] < [[AddressCollector sharedCollector] Num_Of_Contacts]) {        // *** Send a message to friend ***
            
            // Quick chat button (for friends)
#if APP_TARGET_Yabb_Dialer
//            [self.navigationController.tabBarController setSelectedIndex:1];
#else
//            [self.navigationController.tabBarController setSelectedIndex:0];
#endif
            
            ChatRoomsViewController * chatRoomsViewController = nil;
//#if APP_TARGET_Yabb_Dialer
            chatRoomsViewController = [((AppDelegate *)[[UIApplication sharedApplication] delegate]) chatRoomsViewController];
//#else
//            chatRoomsViewController = [self.navigationController.tabBarController.viewControllers objectAtIndex:0];
//#endif
            if (chatRoomsViewController && [chatRoomsViewController respondsToSelector:@selector(Send_Private_Message:withKeyboard:)])
                [chatRoomsViewController Send_Private_Message:[person_id intValue] withKeyboard:YES];
        }
        else {                  //  *** Invate friend ***
            
            
            if (indexPath.row == 0) {
                
                if (__phones != nil && [__phones count])
                    [self invateSMS];
                else
                    [self invateEmail];
            }
            else
                [self invateEmail];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        int height = 0;
        
        NSString * text_1 = __name1;
        CGRect rect1 = [[AddressCollector sharedCollector] rectForText:text_1 inFrame:CGRectMake(0, 0, 200, 21) withFont:[UIFont boldSystemFontOfSize:17.0]];
        height += rect1.size.height;

        NSString * text_2 = __name2;
        CGRect rect2 = [[AddressCollector sharedCollector] rectForText:text_2 inFrame:CGRectMake(0, 0, 200, 21) withFont:[UIFont systemFontOfSize:13.0]];
        height += rect2.size.height;

        return (height > 76)?height:76;
    }
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    [tableView setBackgroundColor:DEFAULT_BACK_COLOR];
    [tableView setSeparatorColor:[UIColor lightGrayColor]];

    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    BOOL is_friend = __isFriend;

    if (section == 1) {
        
        return __phones?[__phones count]:0;
    }
    else if (section == 2) {
        
        int num_of_rows = 0;
        
        if (__phones != nil && [__phones count])
            num_of_rows++;
        if (__email != nil)
            num_of_rows++;
        
        return is_friend?1:num_of_rows;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int row = indexPath.row, section = indexPath.section;
    
    switch (section) {
            
        // Person name
        case 0: {
            
            PersonNameCell * person_cell = [tableView dequeueReusableCellWithIdentifier:@"PersonNameCellID"];
            if (person_cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PersonNameCell" owner:self options:nil];
                person_cell = (PersonNameCell *)[nib objectAtIndex:0];
                
                UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTapPhoto:)];
                [person_cell.person_image addGestureRecognizer:tapGestureRecognizer];
                person_cell.person_image.userInteractionEnabled = YES;
            }
            
            person_cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];

            UIImage * avatar = __photo;
            
            [person_cell.person_image setImage:avatar?avatar:[UIImage imageNamed:@"placeholderPerson_52.png"]];

            if (avatar) {
                [person_cell.image_back.layer setMasksToBounds:YES];
                [person_cell.image_back.layer setCornerRadius:4.0];
                person_cell.image_back.layer.backgroundColor = [UIColor darkGrayColor].CGColor;
            }
            else {
                [person_cell.image_back.layer setMasksToBounds:NO];
                person_cell.image_back.layer.backgroundColor = [UIColor clearColor].CGColor;
            }
            
            NSString * text_1 = __name1;
            NSString * text_2 = __name2;
            NSString * text_3 = __nickname;
            
            [person_cell.person_name_1 setText:text_1];
            [person_cell.person_name_1 setFrame:[[AddressCollector sharedCollector] rectForText:text_1 inFrame:person_cell.person_name_1.frame withFont:person_cell.person_name_1.font]];
            
            CGRect rect = person_cell.person_name_2.frame;
            rect.origin.y = person_cell.person_name_1.frame.origin.y + person_cell.person_name_1.frame.size.height;
            
            [person_cell.person_name_2 setText:text_2];
            [person_cell.person_name_2 setFrame:rect];
            [person_cell.person_name_2 setFrame:[[AddressCollector sharedCollector] rectForText:text_2 inFrame:person_cell.person_name_2.frame withFont:person_cell.person_name_2.font]];
            
            if (text_3 != nil && [text_3 length] > 0) {
                [person_cell.person_nickname setText:[NSString stringWithFormat:@"(%@)", text_3]];
                if (text_2 != nil && [text_2 length] > 0) {
                    CGRect rect = person_cell.person_nickname.frame;
                    rect.origin.y = person_cell.person_name_2.frame.origin.y + person_cell.person_name_2.frame.size.height;
                    [person_cell.person_nickname setFrame:rect];
                }
                else {
                    rect.origin.y += 5;
                    [person_cell.person_nickname setFrame:rect];
                }
                [person_cell.person_nickname setFrame:[[AddressCollector sharedCollector] rectForText:text_3 inFrame:person_cell.person_nickname.frame withFont:person_cell.person_nickname.font]];
            }
            
            [person_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return person_cell;
        }
        break;
            
        case 1: {
            
            PhoneCell * phone_cell = [tableView dequeueReusableCellWithIdentifier:@"PhoneCellID"];
            if (phone_cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PhoneCell" owner:self options:nil];
                phone_cell = (PhoneCell *)[nib objectAtIndex:0];
            }
            
            NSDictionary * phone = [__phones objectAtIndex:indexPath.row];
            
            CFStringRef label = ABAddressBookCopyLocalizedLabel((__bridge CFStringRef)[phone objectForKey:@"name"]);
            [phone_cell.phone_name setText:(__bridge NSString *)label];
            [phone_cell.phone_value setText:[phone objectForKey:@"number"]];
            
            CFRelease(label);
            
            return phone_cell;
        }
        break;
            
        case 2:
        {
            
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewStyleGrouped reuseIdentifier:@"CellID"];
            
            UILabel * label = (UILabel *)[cell viewWithTag:10];
            if (label == nil) {
                label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 44)];
                [cell addSubview:label];
                [label setTag:10];
                [label setBackgroundColor:[UIColor clearColor]];
                [label setTextAlignment:UITextAlignmentCenter];
            }
            
            BOOL is_friend = __isFriend;

            if (is_friend)
                [label setText:NSLocalizedString(@"Send free message", @"Send free message")];
            else {
            
                if (row == 0) {
                    if (__phones != nil && [__phones count])
                        [label setText:NSLocalizedString(@"Invite by SMS", @"Invite by SMS")];
                    else
                        [label setText:NSLocalizedString(@"Invite by email", @"Invite by email")];
                }
                else
                    [label setText:NSLocalizedString(@"Invite by email", @"Invite by email")];
                
            }
            
            return cell;
        }
            break;

            
        default:
            break;
    }
    
    
    return nil;
}

#pragma mark -
#pragma mark Action Sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        _colorRawNavigationBar = [UINavigationBar appearance].barTintColor;
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    }
    
    [InvitedFriends sendInAppSMS:[[__phones objectAtIndex:buttonIndex] objectForKey:@"number"] controller:self delegate:self];
}

#pragma mark -
#pragma mark SMS delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:^{
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [[UINavigationBar appearance] setBarTintColor:_colorRawNavigationBar];
        }
    }];
    return;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller  
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [[UINavigationBar appearance] setBarTintColor:_colorRawNavigationBar];
        }
    }];
}

- (void) userDidTapPhoto:(UITapGestureRecognizer*) recoginzer {
    
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    
    CGRect rt = window.bounds;
    UIView* view = [[UIView alloc] initWithFrame:rt];
    view.backgroundColor = [UIColor blackColor];
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:rt];
    UIImage * avatar = __photo;
    if (avatar == nil)
        return;
        
    imageView.image = avatar;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [view addSubview:imageView];
    [window addSubview:view];
    
    fullImageView = view;

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTapFullPhoto:)];
    [view addGestureRecognizer:tapGestureRecognizer];
    
    view.alpha = 0.0f;
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                         }
                     }];
}

- (void) userDidTapFullPhoto:(UITapGestureRecognizer*) recoginzer {
    
    if (fullImageView) {
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             fullImageView.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 [fullImageView removeFromSuperview];
                                 fullImageView = nil;
                             }
                         }];
    }
}

- (void) onHideDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {

}

- (void) setPerson_id:(NSNumber *)_person_id {

    person_id = _person_id;
    
    int index = [_person_id intValue];

    __name1 = [[AddressCollector sharedCollector] Get_Person_Name_1:index];
    if (__name1 != nil)
        __name1 = [[NSString alloc] initWithString:__name1];
    
    __name2 = [[AddressCollector sharedCollector] Get_Person_Name_2:index];
    if (__name2 != nil)
        __name2 = [[NSString alloc] initWithString:__name2];
    
    __email = [[AddressCollector sharedCollector] Get_Email:index];
    if (__email != nil)
        __email = [[NSString alloc] initWithString:__email];
    
    __phones = [[AddressCollector sharedCollector] Get_Localized_Phones:index];
    if (__phones != nil)
        __phones = [[NSArray alloc] initWithArray:__phones];
    
    __photo = [[AvatarManager sharedInstance] getAvatarWithAddressCollecterIndex:index];
    if (__photo != nil)
        __photo = [[UIImage alloc] initWithCGImage:__photo.CGImage];
    
    __isFriend = [[Friends SharedFriends] isFriendIndex:index];
    
    __nickname = nil;
    NSString* friendPhone = [[Friends SharedFriends] friendPhoneByIndex:index];
    if (friendPhone != nil) {
        __nickname = [[JabberLayer SharedInstance] getNickNameByPhone:friendPhone];
    }
}

@end
