//
//  RegisterSecondController.h
//  Yabb
//
//  Created by admin on 21.06.13.
//
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface RegisterSecondController : BaseViewController {
    
    IBOutlet UILabel * pin_label;
    IBOutlet UITextField * pin_text;
    IBOutlet UIButton * pin_button;
    IBOutlet UIActivityIndicatorView * pin_activity;    

    NSTimer * timer;
    int retry_counter;
    IBOutlet UILabel * retry_label;
    IBOutlet UIButton * resend_button;
    IBOutlet UIButton * call_button;
    
    IBOutlet UIActivityIndicatorView* progressActivate;

}

@property (nonatomic, strong) NSString * phone_number_full;
@property (nonatomic, strong) NSString * phone_country_abbr;

@end
