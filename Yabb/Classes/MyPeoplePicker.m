//
//  MyPeoplePicker.m
//  speedsip
//
//  Created by denebtech on 04.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyPeoplePicker.h"
#import "MyPersonViewController.h"

@interface MyPeoplePicker ()

@end

@implementation MyPeoplePicker

@synthesize people_picker;

- (id) initWithRootViewController:(UIViewController *)rootViewController {

    self = [super initWithRootViewController:rootViewController];
    
    if (self) {
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark People Picker Delegate

- (void) didSelectPerson:(int)index fromViewController:(UIViewController *)controller
{
    MyPersonViewController * view_controller = [[MyPersonViewController alloc] initWithNibName:@"MyPersonViewController" bundle:nil];
    [view_controller setPerson_id:[NSNumber numberWithInt:index]];
    [self pushViewController:view_controller animated:YES];
}

@end
