#!/bin/sh

#  Automatic build script for pjsip 
#  for iPhoneOS and iPhoneSimulator
#
#  Copyright (C) 2011 Samuel <samuelv0304@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
###########################################################################
# Install this script into a directory and define PJPROJECTDIR variable.  #
# You should not put this script directly into pjproject directory.       #
###########################################################################
#  Change values here													  #
#																		  #
PJPROJECTDIR=../
#export DEVPATH=/Developer4/Platforms/iPhoneOS.platform/Developer          #
#export DEVPATH=/Developer/Platforms/iPhoneOS.platform/Developer          #
#export IPHONESDK=iPhoneOS4.1.sdk                                         #
#export IPHONEOS_DEPLOYMENT_TARGET=4.1                                     #
#																		  #
###########################################################################
#																		  #
# Don't change anything under this line!								  #
#																		  #
###########################################################################

CURRENTPATH=`pwd`

set -e

cd ${PJPROJECTDIR}

echo "#define PJ_CONFIG_IPHONE 1" > pjlib/include/pj/config_site.h
echo "#include <pj/config_site_sample.h>" >> pjlib/include/pj/config_site.h
cat pjlib/include/pj/config_site.h

############
# iPhone Simulator
#echo "Building pjproject for iPhoneSimulator i386"
#echo "Please stand by..."
#
#mkdir -p "${CURRENTPATH}/iPhoneSimulator.sdk"
#
#LOG="${CURRENTPATH}/iPhoneSimulator.sdk/build-pjproject-i386.log"
#
#make >> "${LOG}" 2>&1
#make install >> "${LOG}" 2>&1
#make clean >> "${LOG}" 2>&1
#make distclean >> "${LOG}" 2>&1
#############

#############
# iPhoneOS armv7s
echo "Building pjproject for iPhoneOS armv7s"
echo "Please stand by..."

export ARCH="-arch armv7s"

mkdir -p "${CURRENTPATH}/iPhoneOS-armv7s.sdk"

LOG="${CURRENTPATH}/iPhoneOS-armv7s.sdk/build-pjproject-armv7s.log"

echo "Configuring..."
./configure-iphone --prefix="${CURRENTPATH}/iPhoneOS-armv7s.sdk" >> "${LOG}" 2>&1

echo "Compiling..."
make >> "${LOG}" 2>&1
echo "Installing..."
make install >> "${LOG}" 2>&1
echo "Cleaning..."
make clean >> "${LOG}" 2>&1
#make distclean >> "${LOG}" 2>&1
#############

#############
# iPhoneOS armv7
echo "Building pjproject for iPhoneOS armv7"
echo "Please stand by..."

export ARCH="-arch armv7"

mkdir -p "${CURRENTPATH}/iPhoneOS-armv7.sdk"

LOG="${CURRENTPATH}/iPhoneOS-armv7.sdk/build-pjproject-armv7.log"

echo "Configuring..."
./configure-iphone --prefix="${CURRENTPATH}/iPhoneOS-armv7.sdk" >> "${LOG}" 2>&1

echo "Compiling..."
make >> "${LOG}" 2>&1
echo "Installing..."
make install >> "${LOG}" 2>&1
echo "Cleaning..."
make clean >> "${LOG}" 2>&1
#make distclean >> "${LOG}" 2>&1
#############

#############
# universal lib
echo "Build library..."

mkdir -p "${CURRENTPATH}/lib"

for libpath in `ls -d ${CURRENTPATH}/iPhoneOS-armv7s.sdk/lib/lib*.a`; do
	libname=`basename ${libpath}`
	#lipo -create ${CURRENTPATH}/iPhoneOS-armv6.sdk/lib/${libname} ${CURRENTPATH}/iPhoneOS-armv7.sdk/lib/${libname} ${CURRENTPATH}/iPhoneSimulator.sdk/lib/ ${libname} -output ${CURRENTPATH}/lib/${libname}
	lipo -create ${CURRENTPATH}/iPhoneOS-armv7s.sdk/lib/${libname} ${CURRENTPATH}/iPhoneOS-armv7.sdk/lib/${libname} -output ${CURRENTPATH}/lib/${libname}
done

mkdir -p ${CURRENTPATH}/include/pjproject
cp -R ${CURRENTPATH}/iPhoneOS-armv7s.sdk/include/* ${CURRENTPATH}/include/pjproject

echo "Building done."
echo "Cleaning up..."
#rm -rf ${CURRENTPATH}/iPhoneOS-armv?.sdk
#rm -rf ${CURRENTPATH}/iPhoneOS-armv?.sdk/lib
#sdg#rm -rf ${CURRENTPATH}/iPhoneOS-armv?.sdk/include
echo "Done."
