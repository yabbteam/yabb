//
//  ChatRoomCell.h
//  speedsip
//
//  Created by denebtech on 18.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRoomCell : UITableViewCell {
    
    IBOutlet UIView * main_view;
    IBOutlet UILabel * room_name;
    IBOutlet UIImageView * room_type;
    IBOutlet UILabel * user_status;
    IBOutlet UILabel * last_time;
}

- (void) setRoomName:(NSString *)name;
- (void) setRoomType:(int)type;

- (void) setPhoto:(NSArray *)phones;
- (void) setLastMessage:(NSString*)cid;

- (void) setRoomPhoto:(UIImage*)photo;

@end
