//
//  TabBarViewController.m
//  speedsip
//
//  Created by denebtech on 01.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotate {
    
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    
#if 0
    id selected = self.selectedViewController;
    
    if ([selected isKindOfClass:[UINavigationController class]]) {
        
        UIViewController * sc = ((UINavigationController *)selected).visibleViewController;
        
        if (sc.nibName != nil && [sc.nibName isEqual:@"ImageDownloadViewController"]) {
            
            return UIInterfaceOrientationMaskAll;
        }
        
        NSLog(@"%@", sc.nibName);
        
    }
#endif
    
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

#if 0
    id selected = self.selectedViewController;
    
    if ([selected isKindOfClass:[UINavigationController class]]) {
        
        UIViewController * sc = ((UINavigationController *)selected).visibleViewController;
        
        if (sc.nibName != nil && [sc.nibName isEqual:@"ImageDownloadViewController"]) {
            
            return YES;
        }
        
        NSLog(@"%@", sc.nibName);
        
    }
#endif

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
