//
//  RegisterSecondController.m
//  Yabb
//
//  Created by admin on 21.06.13.
//
//

#import "RegisterSecondController.h"
#import "ConnectionQueue.h"
#import "Alert.h"
#import "Notifications.h"
#import "AddressCollector.h"
#import "ConnectionQueue.h"
#import "AppDelegate.h"
#import "PhotoEditViewController.h"

@interface RegisterSecondController ()

@end

@implementation RegisterSecondController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    progressActivate.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
//    [self Show_Pin:YES];
    
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:@"PIN code had been sent for you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
//    [alert show];
//    [alert release];
    
    [pin_text becomeFirstResponder];
    [self Start_Retry_Timer];
    
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(IBAction)PinTextFieldDidChange:(id)sender {
    NSString *str = pin_text.text;
    if (str && [str length] >= 4) {
        [self Button_Activate:self];
    }
}

- (void) Process_Retry_Timer {
    
    
    [retry_label setText:[NSString stringWithFormat:NSLocalizedString(@"Retry (%d sec)", @"Retry (%d sec)"), retry_counter]];
    retry_counter--;
    
    if (retry_counter < 0) {
        [self Stop_Retry_Timer];
        
        [retry_label setHidden:YES];
        [resend_button setEnabled:YES];
        [call_button setEnabled:YES];
    }
    
}
 - (void) Start_Retry_Timer {
 
 
     retry_counter = 60;
 
    [retry_label setHidden:NO];
    [call_button setEnabled:NO];
     [resend_button setEnabled:NO];

    [self Process_Retry_Timer];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(Process_Retry_Timer) userInfo:nil repeats:YES];
 
 }

- (void) Stop_Retry_Timer {
    
    if (timer) {
        
        [timer invalidate];
        timer = nil;
        
    }
    
}

- (IBAction) Button_Activate:(id)sender {
 
    //    NSString * phone = phone_text.text;
    NSString * pin_code = pin_text.text;

    if (pin_code == nil || [pin_code length] != 4)
    return;

    //    NSString * udid = [[UIDevice currentDevice].uniqueIdentifier stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"register", @"cmd", self.phone_number_full, @"phone", pin_code, @"code",
                             self.phone_country_abbr, @"ccode", nil];

    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"activate_pin_code" silent:NO];

    self.view.userInteractionEnabled = NO;
    progressActivate.hidden = NO;
    [progressActivate startAnimating];
}

- (IBAction) Button_Resend_SMS:(id)sender {

    [self Start_Retry_Timer];
    
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"register", @"cmd", self.phone_number_full, @"phone",
                             self.phone_country_abbr, @"ccode", nil];
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"register" silent:NO];

    
}

- (IBAction) Button_Call_Me_Baby_May_Be:(id)sender {
    
    [self Start_Retry_Timer];

    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"callmeback", @"cmd", self.phone_number_full, @"phone", nil];
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"call_me_back" silent:NO];
}

- (void) Connection_Done_Activate:(NSDictionary *)dict {
    
    //    [self Show_Pin:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"pass"] forKey:@"user_pass"];
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"phone"] forKey:@"user_phone"];
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"pin"] forKey:@"user_pin"];
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"id"] forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[Notifications sharedNotifications] setNotif_delegate:self];
    [[Notifications sharedNotifications] Register];

    PhotoEditViewController* photoEditViewController = [[PhotoEditViewController alloc] initWithNibName:@"PhotoEditViewController" bundle:nil];
    photoEditViewController.parentController = self;
    [self.navigationController pushViewController:photoEditViewController animated:YES];
    
//    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark -
#pragma Connection Delegates
- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    NSLog(@"Register connection done");
    
    self.view.userInteractionEnabled = YES;
    progressActivate.hidden = YES;
    
    int error_code = [[dict objectForKey:@"errorcode"] intValue];
    NSDictionary * data = [dict objectForKey:@"data"];
    
    
    if (error_code != 2) {
        
        [Alert show:NSLocalizedString(@"Error", @"Error") withMessage:[dict objectForKey:@"error"]];
        
        return;
    }
    
    if ([connection_name isEqualToString:@"activate_pin_code"])
        [self Connection_Done_Activate:data];
//    else if ([connection_name isEqualToString:@"call_me_back"]) {
//    }
    
}

- (void) didNotificationDone {
    
    [[ConnectionQueue sharedQueue] startConnectionCycle:app.chatRoomsViewController withPeriod:1.0];
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    NSLog(@"Register connection failed");
    
    self.view.userInteractionEnabled = YES;
    progressActivate.hidden = YES;
    
    //    if ([connection_name isEqualToString:@"activate_pin_code"])
    //        [self Enable_Pin:YES];
    
}

@end
