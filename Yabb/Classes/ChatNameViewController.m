//
//  ChatNameViewController.m
//  Baycall
//
//  Created by denebtech on 23.12.12.
//
//

#import "ChatNameViewController.h"

@interface ChatNameViewController ()

@end

@implementation ChatNameViewController


@synthesize chat_name_delegate = _chat_name_delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.title = @"Group name";
        
        UIBarButtonItem * done_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(Button_Done:)];
        self.navigationItem.rightBarButtonItem = done_button;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) Button_Done:(id)sender {
    
    if (_chat_name_delegate && [_chat_name_delegate respondsToSelector:@selector(renameRoom:)])
        [_chat_name_delegate renameRoom:text_field.text];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table Delegates

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    
        CGRect rect = cell.frame;
        
        rect.origin.x = 20;
        rect.origin.y = 12;
        rect.size.width -= 40;
        rect.size.height -= 22;
        
        text_field = [[UITextField alloc] initWithFrame:rect];
        [text_field setBorderStyle:UITextBorderStyleNone];
        [text_field setClearButtonMode:UITextFieldViewModeAlways];
        
        if (_chat_name_delegate && [_chat_name_delegate respondsToSelector:@selector(getRoomName)])
            [text_field setText:[_chat_name_delegate getRoomName]];
        
        [cell addSubview:text_field];
        
        [text_field becomeFirstResponder];
        
    }
    
    return cell;
}

@end
