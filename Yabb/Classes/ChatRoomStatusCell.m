//
//  ChatRoomStatusCell.m
//  speedsip
//
//  Created by denebtech on 02.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatRoomStatusCell.h"
#import "ChatStatusViewController.h"
#import "Friends.h"
#import <FontasticIcons/FontasticIcons.h>

@implementation ChatRoomStatusCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateStatus {
    
    if (![editView isFirstResponder]) {
    
        NSString * user_status = [ChatStatusViewController getStatus];
        
        [editView setText:user_status];
        
        [self setEditing:NO];
    }
}

- (void)setEditing:(BOOL)editing {
    
    if (editing) {
        editView.hidden = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        if (![editView isFirstResponder]) {
            [editView becomeFirstResponder];
        }
    }
    else {
        editView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
        if ([editView isFirstResponder]) {
            [editView resignFirstResponder];
        }
    }
    
    editView.delegate = self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > 20) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.text) {
        [ChatStatusViewController setStatus:textField.text];
        [ChatStatusViewController sendUserStatus];
    }
    
    [self setEditing:NO];
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self setEditing:YES];
}

@end
