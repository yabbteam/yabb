//
//  ImagePickerViewController.h
//  speedsip
//
//  Created by denebtech on 29.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerViewController : UIImagePickerController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) id image_picker_delegate;
@property (nonatomic, strong) UIImage* result;
@property (nonatomic, strong) NSString* resultMoviePath;

- (id) initWithType:(UIImagePickerControllerSourceType)type;
- (id) initWithPhotoMovie;

@end
