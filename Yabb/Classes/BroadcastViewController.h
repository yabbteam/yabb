//
//  BroadcastViewController.h
//  Baycall
//
//  Created by denebtech on 26.02.13.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BroadcastViewController : BaseViewController {
    
    IBOutlet UITextView * text_view;

}

@property (nonatomic, strong) NSArray * array;

@end
