//
//  RegisterViewController.h
//  speedsip
//
//  Created by denebtech on 16.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryPickerViewController.h"
#import "BaseViewController.h"

#define PHONE_DIGITS_MIN    5
#define PHONE_DIGITS_MAX    20

@interface RegisterViewController : BaseViewController <CountryPickerViewControllerDelegate, UITextFieldDelegate> {
    
    IBOutlet UITextField * phone_text;
    IBOutlet UIButton * phone_button;
    IBOutlet UIActivityIndicatorView * phone_activity;
    IBOutlet UIButton* country_button;
    IBOutlet UILabel* explain;
    IBOutlet UIView* contentview;
    IBOutlet UITextField* labelCountry;
    IBOutlet UITextField* labelCCode;
    
    int selected_country;
    int selected_section;
    NSString * country_name;
    NSString * country_code;
    NSString * country_abbr;

    NSMutableString * phone_number_full;
    
    CGFloat     prevOffset;
    CGFloat     keyboardHeight;
}


+ (bool) isActivated;
- (void) clearSelection;

@end
