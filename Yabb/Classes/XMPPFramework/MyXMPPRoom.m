#import "MyXMPPRoom.h"
#import <XMPPIDTracker.h>

@implementation MyXMPPRoom

enum XMPPRoomState
{
	kXMPPRoomStateNone        = 0,
	kXMPPRoomStateCreated     = 1 << 1,
	kXMPPRoomStateJoining     = 1 << 3,
	kXMPPRoomStateJoined      = 1 << 4,
	kXMPPRoomStateLeaving     = 1 << 5,
};

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
	// This method is invoked on the moduleQueue.
	
	XMPPJID *from = [presence from];
	
	if (![roomJID isEqualToJID:from options:XMPPJIDCompareBare])
	{
		return; // Stanza isn't for our room
	}
	
	[xmppRoomStorage handlePresence:presence room:self];
	
	// My presence:
	// 
	// <presence from='coven@chat.shakespeare.lit/thirdwitch'>
	//   <x xmlns='http://jabber.org/protocol/muc#user'>
	//     <item affiliation='member' role='participant'/>
	//     <status code='110'/>
	//     <status code='210'/>
	//   </x>
	// </presence>
	// 
	// 
	// Another's presence:
	// 
	// <presence from='coven@chat.shakespeare.lit/firstwitch'>
	//   <x xmlns='http://jabber.org/protocol/muc#user'>
	//     <item affiliation='owner' role='moderator'/>
	//   </x>
	// </presence>
	
	NSXMLElement *x = [presence elementForName:@"x" xmlns:XMPPMUCUserNamespace];
	
	// Process status codes.
	// 
	// 110 - Inform user that presence refers to one of its own room occupants.
	// 201 - Inform user that a new room has been created.
	// 210 - Inform user that service has assigned or modified occupant's roomnick.
	// 303 - Inform all occupants of new room nickname.
	
	BOOL isMyPresence = NO;
	BOOL didCreateRoom = NO;
	BOOL isNicknameChange = NO;
	
	for (NSXMLElement *status in [x elementsForName:@"status"])
	{
		switch ([status attributeIntValueForName:@"code"])
		{
//by denebtech-			case 110 : isMyPresence = YES;     break;
			case 201 : didCreateRoom = YES;    break;
			case 210 :
			case 303 : isNicknameChange = YES; break;
		}
	}
	
	// Extract presence type
	
	NSString *presenceType = [presence type];
	
	BOOL isAvailable   = [presenceType isEqualToString:@"available"];
	BOOL isUnavailable = [presenceType isEqualToString:@"unavailable"];
	
	// Server's don't always properly send the statusCodes in every situation.
	// So we have some extra checks to ensure the boolean variables are correct.
	
	if (didCreateRoom || isNicknameChange)
	{
		isMyPresence = YES;
	}
	if (!isMyPresence)
	{
		if ([[from resource] isEqualToString:myNickname])
			isMyPresence = YES;
	}
	
	// Process presence
	
	if (didCreateRoom)
	{
		state |= kXMPPRoomStateCreated;
		
		[multicastDelegate xmppRoomDidCreate:self];
	}
	
	if (isMyPresence)
	{
		myRoomJID = from;
		myNickname = [from resource];
		
		if (isAvailable)
		{
			if (state & kXMPPRoomStateJoining)
			{
				state &= ~kXMPPRoomStateJoining;
				state |=  kXMPPRoomStateJoined;
				
				if ([xmppRoomStorage respondsToSelector:@selector(handleDidJoinRoom:withNickname:)])
					[xmppRoomStorage handleDidJoinRoom:self withNickname:myNickname];
				[multicastDelegate xmppRoomDidJoin:self];
			}
		}
		else if (isUnavailable && !isNicknameChange)
		{
			state = kXMPPRoomStateNone;
			[responseTracker removeAllIDs];
			
			[xmppRoomStorage handleDidLeaveRoom:self];
			[multicastDelegate xmppRoomDidLeave:self];
		}
	}
	else
	{
		if (isAvailable)
		{
			[multicastDelegate xmppRoom:self occupantDidJoin:from withPresence:presence];
		}
		else if (isUnavailable)
		{
			[multicastDelegate xmppRoom:self occupantDidLeave:from withPresence:presence];
		}
	}
}


@end
