
//
//  ChatTextView.m
//  speedsip
//
//  Created by denebtech on 26.08.12.
//
//

#import <QuartzCore/QuartzCore.h>
#import "ChatTextView.h"
#import "ChatStorage.h"

@implementation ChatTextView

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    
    return self;
}

#define MAX_HEIGHT 200

- (int) textDidChange:(BOOL) view_is_up  {
    
    if (originRect.size.width == 0)
        originRect = self.frame;
    
    CGRect rect;
    CGFloat bottom = main_view.frame.origin.y+main_view.frame.size.height;

    int height = [self measureHeightOfUITextView:self];

//    NSLog(@"Height: %d", height);

    BOOL isOverflowed = NO;
    if ([self.text isEqualToString:@""])
        height = 34;
    else if (height > 196) {
        height = 196;
        isOverflowed = YES;
    }
    
    if (main_view.frame.size.width > 320) {
        if (height > 80) {
            isOverflowed = YES;
            height = 80;
        }
    }
    
    [chat_text_image setImage:[[UIImage imageNamed:@"ChatTextView.png"] stretchableImageWithLeftCapWidth:105 topCapHeight:13]];

    rect = chat_text_image.frame;
    rect.size.height = height - 6;
    [chat_text_image setFrame:rect];
        
    [main_view setFrame:CGRectMake(0, (bottom-height-10), main_view.frame.size.width, height+10)];
    
    rect = self.frame;
    rect.origin.y = originRect.origin.y + (isOverflowed?5:0);
    rect.size.height = height + (isOverflowed?-10:10);
    [super setFrame:CGRectZero];
    [super setFrame:rect];
    
    [self reloadInputViews];
    
    return height+10;
}

- (CGFloat)measureHeightOfUITextView:(UITextView *)textView
{
    if ([textView respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)])
    {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)
        
        CGRect frame = textView.bounds;
        
        // Take account of the padding added around the text.
        
        UIEdgeInsets textContainerInsets = textView.textContainerInset;
        UIEdgeInsets contentInsets = textView.contentInset;
        
        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
        
        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;
        
        NSString *textToMeasure = textView.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
        }
        
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };
        
        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        
        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight;
    }
    else
    {
        return textView.contentSize.height;
    }
}


@end
