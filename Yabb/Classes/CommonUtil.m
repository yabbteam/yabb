//
//  CommonUtil.m
//  Yabb
//
//  Created by lion on 11/27/13.
//
//

#import "CommonUtil.h"
#import <TTTAttributedLabel.h>
#import <AVFoundation/AVFoundation.h>

#define RAD(a)          (a * M_PI/180.0f)

@implementation CommonUtil


+ (CGSize) getTextSize:(NSString*)text font:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode {
    if (text == nil || [text length] == 0)
        return CGSizeZero;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        return [text boundingRectWithSize:size options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:font} context:nil].size;
    }
    else {
      return [text sizeWithFont:font constrainedToSize:size lineBreakMode:lineBreakMode];
    }
}

+ (CGSize) getAttributedLabelSize:(NSString*)text font:(UIFont*)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode {
    if (text == nil || [text length] == 0)
        return CGSizeZero;
    
    static TTTAttributedLabel *label = nil;
    if (label == nil)
        label = [[TTTAttributedLabel alloc] init];
    label.font = font;
    label.numberOfLines = 0;
    label.text = text;
    CGSize ret = [label sizeThatFits:size];
    label.font = nil;
    return ret;
}

+(UIImage*)imageByScalingToSize:(CGSize)targetSize cgimage:(CGImageRef)cgimage orientation:(int)orientation
{
	CGFloat targetWidth = targetSize.width;
	CGFloat targetHeight = targetSize.height;
    
	CGImageRef imageRef = cgimage;
	CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
	if (bitmapInfo == kCGImageAlphaNone) {
		bitmapInfo = kCGImageAlphaNoneSkipLast;
	}
    
	CGContextRef bitmap;
    
	if (orientation == UIImageOrientationUp || orientation == UIImageOrientationDown) {
		bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef),
                                       CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
	} else {
		bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef),
                                       CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
	}
    
	if (orientation == UIImageOrientationLeft) {
		CGContextRotateCTM (bitmap, RAD(90));
		CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
	} else if (orientation == UIImageOrientationRight) {
		CGContextRotateCTM (bitmap, RAD(-90));
		CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
	} else if (orientation == UIImageOrientationUp) {
		// NOTHING
	} else if (orientation == UIImageOrientationDown) {
		CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
		CGContextRotateCTM (bitmap, RAD(-180.));
	}
    
	CGContextDrawImage(bitmap, CGRectMake(0, 0, targetWidth, targetHeight), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage* newImage = [UIImage imageWithCGImage:ref];
    
	CGContextRelease(bitmap);
	CGImageRelease(ref);
    
	return newImage; 
}

+ (UIImage *)fixOrientation:(UIImage*) image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+ (UIImage*) drawTextOnImage:(NSString*) text
                     inImage:(UIImage*)  image
                       color:(UIColor*)   color
                        fontSize:(int)fontSize
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:fontSize];
    
    CGSize expected = [CommonUtil getTextSize:text font:font constrainedToSize:CGSizeMake(image.size.width, image.size.height) lineBreakMode:UILineBreakModeWordWrap];
    
#if 0
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    /// Set line break mode
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    /// Set text alignment
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle,
                                  NSForegroundColorAttributeName : color};
    
    
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rtText = CGRectMake((image.size.width-expected.width)/2, (image.size.height-expected.height)/2, expected.width, expected.height);
    [text drawInRect:CGRectIntegral(rtText) withAttributes:attributes];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
#endif

    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rtText = CGRectMake((image.size.width-expected.width)/2, (image.size.height-expected.height)/2, expected.width, expected.height);
    [color set];
    [text drawInRect:CGRectIntegral(rtText) withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

+ (UIImage*) drawIconOnImage:(UIImage*) icon inImage:(UIImage*)  image {
    
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rt = CGRectMake(image.size.width/3, image.size.height/3, image.size.width/3, image.size.height/3);
    [icon drawInRect:rt];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


UIImage* addPhotoAvatarImage = nil;

+ (UIImage*) getNoAvatarImage {
    
    if (addPhotoAvatarImage)
        return addPhotoAvatarImage;
    
    addPhotoAvatarImage = [CommonUtil drawTextOnImage:NSLocalizedString(@"Add Photo", @"Add Photo") inImage:[UIImage imageNamed:@"avatar_noexist.png"] color:[UIColor darkGrayColor] fontSize:28];
    
    return addPhotoAvatarImage;
}

+ (UIImage*) getMovieScreenShot:(NSString*)filePath {
    
    NSURL* url = [NSURL fileURLWithPath:filePath];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    return [[UIImage alloc] initWithCGImage:imgRef];
}

@end
