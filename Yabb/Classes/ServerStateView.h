//
//  ServerStateView.h
//  Yabb
//
//  Created by denebtech on 2/17/14.
//
//

#import <UIKit/UIKit.h>

@interface ServerStateView : UIView {
    UIImageView* stateIcon;
    UILabel*    stateLabel;
    __strong NSTimer*    checkTimer;
}

- (void) setState:(BOOL)active;

@end
