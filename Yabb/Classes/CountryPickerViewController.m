//
//  CountriesViewController.m
//  speedsip
//
//  Created by denebtech on 06.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CountryPickerViewController.h"
#import "LafUtil.h"

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>


@interface CountryPickerViewController ()

@end

@implementation CountryPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"dialcodes" ofType:@"plist"];  
        
        _country_list = [[NSArray alloc] initWithContentsOfFile:filePath];
        _grouped_countries = [[NSMutableDictionary alloc] init];
        _letters = [[NSMutableArray alloc] init];

        _selected_index = -1;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //[country_table applyDefaultAppearance];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    if (_country_list)
        _country_list = nil;
}

- (void) viewWillAppear:(BOOL)animated {
}

- (void) viewDidAppear:(BOOL)animated {
    
    [self performSelector:@selector(setDefaultCountry) withObject:nil afterDelay:0.0f];
    
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void) setDefaultCountry {
    if (_selected_index != -1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:_selected_index inSection:_selected_section];
        [_country_table scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
}

- (void) Sort_Letters {
    
    [_grouped_countries removeAllObjects];
    [_letters removeAllObjects];

    for (NSArray * country in _country_list) {
        
        NSString * country_name = [country objectAtIndex:0];
        NSString * first_letter = [[country_name substringToIndex:1] uppercaseString];
        
        NSMutableArray * letter_dict = [_grouped_countries objectForKey:first_letter];
        if (letter_dict == nil) {
            letter_dict = [NSMutableArray arrayWithObject:country];
			[_grouped_countries setObject:letter_dict forKey:first_letter];
            [_letters addObject:first_letter];
		}
		else {
			[letter_dict addObject:country];
		}
    }
    
    // Sort letters
    NSArray * sorted_letters = [_letters sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        return [a localizedCaseInsensitiveCompare:b];
    }];
    [_letters removeAllObjects];
    [_letters addObjectsFromArray:sorted_letters];
    
    // Sort countries for each letter
    for (NSString * letter in _letters) {
        
        NSMutableArray * group = [_grouped_countries objectForKey:letter];
        NSArray * sorted_group = [group sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            return [[a objectAtIndex:0] localizedCaseInsensitiveCompare:[b objectAtIndex:0]];
        }];
        
        [group removeAllObjects];
        [group addObjectsFromArray:sorted_group];
    }
    
}

#pragma mark -
#pragma mark Table Delegates

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)index {
    
    return [_letters objectAtIndex:index];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	
    return _letters;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    [self Sort_Letters];
    return [_letters count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (_selected_index != -1 && (_selected_index != indexPath.row || _selected_section != indexPath.section)) {
     
        UITableViewCell * prev_cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selected_index inSection:_selected_section]];
        [prev_cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    _selected_index = indexPath.row;
    _selected_section = indexPath.section;
    
    // Move to previous controller
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (_delegate && [_delegate respondsToSelector:@selector(Select_Country:)]) {
        
        
        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:_selected_index] forKey:@"country_index"];
        [dict setObject:[NSNumber numberWithInt:_selected_section] forKey:@"country_section"];
        
        if (_selected_index != -1) {
            
            NSArray * country = [[_grouped_countries objectForKey:[_letters objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];

            [dict setObject:[country objectAtIndex:0] forKey:@"country_name"];
            [dict setObject:[country objectAtIndex:1] forKey:@"country_code"];
            [dict setObject:[country objectAtIndex:2] forKey:@"country_abbr"];
        }
        
        [_delegate Select_Country:dict];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray * array = [_grouped_countries objectForKey:[_letters objectAtIndex:section]];
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int row = indexPath.row, section = indexPath.section;
    
    NSArray * countries = [_grouped_countries objectForKey:[_letters objectAtIndex:indexPath.section]];

    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCellID"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CountryCellID"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if (_selected_index == row && _selected_section == section)
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    else
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    NSString * country_name = [[countries objectAtIndex:row] objectAtIndex:0];
    
    [cell.textLabel setText:country_name];
    
    return cell;
}

- (void) Get_Default_Country_Code {
    
    NSString* countryCode = [self getSIMCountryCode];
    if (countryCode == nil) {
        NSLocale *locale = [NSLocale currentLocale];
        countryCode = [locale objectForKey: NSLocaleCountryCode];
    }
    
    [self Sort_Letters];
    
    // Sort countries for each letter
    int section = 0;
    for (NSString * letter in _letters) {
        
        NSArray * array = [_grouped_countries objectForKey:[_letters objectAtIndex:section]];
        
        int row = 0;
        for (NSArray * country in array) {
            
            NSString * code = [country objectAtIndex:2];
            
            if ([code isEqualToString:countryCode]) {
            
                _selected_index = row;
                _selected_section = section;
                
                if (_delegate && [_delegate respondsToSelector:@selector(Select_Country:)]) {                    
                    
                    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:_selected_index] forKey:@"country_index"];
                    [dict setObject:[NSNumber numberWithInt:_selected_section] forKey:@"country_section"];
                    
                    if (_selected_index != -1) {
                        
                        NSArray * country = [[_grouped_countries objectForKey:[_letters objectAtIndex:section]] objectAtIndex:row];
                        
                        [dict setObject:[country objectAtIndex:0] forKey:@"country_name"];
                        [dict setObject:[country objectAtIndex:1] forKey:@"country_code"];
                        [dict setObject:[country objectAtIndex:2] forKey:@"country_abbr"];
                    }
                    
                    [_delegate Select_Country:dict];
                    
                    return;
                }
            }
            
            row++;
        }
        
        section++;
    }

    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (NSString*) getSIMCountryCode {
    
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    NSString *icc = [carrier isoCountryCode];
    if (icc != nil)
        NSLog(@"Mobile Country Code (ICC): %@", icc);
    
    // Get mobile network code
    NSString *mnc = [carrier mobileNetworkCode];
    
    if (mnc != nil)
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
    
    if (icc && icc.length > 0)
        return [icc uppercaseString];
    
    return nil;
}


@end
