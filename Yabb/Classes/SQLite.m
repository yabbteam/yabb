//
//  SQLite.m
//  WeatherNow
//
//  Created by Tolian T on 11.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SQLite.h"

@implementation SQLite

static SQLite * sql = nil;

+ (SQLite *) shared_sql {
    
    if (sql == nil) {
        sql = [SQLite alloc];
        sql->contactDB = nil;
    }
    
    return sql;
}

- (int) Open_DB:(NSString *)db_name {
    
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    NSString * databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: db_name]];

    NSLog(@"Database path: %@", databasePath);
    
    const char *dbpath = [databasePath UTF8String];
    
    int status = sqlite3_open(dbpath, &contactDB);
    if (status != SQLITE_OK) {
        contactDB = nil;
        return status;
    }

    return SQLITE_OK;
}

- (void) Close_DB {
    
    if (contactDB)
        sqlite3_close(contactDB);

}

- (void) Exec:(NSString *)exec_str {
    
    const char *sql_stmt = [exec_str UTF8String];
    char * errMsg;
    
//    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    if (sqlite3_exec(contactDB, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
    {
//        float period = [[NSDate date] timeIntervalSince1970] - timer;
        
//        NSLog(@"Exec done (%.03f): %@", period, exec_str);
    }    
    else {
        
        NSString * err = [NSString stringWithCString:errMsg encoding:NSUTF8StringEncoding];
        NSLog(@"Exec error: %@", err);
    }
    
}

- (NSArray *) Query:(NSString *)query_str {
    
    sqlite3_stmt *statement;
    const char *query_stmt = [query_str UTF8String];
    
//    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    if (sqlite3_prepare_v2(contactDB, query_stmt, -1, &statement, NULL) != SQLITE_OK)
        return nil;

    NSMutableArray * array = [NSMutableArray array];
    while (sqlite3_step(statement) == SQLITE_ROW) {
        
        NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];

        int num_of_fields = sqlite3_column_count(statement);
        for (int i = 0; i < num_of_fields; i++) {
            
            NSString * col_name = [[NSString alloc] initWithUTF8String:sqlite3_column_name(statement, i)];
            
            const char * value = (const char *)sqlite3_column_text(statement, i);
            NSString * col_text;
            
            if (value)
                col_text = [[NSString alloc] initWithUTF8String:value];
            else
                col_text = [[NSString alloc] initWithString:@""];
            
            if (col_name && col_text) {
                if ([col_text isEqualToString:@"--"] == NO && [col_text isEqualToString:@""] == NO)
                    [dict setObject:col_text forKey:col_name];
            }
            
        }
        [array addObject:dict];
    }
    
//    float period = [[NSDate date] timeIntervalSince1970] - timer;
//    NSLog(@"Query str (%.03f): %@", period, query_str);
    
    return array;    
}

- (int) Delete_DB:(NSString *)db_name {
    
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    NSString * databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: db_name]];
    
    [[NSFileManager defaultManager] removeItemAtPath:databasePath error:nil];
    
    return SQLITE_OK;
}

@end
