//
//  LocationViewController.h
//  Yabb
//
//  Created by denebtech on 5/31/14.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LocationViewController : UIViewController<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSDictionary* param;
@property (weak, nonatomic) IBOutlet UIImageView *markerView;

+ (void) getMapSnapShot:(CLLocationCoordinate2D) point handler:(void (^)(UIImage* obj)) block;

@end
