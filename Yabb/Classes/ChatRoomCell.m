//
//  ChatRoomCell.m
//  speedsip
//
//  Created by denebtech on 18.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatRoomCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ChatStorage.h"
#import "Friends.h"
#import "AvatarManager.h"

@implementation ChatRoomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setRoomName:(NSString *)name {
    
    room_name.adjustsFontSizeToFitWidth = YES;
    [room_name setText:name];
}

- (void) setRoomType:(int)type {
    
    if (type == 1) {
        [user_status setFrame:CGRectMake(56, 20, 222, 36)];
        [user_status setNumberOfLines:2];
    }
    else {
        [user_status setFrame:CGRectMake(56, 52, 222, 19)];
        [user_status setNumberOfLines:1];
    }
    
    [user_status setContentMode:UIViewContentModeLeft];

    [self bringSubviewToFront:user_status];
}

- (void) setPhoto:(NSArray *)phones {
    
    // Remove all opponents
    NSArray * subs = [main_view subviews];
    for (id obj in subs) {
        if (![obj isKindOfClass:[UIView class]])
            continue;
        
        UIView* view = (UIView*) obj;
        if (view.tag >= 100)
            [obj removeFromSuperview];
    }
    
    if (phones == nil)
        return;
    
    if (phones == nil || phones.count == 0) {
        
        [room_type setImage:[UIImage imageNamed:@"placeholderPerson_52.png"]];
        CALayer * l = [room_type layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:4.0];
        return;
    }
    
    int avatarCount = (int)[phones count];
    
    BOOL needShowThreeComma = NO;
    if (avatarCount > 8) {
        avatarCount = 8;
        needShowThreeComma = YES;
    }
    
    for (int i = 0; i < avatarCount; i++) {

        NSString * phone = [phones objectAtIndex:i];
        
        UIImage * image = [[AvatarManager sharedInstance] getAvatarWithPhone:phone];
        
        int tag = 100 + i * 10;
        
        if (i != avatarCount-1 || !needShowThreeComma) {
            UIImageView * opponent = (UIImageView *)[self viewWithTag:tag];
            
            if (opponent == nil) {
                opponent = [[UIImageView alloc] initWithFrame:CGRectMake(54 + 30 * i, 25, 25, 25)];
                [opponent setTag:tag];
                [main_view addSubview:opponent];
            }
            
            [opponent setImage:image?image:[UIImage imageNamed:@"placeholderPerson_25.png"]];
            
            CALayer * l = [opponent layer];
            [l setMasksToBounds:YES];
            [l setCornerRadius:2.0];
        }
        else {
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(54 + 30 * i, 30, 25, 25)];
            [label setTag:tag];
            [label setTextColor:[UIColor darkGrayColor]];
            label.backgroundColor = [UIColor clearColor];
            [main_view addSubview:label];
            [label setText:@"..."];
        }
    }
    

/*
    [room_type setImage:image];
    CALayer * l = [room_type layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
*/
}

- (void) setLastMessage:(NSString*) cid {
    
    NSDictionary * last_message = [[ChatStorage sharedStorage] getLastMessage:cid];

    if (last_message) {
        [user_status setText:[last_message objectForKey:@"text"]];
        [last_time setText:[last_message objectForKey:@"time"]];
    }
    else {
        
        [user_status setText:@""];
        [last_time setText:@""];
    }
}

- (void) setRoomPhoto:(UIImage*)photo {
    
    [room_type setImage:photo];
    
    CALayer * l = [room_type layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
}

@end
