//
//  JabberLayer.h
//  Yabb
//
//  Created by lion on 1/23/14.
//
//

#import <Foundation/Foundation.h>

#import <XMPPFramework.h>
#import <XMPP.h>
#import <XMPPStream.h>
#import <XMPPReconnect.h>
#import <XMPPLogging.h>
#import <XMPPRoster.h>
#import <XMPPRosterMemoryStorage.h>
#import <XMPPRoom.h>
#import <XMPPMessageDeliveryReceipts.h>
#import <XMPPLastActivity.h>
#import <XMPPRoom.h>
#import <XMPPMUC.h>
#import <XMPPPing.h>
#import <XMPPvCardTempModule.h>
#import <XMPPvCardCoreDataStorage.h>
#import <XMPPAutoPing.h>

#define GROUP_CHAT_SUFFIX @"conference.yabb.com"

#define SERVER_STATE_CONNECTED @"xmpp-server-state-connected"
#define SERVER_STATE_DISCONNECTED @"xmpp-server-state-disconnected"
#define SERVER_STATE_RECONNECTING @"xmpp-server-state-reconnecting"
#define SERVER_STATE_RECONNECTED @"xmpp-server-state-reconnected"

@interface JabberLayer : NSObject<XMPPRoomStorage, XMPPMUCDelegate> {
    
    XMPPStream* xmppStream;
    XMPPRosterMemoryStorage *xmppRosterMemStorage;
    XMPPRoster*     xmppRoster;
    XMPPReconnect * xmppReconnect;
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts;
    XMPPLastActivity*   xmppLastActivity;
    XMPPMUC*            xmppMuc;
    XMPPAutoPing*           xmppAutoPing;
    XMPPvCardTempModule*    xmppvCardTempModule;
    XMPPvCardAvatarModule*  xmppvCardAvatarModule;
    
    NSMutableDictionary*        xmppRooms;
    NSOperationQueue*           op_queue;
    
    NSString* user_password;
    NSString* sessionToken;
    
    id login_delegate;
    
    dispatch_queue_t workingQueue;
    void (^abookBlock)(void);
    void (^createMucBlock)(void);
    void (^uploadAvatar)(void);
    
    NSMutableDictionary*    hashCmdClass;
    
    XMPPJID*       currentUserJid;
    
    NSMutableArray*     uploadFileInfos;
    BOOL        _isConnectedServer;
    
    NSMutableDictionary*     arrayNicknames;
    
    int loggingState;
}


+ (JabberLayer *) SharedInstance;

- (void) setLoggedinDelegate:(id)delegate;
- (BOOL) processThis:(NSDictionary*) dict name:(NSString*)name delegate:(id)delegate silent:(BOOL)silent;
- (void) registerConnectionDelegate:(id)delegate name:(NSString*)name;

- (BOOL) isActive;
- (BOOL) isConnectedServer;
- (BOOL) login:(NSString*) userid password:(NSString*)password;
- (void) sayOffline;
- (void) sayOnline;
- (void) sayAway;

- (NSString*) getMyPhoneNo;

- (NSString*) requestRandomMessageId;
- (void) sendMessage:(NSDictionary*) message;
- (void) sendMessageDelivered:(NSString*) message_id;
- (void) sendMessageSeen:(NSString*) message_id;
- (void) sendMessageComposingStarted:(NSString*) message_id;
- (void) sendMessageComposingCanceled:(NSString*) message_id;

- (void) notifyEnteredChatBoard:(NSString*) chat_id;
- (void) removeGroupChat:(NSString*) chat_id;
- (BOOL) isGroupChat:(NSString*) chat_id;

- (void) uploadFile:(NSData*) data thumb:(NSData*)thumb name:(NSString*)name target:(id)delegate local_id:(NSString*)local_id;
- (void) cancelUpload:(NSString*)local_id;

- (void) clear;

- (NSString*) getMyNickName;
- (NSString*) getNickNameByPhone:(NSString*) phoneNo;

- (void) updateAvatar:(UIImage *)avatar nickname:(NSString*)nickname;
- (void) updateAvatarAfter:(UIImage *)avatar nickname:(NSString*)nickname;

- (NSString*) getRoomIdFromJID:(NSString*) chatId;
- (NSString*) getServerChatID:(NSString*) room_id phone:(NSString*) phone;

- (NSString*) getKindStringOfGroupChat:(NSString*)chatid;
- (NSString*) getAdminOfGroupChat:(NSString*) chatid;

//check log/reconnecting state
- (BOOL) isLoggingProgress;
- (BOOL) isReconnecting;
- (BOOL) isLoggingInitState;


@end
