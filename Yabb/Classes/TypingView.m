//
//  TypingView.m
//  Baycall
//
//  Created by denebtech on 09.11.12.
//
//

#import "TypingView.h"
#import "AppDelegate.h"

@implementation TypingView

@synthesize typing = _typing;


- (id) initWithCoder:(NSCoder *)aDecoder {
    
    if ( self = [super initWithCoder:aDecoder]) {
        
        _typing = NO;
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
    }
    
    return self;
}

- (void) clearText {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(clearText) object:nil];
    
    [text_label setText:@""];

    CGRect rect = self.frame;
    rect.size.height = 0;
    [self setFrame:rect];
    
    _typing = NO;
}

- (void) setText:(NSString *)str atPosition:(CGPoint)position {

    if (pencil_image.isAnimating == NO) {
        [pencil_image setAnimationImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"pencil.png"], [UIImage imageNamed:@"pencil.png"], [UIImage imageNamed:@"pencil_empty.png"], nil]];
        [pencil_image setAnimationDuration:1.2];
        [pencil_image startAnimating];
    }

    [text_label setText:str];
    
    _typing = YES;

    [self update:position];
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(clearText) object:nil];
    [self performSelector:@selector(clearText) withObject:nil afterDelay:KEEP_SHOW_TYPING_THRESHOLD];
}

- (void) update:(CGPoint) position {

    CGRect rect = self.frame;
    
    rect.origin.y = position.y/* - TYPING_VIEW_HEIGHT*/;
    rect.size.height = TYPING_VIEW_HEIGHT;
    [self setFrame:rect];
    
    CGSize size = [CommonUtil getTextSize:text_label.text font:text_label.font constrainedToSize:CGSizeMake(FLT_MAX, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    rect = text_label.frame;
    rect.origin.x = 310 - size.width;
    text_label.frame = rect;

    rect = pencil_image.frame;
    rect.origin.x = 310 - size.width - 20;
    pencil_image.frame = rect;
}

@end
