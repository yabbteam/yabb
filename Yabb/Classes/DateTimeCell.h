//
//  DateTimeCell.h
//  Baycall
//
//  Created by denebtech on 19.09.12.
//
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"

@interface DateTimeCell : ChatCell

@property (nonatomic, strong) IBOutlet UILabel * date_time_label;


@end
