//
//  PickerCell.h
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import <UIKit/UIKit.h>

enum {
    
    SELECTOR_EMPTY = 0,
    SELECTOR_DISABLED, SELECTOR_DESELECTED, SELECTOR_SELECTED
    
};

@interface PickerCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel * person_label;
@property (nonatomic, strong) IBOutlet UIImageView * user_status;
@property (nonatomic, strong) IBOutlet UIImageView * user_selector;
@property (nonatomic, strong) IBOutlet UILabel * origin_label;
@property (nonatomic, strong) IBOutlet UIImageView * person_photo;

- (void) enableSelector:(BOOL)active;
- (void) setOriginText:(NSString *)text;
- (void) resetOriginText;

@end
