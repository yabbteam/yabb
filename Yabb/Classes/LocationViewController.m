//
//  LocationViewController.m
//  Yabb
//
//  Created by denebtech on 5/31/14.
//
//

#import "LocationViewController.h"
#import "CommonUtil.h"
#import "ChatStorage.h"
#import "ASIHTTPRequest.h"

#define MY_SPAN_VALUE 0.00725
void runOnMainQueueWithoutDeadlocking(void (^block)(void));

@interface LocationViewController () {

    BOOL isInitial;
}

@end

@implementation LocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    isInitial = YES;

    if (self.param == nil) {
        // Send Button
        UIBarButtonItem * done_button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send", @"Send") style:UIBarButtonItemStylePlain target:self action:@selector(didDoneButton:)];
        [self.navigationItem setRightBarButtonItem:done_button];
        
        [self.navigationItem setTitle:NSLocalizedString(@"Send Location", @"Send Location")];
    }
    else {
        [self.navigationItem setTitle:NSLocalizedString(@"Location Info", @"Location Info")];
    }
    
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    
    if (self.param == nil) {
        self.mapView.showsUserLocation = YES;
        self.markerView.hidden = YES;
    }
    else {
        self.markerView.hidden = YES;
        
        float spanX = MY_SPAN_VALUE;
        float spanY = MY_SPAN_VALUE;
        
        MKCoordinateRegion region;
        region.center.latitude = [[self.param objectForKey:@"latitude"] floatValue];
        region.center.longitude = [[self.param objectForKey:@"longitude"] floatValue];
        region.span.latitudeDelta = spanX;
        region.span.longitudeDelta = spanY;
        
        [self.mapView setRegion:region animated:YES];

        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = region.center;
        //point.title = @"Location";
        //point.subtitle = @"I'm here!!!";
        [self.mapView addAnnotation:point];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)zoomToCurrentLocation {
    
    float spanX = MY_SPAN_VALUE;
    float spanY = MY_SPAN_VALUE;
    
    MKCoordinateRegion region;
    region.center.latitude = self.mapView.userLocation.coordinate.latitude;
    region.center.longitude = self.mapView.userLocation.coordinate.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    
    [self.mapView setRegion:region animated:NO];
}

- (void)zoomToCurrentLocationLooksGood {
    
    float spanX = MY_SPAN_VALUE;
    float spanY = MY_SPAN_VALUE;
    
    MKCoordinateRegion region;
    region.center.latitude = self.mapView.userLocation.coordinate.latitude;
    region.center.longitude = self.mapView.userLocation.coordinate.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    
    [self.mapView setRegion:region animated:YES];
    
    self.markerView.hidden = NO;
    CGPoint pt = [self.mapView convertCoordinate:self.mapView.userLocation.coordinate toPointToView:self.view];
    pt.y -= self.markerView.frame.size.height/2;
    self.markerView.center = pt;
}

#pragma mark - MKMapView Delegate Stuf...

- (void)zoomMapAndCenterAtLatitude:(double)latitude andLongitude:(double)longitude WithSpan:(double)spanValue
{
    MKCoordinateRegion region;
    region.center.latitude  = latitude;
    region.center.longitude = longitude;
    
    //Set Zoom level using Span
    MKCoordinateSpan span;
    span.latitudeDelta  = spanValue;
    span.longitudeDelta = spanValue;
    region.span = span;
    
    //Move the map and zoom
    [self.mapView setRegion:region animated:YES];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {

//    [self.mapView setCenterCoordinate:userLocation.coordinate animated:NO];
    if (isInitial) {
        [self zoomToCurrentLocation];
        [self performSelector:@selector(zoomToCurrentLocationLooksGood) withObject:nil afterDelay:0.2f];
    }
    isInitial = NO;
}

- (void) didDoneButton:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
    
    [LocationViewController getMapSnapShot:[self.mapView centerCoordinate] handler:^(UIImage* image) {
        CLLocationCoordinate2D currentCenter = [self.mapView centerCoordinate];
        NSDictionary* dict = @{@"latitude":[NSNumber numberWithFloat:currentCenter.latitude],
                               @"longitude":[NSNumber numberWithFloat:currentCenter.longitude],
                               @"thumb":image
                               };
        
        [LocationViewController sendAddressInfo:dict];
    }];
}


- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (void) sendAddressInfo:(NSDictionary*) param {
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    float latitude = [[param objectForKey:@"latitude"] floatValue];
    float longitude = [[param objectForKey:@"longitude"] floatValue];
    CLLocation* location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             MKPlacemark* placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
//             if ([placemark.postalCode length] != 0)
//             {
//                 if ([strAdd length] != 0)
//                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
//                 else
//                     strAdd = placemark.postalCode;
//             }
//             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
             }
             
             NSMutableDictionary* new_dict = [NSMutableDictionary dictionaryWithDictionary:param];
             [new_dict setObject:strAdd forKey:@"address"];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"send_location" object:new_dict];
         }
         else {
             NSMutableDictionary* new_dict = [NSMutableDictionary dictionaryWithDictionary:param];
             [new_dict setObject:NSLocalizedString(@"Location", @"Location") forKey:@"address"];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"send_location" object:new_dict];
         }
     }];
    
    location = nil;
    geocoder = nil;
}

+ (void) getMapSnapShot:(CLLocationCoordinate2D) point handler:(void (^)(UIImage* obj)) block  {
    
    int SNAPSHOT_WIDTH = 320;
    int SNAPSHOT_HEIGHT = 320;
    NSString *GOOGLE_API_KEY = @"AIzaSyDczPyMiSOAtsgctq6Y9cy0Br6gXqgsRiY";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
        
        MKCoordinateRegion region;
        region.center = point;
        region.span.latitudeDelta = MY_SPAN_VALUE;
        region.span.longitudeDelta = MY_SPAN_VALUE;
        
        options.region = region;
        options.scale = [UIScreen mainScreen].scale;
        
        options.size = CGSizeMake(SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT);
        
        MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
        [snapshotter startWithQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) completionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
            if (snapshot == nil || error != nil) {
                block(nil);
            }
            
            // get the image associated with the snapshot
            
            UIImage *image = snapshot.image;
            
            // Get the size of the final image
            
            //        CGRect finalImageRect = CGRectMake(0, 0, image.size.width, image.size.height);
            
            // Get a standard annotation view pin. Clearly, Apple assumes that we'll only want to draw standard annotation pins!
            
            MKAnnotationView *pin = [[MKPinAnnotationView alloc] initWithAnnotation:nil reuseIdentifier:@""];
            UIImage *pinImage = pin.image;
            
            // ok, let's start to create our final image
            
            UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
            
            // first, draw the image from the snapshotter
            
            [image drawAtPoint:CGPointMake(0, 0)];
            
            // now, let's iterate through the annotations and draw them, too
            
            //        for (id<MKAnnotation>annotation in self.mapView.annotations)
            //        {
            CGPoint view_point = [snapshot pointForCoordinate:point];
            //            if (CGRectContainsPoint(finalImageRect, point)) // this is too conservative, but you get the idea
            //            {
            CGPoint pinCenterOffset = pin.centerOffset;
            view_point.x -= pin.bounds.size.width / 2.0;
            view_point.y -= pin.bounds.size.height / 2.0;
            view_point.x += pinCenterOffset.x;
            view_point.y += pinCenterOffset.y;
            
            [pinImage drawAtPoint:view_point];
            //            }
            //        }
            
            // grab the final image
            
            UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            block(finalImage);
        }];
    }
    else {
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=14&size=%dx%d&maptype=roadmap&sensor=false&key=%@", point.latitude, point.longitude, SNAPSHOT_WIDTH, SNAPSHOT_HEIGHT, GOOGLE_API_KEY]];
        
        ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:url];
        __weak ASIHTTPRequest *_request = request;
        
        [request setRequestMethod:@"GET"];
        
        [request setFailedBlock:^{
            NSError* _error = _request.error;
            NSLog(@"Httperror:%@%lu", _error.localizedDescription,_error.code);
            runOnMainQueueWithoutDeadlocking(^{
                block(nil);
            });
        }];
        
        [request setCompletionBlock:^{
            
            NSInteger responseCode = [_request responseStatusCode];
            if (responseCode == 200) {
                
                UIImage* image = [UIImage imageWithData:[_request responseData]];
                
                MKAnnotationView *pin = [[MKPinAnnotationView alloc] initWithAnnotation:nil reuseIdentifier:@""];
                UIImage *pinImage = pin.image;
                
                // ok, let's start to create our final image
                
                UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
                
                [image drawAtPoint:CGPointMake(0, 0)];
                
                CGPoint view_point;

                CGPoint pinCenterOffset = pin.centerOffset;
                view_point.x = image.size.width/2 - pin.bounds.size.width / 2.0;
                view_point.y = image.size.height/2 - pin.bounds.size.height / 2.0;
                view_point.x += pinCenterOffset.x;
                view_point.y += pinCenterOffset.y;
                
                [pinImage drawAtPoint:view_point];
                
                // grab the final image
                UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                runOnMainQueueWithoutDeadlocking(^{
                    block(finalImage);
                });
            }
        }];
        
        [request startAsynchronous];
    }
}

//

@end
