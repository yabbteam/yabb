//
//  ChatBoardTable.m
//  speedsip
//
//  Created by denebtech on 18.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ChatBoardTable.h"
#import "ChatBoardCell.h"
#import "ChatImageCell.h"
#import "AnimationCell.h"
#import "ChatEditCell.h"
#import "DateTimeCell.h"
#import "ChatStorage.h"
#import "AddressCollector.h"
#import "ConnectionQueue.h"
#import "NSData+Base64.h"
#import "ImageDownloadViewController.h"
#import "ChatBoardViewController.h"
#import "Config.h"
#import "SoundManager.h"
#import "JabberLayer.h"
#import "TimeBomb.h"
#import "AvatarManager.h"
#import "LocationViewController.h"

@implementation ChatBoardTable

@synthesize chat_board_delegate = _chat_board_delegate;
@synthesize chat_board_controller = _chat_board_controller;
@synthesize at_screen = _at_screen, edit_mode = _edit_mode;

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.dataSource = self;

        self->time_sections = [[NSMutableArray alloc] init];
        self->selected_cells = [[NSMutableArray alloc] init];
        _at_screen = NO;
        num_of_selected = 0;
        
        array_seen_messages = [[NSMutableDictionary alloc] initWithCapacity:100];
    }
    return self;
}


- (void) UpdateMessagesWithGroups:(NSArray *) groups {
    
    [time_sections removeAllObjects];
    [time_sections addObjectsFromArray: [[ChatStorage sharedStorage] getTimeGroups:chat_id count:self.showingCount]];
    
    _is_private = [[ChatStorage sharedStorage] getRoomPrivate:chat_id];
    _messageTotalCount = [[ChatStorage sharedStorage] getMessageCount:chat_id];
}

- (void) Update_Messages:(NSArray *)groups scroll:(BOOL)isScroll {
    
    // Update cached contacts
    if (contacts)
        [contacts removeAllObjects];
    
    if (_at_screen) {
        NSLog(@"*** RELOAD DATA ***");

        [self UpdateMessagesWithGroups:groups];
        [self reloadData];
        
        if (isScroll)
            [self Scroll_To_Bottom:NO];
    }
}

- (void) Scroll_To_Bottom:(BOOL)animated {
    
    int last_section = [self numberOfSections] - (!_edit_mode?1:2);
    if (last_section < 0)
        return;
    
    int last_idx = [self numberOfRowsInSection:last_section] - 1;
    if (last_idx >= 0)
        [self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:last_idx inSection:last_section] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
}

- (void) setChatId:(NSString*)value {
    chat_id = value;
}

- (NSString *) Get_Contact_Name:(NSString *)phone_number {
    
    if (phone_number == nil)
        return @"";
    
    if (contacts == nil)
        contacts = [[NSMutableDictionary alloc] init];
    
    NSString * name = [contacts objectForKey:phone_number];
    if (name == nil) {
        name = [[AddressCollector sharedCollector] Get_Name_By_Phone:phone_number withPlus:YES];
        [contacts setObject:[NSString stringWithString:name] forKey:phone_number];
    }
    
    return name;
}

#pragma mark -
#pragma mark Selected Array actions

- (void) removeAllSelected {
    
    [selected_cells removeAllObjects];
}

- (void) addSelected:(NSIndexPath *)indexPath withSid:(NSNumber *)sid {
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:indexPath, @"indexPath", sid, @"server_id", nil];
    
    NSLog(@"Add selection: %@", dict);
    
    [selected_cells addObject:dict];
}

- (void) deleteSelected:(NSIndexPath *)indexPath {
    
    for (NSDictionary * dict in selected_cells) {
        
        NSIndexPath * idx = [dict objectForKey:@"indexPath"];
        
        if (indexPath.section == idx.section && indexPath.row == idx.row) {
            
            [selected_cells removeObject:dict];
            return;
        }
    }
}

- (NSString*) isSelectedCell:(NSIndexPath *)indexPath {
    
    
    for (NSDictionary * dict in selected_cells) {

        NSIndexPath * idx = [dict objectForKey:@"indexPath"];

        if (indexPath.section == idx.section && indexPath.row == idx.row) {

            return [dict objectForKey:@"server_id"];
            
        }
    }
    
    return 0;
}

#pragma mark -
#pragma mark Table Delegates


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (!_edit_mode || indexPath.row == 0)
        return;

    ChatCell * cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell) {
    
        [self deleteSelected:indexPath];
        
        num_of_selected--;
        
        if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(selectCell:)])
            [_chat_board_delegate selectCell:num_of_selected];
        
        NSLog(@"Selected: %d", num_of_selected);
    }

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (!_edit_mode || indexPath.row == 0)
        return;
    
    ChatCell * cell = (ChatCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell) {

        
        [self addSelected:indexPath withSid:cell.server_id];
        
        num_of_selected ++;
        
        if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(selectCell:)])
            [_chat_board_delegate selectCell:num_of_selected];

        NSLog(@"Selected: %d", num_of_selected);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    //NSLog(@"Sections: %d", [time_sections count]+(_edit_mode?0:1));
    
    return [time_sections count]+(_edit_mode?0:1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    int section = indexPath.section;

    if (!_edit_mode && section == 0) {
//        NSLog(@"H: %d", 46);
        if (self.showingCount >= _messageTotalCount)
            return 46;
        else
            return 82;
    }
    
    int row = indexPath.row - 1;
    if (row == -1) {
//        NSLog(@"H: %d", 18);
        return 18;
    }
    
    NSArray * messages = [[time_sections objectAtIndex:(section-(_edit_mode?0:1))] objectForKey:@"messages"];
    NSDictionary * msg = [messages objectAtIndex:row];
    NSString * phone_str = [msg objectForKey:@"phone"];
    BOOL is_owner = phone_str?NO:YES;
    
    int height = [[msg objectForKey:@"msg_height"] intValue];
    
    height -= 10;
  
    
    if (_is_private == YES && is_owner == NO)
        height -= 10;

//    NSLog(@"H: %d", height);

    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

//    NSLog(@"w: %f, h: %f", tableView.frame.size.width, tableView.frame.size.height);
    
    if (!_edit_mode) {
        if (section == 0)
            return 1;
        
        section--;
    }
    
    NSArray * messages = [[time_sections objectAtIndex:section] objectForKey:@"messages"];

//    NSLog(@"Msg: %d", [messages count]);
    
    return [messages count]+1;
}

- (void) edit {
    
    num_of_selected = 0;
    [self removeAllSelected];
    
    _edit_mode = !_edit_mode;
    
    [self reloadData];
    
    if (_edit_mode == NO) {
        
//        [self Scroll_To_Bottom:NO];
    }
    
    if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(editMode:)])
        [_chat_board_delegate editMode:_edit_mode];
    
}

- (void) tapContactInfo {
    
    NSLog(@"Contact Info");
    
    if (_is_private) {
        if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(showContactInfo:)])
            [_chat_board_delegate showContactInfo:chat_id];
    }
    else {
        if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(Button_Settings)])
            [_chat_board_delegate Button_Settings];
        
    }
    
}

- (void) tapAddNumber {

    [[NSNotificationCenter defaultCenter] postNotificationName:@"add_contact_number" object:nil];
}

- (void) deleteSelectedCells {
    
    int const_section = 0;
    for (int section = 0; section < [self numberOfSections]; section++) {
        
        int num_of_cells = [self numberOfRowsInSection:section];
        int deleted_cells = 0;
        
        NSMutableArray * array = [NSMutableArray array];
        
        for (int row = 1; row < num_of_cells; row++) {
         
            NSString* sid = [self isSelectedCell:[NSIndexPath indexPathForRow:row inSection:const_section]];    // !!! section number from the start (not reloaded) !!!
            if (sid == 0)
                continue;
            
            [[ChatStorage sharedStorage] deleteMessage:sid];
            
            NSLog(@"Delete message: %@, [%d, %d]", sid, section, row);
            
            
            [array addObject:[NSIndexPath indexPathForRow:row inSection:section]];
            
            deleted_cells++;
        }
        
        if (num_of_cells - deleted_cells == 1) {
            
            [self UpdateMessagesWithGroups:nil];
            [self deleteSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationFade];

            section--;
        }
        else {
            
            if (array && [array count]) {
                [self UpdateMessagesWithGroups:nil];
                [self deleteRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        
        const_section++;
    }

    [self removeAllSelected];
    num_of_selected = 0;
    
    if (_chat_board_delegate && [_chat_board_delegate respondsToSelector:@selector(selectCell:)])
        [_chat_board_delegate selectCell:num_of_selected];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];

    if (_edit_mode) {
    
        
    }
    else {
        
        [self deselectRowAtIndexPath:indexPath animated:NO];
        
        if (section == 0) {
            
            ChatEditCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatEditCellID"];
            if (cell == nil) {
            
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
                cell = (ChatEditCell *)[nib objectAtIndex:CHAT_EDIT_CELL];
            }
            
            UIButton * button = (UIButton *)[cell viewWithTag:10];
            if (button)
                [button addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
            
            button = (UIButton *)[cell viewWithTag:20];
            BOOL isUnknown = NO;
            if (_is_private) {
                NSString* phone_number = [@"+" stringByAppendingString:chat_id];
                int contact_index = [[AddressCollector sharedCollector] Get_Index_By_Phone:phone_number];
                if (contact_index < 0) {
                    //unknown user
                    [button setTitle:NSLocalizedString(@"Add This Number", @"Add This Number") forState:UIControlStateNormal];
                    isUnknown = YES;
                }
                else {
                    [button setTitle:NSLocalizedString(@"Contact Info", @"Contact Info") forState:UIControlStateNormal];
                }
            }
            if (button) {
                [button removeTarget:self action:@selector(tapContactInfo) forControlEvents:UIControlEventTouchUpInside];
                [button removeTarget:self action:@selector(tapAddNumber) forControlEvents:UIControlEventTouchUpInside];
                
                if (!isUnknown)
                    [button addTarget:self action:@selector(tapContactInfo) forControlEvents:UIControlEventTouchUpInside];
                else
                    [button addTarget:self action:@selector(tapAddNumber) forControlEvents:UIControlEventTouchUpInside];
            }
            
            button = (UIButton *)[cell viewWithTag:30];
            if (button) {
                [button addTarget:self action:@selector(tapLoadEarlierMessage) forControlEvents:UIControlEventTouchUpInside];
                if (self.showingCount >= _messageTotalCount)
                    button.hidden = YES;
                else
                    button.hidden = NO;
            }
            
            return cell;
        }
        
        section--;
    }
    
    NSDictionary * dict = [time_sections objectAtIndex:section];
    
    if (row == 0) {
        
        DateTimeCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DateTimeCellID"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
            cell = (DateTimeCell *)[nib objectAtIndex:CHAT_DATE_TIME_CELL];
        }
        
        [cell.date_time_label setText:[dict objectForKey:@"day"]];
        [cell enableIndicator:_edit_mode];
        
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];

        return cell;
    }
    else {

        row--;
        NSDictionary * record = [[[time_sections objectAtIndex:section] objectForKey:@"messages"] objectAtIndex:row];

        int type = [[record objectForKey:@"message_type"] intValue];
        int status = [[record objectForKey:@"status"] intValue];
        NSString * phone_str = [record objectForKey:@"phone"];
        long long phone_number = phone_str?[phone_str longLongValue]:0;
        BOOL is_owner = phone_number?NO:YES;
        BOOL delivered = [[record objectForKey:@"delivered"] intValue];
        NSString * contact_name = [self Get_Contact_Name:phone_str];
        NSString* sid = [record objectForKey:@"server_id"];
        UIImage* personPhoto = [self getPersonPhotoByPhone:phone_str];

        NSTimeInterval timestamp = [[ChatStorage sharedStorage] getTime:[record objectForKey:@"created"]];
        NSDate * created = [NSDate dateWithTimeIntervalSince1970:timestamp];
        
        if (type == 0) {    // text message
            
            NSString * original = [[ChatStorage sharedStorage] decodeString:[record objectForKey:@"original_message"]];
            
            NSString * anim_name = [[ChatStorage sharedStorage] getAnimationName:original];
            
            // *************************
            // Animation cell
            // *************************
            if (anim_name) {

                AnimationCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AnimationCellID"];
                if (cell == nil) {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
                    cell = (AnimationCell *)[nib objectAtIndex:CHAT_ANIMATION_CELL];
                }
                
                cell.parent = self;
                [cell setIs_private:_is_private];
                [cell setIs_owner:is_owner];
                [cell setServer_id:sid];
                [cell setPerson:contact_name];
                [cell setMessageStatus:status delivered:delivered];
                [cell setTime:created];
                [cell setAnimation:anim_name editMode:_edit_mode];
                [cell enableIndicator:_edit_mode];
                [cell setPersonPhoto:personPhoto];
                [cell setIs_marked_seen:delivered];
                
                return cell;
            }
            // *************************
            // Text cell
            // *************************
            else {
                
                ChatBoardCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatBoardCellID"];
                if (cell == nil) {
                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
                    cell = (ChatBoardCell *)[nib objectAtIndex:CHAT_BOARD_CELL];
                }

                NSString * parsed_msg = [record objectForKey:@"parsed_message"];
                NSDictionary * decoded = [[ChatStorage sharedStorage] decodeStringToDictionary:parsed_msg];
                
                cell.parent = self;
                [cell setIs_private:_is_private];
                [cell setIs_owner:is_owner];
                [cell setServer_id:sid];
                [cell setPerson:contact_name];
                [cell setMessageStatus:status delivered:delivered];
                [cell setTime:created];
                [cell setPersonPhoto:personPhoto];
                [cell setIs_marked_seen:delivered];
                
                //NSLog(@"parsed_msg: %@", parsed_msg);
                //NSLog(@"message text: %@", decoded);
                [cell setText:decoded editMode:_edit_mode];

                [cell enableIndicator:_edit_mode];

                return cell;
            }
        }
        // *************************
        // Image cell
        // *************************
        else if (type == 1 || type == 2) {
            ChatImageCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatImageCellID"]; 
            if (cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
                cell = (ChatImageCell *)[nib objectAtIndex:CHAT_IMAGE_CELL];
            }

            int media_size = [[record objectForKey:@"media_size"] intValue];
            
            cell.parent = self;
            
            [cell setMessageType:type];
            [cell setMediaSize:media_size];
            [cell setIs_private:_is_private];
            [cell setIs_owner:is_owner];
            [cell setServer_id:sid];
            [cell setPerson:contact_name];
            [cell setMessageStatus:status delivered:delivered];
            [cell setTime:created];
            [cell setButton:status];
            [cell setPersonPhoto:personPhoto];
            [cell setIs_marked_seen:delivered];

            [cell setCell_delegate:self];
            [cell setData:record editMode:_edit_mode];
            [cell enableIndicator:_edit_mode];
            
            return cell;
        }
        else if (type == 3) { //location message
            ChatImageCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatImageCellID"];
            if (cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatBoardCell" owner:self options:nil];
                cell = (ChatImageCell *)[nib objectAtIndex:CHAT_IMAGE_CELL];
            }
            
            cell.parent = self;
            
            [cell setMessageType:type];
            [cell setIs_private:_is_private];
            [cell setIs_owner:is_owner];
            [cell setServer_id:sid];
            [cell setPerson:contact_name];
            [cell setMessageStatus:status delivered:delivered];
            [cell setTime:created];
            [cell setButton:status];
            [cell setPersonPhoto:personPhoto];
            [cell setIs_marked_seen:delivered];
            
            [cell setCell_delegate:self];
            [cell setData:record editMode:_edit_mode];
            [cell enableIndicator:_edit_mode];
            
            return cell;
        }
        else
            return nil;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    [super touchesBegan:touches withEvent:event];
    
    if ([_chat_board_delegate respondsToSelector:@selector(tapOnTable)])
        [_chat_board_delegate tapOnTable];
}

#pragma mark Media Delegates
- (void) didClickImage:(NSDictionary *)dict {
    NSString* sid = [dict objectForKey:@"server_id"];
    NSNumber* owner = [dict objectForKey:@"owner"];
    int type = [[dict objectForKey:@"type"] intValue];
    
    if (![owner boolValue] && type != 3) {
        
        NSDictionary* info = [[ChatStorage sharedStorage] getRecordByServerId:sid];
        if ([info objectForKey:@"media_asset"] == nil)
            return;
    }
    
    server_id = sid;
    NSLog(@"Click on image msg: %@", server_id);
    
    if (type == 3) {
        LocationViewController* locationViewController = [[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:nil];
        ChatBoardViewController * controller = _chat_board_controller;
        locationViewController.param = [[ChatStorage sharedStorage] getLocationInfo:server_id];
        [controller.navigationController pushViewController:locationViewController
                                             animated:YES];
    }
    else {
        ImageDownloadViewController * image_download = [[ImageDownloadViewController alloc] init];
        
        [image_download setServer_id:server_id];
        [image_download setChat_id:chat_id];
        [image_download setSource_controller:self.chat_board_controller];
        [image_download setIs_owner:[owner boolValue]];
        
        ChatBoardViewController * controller = _chat_board_controller;
        
        [image_download setTitle:controller.title];
        
        [controller.navigationController pushViewController:image_download animated:YES];
    }
}

- (void) resendMedia:(NSString*)msgId {
    
    ChatBoardViewController * controller = _chat_board_controller;
    [controller resendMedia:msgId];
}

- (void) refreshTable:(NSString*)msgId {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:@{@"msgid":msgId}];
}

#pragma mark Alert delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    NSLog(@"Button index: %d", buttonIndex);
    
    if (buttonIndex != 1)
        return;
    
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];

    [app.acc showActivityViewer:NSLocalizedString(@"Downloading...", @"Downloading...")];

    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:user_phone, @"phone", server_id, @"mid", @"getmedia", @"cmd", nil];

    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"download_image" silent:YES];
}

#pragma mark Connection Delegate
- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    [app.acc hideActivityViewer];
    
    NSDictionary * data_dict = [dict objectForKey:@"data"];
    if (data_dict == nil)
        return;
    
    NSString * media_str = [data_dict objectForKey:@"media"];
    if (media_str == nil)
        return;
    
    NSData * data = [NSData dataFromBase64String:media_str];
    if (data == nil)
        return;
    
    UIImage * image = [UIImage imageWithData:data];
    
    if (image)
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);

}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    [app.acc hideActivityViewer];    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    UIMenuController* controller = [UIMenuController sharedMenuController];
    [controller setMenuVisible:NO animated:YES];
}

- (void) deleteCell:(NSString*) sid {
    [[ChatStorage sharedStorage] deleteMessage:sid];
    [self UpdateMessagesWithGroups:nil];
    [self reloadData];
}

- (void) tapLoadEarlierMessage {

    self.showingCount += INITIAL_SHOW_COUNT;
    [self UpdateMessagesWithGroups:nil];
    [self reloadData];
}

- (UIImage*) getPersonPhotoByPhone:(NSString*)phone {
    
    UIImage *photo = [[AvatarManager sharedInstance] getAvatarWithPhone:phone];
    
    if  (photo == nil)
        return [CommonUtil getNoAvatarImage];
    else
        return photo;
}

- (void) checkShowCells {
    
    NSArray *visibles = [self visibleCells];
    
    for (ChatCell* cell in visibles) {
        
        if(![cell isKindOfClass:[ChatCell class]])
            continue;
        
        if (cell.is_owner)
            continue;
        
        if (!cell.is_private)
            continue;

        if (cell.is_marked_seen)
            continue;
        
        if ([[TimeBomb sharedInstance] isTimeBombMessage:cell.server_id])
            continue;
        
        if ([array_seen_messages objectForKey:cell.server_id]) {
            
            [[ChatStorage sharedStorage] setMessageReadMark:cell.server_id];
            cell.is_marked_seen = 1;
            [array_seen_messages removeObjectForKey:cell.server_id];
        }
        else {
            [array_seen_messages setObject:@YES forKey:cell.server_id];
        }
        
    }
}

- (void) startObserveSeen {
    
    [array_seen_messages removeAllObjects];
}

- (void) stopObserveSeen {
    
    [array_seen_messages removeAllObjects];
}

@end
