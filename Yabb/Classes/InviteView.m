//
//  InviteView.m
//  Baycall
//
//  Created by denebtech on 09.10.12.
//
//

#import "InviteView.h"

@implementation InviteView

@synthesize invite_delegate = _invite_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_invite_delegate && [_invite_delegate respondsToSelector:@selector(didActive)])
        [_invite_delegate didActive];
    
}

@end
