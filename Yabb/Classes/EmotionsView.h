//
//  EmotionsView.h
//  speedsip
//
//  Created by denebtech on 19.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol EmotionsDelegate;

#define EMOTIONS_VIEW       0
#define ANIMATIONS_VIEW     1

@interface EmotionsView : UIView <UIScrollViewDelegate> {
    
    IBOutlet UIScrollView * parent_scroll_view;
    IBOutlet UIScrollView * emotion_scroll_view;
    IBOutlet UIScrollView * animation_scroll_view;
    IBOutlet UIView * button_view;
    IBOutlet UIPageControl * page_control;
    IBOutlet UIButton*      galleryButton;
    IBOutlet UIButton*      takeButton;
    IBOutlet UIButton*      locationButton;
    IBOutlet UIButton*      winkButton;
    
    int current_type;
    
    id<EmotionsDelegate> __weak _delegate;
    
    BOOL monitorVelocity;
    NSTimeInterval currentTime;
    NSTimeInterval prevTime;
    CGFloat currentPosition;
    CGFloat prevPosition;
    
    BOOL    _isLandscape;
    int     currentPage;
}

@property (nonatomic, weak) IBOutlet id<EmotionsDelegate> delegate;
@property (nonatomic, assign) BOOL isLandscape;

- (void) initIcons;
+ (NSDictionary *) getEmoDictionary;
+ (NSString *) getAnimationString:(int)index;
- (CGFloat) getViewHeight;
- (void) prepareRotate;

@end

@protocol EmotionsDelegate <NSObject> 

@optional
- (void) didSelectEmotion:(NSString *)emotion_key;
- (void) didSelectAnimation:(NSString *)animation_key;
- (void) didSelectPhotoFromGallery;
- (void) didSelectTakePicture;
- (void) didSelectPhotoWink;
- (void) didSelectLocation;


@end
