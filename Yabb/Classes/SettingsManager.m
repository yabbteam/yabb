//
//  SettingsManager.m
//  Yabb
//
//  Created by denebtechsaplin on 10/11/13.
//
//

#import "SettingsManager.h"

#define ENABLE_SOUNDS_KEY @"ENABLE_SOUNDS"
#define ENABLE_SOUNDS_DEFAULT_VALUE YES

#define ENABLE_VIBRATION_KEY @"ENABLE_VIBRATION"
#define ENABLE_VIBRATION_DEFAULT_VALUE YES

#define ENABLE_GROUP_SOUNDS_KEY @"ENABLE_GROUP_SOUNDS"
#define ENABLE_GROUP_SOUNDS_DEFAULT_VALUE YES

#define ENABLE_GROUP_VIBRATION_KEY @"ENABLE_GROUP_VIBRATION"
#define ENABLE_GROUP_VIBRATION_DEFAULT_VALUE YES

#define SHOW_LAST_SEEN_TIMESTAMP_KEY @"SHOW_LAST_SEEN_TIMESTAMP"
#define SHOW_LAST_SEEN_TIMESTAMP_DEFAULT_VALUE YES

#define SAVE_TO_GALLLERY_KEY @"SAVE_TO_GALLERY"
#define SAVE_TO_GALLLERY_DEFAULT_VALUE YES

#define ENABLE_SEND_SEEN_KEY @"SEND_SEEN"
#define ENABLE_SEND_SEEN_DEFAULT_VALUE YES

@implementation SettingsManager
    
+ (instancetype)sharedInstance
{
    static SettingsManager *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [self new];
    }
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        [[NSUserDefaults standardUserDefaults] registerDefaults:@{ ENABLE_SOUNDS_KEY:[NSNumber numberWithBool:ENABLE_SOUNDS_DEFAULT_VALUE],
                                                                   ENABLE_VIBRATION_KEY:[NSNumber numberWithBool:ENABLE_VIBRATION_DEFAULT_VALUE],
                                                                   ENABLE_GROUP_SOUNDS_KEY:[NSNumber numberWithBool:ENABLE_GROUP_SOUNDS_DEFAULT_VALUE],
                                                                   ENABLE_GROUP_VIBRATION_KEY:[NSNumber numberWithBool:ENABLE_GROUP_VIBRATION_DEFAULT_VALUE],
                                                                   SHOW_LAST_SEEN_TIMESTAMP_KEY:[NSNumber numberWithBool:SHOW_LAST_SEEN_TIMESTAMP_DEFAULT_VALUE],
                                                                   SAVE_TO_GALLLERY_KEY:[NSNumber numberWithBool:SAVE_TO_GALLLERY_DEFAULT_VALUE],
                                                                   ENABLE_SEND_SEEN_KEY:[NSNumber numberWithBool:ENABLE_SEND_SEEN_DEFAULT_VALUE]
                                                                   }];
    }
    return self;
}

- (BOOL)enableSounds
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_SOUNDS_KEY];
}

- (void)setEnableSounds:(BOOL)enableSounds
{
    [[NSUserDefaults standardUserDefaults] setBool:enableSounds forKey:ENABLE_SOUNDS_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_CHANGE_SETTINGS_NOTIFICATION object:ENABLE_SOUNDS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)enableVibration
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_VIBRATION_KEY];
}

- (void)setEnableVibration:(BOOL)enableVibration
{
    [[NSUserDefaults standardUserDefaults] setBool:enableVibration forKey:ENABLE_VIBRATION_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_CHANGE_SETTINGS_NOTIFICATION object:ENABLE_VIBRATION_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)showLastSeenTimestamp
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:SHOW_LAST_SEEN_TIMESTAMP_KEY];
}

- (void)setShowLastSeenTimestamp:(BOOL)showLastSeenTimestamp
{
    [[NSUserDefaults standardUserDefaults] setBool:showLastSeenTimestamp forKey:SHOW_LAST_SEEN_TIMESTAMP_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_CHANGE_SETTINGS_NOTIFICATION object:SHOW_LAST_SEEN_TIMESTAMP_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)saveToGallery
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:SAVE_TO_GALLLERY_KEY];
}

- (void)setSaveToGallery:(BOOL)saveToGallery
{
    [[NSUserDefaults standardUserDefaults] setBool:saveToGallery forKey:SAVE_TO_GALLLERY_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_CHANGE_SETTINGS_NOTIFICATION object:SAVE_TO_GALLLERY_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)enableSendSeen
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:ENABLE_SEND_SEEN_KEY];
}

- (void)setEnableSendSeen:(BOOL)enableSendSeen
{
    [[NSUserDefaults standardUserDefaults] setBool:enableSendSeen forKey:ENABLE_SEND_SEEN_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
