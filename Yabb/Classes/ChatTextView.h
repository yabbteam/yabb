//
//  ChatTextView.h
//  speedsip
//
//  Created by denebtech on 26.08.12.
//
//

#import <UIKit/UIKit.h>

@interface ChatTextView : UITextView <UITextViewDelegate> {

    IBOutlet UIView * border_view;
    IBOutlet UIImageView * chat_text_image;
    IBOutlet UIView * main_view;
    
    CGRect  originRect;
}


- (int) textDidChange:(BOOL)view_is_up;

@end
