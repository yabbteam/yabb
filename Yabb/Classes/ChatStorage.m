//
//  ChatStorage.m
//  speedsip
//
//  Created by denebtech on 30.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatStorage.h"
#import "Friends.h"
#import "SQLite.h"
#import "EmotionsView.h"
#import "NSData+Base64.h"
#import "ConnectionQueue.h"
#import "AddressCollector.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import "AppDelegate.h"
#import "Config.h"
#import "ChatRoomsViewController.h"
#import "ChatRoomsViewController.h"
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import "Notifications.h"
#import "JabberLayer.h"
#import "TimeBomb.h"
#import "AvatarManager.h"

@implementation ChatStorage

@synthesize typing = _typing;

static ChatStorage * storage = nil;

+ (ChatStorage *) sharedStorage {
    
    if (storage == nil) {
        
        storage = [ChatStorage alloc];

        storage->formatter = [[NSDateFormatter alloc] init];
//        NSArray* preferredLocalizations = [[NSBundle mainBundle] preferredLocalizations];
//        NSString* currentLocalization = [preferredLocalizations objectAtIndex:0];
//        [storage->formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:currentLocalization] autorelease]];

        // Init
        [[SQLite shared_sql] Open_DB:@"Chat.db"];
    
        if ([storage isNeedDeleteDB]) {
            NSLog(@"prev version is old, so i need to delete Chat DB.");
            
            [[SQLite shared_sql] Close_DB];
            
            [[SQLite shared_sql] Delete_DB:@"Chat.db"];
            
            [[SQLite shared_sql] Open_DB:@"Chat.db"];
            
            NSLog(@"Finished resintalling chat DB.");
        }

        // Messages
        [[SQLite shared_sql] Exec:@"CREATE TABLE IF NOT EXISTS 'chat_messages' ('id' INTEGER(8) PRIMARY KEY ASC, 'room_id' VARCHAR(255), 'server_id' VARCHAR(40), 'local_id' CHAR(16), 'original_message' VARCHAR(255), 'parsed_message' BLOB, created DATETIME, 'status' INTEGER(8), 'phone' VARCHAR(50), 'time_group' INTEGER(32), message_type INTEGER(8), media_thumb BLOB, 'media_status' INTEGER(8), 'media_asset' VARCHAR(255), 'media_name' VARCHAR(40), 'read' INTEGER(8), msg_height INTEGER(10), 'delivered' INTEGER(8), 'media_size' INTEGER(8))"];
        
        // Rooms
        [[SQLite shared_sql] Exec:@"CREATE TABLE IF NOT EXISTS 'chat_rooms' ('room_id' VARCHAR(40)  PRIMARY KEY, 'room_name' VARCHAR(255) NOT NULL, 'private' INTEGER(8), 'phones' VARCHAR(255), 'background_image' VARCHAR(255), 'enable_sound' INTEGER(8), 'enable_vibrate' INTEGER(8))"];
    
        storage->_typing = nil;
    }
    
    return storage;
}



- (NSString *)encodeString:(NSString *)str {
    
    if (str == nil)
        return nil;
    
    NSData *plainTextData = [str dataUsingEncoding:NSUTF8StringEncoding];
    return [plainTextData base64EncodedString];
}


#define THUMB_SIZE  160
#define PORTRAIT    0
#define LANDSCAPE   1

+ (CGSize) getThumbSize:(CGSize)size {
    
    float thumb_kx = size.width / size.height;
    
    int orient = LANDSCAPE;
    if (thumb_kx < 1.0) {
        orient = PORTRAIT;
        thumb_kx = 1.0 / thumb_kx;
    }
    
    CGSize thumb_size;
    
    thumb_kx = (thumb_kx < 1.33333)?1:1.33333;
    
    if (thumb_kx == 1)
        thumb_size = CGSizeMake(THUMB_SIZE / 1.33333, THUMB_SIZE / 1.33333);
    else if (orient == LANDSCAPE)
        thumb_size = CGSizeMake(THUMB_SIZE, THUMB_SIZE / thumb_kx);
    else
        thumb_size = CGSizeMake(THUMB_SIZE / thumb_kx, THUMB_SIZE);
    
    return thumb_size;
}

- (NSString *)getEncodedDictionary:(NSDictionary *)dict
{
    NSError *error = nil;
    
    if (dict == nil)
        return nil;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&error];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];//[NSString stringWithUTF8String:[data bytes]];
    
    //NSString * str = [dict JSONRepresentation];
    NSData *plainTextData = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    return [plainTextData base64EncodedString];
}

- (NSString *)decodeString:(NSString *)str {
    
    NSData * data = [NSData dataFromBase64String:str];
    NSString * decoded = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return decoded;
}

- (NSDictionary *)decodeStringToDictionary:(NSString *)str {
    NSError *error = nil;
    NSData * data = [NSData dataFromBase64String:str];
    NSDictionary *decoded = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    return decoded;
}

- (NSString *) getTimeStamp:(NSTimeInterval)time {

    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"]; //this is the sqlite's format
    NSDate *stringTime = [NSDate dateWithTimeIntervalSince1970:time];
    
    return [formatter stringFromDate:stringTime];
}

- (NSTimeInterval) getTime:(NSString *)time_stamp {

    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"]; //this is the sqlite's format

    NSDate * date = [formatter dateFromString:time_stamp];
    
    return [date timeIntervalSince1970];
}

- (NSString *) generateRandom:(int)length {

    NSString *alphabet  = @"abcdef0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:length];
    for (NSUInteger i = 0U; i < length; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return s;
}

#pragma mark -
#pragma mark Messages

- (int) countGroupMessages:(NSString*)chat_id forTimeGroup:(long long)time_group {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(room_id) as nums FROM chat_messages WHERE room_id = '%@' AND time_group = '%llu'", chat_id, time_group]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (int)countNewMessages {

    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(room_id) as nums FROM chat_messages WHERE read = 0"]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (int)countRoomNewMessages:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(room_id) as nums FROM chat_messages WHERE read = 0 AND room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (int)countNewMessagesWithEnableSound {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(chat_messages.room_id) as nums FROM chat_messages, chat_rooms WHERE chat_rooms.room_id = chat_messages.room_id AND chat_messages.read = 0 AND chat_rooms.enable_sound = 1"]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (int)countNewMessagesWithEnableVibrate {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(chat_messages.room_id) as nums FROM chat_messages, chat_rooms WHERE chat_rooms.room_id = chat_messages.room_id AND chat_messages.read = 0 AND chat_rooms.enable_vibrate = 1"]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (void)markRoomRead:(NSString*) chat_id {
    
    NSString * query = [NSString stringWithFormat:@"UPDATE chat_messages SET read = 1 WHERE room_id = '%@'", chat_id];
    [[SQLite shared_sql] Exec:query];
}

- (long long) getMaxServerId {
    
    NSString * query = [NSString stringWithFormat:@"SELECT max(server_id) as max_server_id FROM chat_messages"];
    NSArray * array = [[SQLite shared_sql] Query:query];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"max_server_id"] longLongValue];
}

- (long long) getLastServerTime {
    
    NSString * query = [NSString stringWithFormat:@"SELECT max(created) as max_created FROM chat_messages"];
    NSArray * array = [[SQLite shared_sql] Query:query];
    
    
    NSTimeInterval last_interval = 0;
    NSTimeInterval current_interval = [[NSDate date] timeIntervalSince1970];
    
    if (array != nil && [array count] > 0) {
    
        NSString * time_stamp = [[array objectAtIndex:0] objectForKey:@"max_created"];
        last_interval = [self getTime:time_stamp];
    }
    
    return (current_interval >= last_interval)?current_interval:last_interval;    
}

- (long long) addMessage:(NSString *)message withChatId:(NSString*)chat_id {
    
    NSDictionary * parsed_dict = [self Extract_Smiles:message];
    NSString * parsed_str = [self getEncodedDictionary:parsed_dict];
    NSString * encoded = [self encodeString:message];
    
    NSString * anim_text = [[ChatStorage sharedStorage] getAnimationName:message];
//    int anim_index = [[ChatStorage sharedStorage] getAnimationIndex:message];

    
    NSString* max_id = @"9999999999";//[self getMaxServerId] + 9999999999;  // Order messages fix before real 'server_id' comes
//    long long time_interval  = [[NSDate date] timeIntervalSince1970];
    long long time_interval  = [self getLastServerTime];
    long long time_group = time_interval / (long long)TIME_GROUP_INTERVAL;
    
    
    int height;
    
    if (anim_text)
        height = EMOTION_CELL_HEIGHT;
    else
        height = [self getMessageHeight:[NSDictionary dictionaryWithObjectsAndKeys:parsed_str, @"parsed_message", [NSNumber numberWithInt:0], @"message_type", nil]];
    
    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_messages (room_id, local_id, server_id, original_message, parsed_message, created, status, time_group, message_type, read, msg_height) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%d', '%llu', 0, 1, '%d')", chat_id, [self generateRandom:16], max_id, encoded, parsed_str, [self getTimeStamp:time_interval], MSG_STATUS_PENDING, time_group, height];
    [[SQLite shared_sql] Exec:query];
    [[ConnectionQueue sharedQueue] forceCycle];
    
    return time_group;
}

- (long long) addImageMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withAsset:(NSString *)asset withName:(NSString *)media_name comment:(NSString*)comment {
    
    long long time_interval  = [self getLastServerTime];
    long long time_group = time_interval / (long long)TIME_GROUP_INTERVAL;
    NSData * image_data = UIImageJPEGRepresentation(thumb, 0.85);

    CGSize thumbSize = [ChatStorage getThumbSize:thumb.size];
    CGSize labelSize = [CommonUtil getAttributedLabelSize:comment font:[UIFont systemFontOfSize:IMAGE_COMMENT_FONT_SIZE]
                     constrainedToSize:CGSizeMake(thumbSize.width-10, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    NSString* encodedComment = [self encodeString:comment];
    
    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_messages (room_id, local_id, server_id, created, status, time_group, message_type, media_thumb, media_asset, media_name, msg_height, original_message) VALUES ('%@', '%@', '%@', '%@', '%d', '%llu', '1', '%@', '%@', '%@', %d, '%@')", chat_id, local_id, local_id, [self getTimeStamp:time_interval], MSG_STATUS_IMAGE_UPLOADING, time_group, [image_data base64EncodedString], asset, media_name, (int)(thumbSize.height + labelSize.height+ 20), encodedComment];
    //NSLog(@"%@", query);
    [[SQLite shared_sql] Exec:query];
    [[ConnectionQueue sharedQueue] forceCycle];
    
    return time_group;
}

- (long long) addVideoMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withAsset:(NSString *)asset withName:(NSString *)media_name comment:(NSString*)comment {
    
    long long time_interval  = [self getLastServerTime];
    long long time_group = time_interval / (long long)TIME_GROUP_INTERVAL;
    NSData * image_data = UIImageJPEGRepresentation(thumb, 0.85);
    
    CGSize thumbSize = [ChatStorage getThumbSize:thumb.size];
    CGSize labelSize = [CommonUtil getAttributedLabelSize:comment font:[UIFont systemFontOfSize:IMAGE_COMMENT_FONT_SIZE]
                                        constrainedToSize:CGSizeMake(thumbSize.width-10, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    NSString* encodedComment = [self encodeString:comment];
    
    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_messages (room_id, local_id, server_id, created, status, time_group, message_type, media_thumb, media_asset, media_name, msg_height, original_message) VALUES ('%@', '%@', '%@', '%@', '%d', '%llu', '2', '%@', '%@', '%@', %d, '%@')", chat_id, local_id, local_id, [self getTimeStamp:time_interval], MSG_STATUS_IMAGE_UPLOADING, time_group, [image_data base64EncodedString], asset, media_name, (int)(thumbSize.height + labelSize.height+ 20), encodedComment];
    //NSLog(@"%@", query);
    [[SQLite shared_sql] Exec:query];
    [[ConnectionQueue sharedQueue] forceCycle];
    
    return time_group;
}

- (long long) addLocationMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withLocation:(NSString *)location comment:(NSString*)comment {
    
    long long time_interval  = [self getLastServerTime];
    long long time_group = time_interval / (long long)TIME_GROUP_INTERVAL;
    NSData * image_data = UIImageJPEGRepresentation(thumb, 0.85);
    
    CGSize thumbSize = [ChatStorage getThumbSize:thumb.size];
    CGSize labelSize = [CommonUtil getAttributedLabelSize:comment font:[UIFont systemFontOfSize:IMAGE_COMMENT_FONT_SIZE]
                                        constrainedToSize:CGSizeMake(thumbSize.width-10, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    NSString* encodedComment = [self encodeString:comment];
    
    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_messages (room_id, local_id, server_id, created, status, time_group, message_type, media_thumb, media_name, msg_height, original_message) VALUES ('%@', '%@', '%@', '%@', '%d', '%llu', '3', '%@', '%@', %d, '%@')", chat_id, local_id, local_id, [self getTimeStamp:time_interval], MSG_STATUS_PENDING, time_group, [image_data base64EncodedString], location, (int)(thumbSize.height + labelSize.height+ 20), encodedComment];
    //NSLog(@"%@", query);
    [[SQLite shared_sql] Exec:query];
    [[ConnectionQueue sharedQueue] forceCycle];
    
    return time_group;
}

- (NSDictionary *)getRecord:(NSString*)chat_id withIndex:(int)idx forTimeGroup:(long long)time_group {
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE room_id = '%@' AND time_group = '%llu' order by id ASC limit %d, 1", chat_id, time_group, idx];
    NSArray * array = [[SQLite shared_sql] Query:query];
    
    if (array && [array count])
        return [array objectAtIndex:0];
    return nil;
}



- (NSArray *)getRecords:(NSString*)chat_id forTimeGroup:(long long)time_group {
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE room_id = '%@' AND time_group = '%llu' order by id ASC", chat_id, time_group];
    return [[SQLite shared_sql] Query:query];
}

- (NSArray *) getAllRecords:(NSString*)chat_id {

    NSString * query = [NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE room_id = '%@' ORDER BY id DESC LIMIT 0, 30", chat_id];
    return [[SQLite shared_sql] Query:query];

}

- (NSDictionary *) getMessage:(NSString*)chat_id withIndex:(int)idx forTimeGroup:(long long)time_group {
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE room_id = '%@' AND time_group = '%llu' order by id ASC limit %d, 1", chat_id, time_group, idx];
    NSArray * array = [[SQLite shared_sql] Query:query];

    NSDictionary * dict = [array objectAtIndex:0];
    
    NSString * parsed_str = [dict objectForKey:@"parsed_message"];
    
    NSDictionary * parsed_message = [self decodeStringToDictionary:parsed_str];
    return parsed_message;
}

- (NSArray *)getLocalMessages {
    
    NSString* query = [NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE status = %d order by id ASC", MSG_STATUS_PENDING];
    return [[SQLite shared_sql] Query:query];
}

- (NSArray *)getUnconfirmedMessages {
    
    NSString* query = [NSString stringWithFormat:@"SELECT server_id FROM chat_messages WHERE status = %d", MSG_STATUS_SENT];
    return [[SQLite shared_sql] Query:query];
}

- (NSArray *)getDeliveredMessages {
    
    return [[SQLite shared_sql] Query:@"SELECT server_id FROM chat_messages WHERE delivered = 1"];
}

- (NSDictionary *) getLastMessage:(NSString*) chat_id {
    
    NSString * query;
    
    if (chat_id != nil) {
        query = [NSString stringWithFormat:@"SELECT message_type, parsed_message, server_id, created, media_asset, phone FROM chat_messages WHERE room_id = '%@' order by id DESC limit 0, 1", chat_id];
    }
    else {
        query = [NSString stringWithFormat:@"SELECT message_type, parsed_message, server_id, created, media_asset, phone FROM chat_messages order by id DESC limit 0, 1"];
    }
    NSArray * array = [[SQLite shared_sql] Query:query];
    
    if (array == nil || [array count] == 0)
        return nil;
    
    NSDictionary * dict = [array objectAtIndex:0];
    NSMutableDictionary * result = [NSMutableDictionary dictionary];
    
    BOOL is_own = [dict objectForKey:@"phone"] == nil;
                   
    NSTimeInterval timestamp = [[ChatStorage sharedStorage] getTime:[dict objectForKey:@"created"]];
    NSDate * created = [NSDate dateWithTimeIntervalSince1970:timestamp];

    NSString * created_str = [[ChatStorage sharedStorage] dateToString:created format:@"dd/MM/YY"];
    NSString * current_str = [[ChatStorage sharedStorage] dateToString:[NSDate date] format:@"dd/MM/YY"];
    
    [result setObject:@"" forKey:@"text"];
    
    if ([created_str isEqualToString:current_str])
        [result setObject:[[ChatStorage sharedStorage] dateToString:created format:@"HH:mm"] forKey:@"time"];
    else
        [result setObject:created_str forKey:@"time"];

    if ([[dict objectForKey:@"message_type"] intValue] == 1) {
        
        NSString* serverId = [dict objectForKey:@"server_id"];
        if ([[TimeBomb sharedInstance] isTimeBombMessage:serverId]) {
            if (is_own)
                [result setObject:NSLocalizedString(@"sent a photo wink", @"sent a photo wink") forKey:@"text"];
            else
                [result setObject:NSLocalizedString(@"sent you a photo wink", @"sent you a photo wink") forKey:@"text"];
        }
        else {
            if (is_own)
                [result setObject:NSLocalizedString(@"sent a photo", @"sent a photo") forKey:@"text"];
            else
                [result setObject:NSLocalizedString(@"sent you a photo", @"sent you a photo") forKey:@"text"];
        }
        [result setObject:@YES forKey:@"is_photo"];
    }
    else if ([[dict objectForKey:@"message_type"] intValue] == 2) {
        
        if (is_own)
            [result setObject:NSLocalizedString(@"sent a video", @"sent a video") forKey:@"text"];
        else
            [result setObject:NSLocalizedString(@"sent you a video", @"sent you a video") forKey:@"text"];
        
        [result setObject:@YES forKey:@"is_photo"];
    }
    else if ([[dict objectForKey:@"message_type"] intValue] == 3) {
        
        NSString * parsed_str = [dict objectForKey:@"parsed_message"];
        NSDictionary * parsed_message = [self decodeStringToDictionary:parsed_str];
        NSString * str = [[parsed_message objectForKey:@"parsed_message"] stringByReplacingOccurrencesOfString:SMILE_REPLACE withString:@":) "];
        
        if (is_own)
            [result setObject:NSLocalizedString(@"sent a location", @"sent a location") forKey:@"text"];
        else
            [result setObject:[NSString stringWithFormat:NSLocalizedString(@"is at %@", @"is at %@"), str] forKey:@"text"];
    }
    else {
        NSString * parsed_str = [dict objectForKey:@"parsed_message"];
        
        if (parsed_str) {
            NSDictionary * parsed_message = [self decodeStringToDictionary:parsed_str];
            NSString * str = [[parsed_message objectForKey:@"parsed_message"] stringByReplacingOccurrencesOfString:SMILE_REPLACE withString:@":) "];
            [result setObject:str forKey:@"text"];
        }
    }
    [result setObject:[dict objectForKey:@"server_id"] forKey:@"server_id"];
    
    return result;
}

- (void) resetDeliveredMessages {
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_messages SET delivered = 2 WHERE delivered = 1"]];
    
}

- (NSString *) dateToString:(NSDate *)date format:(NSString *)format {
    
    [formatter setDateFormat:format];
    
    NSString * str = [formatter stringFromDate:date];
    
    return str;
}

- (NSString *) dateToStrTime:(NSDate *)date {

    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    return [formatter stringFromDate:date];
}

- (NSArray *) getTimeGroups:(NSString*)chat_id count:(int)count {
    
    NSArray * messages = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT created, delivered, message_type, media_thumb, media_status, msg_height, parsed_message, phone, read, server_id, status, original_message, media_size FROM chat_messages WHERE room_id = '%@' ORDER BY id DESC LIMIT 0, %d", chat_id, count]];
    
    NSMutableArray * days = [NSMutableArray array];

    for (int i = (int)[messages count] - 1; i >= 0; i--) {
    
//    for (NSDictionary * msg in messages) {
  
        NSDictionary * msg = [messages objectAtIndex:i];
        
        NSTimeInterval timestamp = [self getTime:[msg objectForKey:@"created"]];
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:timestamp];

        NSString * str = [self dateToString:date format:@"d MMM Y"];
        
        BOOL day_exist = NO;
        NSDictionary * dict;
        for (dict in days) {
            if ([[dict objectForKey:@"day"] isEqualToString:str]) {
                day_exist = YES;
                break;
            }
        }
        
        if (day_exist) {
            NSMutableArray * messages = [dict objectForKey:@"messages"];
            [messages addObject:msg];
        }
        else {
            NSMutableArray * messages = [NSMutableArray arrayWithObject:msg];
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:str, @"day", messages, @"messages", nil];
            
            [days addObject:dict];
        }
    }

    return days;    
    /*
    
    long long min_group = LONG_MAX;
    for (NSDictionary * dict in messages) {
        
        long long time_group = [[dict objectForKey:@"time_group"] longLongValue];
        
        if (time_group < min_group)
            min_group = time_group;
    }
    
    return [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT time_group FROM chat_messages WHERE room_id = '%llu' AND time_group >= '%llu' GROUP BY time_group", chat_id, min_group]];    */
}


- (NSDictionary *)getRecordByServerId:(NSString*)server_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE server_id = '%@'", server_id]];
    
    if (array == nil || [array count] == 0)
        return nil;
    
    return [array objectAtIndex:0];
}


- (void) Put_Unique_Number:(NSNumber *)num toArray:(NSMutableArray *)array {
    
    for (NSNumber * unique in array) {
        if ([unique isEqualToNumber:num])
            return;
    }
    
    [array addObject:num];
}

//#warning Encode room name
- (NSArray *) syncMessages:(NSDictionary *)dict {
    
//    BOOL reload_data = NO;
    
    if (dict == nil)
        return nil;
    
    NSArray * typing = [dict objectForKey:@"typing"];
    if (typing && [typing isKindOfClass:[NSArray class]] && [typing count] != 0) {
        
        for (NSDictionary * d in typing) {
            NSString* chat_id = [d objectForKey:@"chatid"];
            NSString * type_str = [[ChatStorage sharedStorage] decodeString:[d objectForKey:@"contacts"]];
            [app.chatRoomsViewController.chat_board setTyping:chat_id withString:type_str];
        }
    }
    
    NSArray * friends = [dict objectForKey:@"friends"];
    if (friends && [friends isKindOfClass:[NSArray class]] && [friends count] != 0) {
        NSLog(@"Friends: %@", [dict objectForKey:@"friends"]);
    
        [[Friends SharedFriends] updateOnline:friends];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"force":@YES}];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
    }
    
    // Create chat room if needed
    NSArray * chat_rooms = [dict objectForKey:@"chats"];
    if (chat_rooms && [chat_rooms isKindOfClass:[NSArray class]] && [chat_rooms count] != 0) {
        
        for (NSDictionary * d in chat_rooms) {
            
            NSString* chat_id = [d objectForKey:@"chatid"];
            BOOL private = [[d objectForKey:@"private"] boolValue];
            NSString * phone;
            NSString * chat_name = nil;
            NSArray * phones;
            
            if (private) {
                phone = [d objectForKey:@"phone"];

                if (phone == nil || [phone isEqualToString:@""]) {
                    
                    phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
                }
                
                chat_name = [[AddressCollector sharedCollector] Get_Name_By_Phone:[[AddressCollector sharedCollector] clearedPhoneNumber:phone withPlus:NO] withPlus:NO];
                if (chat_name == nil)
                    chat_name = phone;
            
                phones = [NSArray arrayWithObject:phone];
            }
            else {
                chat_name = [[ChatStorage sharedStorage] decodeString:[d objectForKey:@"name"]];
                
                phones = [d objectForKey:@"phones"];
                
                if (phones && [phones count])
                    phone = [phones componentsJoinedByString:@":"];
                else
                    phone = @"";
            }
            NSArray * exist = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
            if (exist == nil || [exist count] == 0) {

                if (chat_name) {
                    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_rooms (room_id, room_name, private, phones, enable_sound, enable_vibrate) VALUES ('%@', '%@', '%d', '%@', 1, 1)", chat_id, chat_name, private, phone];
                    [[SQLite shared_sql] Exec:query];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"chataid":chat_id}];
                }
            }
            else {
                NSString * old_room_name = [[exist objectAtIndex:0] objectForKey:@"room_name"];
                NSArray * old_phones = [[[exist objectAtIndex:0] objectForKey:@"phones"] componentsSeparatedByString:@":"];
                
                if ([old_room_name isEqualToString:chat_name] == NO || [phones isEqualToArray:old_phones] == NO ) {
                    
                    NSString * new_phones = phones?[phones componentsJoinedByString:@":"]:@"";
                    
                    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_rooms SET room_name = '%@', phones = '%@' WHERE room_id = '%@'", chat_name, new_phones, chat_id]];
                
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"chataid":chat_id}];
                }
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:nil];
    }
    
    // *****************
    // MESSAGES
    // *****************
    
    NSMutableArray * updated_time_groups = [NSMutableArray array];
    
    // Save posted messages
    NSArray * posted_messages = [dict objectForKey:@"posted"];
    if (posted_messages && [posted_messages isKindOfClass:[NSArray class]] && [posted_messages count] != 0) {
        
        for (NSDictionary * d in posted_messages) {
            
            NSString* server_id = [d objectForKey:@"messageid"];
            NSString * local_id = [d objectForKey:@"key"];
            long long time = [[d objectForKey:@"date"] longLongValue];
            NSString * time_stamp = [[ChatStorage sharedStorage] getTimeStamp:time];
            long long time_group = time / (long long)TIME_GROUP_INTERVAL;
            
            NSArray * exist = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE local_id = '%@'", local_id]];
            if (exist && [exist count]) {
            
                NSString * tg = [[exist objectAtIndex:0] objectForKey:@"time_group"];
                [self Put_Unique_Number:[NSNumber numberWithLongLong:[tg longLongValue]] toArray:updated_time_groups];
            }
            
            NSString * query = [NSString stringWithFormat:@"UPDATE chat_messages SET server_id = '%@', local_id = '', status = '%d', created = '%@', time_group = '%llu' WHERE local_id = '%@'", server_id, MSG_STATUS_SENT, time_stamp, time_group, local_id];

            [[SQLite shared_sql] Exec:query];
            
            [self Put_Unique_Number:[NSNumber numberWithLongLong:time_group] toArray:updated_time_groups];
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:@{@"force":@YES}];
        //[[ConnectionQueue sharedQueue] forceCycle];
    }
    
    // Get new messages
    NSArray * new_messages = [dict objectForKey:@"new"];
    if (new_messages && [new_messages isKindOfClass:[NSArray class]] && [new_messages count]) {
        
        for (NSDictionary * d in new_messages) {
        
            NSString* chat_id = [d objectForKey:@"chatid"];
            NSString* message_id = [d objectForKey:@"messageid"];
        
            NSArray * exist = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE server_id = '%@'", message_id]];
            if (exist == nil || [exist count] == 0) {
                
                NSString * message = [d objectForKey:@"message"];
                NSString * decoded_message = [self decodeString:message];
                long long time = [[d objectForKey:@"date"] longLongValue];
                long long time_group = time / (long long)TIME_GROUP_INTERVAL;
                NSString * timestamp = [[ChatStorage sharedStorage] getTimeStamp:time];
                NSString * parsed_message = nil;
                NSString* location = [d objectForKey:@"location"];
                
                NSString * phone = [d objectForKey:@"phone"];
                if (phone == nil)
                    phone = @"";
                
                NSString * encoded_thumb = [d objectForKey:@"mthumb"]?[d objectForKey:@"mthumb"]:@"";
                NSString* media_url = [d objectForKey:@"url"];
                int media_size = [[d objectForKey:@"media_size"] intValue];
                if (media_url == nil)
                    media_url = @"";
                int message_type = 0;
                if (![encoded_thumb isEqualToString:@""]) {
                    NSString* converted = [media_url lowercaseString];
                    if ([converted hasSuffix:@".jpg"] || [converted hasSuffix:@".png"])
                        message_type = 1;
                    else
                        message_type = 2;
                }
                
                if (location != nil)
                    message_type = 3;
                
                media_url = [self encodeString:media_url];
                int sdelivered = [[d objectForKey:@"sdelivered"] intValue];
                int sread = [[d objectForKey:@"sread"] intValue];
                
                NSString * animation_text = [self getAnimationName:decoded_message];
//                int animation_index = [self getAnimationIndex:decoded_message];
                
                if (message_type == 1/* || animation_text*/)
                    parsed_message = @"";
                else
                    parsed_message = [self getEncodedDictionary:[self Extract_Smiles:decoded_message]];
                
                int height;
                
                if (animation_text)
                    height = EMOTION_CELL_HEIGHT;
                else {
                    
                    if (message_type == 1 || message_type == 2 || message_type == 3) {
//                        NSString * image_str = [self decodeString:encoded_thumb];
                        UIImage * image = [UIImage imageWithData:[NSData dataFromBase64String:encoded_thumb]];
                        CGSize thumbSize = [ChatStorage getThumbSize:image.size];
                        CGSize labelSize = [CommonUtil getAttributedLabelSize:message font:[UIFont systemFontOfSize:IMAGE_COMMENT_FONT_SIZE]
                                                            constrainedToSize:CGSizeMake(thumbSize.width-10, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
                        height = thumbSize.height + labelSize.height + 24;
                    }
                    else {
                        height = [self getMessageHeight:[NSDictionary dictionaryWithObjectsAndKeys:parsed_message, @"parsed_message", [NSNumber numberWithInt:message_type], @"message_type", nil]];
                    }
                }
                NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_messages (room_id, server_id, original_message, parsed_message, created, status, phone, time_group, message_type, media_thumb, media_asset, read, msg_height, delivered, media_size) VALUES ('%@', '%@', '%@', '%@', '%@', '%d', '%@', '%llu', '%d', '%@', '%@', '%d', '%d', '%d', '%d')", chat_id, message_id, message, parsed_message, timestamp, sdelivered?MSG_STATUS_CONFIRMED:MSG_STATUS_SENT, phone, time_group, message_type, encoded_thumb, media_url, sread?1:0, height, sdelivered?2:0, media_size];
                [[SQLite shared_sql] Exec:query];
                
                if (location != nil)
                     [self setMediaName:location forServerId:message_id];

                [self Put_Unique_Number:[NSNumber numberWithLongLong:time_group] toArray:updated_time_groups];
                
                [app.chatRoomsViewController.chat_board onNewMessageReceived:message_id];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"added_messages" object:nil];
                
                [[JabberLayer SharedInstance] sendMessageDelivered:message_id];
            }
            else {
                NSString * query = [NSString stringWithFormat:@"UPDATE chat_messages SET status = %d WHERE server_id = '%@'", MSG_STATUS_SENT, message_id];
                [[SQLite shared_sql] Exec:query];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
            }
        }
        [self bk_performBlock:^(id sender){
                [app.chatRoomsViewController.chat_board clearTyping];
            } afterDelay:0.3f];
    }
    
    // Get delivered
    NSArray * delivered = [dict objectForKey:@"rcvs"];
    if (delivered && [delivered isKindOfClass:[NSArray class]] && [delivered count]) {
        
        for (NSString * dlvr in delivered) {
            NSString* cid = dlvr;
            NSString * query = [NSString stringWithFormat:@"UPDATE chat_messages SET status = %d WHERE server_id = '%@'", MSG_STATUS_CONFIRMED, cid];
            [[SQLite shared_sql] Exec:query];
            
            NSArray * exist = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE server_id = '%@'", cid]];
            if (exist && [exist count] != 0) {
                long long time_group = [[[exist objectAtIndex:0] objectForKey:@"time_group"] longLongValue];
                [self Put_Unique_Number:[NSNumber numberWithLongLong:time_group] toArray:updated_time_groups];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
    }
    
    // Get displayed
    NSArray * displayed = [dict objectForKey:@"drs"];
    if (displayed && [displayed isKindOfClass:[NSArray class]] && [displayed count]) {
        
        for (NSString * dlvr in displayed) {
            NSString* cid = dlvr;
            NSString * query = [NSString stringWithFormat:@"UPDATE chat_messages SET delivered = 2 WHERE server_id = '%@'", cid];
            [[SQLite shared_sql] Exec:query];

            NSArray * exist = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_messages WHERE server_id = '%@'", cid]];
            if (exist && [exist count] != 0) {
                long long time_group = [[[exist objectAtIndex:0] objectForKey:@"time_group"] longLongValue];
                [self Put_Unique_Number:[NSNumber numberWithLongLong:time_group] toArray:updated_time_groups];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
    }
    
    return [updated_time_groups count]?updated_time_groups:nil;
}

- (void) deleteMessage:(NSString*)server_id {
    
    NSString * query = [NSString stringWithFormat:@"DELETE FROM chat_messages WHERE server_id = '%@'", server_id];
    [[SQLite shared_sql] Exec:query];
    
}

- (void) deleteAllMessages:(NSString*)room_id {
    
    NSString * query = [NSString stringWithFormat:@"DELETE FROM chat_messages WHERE room_id = '%@'", room_id];
    [[SQLite shared_sql] Exec:query];
    
}

- (void) resetAccount {

    [[ConnectionQueue sharedQueue] stop];
    
    [app unregisterSipAccount];
    
    [app.tabBarController setSelectedIndex:0];

    [[SQLite shared_sql] Exec:@"DELETE FROM chat_messages"];
    [[SQLite shared_sql] Exec:@"DELETE FROM chat_rooms"];

    [self clearAccount];
}

- (void) clearAccount {
    
    [[AddressCollector sharedCollector] clearAbookCache];
    [[TimeBomb sharedInstance] clear];
    [[AvatarManager sharedInstance] clear];
}

#pragma mark -
#pragma Chat Rooms

- (NSString *) addChatRoom:(NSString *)chat_name withChatId:(NSString*)chat_id withPhones:(NSArray *)phones {
    
    if (chat_id == 0 || phones == nil)
        return nil;
    
    // Compose phones
    NSString * composed_phones = [phones count]?[phones componentsJoinedByString:@":"]:@"";
    
    NSArray * result = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    if (result && [result count] > 0) {   // chat_id exists already
        
        [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_rooms SET phones = '%@' WHERE room_id = '%@'", composed_phones, chat_id]];
        
        NSDictionary * chat_room = [result objectAtIndex:0];
        
        return [chat_room objectForKey:@"room_name"];
    }
    int private = [[JabberLayer SharedInstance] isGroupChat:chat_id]?0:1;
    NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_rooms (room_id, room_name, private, phones, enable_sound, enable_vibrate) VALUES ('%@', '%@', %d, '%@', 1, 1)", chat_id, chat_name, private, composed_phones];
    [[SQLite shared_sql] Exec:query];

    return nil;
}

- (NSString *) getRoomName:(NSString*)chat_id {
    
    NSArray * result = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (result == nil || [result count] == 0)
        return nil;
    
    return [self getRoomNameByRoomDict:[result objectAtIndex:0]];
}

- (NSString*) getRoomNameByRoomDict:(NSDictionary*) room_dict {
    
    NSString* private = [room_dict objectForKey:@"private"];
    if (private == nil || [private isEqualToString:@"0"])
        return [room_dict objectForKey:@"room_name"];
    
    NSString* phone =  [room_dict objectForKey:@"phones"];
    return [[AddressCollector sharedCollector] Get_Name_By_Phone:phone withPlus:YES];
}

- (void) renameRoom:(NSString*)chat_id withName:(NSString *)name {
    
    if (name) {
        [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_rooms SET room_name = '%@' WHERE room_id = '%@'", name, chat_id]];
    }
    
}

- (NSString *) getRoomPhone:(NSString*)chat_id {
    
    NSArray * result = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (result == nil || [result count] == 0)
        return nil;
    
    return [[result objectAtIndex:0] objectForKey:@"phones"];
}


- (BOOL) getRoomPrivate:(NSString*)chat_id {

    NSArray * result = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    if (result == nil || [result count] == 0)
        return NO;
    
    return [[[result objectAtIndex:0] objectForKey:@"private"] boolValue];
}

- (NSArray *) getChatRooms {
    
    NSMutableArray * m_array = [NSMutableArray arrayWithArray:[[SQLite shared_sql] Query:@"SELECT * FROM chat_rooms"]];
    
    for (NSMutableDictionary * m_dict in m_array) {
        
        
        NSString* chat_id = [m_dict objectForKey:@"room_id"];
        
        NSString * query = [NSString stringWithFormat:@"SELECT message_type, parsed_message, created FROM chat_messages WHERE room_id = '%@' order by id DESC limit 0, 1", chat_id];
        NSArray * array = [[SQLite shared_sql] Query:query];
        
        if (array && [array count]) {
            
            NSDictionary * message = [array objectAtIndex:0];
            NSTimeInterval timestamp = [[ChatStorage sharedStorage] getTime:[message objectForKey:@"created"]];

            [m_dict setObject:[NSNumber numberWithLongLong:timestamp] forKey:@"order_by"];
        }
        else {
            [m_dict setObject:[NSNumber numberWithLongLong:0] forKey:@"order_by"];
        }
        
        NSString* roomName = [self getRoomNameByRoomDict:m_dict];
        [m_dict setObject:roomName forKey:@"room_name"];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order_by" ascending:NO];
    
    [m_array sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    
    return m_array;
}

- (NSDictionary *) getChatRoom:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return nil;
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionaryWithDictionary:[array objectAtIndex:0]];
    [ret_dict setObject:[self getRoomNameByRoomDict:ret_dict] forKey:@"room_name"];
    
    return ret_dict;
}

- (NSString*) getPrivateRoomByPhone:(NSString *)phone {
    
    NSArray * result = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms WHERE private = 1"]];
    if (result == nil || [result count] == 0)
        return nil;
    
    for (NSDictionary * dict in result) {
        
        NSString * composed_phones = [dict objectForKey:@"phones"];
        if (composed_phones == nil || [composed_phones isEqualToString:@""])
            continue;

        if ([composed_phones isEqualToString:phone])
            return [dict objectForKey:@"room_id"];
    }
    
    return nil;
}

- (void) deleteChatRoom:(NSString*)chat_id {
    
    NSString * query = [NSString stringWithFormat:@"DELETE FROM 'chat_rooms' WHERE room_id = '%@'", chat_id];
    [[SQLite shared_sql] Exec:query];    

    NSString * query2 = [NSString stringWithFormat:@"DELETE FROM 'chat_messages' WHERE room_id = '%@'", chat_id];
    [[SQLite shared_sql] Exec:query2];

    [[JabberLayer SharedInstance] removeGroupChat:chat_id];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:[NSString stringWithFormat:SAVE_TEMP_TEXT_FORMAT, chat_id]];
    [prefs synchronize];
}

- (void) setRoomBackground:(NSString *)image_name forRoom:(NSString*)chat_id {
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE 'chat_rooms' SET background_image = '%@' WHERE room_id = '%@'", image_name?image_name:@"", chat_id]];
}

- (NSString *) getRoomBackground:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT background_image FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return nil;
    
    return [[array objectAtIndex:0] objectForKey:@"background_image"];
}

- (void) appendNewPhoneNumberToGroupChat:(NSString*)chat_id phone:(NSString*)phone {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT phones FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return;
    
    NSString* phones = [[array objectAtIndex:0] objectForKey:@"phones"];
    if (phones == nil)
        phones = @"";
    
    NSRange range = [phones rangeOfString:phone];
    if (range.location != NSNotFound)
        return;
    
	NSMutableArray *components = [NSMutableArray arrayWithArray:[phones componentsSeparatedByString:@":"]];
    [components addObject:phone];
    NSString* new_phones = [components componentsJoinedByString:@":"];
    if ([new_phones characterAtIndex:0] == ':')
        new_phones = [new_phones substringFromIndex:1];
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE 'chat_rooms' SET phones = '%@' WHERE room_id = '%@'", new_phones, chat_id]];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"chataid":chat_id}];
}

- (void) removePhoneNumberFromGroupChat:(NSString*)chat_id phone:(NSString*)phone {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT phones FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return;
    
    NSString* phones = [[array objectAtIndex:0] objectForKey:@"phones"];
    NSRange range = [phones rangeOfString:phone];
    if (range.location == NSNotFound)
        return;
    
	NSMutableArray *components = [NSMutableArray arrayWithArray:[phones componentsSeparatedByString:@":"]];
    int index = 0;
    for (NSString* s in components) {
        if ([s isEqualToString:phone]) {
            break;
        }
        index++;
    }
    if (index >= [components count])
        return;
    
    [components removeObjectAtIndex:index];
    NSString* new_phones = [components componentsJoinedByString:@":"];
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE 'chat_rooms' SET phones = '%@' WHERE room_id = '%@'", new_phones, chat_id]];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"chataid":chat_id}];
}

#pragma mark -
#pragma mark Parse Text

- (int) getMessageHeight:(NSDictionary *)msg_dict {
    
    if ([[msg_dict objectForKey:@"message_type"] intValue] == 1) {
        return 96;
    }
    else {
        NSString * encoded_message = [msg_dict objectForKey:@"parsed_message"];
        NSDictionary * decoded = [[ChatStorage sharedStorage] decodeStringToDictionary:encoded_message];
        NSString * parsed = [decoded objectForKey:@"parsed_message"];
        
        UIFont * font = [UIFont systemFontOfSize:CHAT_FONT_SIZE];
        CGSize theSize = [CommonUtil getAttributedLabelSize:parsed font:font constrainedToSize:CGSizeMake(CHAT_BUBBLE_WIDTH, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];

        return theSize.height + 33;
    }
}
    
- (int) smileWidth {
    
    CGSize size = [CommonUtil getTextSize:SMILE_REPLACE font:[UIFont systemFontOfSize:CHAT_FONT_SIZE] constrainedToSize:CGSizeMake(FLT_MAX, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
//    return 20;
    
    return size.width;
}

- (int) textWidth:(NSString *)str {
    
    CGSize size = [CommonUtil getTextSize:str font:[UIFont systemFontOfSize:CHAT_FONT_SIZE] constrainedToSize:CGSizeMake(FLT_MAX, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    return size.width;
}


- (BOOL) textInBlock:(NSString *)str {
    
    CGSize size = [CommonUtil getAttributedLabelSize:str font:[UIFont systemFontOfSize:CHAT_FONT_SIZE] constrainedToSize:CGSizeMake(CHAT_BUBBLE_WIDTH, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    if (size.height > (CHAT_FONT_SIZE + 4.0))
        return NO;
    
    return YES;
}

- (BOOL) isEmotionTemplate:(NSString *)str {
    
    if (str == nil || [str isEqualToString:@""])
        return NO;
    
    NSDictionary * emo_dict = [EmotionsView getEmoDictionary];
    
    for (NSString * key in emo_dict) {
        
        if ([key isEqualToString:str])
            return YES;
        
    }
    
    return NO;
}
- (int) Get_Smile_Length:(NSString *)text fromPosition:(int)from_pos {
    
    NSString * alp;
    NSMutableString * template_str = [NSMutableString string];
    
    int pos = from_pos + 1;
    while (pos < [text length]) {
        
        alp = [text substringWithRange:NSMakeRange(pos, 1)];
        if ([alp isEqualToString:@"}"]) {
            
            if ([self isEmotionTemplate:template_str]) {
                NSLog(@"Template: %@", template_str);
                return pos - from_pos + 1;
            }
            
            return 0;
        }
        else {
            [template_str appendString:alp];
        }
        pos++;
    }
    
    return 0;
}

- (NSString *) getSmileFileName:(NSString *)template_name {
    
    NSDictionary * emo_dict = [EmotionsView getEmoDictionary];
    
    for (NSString * key in emo_dict) {
        
        if ([key isEqualToString:template_name])
            return [emo_dict objectForKey:key];
        
    }

    return nil;
}

- (NSDictionary *) Extract_Smiles:(NSString *)text {
    
    NSMutableArray * emotions = [NSMutableArray array];
    NSMutableArray * message_rows = [NSMutableArray array];
    
    NSArray * strings = [text componentsSeparatedByString:@"\n"];
    CGFloat smileOffsetX = 0.0f;
    CGFloat smileOffsetY = 0.0f;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        smileOffsetX =  -3.0f;
        smileOffsetY = 3.0f;
    }
    else {
        smileOffsetX = -4.0f;
        smileOffsetY = 4.0f;
    }
    
    int realLineCount = 0;
    for (__strong NSString * s in strings) {
    
        if ([s isEqualToString:@""]) {
            [message_rows addObject:s ];
            realLineCount++;
            [message_rows addObject:@"\n"];
            continue;
        }
        
        int pos = 0;
        int last_space = 0;
        NSString * linear_str = @"";
        while (pos < [s length]) {
            
            NSString * alp = [s substringWithRange:NSMakeRange(pos, 1)];
            NSString * template_name;
            bool is_a_smile = NO;
            int pos_x = 0;
            int length;
            
            if ([alp isEqualToString:@" "]) {
                last_space = pos;
            }
            if ([alp isEqualToString:@"{"]) {
                length = [self Get_Smile_Length:s fromPosition:pos];
                
                
                if (length) {
                    template_name = [s substringWithRange:NSMakeRange(pos+1, length-2)];

                    is_a_smile = YES;   // Need to draw a smile icon
                    pos_x = [self textWidth:[linear_str substringWithRange:NSMakeRange(0, pos)]];
                    s = [s stringByReplacingCharactersInRange:NSMakeRange(pos, length) withString:SMILE_REPLACE];
                    pos += ([SMILE_REPLACE length] - 1);
                    last_space = pos - [SMILE_REPLACE length];
                }
            }
            
            pos++;
            
            linear_str = [s substringWithRange:NSMakeRange(0, pos)];
            
            if ([self textInBlock:linear_str] == NO) {
                
                if (last_space > 0)
                    pos = last_space+1;
                else
                    pos = pos-1;
                
                linear_str = [s substringWithRange:NSMakeRange(0, pos)];
                
                [message_rows addObject:linear_str ];
                realLineCount++;
                s = [s stringByReplacingCharactersInRange:NSMakeRange(0, pos) withString:@""];
                pos = 0;
                pos_x = 0;
                linear_str = @"";
                last_space = 0;
            }
            
            if (is_a_smile) {

                int y_offset = 0;
                
                if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0)
                    y_offset = 2;
                
                NSString * file_name = [self getSmileFileName:template_name];            
                NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:(pos_x + smileOffsetX)], @"pos_x", [NSNumber numberWithInt:(realLineCount * (CHAT_FONT_SIZE + smileOffsetY) + y_offset)], @"pos_y", file_name, @"file_name", [NSNumber numberWithInt:[self smileWidth]], @"width", [NSNumber numberWithInt:[self smileWidth]], @"height", nil];
                [emotions addObject:dict];
                
                if (pos > [SMILE_REPLACE length])
                    last_space = pos-1;
            }
            
        }
    
    
        if ([linear_str isEqualToString:@""] == NO) {
            [message_rows addObject:linear_str ];
            realLineCount++;
            [message_rows addObject:@"\n"];
        }
        
    }
    
//    NSString * parsed_message = [message_rows componentsJoinedByString:@"\n"];
    NSString * parsed_message = [message_rows componentsJoinedByString:@""];
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:emotions, @"smiles", parsed_message, @"parsed_message", nil];
    
    return dict;
}

- (NSString *) getAnimationName:(NSString *) str {
    
    
    if (str == nil || [str length] < 8)
        return nil;
    
    NSString * first = [str substringWithRange:NSMakeRange(0, 1)];
    NSString * last = [str substringWithRange:NSMakeRange([str length] - 1, 1)];
    
    if ( ([first isEqualToString:@"<"] == NO) || ([last isEqualToString:@">"] == NO) )
        return nil;
    
    NSString * key = [str substringWithRange:NSMakeRange(1, [str length] - 2)];
    NSArray * part = [key componentsSeparatedByString:@"-"];
    
    if (part  == nil || [part count] != 2)
        return nil;
    
    NSString * p1 = [part objectAtIndex:0];
    NSString * p2 = [part objectAtIndex:1];
    
    if ([p1 isEqualToString:@"ANIM"] == NO)
        return nil;
    
    return p2;
}

- (int) getAnimationIndex:(NSString *) str {
    
    if (str == nil || [str length] < 8)
        return 0;
    
    NSString * first = [str substringWithRange:NSMakeRange(0, 1)];
    NSString * last = [str substringWithRange:NSMakeRange([str length] - 1, 1)];
    
    if ( ([first isEqualToString:@"<"] == NO) || ([last isEqualToString:@">"] == NO) )
        return 0;
    
    NSString * key = [str substringWithRange:NSMakeRange(1, [str length] - 2)];
    NSArray * part = [key componentsSeparatedByString:@"-"];
    
    if (part  == nil || [part count] != 2)
        return 0;
    
    NSString * p1 = [part objectAtIndex:0];
    NSString * p2 = [part objectAtIndex:1];
    
    if ([p1 isEqualToString:@"ANIM"] == NO)
        return 0;
    
    return [p2 intValue];
}


- (void) imageFailed:(id)target failed:(SEL)failed_callback {
    
    if (target && failed_callback && [target respondsToSelector:failed_callback])
        [target performSelector:failed_callback];
    
}

- (void) getVideo:(NSString*)server_id target:(id)target done:(SEL)done_callback failed:(SEL)failed_callback {
    
    NSLog(@"Get record for %@", server_id);
    NSDictionary * record = [self getRecordByServerId:server_id];
    if (record == nil) {
        if (target && failed_callback && [target respondsToSelector:failed_callback])
            [target performSelector:failed_callback];
        return;
    }

    NSString * media_name = [record objectForKey:@"media_name"];
    if (media_name == nil || [media_name isEqualToString:@""]) {
        
        if (target && done_callback && [target respondsToSelector:done_callback])       // Does not exist
            [target performSelector:done_callback withObject:nil];
        return;
    }
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:media_name];
    NSURL *url = [NSURL fileURLWithPath:fullPath];
    
    if (target && done_callback && [target respondsToSelector:done_callback]) {
        [target performSelector:done_callback withObject:url ];
    }
}

// Images
- (void) getImage:(NSString*)server_id target:(id)target done:(SEL)done_callback failed:(SEL)failed_callback {
    
    NSLog(@"Get record for %@", server_id);
    NSDictionary * record = [self getRecordByServerId:server_id];
    if (record == nil) {
        [self imageFailed:target failed:failed_callback];                               // Failed
        return;
    }
    
    NSString * media_name = [record objectForKey:@"media_name"];
    if (media_name == nil || [media_name isEqualToString:@""]) {
        
        if (target && done_callback && [target respondsToSelector:done_callback])       // Does not exist
            [target performSelector:done_callback withObject:nil];
        return;
    }
    

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:media_name];
    NSData * data = [fileManager contentsAtPath:fullPath];
    
    if (data) {
    
        if (target && done_callback && [target respondsToSelector:done_callback]) {   // Done
            [target performSelector:done_callback withObject:[UIImage imageWithData:data] ];
        }
    }
    else {

        if (target && done_callback && [target respondsToSelector:done_callback]) {   // Does not exist at path
            [target performSelector:done_callback withObject:[UIImage imageWithData:data] ];
        }

    }
}

- (void) setImageAsset:(NSString *)asset forServerId:(NSString*)server_id {
    
    if (asset && server_id) {
        NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET media_asset = '%@' WHERE server_id = '%@'", asset, server_id];
        [[SQLite shared_sql] Exec:query];    
    }
}

- (void) setMediaName:(NSString *)name forServerId:(NSString*)server_id {
    
    if (name && server_id) {
        NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET media_name = '%@' WHERE server_id = '%@'", name, server_id];
        [[SQLite shared_sql] Exec:query];    
    }
    
}

- (NSString*) getMediaSavePath:(NSString*)name {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    return fullPath;
}

- (BOOL) Save_Image_Data:(NSData *)data withName:(NSString *)name {
    
    if (data == nil || name == nil)
        return NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    
    return [fileManager createFileAtPath:fullPath contents:data attributes:nil];    
}

- (NSString*) Save_Video_Data:(ALAsset*) asset withName:(NSString*)name {
    
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    if (!handle) {
        return nil;
    }
    static const NSUInteger BufferSize = 1024*1024;
    
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    uint8_t *buffer = calloc(BufferSize, sizeof(*buffer));
    NSUInteger offset = 0, bytesRead = 0;
    
    do {
        @try {
            bytesRead = [rep getBytes:buffer fromOffset:offset length:BufferSize error:nil];
            [handle writeData:[NSData dataWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO]];
            offset += bytesRead;
        } @catch (NSException *exception) {
            free(buffer);
            
            return nil;
        }
    } while (bytesRead > 0);
    
    free(buffer);
    return filePath;
}

- (NSString*) Save_Video_Data_From_File:(NSString*) videoPath withName:(NSString*)name {
    
    NSString *fileDBPath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];

    BOOL result = [[NSFileManager defaultManager] moveItemAtPath:videoPath toPath:fileDBPath error:nil];
    if (result)
        return fileDBPath;
    
    return nil;
}

- (BOOL) Delete_Media:(NSString*)name {
    
    if (name == nil || name.length == 0)
        return NO;
    
    NSString* path = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    return [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

- (BOOL) Media_Exist:(NSString*)name {
    
    if (name == nil || name.length == 0)
        return NO;
    
    NSString* path = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}


- (UIImage *) Load_Image_Data:(NSString *)name {

    if (name == nil)
        return nil;

   return [UIImage imageWithContentsOfFile:[NSTemporaryDirectory() stringByAppendingPathComponent:name]];
}

- (NSData *) Load_Video_Data:(NSString *)name {
    
    NSString* filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];

    NSError *error = nil;
    NSData *original_data = [[NSData alloc] initWithContentsOfFile:filePath
                                                           options:NSDataReadingMappedIfSafe
                                                             error:&error];
    return original_data;
}

- (int) Get_Media_Size:(NSString *)name {
    
    NSString* filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    
    return [[[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] objectForKey:@"NSFileSize"] intValue];
}

- (BOOL) connectedToNetwork
{
	// Create zero addy
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags\n");
		return 0;
	}
	
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	return (isReachable && !needsConnection) ? YES : NO;
}

- (void) appDidLaunch {
    
    if ([RegisterViewController isActivated] == YES) {
        [[Notifications sharedNotifications] setNotif_delegate:self];
        [[Notifications sharedNotifications] Register];
    }
}

- (void) didNotificationDone {

    [[ConnectionQueue sharedQueue] startConnectionCycle:app.chatRoomsViewController withPeriod:1.0];
    
    //    [ChatStatusViewController sendUserStatus];
}

- (NSArray*) getImagesServerId:(NSString*) chat_id {
    NSString * query = [NSString stringWithFormat:@"SELECT server_id FROM chat_messages WHERE room_id = '%@' AND (message_type=1 OR message_type=2) AND (phone = NULL OR media_asset NOT NULL)", chat_id];
    
    NSMutableArray* ret = [[NSMutableArray alloc] initWithArray:[[SQLite shared_sql] Query:query]];
    
    NSMutableIndexSet* deleted = [[NSMutableIndexSet alloc] init];
    for (int i = 0; i < [ret count]; i++) {
        if ([[TimeBomb sharedInstance] isTimeBombMessage:[ret objectAtIndex:i]])
            [deleted addIndex:i];
    }
    
    [ret removeObjectsAtIndexes:deleted];
    return ret;
}

- (void) setMessageReadMark:(NSString*) server_id {
    NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET delivered = '1' WHERE server_id = '%@' AND delivered = 0", server_id];
    [[SQLite shared_sql] Exec:query];
}

- (void) setMessageStatusByLocalId:(NSString*) localid status:(int)status {
    NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET status = '%d' WHERE local_id = '%@'", status, localid];
    [[SQLite shared_sql] Exec:query];
}

- (NSString*) getLastIncomingMessage:(NSString*) chat_id {

    NSString * query = [NSString stringWithFormat:@"SELECT server_id FROM chat_messages  WHERE room_id = '%@' AND phone NOT NULL  ORDER BY id DESC LIMIT 1", chat_id];
    NSArray * array = [[SQLite shared_sql] Query:query];
    
    if (array == nil || [array count] == 0)
        return nil;
    
    return [[array objectAtIndex:0] objectForKey:@"server_id"];
}

- (void) initImageMessageState {
    
    NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET status = %d WHERE status = %d", MSG_STATUS_SEND_FAILED, MSG_STATUS_IMAGE_UPLOADING];
    [[SQLite shared_sql] Exec:query];
}

- (void) setMessageWinkMark:(NSString*) server_id {

    NSData* winkData = UIImageJPEGRepresentation([UIImage imageNamed:@"eye_closed.png"], 0.85);
    
    NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET media_thumb = '%@', media_asset=NULL WHERE server_id = '%@'", [winkData base64EncodedString], server_id];
    [[SQLite shared_sql] Exec:query];
};

- (BOOL) isOwnMessage:(NSString*)server_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT phone FROM chat_messages WHERE server_id = '%@'", server_id]];
    
    if (array == nil || [array count] == 0)
        return NO;
    
    if ([[array objectAtIndex:0] objectForKey:@"phone"] != nil)
        return NO;
    
    return YES;
}

- (int) getMessageCount:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT count(room_id) as nums FROM chat_messages WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return 0;
    
    return [[[array objectAtIndex:0] objectForKey:@"nums"] intValue];
}

- (BOOL) isRecordVideoByServerId:(NSString*)server_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT message_type FROM chat_messages WHERE server_id = '%@'", server_id]];
    
    if (array == nil || [array count] == 0)
        return NO;
    
    NSNumber* type = [[array objectAtIndex:0] objectForKey:@"message_type"];
    if (type == nil || [type intValue] != 2)
        return NO;
    
    return YES;
}

- (void) changeMediaNameWithLocalId:(NSString*)msgid name:(NSString*)name {
    
    NSString * query = [NSString stringWithFormat:@"UPDATE 'chat_messages' SET media_name = '%@' WHERE server_id = '%@'", name, msgid];
    [[SQLite shared_sql] Exec:query];
}

- (BOOL) getChatSoundSetting:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT enable_sound FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return NO;
    
    return [[[array objectAtIndex:0] objectForKey:@"enable_sound"] integerValue];
}

- (BOOL) getChatVibrateSetting:(NSString*)chat_id {
    
    NSArray * array = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT enable_vibrate FROM chat_rooms WHERE room_id = '%@'", chat_id]];
    
    if (array == nil || [array count] == 0)
        return NO;
    
    return [[[array objectAtIndex:0] objectForKey:@"enable_vibrate"] integerValue];
}

- (void) setChatSoundSetting:(NSString*)chat_id setting:(BOOL)enabled {
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_rooms SET enable_sound = %d WHERE room_id = '%@'", enabled, chat_id]];
}

- (void) setChatVibrateSetting:(NSString*)chat_id setting:(BOOL)enabled {
    
    [[SQLite shared_sql] Exec:[NSString stringWithFormat:@"UPDATE chat_rooms SET enable_vibrate = %d WHERE room_id = '%@'", enabled, chat_id]];
}

- (NSDictionary*) getLocationInfo:(NSString*)server_id {

    NSDictionary* dict = [self getRecordByServerId:server_id];
    if (dict == nil)
        return nil;
    
    int type = [[dict objectForKey:@"message_type"] intValue];
    if (type != 3)
        return nil;
    
    NSString* locationInfo = [dict objectForKey:@"media_name"];
    if (locationInfo == nil || locationInfo.length == 0)
        return nil;
    
    return [self getLocationInfoFromLocationString:locationInfo];
}

- (NSDictionary*) getLocationInfoFromLocationString:(NSString*)location {
    
    NSArray* infos = [location componentsSeparatedByString:@","];
    if ([infos count] != 2)
        return nil;
    
    float latitude = [[infos objectAtIndex:0] floatValue];
    float longitude = [[infos objectAtIndex:1] floatValue];
    
    return @{@"latitude":[NSNumber numberWithFloat:latitude], @"longitude":[NSNumber numberWithFloat:longitude]};
}

- (BOOL) isNeedDeleteDB {

    NSArray * exist_rooms = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms LIMIT 0, 1"]];
    if (exist_rooms && [exist_rooms count] == 1) {
        
        NSDictionary* dict = [exist_rooms objectAtIndex:0];
        if ([dict objectForKey:@"enable_sound"])
            return NO;
        
        return YES;
    }
    else {
        
        NSString * temp_chatid = @"123456789123456789";
        NSString * query = [NSString stringWithFormat:@"INSERT INTO chat_rooms (room_id, room_name, private, phones, enable_sound, enable_vibrate) VALUES ('%@', '%@', '%d', '%@', 1, 1)", temp_chatid, @"", 1, @""];
        [[SQLite shared_sql] Exec:query];
        
        NSArray * exist_rooms = [[SQLite shared_sql] Query:[NSString stringWithFormat:@"SELECT * FROM chat_rooms LIMIT 0, 1"]];
        if (exist_rooms && [exist_rooms count] == 1) {
            
            NSString * query = [NSString stringWithFormat:@"DELETE FROM 'chat_rooms' WHERE room_id = '%@'", temp_chatid];
            [[SQLite shared_sql] Exec:query];
            
            return NO;
        }
        
        return YES;
    }
}

@end
