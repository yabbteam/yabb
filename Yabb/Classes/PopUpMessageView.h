//
//  PopUpMessageView.h
//  Yabb
//
//  Created by denebtech on 3/8/14.
//
//

#import <UIKit/UIKit.h>

@interface PopUpMessageView : UIView {

    IBOutlet UIImageView* photoView;
    IBOutlet UILabel* labelName;
    IBOutlet UILabel* labelText;
    
    NSString* _chatid;
    
    id _delegate;
}

-(IBAction) onCancelClicked:(id)sender;

- (id) initDefault:(id)delegate;
- (void) hide;
- (void) setInfo:(UIImage*) photo name:(NSString*) name text:(NSString*)text chatid:(NSString*)chatid;
- (void) setText:(NSString*) text;

@end


