//
//  PhotoWinkViewController.h
//  Yabb
//
//  Created by denebtech on 3/13/14.
//
//

#import <UIKit/UIKit.h>

@class UIPlaceHolderTextView;
@class TPKeyboardAvoidingScrollView;

@protocol PhotoWinkViewControllerDelegate <NSObject>
@optional

- (void) onSelectedPhotoWink:(UIImage*) image message:(NSString*) text time:(int)seconds;
- (void) onSelectedPhotoWinkCancel;

@end

@interface PhotoWinkViewController : UIViewController<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    IBOutlet UIView*     backView;
    IBOutlet UILabel*    photoBackView;
    IBOutlet UISegmentedControl*    segTime;
    IBOutlet UIImageView*           photoView;
    IBOutlet UIPlaceHolderTextView*            textView;
    IBOutlet TPKeyboardAvoidingScrollView*      scrollView;
    IBOutlet UIButton*  sendButton;
}

@property (nonatomic, strong) id<PhotoWinkViewControllerDelegate> delegate;

@end
