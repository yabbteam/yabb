//
//  MyPersonViewController.h
//  speedsip
//
//  Created by denebtech on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MyPersonViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate> {
    UIView* fullImageView;
}

@property (nonatomic, strong) NSNumber * person_id;

@end
