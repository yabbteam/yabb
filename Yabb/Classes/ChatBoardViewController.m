//
//  ChatBoardViewController.m
//  speedsip
//
//  Created by denebtech on 14.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <NSDate-Extensions/NSDate-Utilities.h>
#import <FontasticIcons.h>
#import <FIFontAwesomeIcon.h>

#import "ChatBoardViewController.h"
#import "AppDelegate.h"
#import "ChatStorage.h"
#import "ConnectionQueue.h"
#import "NSData+Base64.h"
#import "Alert.h"
#import "MyPersonViewController.h"
#import "Friends.h"
#import "ChatSettingsViewController.h"
#import "CustomBadge.h"
#import "Settings.h"
#import "TitleView.h"
#import "Friends.h"
#import "AddressCollector.h"
#import "SoundManager.h"
#import "SettingsManager.h"
#import "ChatRoomsViewController.h"
#import "JabberLayer.h"
#import "ServerStateView.h"
#import "PhotoEditViewController.h"
#import "PopUpMessageView.h"
#import "TimeBomb.h"
#import "AvatarManager.h"
#import "AddressCollector.h"
#import "CommonUtil.h"
#import "LocationViewController.h"

#define SAVE_TEMP_TEXT_FORMAT @"tempText_%@"


enum {
    ACTION_SHEET_SELECT_AVATAR = 135 + 0,
    ACTION_SHEET_DELETE_ALL,
    ACTION_SHEET_GALLERY,
    ACTION_SHEET_ADD_CONTACT_NUMBER
};

@interface ChatBoardViewController ()

@property (nonatomic, strong) TitleView *titleView;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation ChatBoardViewController

@synthesize board_table, send_to_chat = _send_to_chat;
@synthesize dictForward = _dictForward;
@synthesize isGroupChat = _isGroupChat;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {        
        [self setHidesBottomBarWhenPushed:YES];
        
        view_is_up = NO;
        
        bottom_type = BOTTOM_EMPTY;
        
        UIBarButtonItem * settings_button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settingsIcon"] landscapeImagePhone:[UIImage imageNamed:@"settingsIconLandscape"] style:UIBarButtonItemStyleBordered target:self action:@selector(Button_Settings)];
        
        ServerStateView* serverStateView = [[ServerStateView alloc] init];
        UIBarButtonItem *serveState = [[UIBarButtonItem alloc] initWithCustomView:serverStateView];
        NSArray* arrayItem = [NSArray arrayWithObjects:serveState, settings_button, nil];
        
        [self.navigationItem setRightBarButtonItems:arrayItem];
        
        arrayAboutUploadImages = [[NSMutableArray alloc] init];
        lastTimeSentComposing = 0.0f;
        
        popups = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [emotions_view initIcons];
    [self.navigationController setHidesBottomBarWhenPushed:YES];
    
    self.titleView = [[TitleView alloc] initWithFrame:CGRectMake(0, 0, 160, 35)];
    [self.navigationItem setTitleView:self.titleView];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [bar_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
//    [self.sendButton setImage:[[FIFontAwesomeIcon arrowRightIcon] imageWithBounds:CGRectMake(0, 0, 24, 24) color:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]] forState:UIControlStateNormal];
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.sendButton setImage:[CommonUtil drawTextOnImage:NSLocalizedString(@"Send", @"Send") inImage:[UIImage imageNamed:@"chat_send_normal.png"] color:[UIColor darkGrayColor] fontSize:16] forState:UIControlStateNormal];
        [bar_button setImage:[[FIFontAwesomeIcon plusSignIcon] imageWithBounds:CGRectMake(0, 0, 24, 24) color:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]] forState:UIControlStateNormal];
    }
    else {
        [self.sendButton setImage:[CommonUtil drawTextOnImage:NSLocalizedString(@"Send", @"Send") inImage:[UIImage imageNamed:@"chat_send_normal6.png"] color:[UIColor colorWithRed:0.957 green:0.52 blue:0.195 alpha:1.0] fontSize:16] forState:UIControlStateNormal];
        [bar_button setImage:[UIImage imageNamed:@"chat_smile_normal6.png"] forState:UIControlStateNormal];
    }
    
    chat_text_view.delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSendLocation:) name:@"send_location" object:nil];
    
    _dictForward = [[NSMutableDictionary alloc] init];
    keyboardHeight = 0.0f;
    prevKeyboardY = -CGFLOAT_MAX;
    
    self.board_table.showingCount = INITIAL_SHOW_COUNT;
}

- (void)viewDidUnload
{
    [self setSendButton:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void) viewDidEnterBackground {
    
    [chat_text_view resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.view setFrame:CGRectMake(0, 0, 320, 568)];
    [self.view setBackgroundColor:[UIColor redColor]];
    
    isAppear = YES;

//    CGRect boardTableRect = board_table.frame;
//    boardTableRect.size.width = 320;
//    [board_table setFrame:boardTableRect];

    [board_table setAt_screen:YES];
    [board_table setChat_board_controller:self];
    
    [[ChatStorage sharedStorage] markRoomRead:chat_id];
    
    UIImage * bi = [[ChatStorage sharedStorage] Load_Image_Data:[[ChatStorage sharedStorage] getRoomBackground:chat_id]];
    if (bi) {
        board_table.backgroundColor = [UIColor clearColor];
        [background_image setContentMode:UIViewContentModeScaleAspectFill];
        [background_image setImage:bi ];
    } else {
        [background_image setContentMode:UIViewContentModeScaleToFill];
        [background_image setImage:[UIImage imageNamed:@"app-default-chat-bg.png"]];
        [self.view setBackgroundColor:DEFAULT_BACK_COLOR];
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [chat_text_view setText:[prefs stringForKey:[NSString stringWithFormat:SAVE_TEMP_TEXT_FORMAT, chat_id]]];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange) name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    [self performSelector:@selector(ttt) withObject:nil afterDelay:0.0];

    if (!manualViewOperation) {
        _orientation = UIDeviceOrientationPortrait;
        [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
        emotions_view.isLandscape = NO;
    }
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rotationChanged:)
                                                 name:@"UIDeviceOrientationDidChangeNotification"
                                               object:nil];
    
    [self notifyToJabberEnteredChatBoard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasLeaveFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedChatRoomInfo:) name:@"update_chatroom_info" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedFriends:) name:@"update_friends" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdatedMessages:) name:@"update_messages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddedMessages:) name:@"added_messages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forwardClicked) name:@"forward_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copyClicked) name:@"copy_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(person_photo_clicked:) name:@"person_photo_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAddContactNumber) name:@"add_contact_number" object:nil];
    
    [self refreshRoomInfo];
    [self refreshMessagesWithMakeGroup:YES msgid:nil];
    
    [board_table startObserveSeen];
    seenTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(seenTimerTick) userInfo:nil repeats:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self resetTyping];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_chatroom_info" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_friends" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_messages" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"added_messages" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"forward_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"copy_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"person_photo_clicked" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"add_contact_number" object:nil];
    
    if (seenTimer) {
        [seenTimer invalidate];
        seenTimer = nil;
    }
    [board_table stopObserveSeen];
}

- (void)setRoomInfo:(NSDictionary *)roomInfo
{
    chat_id = [roomInfo objectForKey:@"room_id"];
    _roomInfo  = [[ChatStorage sharedStorage] getChatRoom:chat_id];
    [board_table setChatId:chat_id];
    
    if ([self isViewLoaded])
    {
        // update title
        self.titleView.title = [_roomInfo objectForKey:@"room_name"];
        
        _isGroupChat = [[JabberLayer SharedInstance] isGroupChat:chat_id];
        // update subtitle
        NSArray *phones = [[_roomInfo objectForKey:@"phones"] componentsSeparatedByString:@":"];
        if (_isGroupChat) {
            
            // in a group chat we show the list of participants
            NSMutableArray* names = [[NSMutableArray alloc] init];
            for (NSString* phone in phones ) {
                NSString* name = [[AddressCollector sharedCollector] Get_Name_By_Phone:[[AddressCollector sharedCollector] clearedPhoneNumber:phone withPlus:NO] withPlus:NO];
                [names addObject:name];
            }
            self.titleView.subtitle = [names componentsJoinedByString:@", "];
            self.titleView.subtitleLabel.textColor = [UIColor colorWithRed:245.0f/255.0f green:191.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
        } else if ([[SettingsManager sharedInstance] showLastSeenTimestamp]) {
            
            // in a private chat we show friend's online status
            NSDictionary *friendInfo = [[Friends SharedFriends] friendWithPhone:[phones objectAtIndex:0]];
            NSString *online = [friendInfo objectForKey:@"online"];
            self.titleView.subtitleLabel.textColor = [UIColor lightGrayColor];
            if ([online isEqualToString:@"1"] && [[JabberLayer SharedInstance] isConnectedServer]) {
                
                self.titleView.subtitle = NSLocalizedString(@"online", @"online");
                self.titleView.subtitleLabel.textColor = [UIColor colorWithRed:245.0f/255.0f green:191.0f/255.0f blue:66.0f/255.0f alpha:1.0f];
            } else if ([friendInfo objectForKey:@"lastSeenAt"]) {
                
                NSDate * lastSeen = [NSDate dateWithTimeIntervalSince1970:[[friendInfo objectForKey:@"lastSeenAt"] doubleValue]];
                self.titleView.subtitle = [self formatLastSeenDate:lastSeen];
            } else {
                
                self.titleView.subtitle = NSLocalizedString(@"offline", @"offline");
            }
        } else {
            self.titleView.subtitle = nil;
        }
    }
    [self clearTyping];
}

- (void) ShowCustomBadge {
    
    int new_messages = [[ChatStorage sharedStorage] countNewMessages];
    if (new_messages > 0) {
        
        custom_badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d", new_messages]];
        
        CGRect rect = [custom_badge frame];
        rect.origin.x = 52;
        rect.origin.y = -2;
        [custom_badge setFrame:rect];
        
        [self.navigationController.navigationBar addSubview:custom_badge];
    }
    else {
        if (custom_badge) {
            [custom_badge removeFromSuperview];
            custom_badge = nil;
        }
    }
}

- (void) RemoveCustomBadge {
    if (custom_badge) {
        [custom_badge removeFromSuperview];
        custom_badge = nil;
    }
}

- (void) ttt {
  
    [self resizeBoard:[chat_text_view textDidChange:view_is_up]];
    [board_table Scroll_To_Bottom:NO];
}

- (void) viewDidAppear:(BOOL)animated {

    [self ShowCustomBadge];
    
    if (self.shouldShowKeyboard) {
        [self Show_Keyboard];
    }
    self.shouldShowKeyboard = NO;
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [self RemoveCustomBadge];
    
    [board_table setAt_screen:NO];

    [self tapOnTable];
    
    [[ChatStorage sharedStorage] markRoomRead:chat_id];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    if (board_table.edit_mode)
        [board_table edit];
    
    [self clearTyping];
    
    [chat_text_view resignFirstResponder];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:chat_text_view.text forKey:[NSString stringWithFormat:SAVE_TEMP_TEXT_FORMAT, chat_id]];
    [prefs synchronize];
    
    if (!manualViewOperation)
        [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    isAppear = NO;
    [self removeAllPopups];
}

- (void) Button_Settings
{
    ChatSettingsViewController * chat_settings_controller = [[ChatSettingsViewController alloc] init];
    
    [chat_settings_controller setChat_id:chat_id];
    [chat_settings_controller setRoom_dict:[[ChatStorage sharedStorage] getChatRoom:chat_id]];
    [chat_settings_controller setParent_controller:self];
    
    [self.navigationController pushViewController:chat_settings_controller animated:YES];
}

- (void) setChatId:(NSString*)value {
    
    chat_id = value;
}

- (NSString*) getChatId {
    return chat_id;
}

- (void) Show_Keyboard {
    
    [chat_text_view becomeFirstResponder];
}

- (void) clearTyping {
    
    [typing_view clearText];
    
    if (!isAppear)
        return;
    
    [self resizeBoard:bottom_view.frame.size.height];
    
}

- (void) setTyping:(NSString*)typing_chat_id withString:(NSString *)str {
    
    if (!isAppear)
        return;
    
    if (_isGroupChat)
        return;
    
    if ([chat_id isEqualToString:typing_chat_id]) {
        
        str = [NSString stringWithFormat:NSLocalizedString(@"%@ is typing", @"%@ is typing"), [_roomInfo objectForKey:@"room_name"]];
    
        [typing_view setText:str atPosition:CGPointMake(0, board_table.frame.origin.y/* + board_table.frame.size.height*/)];
    }
    else {
        [self clearTyping];
    }

    int height = bottom_view.frame.size.height/* + TYPING_VIEW_HEIGHT*/;
    [self resizeBoard:height];
}

#pragma mark -
#pragma mark Buttons Delegate

- (void) Show_Emotions:(BOOL)show {
    
    CGRect rect = emotions_view.frame;
    
    [emotions_view setBackgroundColor:DEFAULT_BACK_COLOR];
    
    [UIView beginAnimations:@"emotions_anim" context:nil];
    [UIView setAnimationDuration:0.25];
    
    if (show) {
        rect.origin.y = self.view.frame.size.height - [emotions_view getViewHeight];
    }
    else {
        rect.origin.y = self.view.frame.size.height;
    }
    
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        //rect.origin.y += 20;
    }
    
    
    [emotions_view setFrame:rect];
    [emotions_view setAlpha:show?1.0:0.0];
    
    [UIView commitAnimations];
    
    if (show) {
        keyboardHeight += [emotions_view getViewHeight];
        
        if (bottom_type == BOTTOM_KEYBOARD) {
            [chat_text_view resignFirstResponder];
            [self setKeyboardButton];
        }
        
        [emotions_view setHidden:NO];
    
        [self viewResizeHeight:-[emotions_view getViewHeight] withDuration:0.25];
    
        bottom_type = BOTTOM_EMOTIONS;
    }
    else {
        keyboardHeight -= [emotions_view getViewHeight];
        [self viewResizeHeight:[emotions_view getViewHeight] withDuration:0.25];
        
//        [text_field becomeFirstResponder];
        
//        [self performSelector:@selector(Hide_Emotions) withObject:nil afterDelay:0.6];
        
        bottom_type = BOTTOM_KEYBOARD;
    }
}

- (void) setKeyboardButton {
    
    [bar_button setImage:[[FIFontAwesomeIcon keyboardIcon] imageWithBounds:CGRectMake(0, 0, 24, 24) color:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]] forState:UIControlStateNormal];
    //[bar_button setImage:[UIImage imageNamed:@"icon_keyboard.png"] forState:UIControlStateNormal];
    //[bar_button setTitle:@"" forState:UIControlStateNormal];
}

- (void) setEmotionsButton {
  
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [bar_button setImage:[[FIFontAwesomeIcon plusSignIcon] imageWithBounds:CGRectMake(0, 0, 24, 24) color:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]] forState:UIControlStateNormal];
    }
    else {
        [bar_button setImage:[UIImage imageNamed:@"chat_smile_normal6.png"] forState:UIControlStateNormal];
    }
    //[bar_button setTitle:@"+" forState:UIControlStateNormal];

//    [bar_button setImage:[UIImage imageNamed:@"icon_emotions.png"] forState:UIControlStateNormal];
//    [bar_button setTitle:@"" forState:UIControlStateNormal];
}

- (IBAction) Emotions_Button:(id)sender {

    if (bottom_type == BOTTOM_EMOTIONS) {
        
        [self Show_Emotions:NO];
        [chat_text_view becomeFirstResponder];
        bottom_type = BOTTOM_KEYBOARD;
        [self setEmotionsButton];
    }
    else if (bottom_type == BOTTOM_KEYBOARD) {
        
        [chat_text_view resignFirstResponder];
        [self Show_Emotions:YES];
        bottom_type = BOTTOM_EMOTIONS;
        [self setKeyboardButton];
    }
    else {
        [self Show_Emotions:YES];
        bottom_type = BOTTOM_EMOTIONS;
        [self setKeyboardButton];
    }
}

- (IBAction) Image_Gallery_Button:(id)sender {

    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    [chat_text_view resignFirstResponder];

    
    UIActionSheet * actSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Upload image", @"Upload image") delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Image gallery", @"Image gallery")];
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        [actSheet addButtonWithTitle:NSLocalizedString(@"Photo from camera", @"Photo from camera")];
    
    int index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:index];
    actSheet.tag = ACTION_SHEET_GALLERY;
    [actSheet showInView:[self.view window]];
    
    /*
    ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] init];
    [image_picker setImage_picker_delegate:self];
    
    [self presentModalViewController:image_picker animated:YES];
    
    [image_picker release];
*/
    
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}


-(UIImage*)imageByScalingToSize:(CGSize)targetSize sourceImage:(UIImage *)sourceImage needRotate:(BOOL)rotate
{
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    float kx = sourceImage.size.width / sourceImage.size.height;
    
    targetWidth = targetHeight * kx;
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
    }
  
    if (rotate) {
        if (sourceImage.imageOrientation == UIImageOrientationLeft) {
            CGContextRotateCTM (bitmap, RAD(90));
            CGContextTranslateCTM (bitmap, 0, -targetHeight);
            
        } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
            CGContextRotateCTM (bitmap, RAD(-90));
            CGContextTranslateCTM (bitmap, -targetWidth, 0);
            
        } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
            // NOTHING
        } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
            CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
            CGContextRotateCTM (bitmap, RAD(-180.));
        }
    }
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown)
        CGContextDrawImage(bitmap, CGRectMake(0, 0, targetWidth, targetHeight), imageRef);
    else
        CGContextDrawImage(bitmap, CGRectMake(0, 0, targetHeight, targetWidth), imageRef);
        
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref scale:1.0 orientation:rotate?0:[sourceImage imageOrientation]];
    
    CGImageRelease(ref);
    CGContextRelease(bitmap);
    
    return newImage; 
}

#pragma mark -
#pragma mark Image Picker Delegate

- (void) Pick_Video:(NSString*) videoPath {
    
    UIImage* thumb = [CommonUtil getMovieScreenShot:videoPath];
    [self showInputPhotoInfoScreen:thumb other:videoPath isVideo:YES];
}

- (void) Pick_Image:(UIImage *)image assetReference:(NSString *)asset {

    [self showInputPhotoInfoScreen:image other:nil isVideo:NO];
}

- (void) showInputPhotoInfoScreen:(UIImage*) image other:(id)otherParam isVideo:(BOOL)isVideo{
    
    self.inputPhotoInfoController = [[InputPhotoInfoViewController alloc] initWithNibName:@"InputPhotoInfoViewController" bundle:nil];
    self.inputPhotoInfoController.image = image;
    self.inputPhotoInfoController.delegate = self;
    self.inputPhotoInfoController.otherParam = otherParam;
    self.inputPhotoInfoController.isVideo = isVideo;
    
    [self.navigationController presentViewController:self.inputPhotoInfoController animated:YES completion:nil];
}

- (void) onSelectedComment:(UIImage*) image comment:(NSString*) text other:(id)other {
    
    if (other == nil)
        [self Pick_Image:image message:text assetReference:@"" isWink:NO];
    else if ([other isKindOfClass:[ALAsset class]]){
        //photo sending
        BOOL shouldStart = [arrayAboutUploadImages count] == 0;
        
        ALAsset* asset = other;
        
        NSString* type = [asset valueForProperty:ALAssetPropertyType];
        if (type != nil && [type isEqualToString:ALAssetTypeVideo])
            [arrayAboutUploadImages addObject:@{@"asset":asset, @"comment":text, @"isVideo":@YES} ];
        else
            [arrayAboutUploadImages addObject:@{@"asset":asset, @"comment":text} ];
        
        if (shouldStart)
            [self pickProcess:[arrayAboutUploadImages objectAtIndex:0]];
    }
    else if ([other isKindOfClass:[NSString class]]){
        NSString* videoPath = other;
        BOOL shouldStart = [arrayAboutUploadImages count] == 0;
        [arrayAboutUploadImages addObject:@{@"video_path":videoPath, @"comment":text, @"isVideo":@YES} ];
        if (shouldStart)
            [self pickProcess:[arrayAboutUploadImages objectAtIndex:0]];
    }
}

- (NSString*) Pick_Image:(UIImage *)image message:(NSString*)message assetReference:(NSString *)asset isWink:(BOOL)isWink {
    
    if (image == nil)
        return nil;
    
    NSString* msg_local_id = [[NSString alloc] initWithString:[[JabberLayer SharedInstance] requestRandomMessageId]];

    float kx;
    if (image.size.width > image.size.height)
        kx = 240.0 / image.size.width;
    else
        kx = 240.0 / image.size.height;
    
    CGSize target_size = CGSizeMake(image.size.width * kx, image.size.height * kx);
    NSLog(@"Image width: %02f, height: %02f", image.size.width, image.size.height);
    
    UIImage * thumb = [self imageWithImage:image scaledToSize:target_size];
    
    UIImage* msg_thumb = [[UIImage alloc] initWithCGImage:[thumb CGImage]];
    NSLog(@"Thumb width: %02f, height: %02f", thumb.size.width, thumb.size.height);

    NSString* msg_asset = asset;
    
    CGSize image_size = image.size;
    float image_factor = image_size.width / image_size.height;
    
    NSData * original_data = nil;
    
    // Need resize by width
    if (image_factor >= 1.0 && image_size.width > MAX_DIMENSION) {

        float scale_factor = MAX_DIMENSION / image_size.width;
        
        UIImage * resized_image = [self imageByScalingToSize:CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor) sourceImage:image needRotate:NO];
        
        original_data = UIImageJPEGRepresentation(resized_image, 0.85);
    }
    // Need resize by height
    else if (image_factor < 1.0 && image_size.height > MAX_DIMENSION) {
        float scale_factor = MAX_DIMENSION / image_size.height;
        
        UIImage * resized_image = [self imageByScalingToSize:CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor) sourceImage:image needRotate:NO];
        
        original_data = UIImageJPEGRepresentation(resized_image, 0.85);
    }
    // Original size
    else {
        original_data = UIImageJPEGRepresentation(image, 0.85);
    }
    
    NSLog(@"image size = %dbytes", original_data.length);

    // Save image to '/tmp' dir
    NSString * name = [NSString stringWithFormat:@"image_%@.jpg", [[ChatStorage sharedStorage] generateRandom:16]];
    if ([[ChatStorage sharedStorage] Save_Image_Data:original_data withName:name] == NO) {
        [Alert show:NSLocalizedString(@"Error",@"Error") withMessage:NSLocalizedString(@"Can not save image.", @"Can not save image.")];
        return nil;
    }
    
    NSString* image_name = name;
    
    long long time_group = [[ChatStorage sharedStorage] addImageMessage:msg_thumb withLocalId:msg_local_id withChatId:app.chatRoomsViewController.chat_board.send_to_chat withAsset:msg_asset withName:image_name comment:message];
    
    [board_table Update_Messages:[NSArray arrayWithObject:[NSNumber numberWithLongLong:time_group]] scroll:YES];

    NSData* thumbData = nil;
    if (!isWink)
        thumbData = UIImageJPEGRepresentation(msg_thumb, 0.7);
    
    [[JabberLayer SharedInstance] uploadFile:original_data thumb:thumbData name:image_name target:self local_id:msg_local_id];
    
    return msg_local_id;
}

- (NSString*) Pick_Video:(ALAsset*)asset videoPath:(NSString*)videoPath message:(NSString*)message {
    
    if (asset == nil && videoPath == nil)
        return nil;
    
    if (asset != nil) {
        NSString* type = [asset valueForProperty:ALAssetPropertyType];
        if (type == nil || ![type isEqualToString:ALAssetTypeVideo])
            return nil;
    }
    
    NSString* msg_local_id = [[NSString alloc] initWithString:[[JabberLayer SharedInstance] requestRandomMessageId]];
    
    UIImage* msg_thumb;
    NSString* msg_asset = @"";
    
    NSString* extension = nil;
    if (asset != nil)
        extension = [asset.defaultRepresentation.url pathExtension];
    else
        extension = [videoPath pathExtension];
    
    // Save video to '/tmp' dir
    NSString * name = [NSString stringWithFormat:@"video_%@.%@", [[ChatStorage sharedStorage] generateRandom:16], extension];
    
    NSString* path;
    if (asset != nil) {
        path = [[ChatStorage sharedStorage] Save_Video_Data:asset withName:name];
        msg_thumb = [[UIImage alloc] initWithCGImage:asset.thumbnail];
    }
    else {
        msg_thumb = [CommonUtil getMovieScreenShot:videoPath];
        path = [[ChatStorage sharedStorage] Save_Video_Data_From_File:videoPath withName:name];
    }
    
    if (path == nil) {
        [Alert show:NSLocalizedString(@"Error",@"Error") withMessage:NSLocalizedString(@"Can not save video.", @"Can not save video.")];
        return nil;
    }
    
    NSString* image_name = name;
    
    long long time_group = [[ChatStorage sharedStorage] addVideoMessage:msg_thumb withLocalId:msg_local_id withChatId:app.chatRoomsViewController.chat_board.send_to_chat withAsset:msg_asset withName:image_name comment:message];
    
    [board_table Update_Messages:[NSArray arrayWithObject:[NSNumber numberWithLongLong:time_group]] scroll:YES];
    
    NSData* thumbData = nil;
    thumbData = UIImageJPEGRepresentation(msg_thumb, 0.7);
    
    [[JabberLayer SharedInstance] uploadFile:nil thumb:thumbData name:image_name target:self local_id:msg_local_id];
    
    return msg_local_id;
}

#pragma marl Connection Delegate
- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([dict objectForKey:@"cancelled"] != nil) {
        NSString* local_id = [dict objectForKey:@"local_id"];
        if (local_id != nil) {
            [[ChatStorage sharedStorage] setMessageStatusByLocalId:local_id status:MSG_STATUS_SEND_CANCELLED];
        }
        return;
    }

    NSString* local_id = [dict objectForKey:@"local_id"];
    if (local_id != nil) {
        [[ChatStorage sharedStorage] setMessageStatusByLocalId:local_id status:MSG_STATUS_PENDING];
        [[ConnectionQueue sharedQueue] forceCycle];
    }
}

- (void) didConnectionFailed:(NSString *)local_id {
    
    if (local_id != nil) {
        [[ChatStorage sharedStorage] setMessageStatusByLocalId:local_id status:MSG_STATUS_SEND_FAILED];//mark failed
        [self refreshMessagesWithMakeGroup:NO msgid:local_id];
    }
}

#pragma mark -
#pragma mark Resize view

- (void) viewResizeHeight:(int)height withDuration:(float)duration {
    
    if (height > 0)
        view_is_up = NO;
    else
        view_is_up = YES;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // Resize table
    CGRect table_rect = tool_view.frame;
    table_rect.origin.y += height;
    tool_view.frame = table_rect;
    
    CGRect board_rect = board_table.frame;
    board_rect.size.height += height;
    board_table.frame = board_rect;
    
    [board_table Scroll_To_Bottom:NO]; 
    
    CGRect rect = typing_view.frame;
    rect.origin.y = board_rect.origin.y;// + board_rect.size.height;
    [typing_view setFrame:rect];
    
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Click Delegate

- (void) tapOnTable {
    
    if (bottom_type == BOTTOM_EMOTIONS) {
        [self Show_Emotions:NO];
    }
    else if (bottom_type == BOTTOM_KEYBOARD)
        [chat_text_view resignFirstResponder];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [bar_button setImage:[[FIFontAwesomeIcon plusSignIcon] imageWithBounds:CGRectMake(0, 0, 24, 24) color:[UIColor colorWithRed:0.98 green:0.71 blue:0.1 alpha:1.0]] forState:UIControlStateNormal];
    }
    else {
        [bar_button setImage:[UIImage imageNamed:@"chat_smile_normal6.png"] forState:UIControlStateNormal];
    }
    
    bottom_type = BOTTOM_EMPTY;
}

- (void) resizeBoard:(int)height {
    
    if (height > 190)
        height = 190;
    
    if (_orientation == UIDeviceOrientationLandscapeRight || _orientation == UIDeviceOrientationLandscapeLeft) {
        if (height > 80)
            height = 80;
    }

//    if (typing_view.typing) {
//        height += TYPING_VIEW_HEIGHT;
//    }
    
    CGRect rect = board_table.frame;
    CGRect bottomViewRect = bottom_view.frame;
    
    rect.size.height = self.view.frame.size.height - keyboardHeight - height;
    bottomViewRect.origin.y = self.view.frame.size.height - bottomViewRect.size.height - keyboardHeight;
    
    [board_table setFrame:rect];
    [bottom_view setFrame:bottomViewRect];
    
    CGPoint ptScroll = board_table.contentOffset;
    
    if (typing_view.typing) {
        [typing_view update:CGPointMake(0, board_table.frame.origin.y/* + board_table.frame.size.height*/)];
        //ptScroll.y += TYPING_VIEW_HEIGHT;
    }
    
    board_table.contentOffset = ptScroll;
}

- (void) resetTyping {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(resetTyping) object:nil];
    if ([ChatStorage sharedStorage].typing != nil) {
        [[JabberLayer SharedInstance] sendMessageComposingCanceled:[ChatStorage sharedStorage].typing];
    }
    [ChatStorage sharedStorage].typing = nil;
}

- (void) applyTextChange:(BOOL)scroll {
    
    BOOL needSendComposing = NO;
    if (lastTimeSentComposing <= 0.0f)
        needSendComposing = YES;
    else {
        NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
        if ((current - lastTimeSentComposing)/1000 > SEND_TYPING_PERIOD)
            needSendComposing = YES;
    }
    
    if (needSendComposing || [ChatStorage sharedStorage].typing == nil) {
        NSString* lastMsgId = [[ChatStorage sharedStorage] getLastIncomingMessage:chat_id];
        if (lastMsgId != nil) {
            [[JabberLayer SharedInstance] sendMessageComposingStarted:lastMsgId];
            lastTimeSentComposing = [[NSDate date] timeIntervalSince1970];
            [ChatStorage sharedStorage].typing = lastMsgId;
        }
    }
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(resetTyping) object:nil];
    [self performSelector:@selector(resetTyping) withObject:nil afterDelay:SEND_TYPING_PERIOD];
    
    if (chat_text_view.contentSize.height == chat_text_view.frame.size.height)
        return;
    
    int height = [chat_text_view textDidChange:view_is_up];
    
    [self resizeBoard:height];
    
    if (scroll)
        [board_table Scroll_To_Bottom:NO];
}

- (void) cancelResetTyping {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(resetTyping) object:nil];
    [ChatStorage sharedStorage].typing = nil;
}

- (void)textViewDidChange:(UITextView *)textView {

    [self applyTextChange:YES];
}

- (IBAction)SendButtonAction:(id)sender
{
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    if (chat_text_view.text == nil || [chat_text_view.text isEqualToString:@""])
        return;

    NSString* msgText = chat_text_view.text;
    
    [chat_text_view setText:@""];
    [self applyTextChange:YES];
    
    [self cancelResetTyping];
    
    if ([[ChatStorage sharedStorage] getChatSoundSetting:chat_id])
        [[SoundManager sharedInstance] playMessageSentSound];

    [self performSelector:@selector(sendMessage:) withObject:msgText afterDelay:0.01];
}

- (void) sendMessage:(NSString*) msgText {
    /*long long time_group = */[[ChatStorage sharedStorage] addMessage:msgText withChatId:chat_id];
    [[ConnectionQueue sharedQueue] setCycleSilent:NO];
    [self refreshMessagesWithMakeGroup:YES msgid:nil];
    [board_table Scroll_To_Bottom:NO];
}

- (void) showContactInfo:(NSString*) cid {
    
    NSString * phones = [[ChatStorage sharedStorage] getRoomPhone:chat_id];
    
    if (phones == nil || [phones isEqualToString:@""])
        return;
        
    NSDictionary * friend = [[Friends SharedFriends] friendWithPhone:phones];
    if (friend == nil)
        return;
    
    int index = [[friend objectForKey:@"index"] intValue];
    
    MyPersonViewController * person_view_controller = [[MyPersonViewController alloc] init];
    
    [person_view_controller setPerson_id:[NSNumber numberWithInt:index]];
    [person_view_controller setTitle:NSLocalizedString(@"Information", @"Information")];
    
    [self.navigationController pushViewController:person_view_controller animated:YES];
    
}

- (void) editMode:(BOOL)edit {
    
    if (edit) {
        
        [self RemoveCustomBadge];
        
        [self selectCell:0];
        [chat_text_view setText:@""];
        [self applyTextChange:NO];
        
        backupRightItems = self.navigationItem.rightBarButtonItems;
        self.navigationItem.rightBarButtonItems = nil;
        
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"Cancel") style:UIBarButtonSystemItemCancel target:self action:@selector(cancelEditMode:)];
        self.navigationItem.rightBarButtonItem = rightButton;
        
        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Delete all", @"Delete all") style:UIBarButtonSystemItemCancel target:self action:@selector(deleteAll:)];
        self.navigationItem.leftBarButtonItem = leftButton;
    }
    else {
        
        UIBarButtonItem * settings_button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settingsIcon"] landscapeImagePhone:[UIImage imageNamed:@"settingsIconLandscape"] style:UIBarButtonItemStyleBordered target:self action:@selector(Button_Settings)];
        
//        [settings_button setEnabled:NO];
        
        self.navigationItem.rightBarButtonItem = settings_button;

        
        self.navigationItem.leftBarButtonItem = nil;
        
        [self performSelector:@selector(ShowCustomBadge) withObject:nil afterDelay:0.5];
        
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    CGRect toolbar_rect = delete_toolbar.frame;
    CGRect chat_bar_rect = tool_view.frame;
    
    if (edit) {
        toolbar_rect.origin.y = self.view.frame.size.height - toolbar_rect.size.height;
        chat_bar_rect.origin.y = self.view.frame.size.height + chat_bar_rect.size.height;
    }
    else {
        toolbar_rect.origin.y = self.view.frame.size.height + toolbar_rect.size.height;
        chat_bar_rect.origin.y = self.view.frame.size.height - chat_bar_rect.size.height;
    }

    [delete_toolbar setFrame:toolbar_rect];
    [tool_view setFrame:chat_bar_rect];
    
    [UIView commitAnimations];

    if (view_is_up) {
        [chat_text_view resignFirstResponder];
    }
}

- (void) selectCell:(int)num_of_cells {
    
    if (num_of_cells) {
        
        [delete_button setTitle:[NSString stringWithFormat:NSLocalizedString(@"       Delete (%d)", @"       Delete (%d)"), num_of_cells] forState:UIControlStateNormal];
        
        [delete_button setEnabled:YES];
    }
    else {
        [delete_button setEnabled:NO];
        [delete_button setTitle:NSLocalizedString(@"     Delete", @"     Delete") forState:UIControlStateNormal];
    }
    
    if ([board_table numberOfSections] == 0) {
        [self cancelEditMode:self];
    }
}

- (IBAction) cancelEditMode:(id)sender {
    
    [board_table edit];

    if (backupRightItems != nil)
        [self.navigationItem setRightBarButtonItems:backupRightItems];
}

- (IBAction) deleteButton:(id)sender {
    
    [board_table deleteSelectedCells];
    
}

- (void) deleteAll:(id)sender {
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    UIActionSheet * actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    int index = [actSheet addButtonWithTitle:NSLocalizedString(@"Delete messages", @"Delete messages")];
    [actSheet setDestructiveButtonIndex:index];
    actSheet.tag = ACTION_SHEET_DELETE_ALL;
    
    index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:index];
    
    [actSheet showInView:[self.view window]];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (actionSheet.tag == ACTION_SHEET_GALLERY) {
        
        [self setSend_to_chat:chat_id];
        
        if (buttonIndex == 0) {
            
            ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypePhotoLibrary];
            [image_picker setImage_picker_delegate:self];
            
            [self presentViewController:image_picker animated:YES completion:^{}];
        }
        else if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera] && buttonIndex == 1) {
            
            ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypeCamera];
            [image_picker setImage_picker_delegate:self];
            
            [self presentViewController:image_picker animated:YES completion:^{}];
        }
    }
    else if (actionSheet.tag == ACTION_SHEET_DELETE_ALL) {
        if (buttonIndex == 0) {
            // Delete all messages
            [[ChatStorage sharedStorage] deleteAllMessages:chat_id];
            
            [board_table UpdateMessagesWithGroups:nil];
            [board_table edit];
        }
    }
    else if (actionSheet.tag == ACTION_SHEET_SELECT_AVATAR) {
        if (buttonIndex == 0) { // take picture
            
            if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
                
                UIImagePickerController * picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:nil];
            }
        }
        else if (buttonIndex == 1) { // from gallery
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (actionSheet.tag == ACTION_SHEET_ADD_CONTACT_NUMBER) {
        if (buttonIndex == 0) {
            
            NSString* phone_number = [@"+" stringByAppendingString:chat_id];
            ABRecordRef newPerson = [[AddressCollector sharedCollector] addNewContactWithPhoneNumber:phone_number];

            // Create and set-up the new person view controller
            ABNewPersonViewController* newPersonViewController = [[ABNewPersonViewController alloc] init];
            [newPersonViewController setDisplayedPerson:newPerson];
            [newPersonViewController setNewPersonViewDelegate:self];
            
            if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                newPersonViewController.view.backgroundColor = [UIColor lightGrayColor];
            
            [self.navigationController pushViewController:newPersonViewController animated:YES];
            
            // Clean up everything
            CFRelease(newPerson);
        }
        else if (buttonIndex == 1) {
            ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
            
            [picker setPeoplePickerDelegate:self];
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }

}

#pragma mark -
#pragma mark Keyboard Delegates

- (void) keyboardWillShow:(NSNotification*)notification {
    
    if (![chat_text_view isFirstResponder])
        return;
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    NSValue* keyboardFrameBeginValue = [userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey];
    NSValue* keyboardFrameEndValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rtBegin = [self.view.window convertRect:[keyboardFrameBeginValue CGRectValue] fromWindow:nil];
    CGRect rtEnd = [self.view.window convertRect:[keyboardFrameEndValue CGRectValue] fromWindow:nil];
    
    if (prevKeyboardY <= -99990.0f) {
        prevKeyboardY = rtBegin.origin.y;

        CGRect rt = [self.view.window convertRect:[UIScreen mainScreen].bounds fromWindow:nil];
        float bottom = rt.size.height;
        if (prevKeyboardY > bottom) {
            prevKeyboardY = bottom;
        }
    }
    
    if (bottom_type == BOTTOM_EMOTIONS)
        [self Show_Emotions:NO];
    
    CGFloat offsetY = rtEnd.origin.y - prevKeyboardY;
    prevKeyboardY = rtEnd.origin.y;
    if (_orientation == UIDeviceOrientationLandscapeRight || _orientation == UIDeviceOrientationLandscapeLeft) {
        keyboardHeight = [UIScreen mainScreen].bounds.size.width-rtEnd.origin.y;
    }
    else {
        keyboardHeight = [UIScreen mainScreen].bounds.size.height-rtEnd.origin.y;
    }
    
    if (bottom_type == BOTTOM_EMPTY || bottom_type == BOTTOM_KEYBOARD)
        [self viewResizeHeight:offsetY withDuration:animationDuration];
    [self setEmotionsButton];
    
    bottom_type = BOTTOM_KEYBOARD;
    
    NSLog(@"Keyboard show");
}

- (void) keyboardWillHide:(NSNotification*)notification {
    
    if (![chat_text_view isFirstResponder])
        return;
    
    NSDictionary* userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    NSValue* keyboardFrameEndValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rtEnd = [self.view.window convertRect:[keyboardFrameEndValue CGRectValue] fromWindow:nil];
    CGFloat offsetY = rtEnd.origin.y - prevKeyboardY;
    prevKeyboardY = rtEnd.origin.y;
    
    if (_orientation == UIDeviceOrientationLandscapeRight || _orientation == UIDeviceOrientationLandscapeLeft) {
        keyboardHeight = [UIScreen mainScreen].bounds.size.width-rtEnd.origin.y;
    }
    else {
        keyboardHeight = [UIScreen mainScreen].bounds.size.height-rtEnd.origin.y;
    }
    if (keyboardHeight < 0)
        keyboardHeight = 0;

    [self viewResizeHeight:offsetY withDuration:animationDuration];
    
    bottom_type = BOTTOM_EMPTY;
    
    NSLog(@"Keyboard hide");
}

#pragma mark -
#pragma mark Emotions Delegate

- (void) didSelectEmotion:(NSString *)emotion_key {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    NSMutableString * str = [NSMutableString stringWithString:chat_text_view.text];
    
    int anim_index = [[ChatStorage sharedStorage] getAnimationIndex:str];
    
    if (anim_index)
        [str setString:[NSString stringWithFormat:@"{%@}", emotion_key]];
    else
        [str appendString:[NSString stringWithFormat:@"{%@}", emotion_key]];
    
    [chat_text_view setText:str];
    [self textViewDidChange:chat_text_view];    
    
    NSLog(@"Comes delegate: %@", emotion_key);
}

- (void) didSelectAnimation:(NSString *)animation_key {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
//    [chat_text_view setText:[NSString stringWithFormat:@"<%@>", animation_key]];
    
    long long time_group = [[ChatStorage sharedStorage] addMessage:[NSString stringWithFormat:@"<%@>", animation_key] withChatId:chat_id];
    [[ConnectionQueue sharedQueue] setCycleSilent:NO];
    [board_table Update_Messages:[NSArray arrayWithObject:[NSNumber numberWithLongLong:time_group]] scroll:YES];
    
    NSLog(@"Comes delegate: %@", animation_key);
}

#pragma mark -
#pragma mark update_chatroom_info Notification

- (void)UpdatedFriends:(NSNotification*) notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(UpdateFriendsProcess) object:nil];
    [self performSelector:@selector(UpdateFriendsProcess) withObject:nil afterDelay:1.0f];
}

- (void) UpdateFriendsProcess {
    
    self.roomInfo = [[ChatStorage sharedStorage] getChatRoom:chat_id];
    [self refreshRoomInfo];
    [self refreshMessagesWithMakeGroup:YES msgid:nil];
}

- (void)UpdatedChatRoomInfo:(NSNotification*) notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(UpdatedChatRoomInfoProcess) object:nil];
    [self performSelector:@selector(UpdatedChatRoomInfoProcess) withObject:nil afterDelay:1.0f];
}

- (void) UpdatedChatRoomInfoProcess {
    
    self.roomInfo = [[ChatStorage sharedStorage] getChatRoom:chat_id];
    [self refreshRoomInfo];
}

- (void) UpdatedMessages:(NSNotification*) notification {
    
    NSDictionary* param = notification.object;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(messageUpdateProcess:) object:nil];
    
    if ([param objectForKey:@"force"] != nil) {
        [self messageUpdateProcess:nil];
        return;
    }
    
    [self performSelector:@selector(messageUpdateProcess:) withObject:nil afterDelay:0.5f];
}

- (void) AddedMessages:(NSNotification*) notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(messageAddedProcess) object:nil];
    [self performSelector:@selector(messageAddedProcess) withObject:nil afterDelay:0.5f];
}

- (void) messageUpdateProcess:(id)param {
    
    NSDictionary* param_dict = param;
    NSString* msgid = nil;
    if (param != nil)
        msgid = [param_dict objectForKey:@"msgid"];
    [self refreshMessagesWithMakeGroup:YES msgid:msgid];
}

- (void) messageAddedProcess {
    
    [self refreshMessagesWithMakeGroup:YES msgid:nil];
    [board_table Scroll_To_Bottom:YES];
}

- (NSString *)formatLastSeenDate:(NSDate *)date
{
    if ([date isToday] || [date isYesterday]) {
        NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
        NSRange containsA = [formatStringForHours rangeOfString:@"a"];
        BOOL hasAMPM = containsA.location != NSNotFound;
        
        static NSDateFormatter *shortDateFormat = nil;
        if (!shortDateFormat) {
            shortDateFormat = [NSDateFormatter new];
        }
        if (hasAMPM)
            [shortDateFormat setDateFormat:@"hh:mm a"];
        else
            [shortDateFormat setDateFormat:@"HH:mm"];

        return [NSString stringWithFormat:NSLocalizedString(@"Last online %@ at %@", @"Last online %@ at %@"), ([date isToday] ? NSLocalizedString(@"today", @"today") : NSLocalizedString(@"yesterday", @"yesterday")), [shortDateFormat stringFromDate:date]];
    }
    static NSDateFormatter *fullDateFormat = nil;
    if (!fullDateFormat) {
        fullDateFormat = [NSDateFormatter new];
        [fullDateFormat setDateFormat:@"dd MMMM yyyy"];
    }
    return [NSString stringWithFormat:NSLocalizedString(@"Last seen %@", @"Last seen %@"), [fullDateFormat stringFromDate:date]];
}

- (void) forwardProcess {
    NSString* msgID = [self.dictForward objectForKey:@"message"];
    if (msgID != nil) {
        NSDictionary * dict = [[ChatStorage sharedStorage] getRecordByServerId:msgID];
        int type = [[dict objectForKey:@"message_type"] intValue];
        if (type == 0) {
            //text message
            NSString * msg = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"original_message"]];
            [self sendMessage:msg];
        }
    }
    [self.dictForward removeAllObjects];
}

- (void) forwardCancel {
    [self.dictForward removeAllObjects];
}

- (void) forwardClicked {
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
    
    [people_picker setShort_version:YES];
    [people_picker setFriends_only:YES];
    [people_picker setDelegate:self];
    
    [self.navigationController pushViewController:people_picker animated:YES];
    
}

- (void) copyClicked {
    NSString* msgID = [self.dictForward objectForKey:@"message"];
    if (msgID != nil) {
        NSDictionary * dict = [[ChatStorage sharedStorage] getRecordByServerId:msgID];
        int type = [[dict objectForKey:@"message_type"] intValue];
        if (type == 0) {
            //text message
            NSString * msg = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"original_message"]];
            [UIPasteboard generalPasteboard].string = msg;
        }
    }
    [self.dictForward removeAllObjects];
}

- (void) didSelectPerson:(int)index fromViewController:(UIViewController *)controller {
    
    [app.chatRoomsViewController setChat_rooms_delegate:self];
    NSString* send_to_chat = [app.chatRoomsViewController Send_Private_Message:index withKeyboard:NO];
    
    if (send_to_chat) {
        
        [app.chatRoomsViewController.chat_board setSend_to_chat:send_to_chat];
        [self forwardProcess];
        [app.chatRoomsViewController setChat_rooms_delegate:nil];
    }
}

- (void) didCreateRoom:(NSString*) room_id {
    
    [app.chatRoomsViewController.chat_board setSend_to_chat:room_id];
    
    [self performSelector:@selector(forwardProcess) withObject:self afterDelay:0.3f];
    
}

#pragma mark -
#pragma mark EmotionsDelegate

- (void) didSelectPhotoFromGallery {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    [self tapOnTable];
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    [self setSend_to_chat:chat_id];
    
    if (self.assetsLibrary == nil) {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        self.assetsLibrary = library;
    }
    
//    WSAssetPickerController *controller = [[WSAssetPickerController alloc] initWithAssetsLibrary:self.assetsLibrary];
//    controller.delegate = self;
//    controller.selectionLimit = 15;
//    [self presentViewController:controller animated:YES completion:NULL];
    
    CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
    picker.delegate = self;
    picker.maximumNumberOfSelections = 15;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)didSelectPhotoWink {

    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    disableAutoRotate = YES;
    
    CGRect rtScreen = [UIScreen mainScreen].bounds;
    
    [self setSend_to_chat:chat_id];
    
    self.photoWinkController = [[PhotoWinkViewController alloc] initWithNibName:@"PhotoWinkViewController" bundle:nil];
    self.photoWinkController.view.frame = rtScreen;
    self.photoWinkController.view.center = CGPointMake(rtScreen.size.width/2, rtScreen.size.height + self.photoWinkController.view.frame.size.height/2);
    self.photoWinkController.delegate = self;

    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self.photoWinkController.view];
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.photoWinkController.view.center = CGPointMake(rtScreen.size.width/2, rtScreen.size.height/2);
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                         }
                     }];
}

- (void) onSelectedPhotoWink:(UIImage*) image message:(NSString*) text time:(int)seconds {
    
    disableAutoRotate = NO;
    if (image != nil) {
        NSString* msgid = [self Pick_Image:image message:text assetReference:@"" isWink:YES];
        [[TimeBomb sharedInstance] setMessageTimeout:msgid value:seconds];
        [board_table reloadData];
    }
}

- (void) onSelectedPhotoWinkCancel {
    
    disableAutoRotate = NO;
}


- (void)assetPickerControllerDidCancel:(WSAssetPickerController *)sender
{
    // Dismiss the WSAssetPickerController.
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)assetPickerControllerDidLimitSelection:(WSAssetPickerController *)sender {
    
    [Alert show:NSLocalizedString(@"Warning",@"Warning") withMessage:NSLocalizedString(@"You can only send 15 photos at a time.",
                                                                                       @"You can only send 15 photos at a time.")];
}


- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    
    // Dismiss the WSAssetPickerController.
    [self dismissViewControllerAnimated:YES completion:^{
        
        BOOL shouldStart = [arrayAboutUploadImages count] == 0;
        
        int selectedCount = 0;
        ALAsset* selectedAsset = nil;
        for (ALAsset* asset in assets) {
            NSString* type = [asset valueForProperty:ALAssetPropertyType];
            if (type && ([type isEqualToString:ALAssetTypePhoto] || [type isEqualToString:ALAssetTypeVideo])) {
                selectedCount++;
                selectedAsset = asset;
            }
        }
        
        if (selectedCount == 1) {
            UIImage* adjustedImage = [self getUploadablePicture:selectedAsset];
            
            BOOL isVideo = NO;
            NSString* type = [selectedAsset valueForProperty:ALAssetPropertyType];
            if (type != nil && [type isEqualToString:ALAssetTypeVideo]) {
                adjustedImage = [[UIImage alloc] initWithCGImage:selectedAsset.thumbnail];
                isVideo = YES;
            }
            
            [self showInputPhotoInfoScreen:adjustedImage other:selectedAsset isVideo:isVideo];
            return;
        }
        
        for (ALAsset* asset in assets) {
            NSString* type = [asset valueForProperty:ALAssetPropertyType];
            if (type && [type isEqualToString:ALAssetTypePhoto]) {
                [arrayAboutUploadImages addObject:@{@"asset":asset} ];
            }
            else if (type && [type isEqualToString:ALAssetTypeVideo]) {
                [arrayAboutUploadImages addObject:@{@"asset":asset, @"isVideo":@YES} ];
            }
        }
        
        if (shouldStart && [arrayAboutUploadImages count] > 0)
            [self pickProcess:[arrayAboutUploadImages objectAtIndex:0]];
    }];
}

- (void)assetPickerController:(WSAssetPickerController *)sender didFinishPickingMediaWithAssets:(NSArray *)assets
{
}

- (UIImage *)getUploadablePicture:(ALAsset *)asset {
    
    NSString* type = [asset valueForProperty:ALAssetPropertyType];
    if (type == nil || ![type isEqualToString:ALAssetTypePhoto])
        return nil;
    
    NSNumber* orientation = [asset valueForProperty:ALAssetPropertyOrientation];
    CGImageRef cgimage = [[asset defaultRepresentation] fullResolutionImage];
    
    CGSize image_size = CGSizeMake(CGImageGetWidth(cgimage), CGImageGetHeight(cgimage));
    float image_factor = image_size.width / image_size.height;
    
    CGSize adjustedSize = image_size;
    
    if (image_factor >= 1.0 && image_size.width > MAX_DIMENSION) {
        float scale_factor = MAX_DIMENSION / image_size.width;
        adjustedSize = CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor);
    }
    // Need resize by height
    else if (image_factor < 1.0 && image_size.height > MAX_DIMENSION) {
        float scale_factor = MAX_DIMENSION / image_size.height;
        adjustedSize = CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor);
    }
    
    UIImage* adjustedImage = [CommonUtil imageByScalingToSize:adjustedSize cgimage:cgimage orientation:[orientation intValue]];
    return adjustedImage;
}

- (void) pickProcess:(NSDictionary*) param {
    
    ALAsset* asset = [param objectForKey:@"asset"];
    NSString* videoPath = [param objectForKey:@"video_path"];
    if (asset == nil && videoPath == nil)
        return;
    
    NSString* msg = [param objectForKey:@"comment"];
    if (videoPath != nil) {
        [self Pick_Video:nil videoPath:videoPath message:msg];
    }
    else if ([param objectForKey:@"isVideo"]) {
        [self Pick_Video:asset videoPath:nil message:msg];
    }
    else {
        UIImage *adjustedImage;
        adjustedImage = [self getUploadablePicture:asset];
        [self Pick_Image:adjustedImage message:msg assetReference:@"" isWink:NO];
    }
    
    [self performSelector:@selector(pickNextImage) withObject:nil afterDelay:0.05f];
}

- (void) pickNextImage {
    
    if ([arrayAboutUploadImages count] <= 0)
        return;
    
    [arrayAboutUploadImages removeObjectAtIndex:0];
    if ([arrayAboutUploadImages count] <= 0)
        return;
    
    [self pickProcess:[arrayAboutUploadImages objectAtIndex:0]];
}

- (void) didSelectTakePicture {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    [self tapOnTable];
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];
    
    [self setSend_to_chat:chat_id];
    
    if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        
        ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithPhotoMovie];
        [image_picker setImage_picker_delegate:self];
        
        [self presentViewController:image_picker animated:YES completion:^{}];
    }
}

- (void) didSelectLocation {
    
    if ([[JabberLayer SharedInstance] isReconnecting])
        return;
    
    [self tapOnTable];
    [self changeWindowOrientation:UIDeviceOrientationPortrait animated:NO];

    LocationViewController* locationViewController = [[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:nil];
    [self.navigationController pushViewController:locationViewController
                                         animated:YES];
}

-(void)rotationChanged:(NSNotification *)notification{
    
    if (!isAppear || [app.acc isShowingActivityViewer])
        return;
    
    if (disableAutoRotate)
        return;
    
    NSInteger orientation = [[UIDevice currentDevice] orientation];
    
    if (_orientation == orientation || orientation > UIDeviceOrientationLandscapeRight)
        return;
    
    if (bottom_type != BOTTOM_EMPTY) {
        _bottom_type = bottom_type;
        [self tapOnTable];
        
        [self bk_performBlock:^(id obj) {
            _orientation = orientation;
            [self changeWindowOrientation:_orientation animated:YES];
            [self performSelector:@selector(restoreKeyboard) withObject:nil afterDelay:0.15f];
            
        } afterDelay:0.15f];

        return;
    }
    
    _orientation = orientation;
    [self changeWindowOrientation:orientation animated:YES];
}

- (void) changeWindowOrientation:(NSInteger)orientation animated:(BOOL) animated {
    
    float duration = 0.0f;
    if (animated)
        duration = 0.4f;
    
    [emotions_view prepareRotate];
    
    CGRect rtScreen = [UIScreen mainScreen].bounds;
    UIWindow *_window = [[[UIApplication sharedApplication] delegate] window];
    _window.backgroundColor = [UIColor blackColor];
    
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:duration];
            
            [_window setTransform:CGAffineTransformMakeRotation (0)];
            [_window setFrame:rtScreen];
            emotions_view.isLandscape = NO;
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:animated];
            
            [UIView commitAnimations];
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:duration];
            
            [_window setTransform:CGAffineTransformMakeRotation (0/*M_PI*/)];
            [_window setFrame:rtScreen];
            emotions_view.isLandscape = NO;
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:animated];
            
            [UIView commitAnimations];
            break;
        case UIDeviceOrientationLandscapeLeft:
            [UIView beginAnimations:@"rotate_screen" context:nil];
            [UIView setAnimationDuration:duration];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationWillStartSelector:@selector(animationWillStart:context:)];
            
            [_window setTransform:CGAffineTransformMakeRotation (M_PI / 2)];
            [_window setFrame:rtScreen];
            emotions_view.isLandscape = YES;
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:animated];
            
            [UIView commitAnimations];
            break;
        case UIDeviceOrientationLandscapeRight:
            [UIView beginAnimations:@"rotate_screen" context:nil];
            [UIView setAnimationDuration:duration];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationWillStartSelector:@selector(animationWillStart:context:)];
            
            [_window setTransform:CGAffineTransformMakeRotation (- M_PI / 2)];
            [_window setFrame:rtScreen];
            emotions_view.isLandscape = YES;
            
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft animated:animated];
            
            [UIView commitAnimations];
            break;
        default:
            break;
    }
    
    [self refreshMessagesWithMakeGroup:NO msgid:nil];
    prevKeyboardY = -CGFLOAT_MAX;
    
    _myRect = self.view.frame;
    _tableRect = board_table.frame;
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)
            _myRect.origin.y += 12;
    }
}

- (void) animationWillStart:(NSString *)animationID context:(void *)context {
    self.view.frame = _myRect;
    board_table.frame = _tableRect;
    
    int height = [chat_text_view textDidChange:view_is_up];
    [self resizeBoard:height];
    [board_table Scroll_To_Bottom:NO];
}

- (void) restoreKeyboard {
    bottom_type = _bottom_type;
    if (bottom_type == BOTTOM_EMOTIONS) {
        
        [self Show_Emotions:YES];
        [self setKeyboardButton];
    }
    else if (bottom_type == BOTTOM_KEYBOARD) {
        
        [chat_text_view becomeFirstResponder];
        [self setEmotionsButton];
    }
}

- (void) appHasLeaveFromBackground {
    [self performSelector:@selector(notifyToJabberEnteredChatBoard) withObject:nil afterDelay:1.25f];
}

- (void) notifyToJabberEnteredChatBoard {
    if (_isGroupChat)
        [[JabberLayer SharedInstance] notifyEnteredChatBoard:chat_id];
}

- (void) resendMedia:(NSString*) msgId {
    
    NSDictionary* dict = [[ChatStorage sharedStorage] getRecordByServerId:msgId];
    
    NSString* thumbStr = [dict objectForKey:@"media_thumb"];
    NSData* dataThumb = [NSData dataFromBase64String:thumbStr];
    
    [[ChatStorage sharedStorage] setMessageStatusByLocalId:msgId status:MSG_STATUS_IMAGE_UPLOADING];
    [self refreshMessagesWithMakeGroup:YES msgid:msgId];
    
    if ([[dict objectForKey:@"message_type"] intValue] == 2) {
        //video
        NSString* video_name = [dict objectForKey:@"media_name"];
        [[JabberLayer SharedInstance] uploadFile:nil thumb:dataThumb name:video_name target:self local_id:msgId];
    }
    else {
        //photo
        NSString* image_name = [dict objectForKey:@"media_name"];
        UIImage* image = [[ChatStorage sharedStorage] Load_Image_Data:image_name];
        [[JabberLayer SharedInstance] uploadFile:UIImageJPEGRepresentation(image, 0.85) thumb:dataThumb name:image_name target:self local_id:msgId];
    }
}

- (void) person_photo_clicked:(NSNotification*) info {
    
    NSDictionary* param = info.object;
    NSString* msgId = [param objectForKey:@"msgid"];

    NSDictionary* msg = [[ChatStorage sharedStorage] getRecordByServerId:msgId];
    NSString* phoneNo = [msg objectForKey:@"phone"];

    if (phoneNo == nil) {
        //me
        PhotoEditViewController * photo_view_controller = [[PhotoEditViewController alloc] init];
        [self.navigationController pushViewController:photo_view_controller animated:YES];
    }
    else {
        //other
        if ([[AvatarManager sharedInstance] getAvatarWithPhone:phoneNo] == nil) {
            UIActionSheet* actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
            
            [actSheet addButtonWithTitle:NSLocalizedString(@"Take Picture",@"Take Picture")];
            [actSheet addButtonWithTitle:NSLocalizedString(@"Choose From Gallery", @"Choose From Gallery")];
            actSheet.tag = ACTION_SHEET_SELECT_AVATAR;
            
            int cancel_index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
            [actSheet setCancelButtonIndex:cancel_index];
            
            [actSheet showInView:self.view];
            
            selectPhotoTargetPhoneNo = phoneNo;
        }
        else {
            NSDictionary * friend = [[Friends SharedFriends] friendWithPhone:phoneNo];
            if (friend == nil)
                return;
            
            int index = [[friend objectForKey:@"index"] intValue];
            
            MyPersonViewController * person_view_controller = [[MyPersonViewController alloc] init];
            
            [person_view_controller setPerson_id:[NSNumber numberWithInt:index]];
            //[person_view_controller setTitle:NSLocalizedString(@"Information", @"Information")];
            
            [self.navigationController pushViewController:person_view_controller animated:YES];
        }
    }
}

- (void) onNewMessageReceived:(NSString*) msgId {
    if (!isAppear)
        return;
    
    NSDictionary* msgDict = [[ChatStorage sharedStorage] getRecordByServerId:msgId];
    if (msgDict == nil) {
        NSLog(@"error - ChatBoardViewController - onNewMessageReceived");
        return;
    }
    
    NSString* chatId = [msgDict objectForKey:@"room_id"];
    if ([chatId isEqualToString:chat_id])
        return;
    
    NSString* phoneNo = [msgDict objectForKey:@"phone"];
    NSDictionary * last_message = [[ChatStorage sharedStorage] getLastMessage:chatId];
    
    NSString* friendName = [[AddressCollector sharedCollector] Get_Name_By_Phone:phoneNo withPlus:YES];
    NSString* text = [last_message objectForKey:@"text"];
    UIImage* friendPhoto = [[AvatarManager sharedInstance] getAvatarWithPhone:phoneNo];

    PopUpMessageView* popup = [popups objectForKey:chatId];
    if (popup == nil) {
        popup = [[PopUpMessageView alloc] initDefault:self];
        UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
        [window addSubview:popup];//[self.view addSubview:popup];
        [popups setObject:popup forKey:chatId];

        if (_orientation == UIDeviceOrientationLandscapeRight || _orientation == UIDeviceOrientationLandscapeLeft)
            popup.frame = CGRectMake(popup.frame.origin.x, 0, [UIScreen mainScreen].bounds.size.height, popup.frame.size.height);
        else
            popup.frame = CGRectMake(0, popup.frame.origin.y, [UIScreen mainScreen].bounds.size.width, popup.frame.size.height);
        
        [popup setInfo:friendPhoto name:friendName text:text chatid:chatId];
    }
    else {
        if (_orientation == UIDeviceOrientationLandscapeRight || _orientation == UIDeviceOrientationLandscapeLeft)
            popup.frame = CGRectMake(popup.frame.origin.x, 0, [UIScreen mainScreen].bounds.size.height, popup.frame.size.height);
        else
            popup.frame = CGRectMake(0, popup.frame.origin.y, [UIScreen mainScreen].bounds.size.width, popup.frame.size.height);
        
        [popup setText:text];
    }
}

- (void) removeAllPopups {

    for (NSString* key in [popups allKeys]) {
        PopUpMessageView* view = [popups objectForKey:key];
        [popups removeObjectForKey:key];
        [view removeFromSuperview];
    }
}

- (void) gotoOtherRoom:(NSString*) chatid {

    manualViewOperation = YES;
    [self viewWillDisappear:NO];
    [self viewDidDisappear:NO];
    
    [self onInitialUpdate];
    self.roomInfo = @{@"room_id":chatid};
    
    [self viewWillAppear:NO];
    [self viewDidAppear:NO];
    
    [self Show_Keyboard];
    manualViewOperation = NO;
}

- (void) refreshMessagesWithMakeGroup:(BOOL) shouldMakeGroup msgid:(NSString*)msgid {

    if (shouldMakeGroup)
        [board_table UpdateMessagesWithGroups:nil];
    [board_table reloadData];
}

- (void) refreshRoomInfo {

    self.roomInfo = self.roomInfo;
}

- (void) onInitialUpdate {
    
    board_table.showingCount = INITIAL_SHOW_COUNT;
    [[ChatStorage sharedStorage] markRoomRead:chat_id];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage * image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage* pickResult = [CommonUtil fixOrientation:image];
    
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:pickResult cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
        }];
    }];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VPImageCropperDelegate

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
    UIImage* myAvatar = [CommonUtil imageByScalingToSize:CGSizeMake(128, 128) cgimage:editedImage.CGImage orientation:editedImage.imageOrientation];
    if (selectPhotoTargetPhoneNo != nil) {
        //[[AvatarManager sharedInstance] setUserAvatar:myAvatar phone:selectPhotoTargetPhoneNo];
        NSDictionary * friend = [[Friends SharedFriends] friendWithPhone:selectPhotoTargetPhoneNo];
        if (friend != nil) {
            int index = [[friend objectForKey:@"index"] intValue];
            if (index >= 0) {
                [[AddressCollector sharedCollector] Set_Photo_By_Index:index photo:myAvatar];
            }
        }
    }
    [board_table reloadData];
    
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) seenTimerTick {
    
    if (!app.isForeground)
        return;
    
    if ([SettingsManager sharedInstance].enableSendSeen)
        [board_table checkShowCells];
}

- (void) onAddContactNumber {
    
    UIActionSheet* actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Create New Contact",@"Create New Contact")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Add to Existing Contact", @"Add to Existing Contact")];
    actSheet.tag = ACTION_SHEET_ADD_CONTACT_NUMBER;
    
    int cancel_index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:cancel_index];
    
    [actSheet showInView:self.view];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSString* phone_number = [@"+" stringByAppendingString:chat_id];
        [[AddressCollector sharedCollector] addPhoneNumberToContact:person phone:phone_number];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changed_contactbook" object:nil];
    }];
    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    return NO;
}

#pragma mark - ABNewPersonViewControllerDelegate

- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changed_contactbook" object:nil];
}

- (void) didSendLocation:(NSNotification*) notification {

    NSDictionary* dict = notification.object;
    float lat = [[dict objectForKey:@"latitude"] floatValue];
    float lon = [[dict objectForKey:@"longitude"] floatValue];
    UIImage* thumb = [dict objectForKey:@"thumb"];
    NSString* address = [dict objectForKey:@"address"];
    
    NSString* info = [NSString stringWithFormat:@"%f,%f", lat, lon];
    NSString* local_id = [[JabberLayer SharedInstance] requestRandomMessageId];

    long long time_group = [[ChatStorage sharedStorage] addLocationMessage:thumb withLocalId:local_id withChatId:chat_id withLocation:info comment:address];

    [self.board_table Update_Messages:[NSArray arrayWithObject:[NSNumber numberWithLongLong:time_group]] scroll:YES];
}

@end
