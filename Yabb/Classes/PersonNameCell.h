//
//  PersonNameCell.h
//  speedsip
//
//  Created by denebtech on 28.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonNameCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIView * image_back;
@property (nonatomic, strong) IBOutlet UIImageView * person_image;
@property (nonatomic, strong) IBOutlet UILabel * person_name_1;
@property (nonatomic, strong) IBOutlet UILabel * person_name_2;
@property (nonatomic, strong) IBOutlet UILabel * person_nickname;
@property (nonatomic, strong) IBOutlet UIView * main_view;

@end
