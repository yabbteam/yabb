//
//  About.h
//  speedsip
//
//  Created by denebtech on 20/01/11.
//  Modified by denebtech, 10.aug.11
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "BaseViewController.h"

@interface About : BaseViewController
    <UITableViewDelegate, 
    UITableViewDataSource,
    MFMailComposeViewControllerDelegate,
    UIAlertViewDelegate,
    MFMessageComposeViewControllerDelegate>

@end
