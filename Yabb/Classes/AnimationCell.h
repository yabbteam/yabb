//
//  AnimationCell.h
//  speedsip
//
//  Created by denebtech on 21.08.12.
//
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"

@interface AnimationCell : ChatCell {
    IBOutlet UIImageView * back_view;
    IBOutlet UIImageView * image_view;
}

- (void) setAnimation:(NSString *) animation_name editMode:(BOOL)edit;

@end
