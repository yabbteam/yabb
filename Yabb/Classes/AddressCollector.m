//
//  AddressCollector.m
//  speedsip
//
//  Created by denebtech on 03.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddressCollector.h"
#import "ConnectionQueue.h"
#import "Notifications.h"
#import "AppDelegate.h"
#import "NSData+Base64.h"
#import "ChatStorage.h"
#import "Friends.h"
#import "ChatStatusViewController.h"
#import "JabberLayer.h"


#define SET_NOT_NIL(a,b,c) if((a)!=nil)[b setObject:(a) forKey:(c)]

@implementation AddressCollector

static AddressCollector * collector = nil;

+ (AddressCollector *) sharedCollector {
    
    if (collector == nil) {
        collector = [AddressCollector alloc];
        [collector initNddInfo];
    }
    
    return collector;
}

- (void) GetPeopleWithNormal:(BOOL) isNormal {

    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    if (temp_allPeople)
        temp_allPeople = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreate();

    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
        temp_allPeople = [[NSMutableArray alloc] init];

        NSArray * array = (NSArray *)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        
//        // Remove linked duplicates
//        NSMutableSet *linkedPersonsToSkip = [[NSMutableSet alloc] init]; // by dmitry's request

        for (int i = 0; i < [array count]; i++) {
            
            ABRecordRef person = (__bridge ABRecordRef)([array objectAtIndex:i]);
//            if ([linkedPersonsToSkip member:(__bridge id)(person)]) // by dmitry's request
//                continue;
//            
//            NSArray *linked = (NSArray *) CFBridgingRelease(ABPersonCopyArrayOfAllLinkedPeople(person)); // by dmitry's request
//            
//            if ([linked count] > 1)
//                [linkedPersonsToSkip addObjectsFromArray:linked]; // by dmitry's request
            
            NSMutableDictionary* dict = [NSMutableDictionary dictionary];
            
            UIImage* contactPhoto = [self Get_Image_By_Person:person];

            SET_NOT_NIL([self Get_Name_By_Person:person], dict, @"name");
            SET_NOT_NIL([self Get_Phones_By_Person:person localized:NO], dict, @"phones_with_plus");
            
            if (isNormal) {
                SET_NOT_NIL([self Get_Sorting_Name_By_Person:person withSort:kABPersonSortByFirstName], dict, @"sorting_name_by_firstname");
                SET_NOT_NIL([self Get_Sorting_Name_By_Person:person withSort:kABPersonSortByLastName], dict, @"sorting_name_by_lastname");
                SET_NOT_NIL([self Get_Person_Name_1_By_Person:person], dict, @"name1");
                SET_NOT_NIL([self Get_Person_Name_2_By_Person:person], dict, @"name2");
                SET_NOT_NIL(contactPhoto, dict, @"image");
                SET_NOT_NIL([self Get_Phones_By_Person:person localized:YES], dict, @"localized_phones");
                SET_NOT_NIL([self Get_Email_By_Person:person], dict, @"email");
                
                [dict setObject:[NSNumber numberWithInt:ABRecordGetRecordID(person)] forKey:@"recordid"];
            }
            
            [temp_allPeople addObject:dict];
        }
        
        if (0) {
            srand(12345);
            for (int k = 0; k < 8000; k++) {
                
                NSMutableDictionary* dict = [NSMutableDictionary dictionary];
                int random = rand() + 10000000;
                
                [dict setObject:[NSString stringWithFormat:@"%d", random] forKey:@"name"];
                [dict setObject:[NSMutableDictionary dictionaryWithDictionary:@{@"sort_char":@"1", @"sort_name":[NSString stringWithFormat:@"%d", random]}] forKey:@"sorting_name_by_firstname"];
                [dict setObject:[NSMutableDictionary dictionaryWithDictionary:@{@"sort_char":@"1", @"sort_name":[NSString stringWithFormat:@"%d", random]}] forKey:@"sorting_name_by_lastname"];
                [dict setObject:[NSString stringWithFormat:@"%d", random] forKey:@"name1"];
                [dict setObject:[NSString stringWithFormat:@"%d", random] forKey:@"name2"];
                [dict setObject:[NSMutableArray arrayWithArray:@[[NSString stringWithFormat:@"+%d", random]]] forKey:@"phones_with_plus"];
                [dict setObject:[NSMutableArray arrayWithArray:@[[NSString stringWithFormat:@"%d", random]]] forKey:@"phones"];
                [dict setObject:[NSMutableArray arrayWithArray:@[[NSMutableDictionary dictionaryWithDictionary:@{@"number":[NSString stringWithFormat:@"%d", random], @"name":@"mobile"}]]] forKey:@"localized_phones"];
                
                [temp_allPeople addObject:dict];
            }
        }
    }
    
    if (isNormal)
        [self isContactBookChanged];
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Get people [%.02f]", timer);

    if ([NSThread isMainThread])
        [self GetPeopleSynchronize];
}

- (BOOL) isContactBookChanged {

    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    NSMutableDictionary* hash = [[NSMutableDictionary alloc] init];

    ABAddressBookRef addressBook = ABAddressBookCreate();
    NSArray * array = (NSArray *)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    
    for (int i = 0; i < [array count]; i++) {
        ABRecordRef person = (__bridge ABRecordRef)([array objectAtIndex:i]);
        
        NSDate *lastModiDate = (__bridge NSDate*)ABRecordCopyValue(person, kABPersonModificationDateProperty);
        int index = ABRecordGetRecordID(person);

        [hash setObject:lastModiDate forKey:[NSNumber numberWithInt:index]];
    }
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"isContactBookChanged [%.02f]", timer);
    
    if (hashModiInfo == nil) {
        hashModiInfo = hash;
        return YES;
    }
    
    if ([hashModiInfo count] != [hash count]) {
        hashModiInfo = hash;
        return YES;
    }
    
    for (NSNumber* number in [hash allKeys]) {
        NSDate* date1 = [hash objectForKey:number];
        NSDate* date2 = [hashModiInfo objectForKey:number];
        if (date2 == nil) {
            hashModiInfo = hash;
            return YES;
        }
        if (![date1 isEqualToDate:date2]) {
            hashModiInfo = hash;
            return YES;
        }
    }
    
    hashModiInfo = hash;
    hash = nil;
    
    return NO;
}

- (void) GetPeopleSynchronize {

    allPeople = temp_allPeople;
    
    [app->contacts Make_Contacts];
    [self makeSearchBook];
    
    temp_allPeople = nil;
}

- (int) Num_Of_Contacts {

    if (allPeople)
        return [allPeople count];
    return 0;
}

- (NSString *) Get_Name_By_Index:(int)index {
    
    if (index < 0)
        return nil;
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    NSString* name = [dict objectForKey:@"name"];
    if ([name length] > 35) {
        name = [name substringToIndex:35];
    }
    
    return name;
}

- (NSDictionary *) Get_Sorting_Name_By_Index:(int)index withSort:(ABPersonSortOrdering)sort {
    
    if (index < 0)
        return nil;
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    if (sort == kABPersonSortByFirstName) {
        return [dict objectForKey:@"sorting_name_by_firstname"];
    }
    else {
        return [dict objectForKey:@"sorting_name_by_lastname"];
    }
}

- (NSString *) Get_Person_Name_1:(int)index {
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    NSString* name = [dict objectForKey:@"name1"];
    if ([name length] > 35) {
        name = [name substringToIndex:35];
    }
    return name;
}

- (NSString *) Get_Person_Name_2:(int)index {
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    NSString* name = [dict objectForKey:@"name2"];
    if ([name length] > 35) {
        name = [name substringToIndex:35];
    }
    return name;
}

- (UIImage *) Get_Image_By_Index:(int)index {
    
    if (index < 0)
        return nil;
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    return [dict objectForKey:@"image"];
}

- (NSMutableArray *) Get_Phones_By_Index:(int)index {
    
    if (index < 0)
        return nil;
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    return [dict objectForKey:@"phones_with_plus"];
}

- (NSMutableArray *) Get_Localized_Phones:(int)index {
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    return [dict objectForKey:@"localized_phones"];
}

- (NSString *) Get_Email:(int)index {
    
    NSDictionary* dict = [allPeople objectAtIndex:index];
    return [dict objectForKey:@"email"];
}

- (NSString *) Get_Name_By_Person:(ABRecordRef)person {
    
    NSString * name = (NSString *)CFBridgingRelease(ABRecordCopyCompositeName(person));
    
    return name?name:NSLocalizedString(@"No name", @"No name");
}

- (NSDictionary *) Get_Sorting_Name_By_Person:(ABRecordRef)person withSort:(ABPersonSortOrdering)sort_order {
    
    CFTypeRef first_ref = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    CFTypeRef last_ref = ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    NSString * first_name = first_ref?[NSString stringWithString:(__bridge NSString *)(first_ref)]:nil;
    NSString * last_name = last_ref?[NSString stringWithString:(__bridge NSString *)(last_ref)]:nil;
    
    NSString * sort_name = nil;
    
    if (sort_order == kABPersonSortByFirstName) {
        if (first_name)
            sort_name = [NSString stringWithString:first_name];
        else if (last_name)
            sort_name = [NSString stringWithString:last_name];
    }
    else if (sort_order == kABPersonSortByLastName) {
        if (last_name)
            sort_name = [NSString stringWithString:last_name];
        else if (first_name)
            sort_name = [NSString stringWithString:first_name];
    }
    
    if (sort_name == nil) {
        CFTypeRef composite_ref = ABRecordCopyCompositeName(person);
        if (composite_ref)
            sort_name = [NSString stringWithString:(__bridge NSString *)(composite_ref)];
    }
    
    if (sort_name == nil || [sort_name length] == 0)
        return nil;
    
    NSString * sort_char = [[NSString stringWithString:[sort_name substringWithRange:NSMakeRange(0, 1)]] uppercaseString];
    
    NSCharacterSet *nonDigits = [NSCharacterSet letterCharacterSet];
    BOOL containsNonDigitChars = ([sort_char rangeOfCharacterFromSet:nonDigits].location == NSNotFound);
    
    if (containsNonDigitChars == NO)
        return [NSDictionary dictionaryWithObjectsAndKeys:sort_char, @"sort_char", sort_name, @"sort_name", nil];
    else
        return [NSDictionary dictionaryWithObjectsAndKeys:@"#", @"sort_char", sort_name, @"sort_name", nil];
}

- (int) Get_Index_By_Phone:(NSString *)phone {
    
    NSNumber* number = [cachePhone_Index objectForKey:phone];
    if (number == nil)
        return -1;
    
    return [number integerValue];
}

- (NSString *) Get_Name_By_Phone:(NSString *)phone_number withPlus:(BOOL)plus {
    
    if (!plus)
        phone_number = [@"+" stringByAppendingString:phone_number];
    
    int index = [self Get_Index_By_Phone:phone_number];
    if (index >= 0)
        return [self Get_Name_By_Index:index];
    else {
        NSString* nickName = [[JabberLayer SharedInstance] getNickNameByPhone:phone_number];
        if (nickName)
            return [NSString stringWithFormat:@"%@ (%@)", phone_number, nickName];
        else
            return phone_number;
    }
}

- (NSString*) getInternationalPhoneNumber:(NSString*)phone {

    if (phone == nil || [phone length] == 0)
        return nil;
    
    if ([phone characterAtIndex:0] == '+')
        return phone;
    
    NSString* abbr = [[NSUserDefaults standardUserDefaults] objectForKey:@"country_abbr"];
    NSString* nddCode = [ndd_infos objectForKey:abbr];
    if (nddCode == nil)
        return nil;
    
    NSString* ccode = [[NSUserDefaults standardUserDefaults] objectForKey:@"ccode"];
    
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
    if (user_phone == nil || ccode == nil) {
        return nil;
    }
    user_phone = [@"+" stringByAppendingString:user_phone];

    if (nddCode.length > 0) {
        NSRange r = [phone rangeOfString:nddCode];
        if (r.location == 0 && r.length == nddCode.length) {
            phone = [phone substringFromIndex:r.length];
        }
    }
    
    phone = [NSString stringWithFormat:@"+%@%@", ccode, phone];

    if (phone.length != user_phone.length)
        return nil;
    
    return phone;
}
/*
- (ABRecordRef) Get_Friend_By_Phone:(NSString *)phone_number {
    
    int index = 0;
    for (NSDictionary * contact in allFriends) {
        
        ABRecordRef ref = [allFriends objectAtIndex:index];
        
        ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        
        for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, i);
            NSString * stripped_phone = [self clearedPhoneNumber:(NSString *)phoneNumberRef];
            
            if ([stripped_phone isEqualToString:phone_number]) {
                CFRelease(phones);
                return ref;
            }
        }
        
        CFRelease(phones);
        
        index++;
    }
    
    return nil;
}*/

- (UIImage *) Get_Image_By_Person:(ABRecordRef)person {
    
    if(ABPersonHasImageData(person)){
//        CFDataRef data = ABPersonCopyImageData(person);
  
        CFDataRef data = ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
        if (data) {
            
            UIImage * image = [UIImage imageWithData:(__bridge NSData *)data];
            
            CFRelease(data);
            
            return image;
        }
    }
    
    return nil;
}

#define MAX_PHONE_LENGTH    20

- (NSString *) clearedPhoneNumberLeft:(NSString *) dirty_number {
    
    NSString * phone_str = [[dirty_number componentsSeparatedByCharactersInSet:
                             [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                              invertedSet]] 
                            componentsJoinedByString:@""];
    int length = [phone_str length] > MAX_PHONE_LENGTH?MAX_PHONE_LENGTH:[phone_str length];
    
    NSRange range = NSMakeRange(0, length);
    
    return [phone_str substringWithRange:range];    
}

- (NSString *) clearedPhoneNumber:(NSString *) dirty_number withPlus:(BOOL)plus {
    
    
    if (dirty_number == nil)
        return nil;
    
    NSString * in_string = plus?@"+0123456789":@"0123456789";
    
    NSString * phone_str = [[dirty_number componentsSeparatedByCharactersInSet:
                             [[NSCharacterSet characterSetWithCharactersInString:in_string]
                              invertedSet]] 
                            componentsJoinedByString:@""];
    
    // Get last 10 digits
    NSRange range = NSMakeRange(0, 0);
    if ([phone_str length] > MAX_PHONE_LENGTH) {
        range.location = 0;
        range.length = [phone_str length] - MAX_PHONE_LENGTH;
    }
    
   return [phone_str stringByReplacingCharactersInRange:range withString:@""];
}

- (NSMutableArray *) Get_Phones_By_Person:(ABRecordRef)person localized:(BOOL)isLocalized {
    
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    
    if (phones == nil)
        return nil;
    
    NSMutableArray * all_phones = [NSMutableArray array];
    
    for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++) {
        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
        NSString * stripped_phone = [self clearedPhoneNumber:(__bridge NSString *)phoneNumberRef withPlus:YES];
        
        stripped_phone = [self getInternationalPhoneNumber:stripped_phone];
        if (stripped_phone != nil) {
            if (!isLocalized)
                [all_phones addObject:stripped_phone];
            else {
                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phones, j);
                NSString *phoneLabel =(NSString*) CFBridgingRelease(ABAddressBookCopyLocalizedLabel(locLabel));
                
                NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:(__bridge NSString *)phoneNumberRef, @"number", phoneLabel, @"name", nil];
                [all_phones addObject:dict];
                
                if (locLabel != nil)
                    CFRelease(locLabel);
            }
        }
        
        CFRelease(phoneNumberRef);
    }     
    
    CFRelease(phones);
    
    return all_phones;
    
}

- (NSString *) Get_Name_For_Person:(ABRecordRef)person {
    
    CFStringRef comp = ABRecordCopyCompositeName(person);
    
    NSString * name = [NSString stringWithString:(__bridge NSString *)comp];
    
    CFRelease(comp);
    
    return name;

}

- (NSString *) Get_Email_By_Person:(ABRecordRef)person {

    ABMultiValueRef value = ABRecordCopyValue(person, kABPersonEmailProperty);
    
    if (value == nil || ABMultiValueGetCount(value) == 0)
        return nil;
    
    CFStringRef email = ABMultiValueCopyValueAtIndex(value, 0);

    NSString * e = [NSString stringWithString:(__bridge NSString *)email];
    
    CFRelease(email);
    CFRelease(value);
    
    return e;
    
}

- (NSString *) Get_Person_Name_1_By_Person:(ABRecordRef)person {
 
    ABMultiValueRef prefix = ABRecordCopyValue(person, kABPersonPrefixProperty);
    ABMultiValueRef first_name = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    ABMultiValueRef middle_name = ABRecordCopyValue(person, kABPersonMiddleNameProperty);
    ABMultiValueRef last_name = ABRecordCopyValue(person, kABPersonLastNameProperty);
    ABMultiValueRef suffix = ABRecordCopyValue(person, kABPersonSuffixProperty);
    
    NSMutableString * str = [NSMutableString string];
    
    if (prefix) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", prefix];
        CFRelease(prefix);
    }
    
    if (ABPersonGetCompositeNameFormat() == kABPersonCompositeNameFormatLastNameFirst) {
        if (last_name) {
            [str appendFormat:@"%@%@", [str length]?@" ":@"", last_name];
            CFRelease(last_name);
        }
    }
    
    if (first_name) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", first_name];
        CFRelease(first_name);
    }
    
    if (middle_name) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", middle_name];
        CFRelease(middle_name);
    }
    
    if (ABPersonGetCompositeNameFormat() == kABPersonCompositeNameFormatFirstNameFirst) {
        if (last_name) {
            [str appendFormat:@"%@%@", [str length]?@" ":@"", last_name];
            CFRelease(last_name);
        }
    }
    
    if (suffix) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", suffix];
        CFRelease(suffix);
    }
    
    return str;
}

- (NSString *) Get_Person_Name_2_By_Person:(ABRecordRef)person {
    
    ABMultiValueRef first_name = ABRecordCopyValue(person, kABPersonFirstNamePhoneticProperty);
    ABMultiValueRef last_name = ABRecordCopyValue(person, kABPersonLastNamePhoneticProperty);
    ABMultiValueRef nickname = ABRecordCopyValue(person, kABPersonNicknameProperty);
    ABMultiValueRef job = ABRecordCopyValue(person, kABPersonJobTitleProperty);
    ABMultiValueRef departament = ABRecordCopyValue(person, kABPersonDepartmentProperty);
    ABMultiValueRef organization = ABRecordCopyValue(person, kABPersonOrganizationProperty);
    
    NSMutableString * str = [NSMutableString string];
    
    if (first_name) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", first_name];
        CFRelease(first_name);
    }
    if (last_name) {
        [str appendFormat:@"%@%@", [str length]?@" ":@"", last_name];
        CFRelease(last_name);
    }
    if (nickname) {
        [str appendFormat:@"%@«%@»", [str length]?@"\n":@"", nickname];
        CFRelease(nickname);
    }
    
    if (job && departament) {
        [str appendFormat:@"%@%@ - %@", [str length]?@"\n":@"", job, departament];
        CFRelease(job);
        CFRelease(departament);
    }
    else if (job) {
        [str appendFormat:@"%@%@", [str length]?@"\n":@"", job];
        CFRelease(job);
    }
    else if (departament) {
        [str appendFormat:@"%@%@", [str length]?@"\n":@"", departament];
        CFRelease(departament);
    }
    
    if (organization) {
        [str appendFormat:@"%@%@", [str length]?@"\n":@"", organization];
        CFRelease(organization);
    }
    
    return str;
}

- (NSString *) Get_Leading_Digit:(NSString *) phone {

    if (phone == nil || [phone isEqualToString:@""])
        return @"";
    
    if ( [[phone substringToIndex:1] isEqualToString:@"+"] )
        return @"8";
    
    if ( [[phone substringToIndex:1] isEqualToString:@"0"] ) {

        for (int i = 1; i < [phone length]-1; i++) {
            
            NSString * s = [phone substringWithRange:NSMakeRange(i, 1)];
        
            if ([s isEqualToString:@"0"] == NO) {
                
                if (i < 8)
                    return [NSString stringWithFormat:@"%d", i];
                else
                    return @"7";
            }
        }
        
    }

    return @"0";
}

- (NSString *)packPhoneNumber:(NSString *)ph {

    NSMutableString * result = [NSMutableString string];
    
    [result appendString:[self Get_Leading_Digit:ph]];
    [result appendFormat:@"%013qx", [ph longLongValue]];
    
    return result;
}

- (NSString *) Get_All_Contacts {
    
    NSMutableString * phoneString = [NSMutableString string];
    CFIndex nPeople = [self Num_Of_Contacts];
    
    NSLog(@"This phone has %ld contacts", nPeople);
    
    for ( int i = 0; i < nPeople; i++ ) {
        NSArray * phones = [self Get_Phones_By_Index:i];
        NSString * name = [self Get_Name_By_Index:i];

        if (phones == nil || [phones count] == 0 || name == nil)
            continue;

        [phoneString appendString:[ [name dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString] ];
        [phoneString appendString:@"\n"];
        
        for (NSString * phone in phones) {
            if (phone == nil || [phone length]==0)
                continue;
            
            NSString *phoneStr = phone;
            if ([phone characterAtIndex:0] != '+')
                phoneStr = [NSString stringWithFormat:@"0%@", phone];
            
            [phoneString appendString:[self packPhoneNumber:phoneStr]];
        }

        [phoneString appendString:@"\n"];
    }
    
    return phoneString;
}

- (void) Upload_To_Server:(BOOL) isLoginProgress {
    
    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];

    NSString * abook = [self Get_All_Contacts];
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];

    if ([abook length] == 0)
        abook = @"same";

    NSLog(@"abook size = %d", [abook length]);
    
    if (abook && [abook length] > 0) {
        NSString * token = [[Notifications sharedNotifications] getToken];
        
        //NSString * version_str = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        //NSString * new_version_str = [version_str stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        
        NSString* savedAbook = [self loadAbookCache];
        if (savedAbook && [savedAbook isEqualToString:abook]) {
            uploadedAbook = nil;
            abook = @"same";
            NSLog(@"abook = %@", abook);
        }
        else {
            uploadedAbook = abook;
        }
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"import", @"cmd", user_phone, @"phone", abook, @"abook", token, @"token", build, @"ver", nil];
        
        NSString* connectionName = nil;
        if (isLoginProgress || uploadedAbook != nil)
            connectionName = @"upload_abook"; //fetch roster info
        
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:connectionName silent:YES];
    }
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    
    NSLog(@"Upload Abook to server: %.02f", timer);
}

- (CGRect) rectForText:(NSString *)text inFrame:(CGRect)currentFrame withFont:(UIFont *)font {
    
    CGSize max = CGSizeMake(currentFrame.size.width, 500);
    CGSize expected = [CommonUtil getTextSize:text font:font constrainedToSize:max lineBreakMode:UILineBreakModeWordWrap];
    currentFrame.size.height = expected.height;
    
    return currentFrame;    
}

#pragma mark -
#pragma mark Connection Delegate

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    NSLog(@"*** Uploaded book: OK ***");
    
    int error_code = [[dict objectForKey:@"errorcode"] intValue];
    
    if (error_code == -2) {

        [[ChatStorage sharedStorage] resetAccount];

        [app.register_view_controller clearSelection];
//        [app.tabBarController.selectedViewController presentModalViewController:app.register_view_controller animated:NO];
        [app.tabBarController.selectedViewController presentViewController:app.register_view_controller animated:NO completion:nil];

        [self runConnectedDelegate:NO];

        return;
    }
    else if (error_code != 1) {
        
        [self runConnectedDelegate:NO];

        return;
    }
    
    [self saveAbookCache:uploadedAbook];
    
    if (connection_name != nil) {
        [self runConnectedDelegate:YES];
        return;
    }
    
    NSDictionary * data = [dict objectForKey:@"data"];
    
    NSArray * friends = [data objectForKey:@"friends"];
    if (friends && [friends isKindOfClass:[NSArray class]]) {
        
        [[Friends SharedFriends] setOnline:friends];
        [app->contacts Make_Friends];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
    }
    
    NSString * status_encoded = [data objectForKey:@"origin"];
    if (status_encoded) {
        NSString * user_status = [[ChatStorage sharedStorage] decodeString:status_encoded];
        
        if (user_status)
            [ChatStatusViewController setStatus:user_status];
    }
    
    NSString * v_promo = [data objectForKey:@"promo"];
    NSString * v_url = [data objectForKey:@"url"];
    NSString * v_ver = [data objectForKey:@"ver"];
    
    if (v_promo && v_url && v_ver/* && app.need_promo*/) {
        
        [self setPromo:v_promo];
        [self setUrl:v_url];
        [self setVer:v_ver];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New version available", @"New version available") message:v_promo delegate:self cancelButtonTitle:NSLocalizedString(@"Later", @"Later") otherButtonTitles:NSLocalizedString(@"Update Now", @"Update Now"), nil];
        [alert show];
        
        app.need_promo = NO;
    }

//    UINavigationController * nav_controller = app.register_view_controller.navigationController;
    
    [app.contactsNavController.people_picker.table_view reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"force":@YES}];
    
}

- (void) didConnectionFailed:(NSString *)connection_name {
    [self runConnectedDelegate:NO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.url]];
    }
    
    return;
}

- (void) setConnectionDoneDeleagate:(id)delegate {
    connection_done_delegate =  delegate;
}

- (void) runConnectedDelegate:(BOOL) succcess {
    if (connection_done_delegate != nil && [connection_done_delegate respondsToSelector:@selector(uploadedABook:)] ) {
        [connection_done_delegate performSelector:@selector(uploadedABook:) withObject:[NSNumber numberWithBool:succcess]];
    }
}

- (void) remakeAllPeopleByFriends:(NSArray*) arrayFriends {
    
    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    [self removeNoNeedPhone];
    
    [self fixDuplicatedRosterPhones];
    
//    if (arrayFriends != nil)
//        [self applyNickNames];
    
    [app->contacts Make_Contacts];
    [app->contacts Make_Friends];

    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Remake Contact book [%.02f]", timer);
}

- (void) removeNoNeedPhone {
    
    [[Friends SharedFriends] updatePhonebookRelation];
    
    NSMutableIndexSet* deleted_people = [[NSMutableIndexSet alloc] init];
    NSMutableDictionary* cacheDict = [NSMutableDictionary dictionary];
    
    for (int index = 0; index < allPeople.count; index ++) {
    
        NSMutableArray* phones1 = [self Get_Phones_By_Index:index];
        NSMutableArray* phones3 = [self Get_Localized_Phones:index];
        
        NSString* mark_deleted = @"noneed";
        [cacheDict removeAllObjects];
        
        for (int i = 0; i < phones1.count; i++) {
            NSString* phone_number = [phones1 objectAtIndex:i];
            
            NSDictionary* friend = [[Friends SharedFriends] friendWithPhone:phone_number];
            if (friend == nil)
                continue;
            
            NSNumber* indexNum = [friend objectForKey:@"index"];
            if (indexNum == nil)
                continue;
            
            int peolpe_index_by_friend = [indexNum intValue];
            
            if (peolpe_index_by_friend != index || [cacheDict objectForKey:phone_number] != nil) {
                [phones1 replaceObjectAtIndex:i withObject:mark_deleted];
                [phones3 replaceObjectAtIndex:i withObject:mark_deleted];
            }
            [cacheDict setObject:phone_number forKey:phone_number];
        }
        
        [phones1 removeObjectIdenticalTo:mark_deleted];
        [phones3 removeObjectIdenticalTo:mark_deleted];
        
        if (phones1.count == 0) {
            [deleted_people addIndex:index];
        }
    }
    
    [allPeople removeObjectsAtIndexes:deleted_people];
    [self makeSearchBook];
    
    [[Friends SharedFriends] updatePhonebookRelation];
    
    cacheDict = nil;
}

- (void) fixDuplicatedRosterPhones {
    
    int index = 0;
    while (true) {
        if (index >= allPeople.count)
            break;
        
        NSMutableDictionary* people = [allPeople objectAtIndex:index];
        
        int rosterCount = 0;
        int firstRoster = -1;
        NSMutableArray* phones1 = [self Get_Phones_By_Index:index];
        for (int i = 0; i < phones1.count; i++) {
            NSDictionary* dict = [[Friends SharedFriends] friendWithPhone:[phones1 objectAtIndex:i]];
            if (dict != nil) {
                if (rosterCount > 0) {
                    NSMutableDictionary* dict = [self makeNewPerson:people withPhoneIndex:i];
                    if (dict == nil) {
                        rosterCount = 0;
                        break;
                    }
                    [allPeople insertObject:dict atIndex:index+rosterCount];
                }
                else {
                    firstRoster = i;
                }
                rosterCount++;
            }
        }
        
        if (rosterCount > 1 && firstRoster >= 0) {
            NSMutableDictionary* dict = [self makeNewPerson:people withPhoneIndex:firstRoster];
            if (dict != nil)
                [allPeople replaceObjectAtIndex:index withObject:dict];
        }
        
        index++;
    }
    
    [self makeSearchBook];
}

- (NSMutableDictionary*) makeNewPerson:(NSMutableDictionary*) parent withPhoneIndex:(int)index {
    
    NSArray* phones = [parent objectForKey:@"phones_with_plus"];
    if (phones == nil || index >= phones.count)
        return nil;
    
    NSString* phone = [phones objectAtIndex:index];
    
    NSMutableDictionary* ret = [self mutableDeepCopy:parent];
    NSString* name = [ret objectForKey:@"name"];
    
    [ret setObject:[NSString stringWithFormat:@"%@ (%@)", name, phone] forKey:@"name"];
    
    NSMutableIndexSet *is = [[NSMutableIndexSet alloc] init];
    for (int i = 0; i < phones.count; i++) {
        if (i != index)
            [is addIndex:i];
    }
    
    NSMutableArray* phones1 = [ret objectForKey:@"phones_with_plus"];
    NSMutableArray* phones2 = [ret objectForKey:@"phones"];
    NSMutableArray* phones3 = [ret objectForKey:@"localized_phones"];

    [phones1 removeObjectsAtIndexes:is];
    [phones2 removeObjectsAtIndexes:is];
    [phones3 removeObjectsAtIndexes:is];
    
    return ret;
}

- (NSMutableDictionary *)mutableDeepCopy:(NSMutableDictionary*) parent
{
    NSData *buffer;
    NSMutableDictionary *_dict2;
    
    // Deep copy "all" objects in _dict1 pointers and all to _dict2
    buffer = [NSKeyedArchiver archivedDataWithRootObject: parent];
    _dict2 = [NSKeyedUnarchiver unarchiveObjectWithData: buffer];

    return _dict2;
}

- (void) Set_Photo_By_Index:(int) index photo:(UIImage*) photo {
    
    NSMutableDictionary* people = [allPeople objectAtIndex:index];
    [people setObject:[[UIImage alloc] initWithCGImage:photo.CGImage] forKey:@"image"];
    
    NSNumber* num = [people objectForKey:@"recordid"];
    if (num != nil) {
        int recordId = [num integerValue];
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(nil, nil);
        
        ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBookRef, recordId);
        ABPersonRemoveImageData(person, NULL);
        
        ABAddressBookAddRecord(addressBookRef, person, nil);
        ABAddressBookSave(addressBookRef, nil);
        
        NSData *picData = UIImageJPEGRepresentation(photo, 0.9f);
        CFDataRef cfData=CFDataCreate(NULL, [picData bytes], [picData length]);
        ABPersonSetImageData(person, cfData, nil);
        
        ABAddressBookSave(addressBookRef, nil);
    }
}

- (void) clearAbookCache {
    
    uploadedAbook = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"uploadedAbook"];
    [defaults removeObjectForKey:@"lastUploadedTime"];
    [defaults synchronize];
}

- (void) saveAbookCache:(NSString*)abook {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (abook != nil)
        [defaults setObject:abook forKey:@"uploadedAbook"];
    [defaults setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"lastUploadedTime"];
    [defaults synchronize];
}

- (NSString*) loadAbookCache {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"uploadedAbook"];
}

- (BOOL) isNeedToUploadAbook {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval savedTime = [defaults doubleForKey:@"lastUploadedTime"];
    return ([[NSDate date] timeIntervalSince1970] - savedTime) > 24*60*60;
}

- (void) makeSearchBook {
    
    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    if (cachePhone_Index == nil)
        cachePhone_Index = [[NSMutableDictionary alloc] initWithCapacity:2500];
    
    [cachePhone_Index removeAllObjects];
    
    NSMutableDictionary* temp_dict = [NSMutableDictionary dictionary];
    
    for (int index = 0; index < [allPeople count]; index++) {
        
        NSArray* phones = [self Get_Phones_By_Index:index];
        for (int i = 0; i < phones.count; i++) {
            NSString* phone = [phones objectAtIndex:i];
            NSNumber* prevIndex = [cachePhone_Index objectForKey:phone];
            if (prevIndex == nil) {
                [cachePhone_Index setObject:[NSNumber numberWithInt:index] forKey:phone];
                [temp_dict setObject:[NSNumber numberWithLong:phones.count] forKey:phone];
            }
            else {
                long prevCount = [[temp_dict objectForKey:phone] integerValue];
                if (phones.count < prevCount) {
                    [cachePhone_Index setObject:[NSNumber numberWithInt:index] forKey:phone];
                    [temp_dict setObject:[NSNumber numberWithLong:phones.count] forKey:phone];
                }
            }
        }
    }

    [temp_dict removeAllObjects];
    temp_dict = nil;

    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"makeSearchBook [%.02f]", timer);
}

- (void) applyNickNames {
    
    for (int index = 0; index < [allPeople count]; index++) {
        
        NSString* friendPhone = [[Friends SharedFriends] friendPhoneByIndex:index];
        if (friendPhone == nil)
            continue;
        
        NSString* nickname = [[JabberLayer SharedInstance] getNickNameByPhone:friendPhone];
        if (nickname == nil || nickname.length == 0)
            continue;
        
        NSMutableDictionary* people = [allPeople objectAtIndex:index];
        NSString* text;
        text = [people objectForKey:@"name"];
        [people setObject:[NSString stringWithFormat:@"%@ (%@)", text, nickname] forKey:@"name"];

//        text = [people objectForKey:@"sorting_name_by_lastname"];
//        [people setObject:[NSString stringWithFormat:@"%@(%@)", text, nickname] forKey:@"sorting_name_by_lastname"];

        text = [people objectForKey:@"name2"];
        [people setObject:[NSString stringWithFormat:@"%@ (%@)", text, nickname] forKey:@"name2"];
    }
}

- (ABRecordRef) addNewContactWithPhoneNumber:(NSString*)phone_number {
    
    ABRecordRef person = ABPersonCreate();
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) phone_number, kABPersonPhoneMainLabel, NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);

    return person;
}

- (void) addPhoneNumberToContact:(ABRecordRef) person phone:(NSString*)phone_number {
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutableCopy(phones);
    
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) phone_number, kABPersonPhoneMobileLabel, NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);
    
    CFErrorRef error = nil;
    ABAddressBookAddRecord(addressBook, person, &error);
    ABAddressBookSave(addressBook, nil);
}

- (void) addNewContactWithPhoneNumber:(NSString*)phone_number name:(NSString*) name {
    
    ABRecordRef person = ABPersonCreate();
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) phone_number, kABPersonPhoneMainLabel, NULL);
    ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) name, nil);
    CFRelease(phoneNumberMultiValue);
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    CFErrorRef error = nil;
    ABAddressBookAddRecord(addressBook, person, &error);
    ABAddressBookSave(addressBook, nil);
}

- (void) initNddInfo {
    
    ndd_infos = [NSMutableDictionary dictionary];
    
    [ndd_infos setObject:@"" forKey:@"AD"];
    [ndd_infos setObject:@"0" forKey:@"AE"];
    [ndd_infos setObject:@"0" forKey:@"AF"];
    [ndd_infos setObject:@"1" forKey:@"AG"];
    [ndd_infos setObject:@"1" forKey:@"AI"];
    [ndd_infos setObject:@"0" forKey:@"AL"];
    [ndd_infos setObject:@"8" forKey:@"AM"];
    [ndd_infos setObject:@"0" forKey:@"AN"];
    [ndd_infos setObject:@"0" forKey:@"AO"];
    [ndd_infos setObject:@"" forKey:@"AQ"];
    [ndd_infos setObject:@"0" forKey:@"AR"];
    [ndd_infos setObject:@"1" forKey:@"AS"];
    [ndd_infos setObject:@"0" forKey:@"AT"];
    [ndd_infos setObject:@"0" forKey:@"AU"];
    [ndd_infos setObject:@"" forKey:@"AW"];
    [ndd_infos setObject:@"8" forKey:@"AZ"];
    [ndd_infos setObject:@"0" forKey:@"BA"];
    [ndd_infos setObject:@"1" forKey:@"BB"];
    [ndd_infos setObject:@"0" forKey:@"BD"];
    [ndd_infos setObject:@"0" forKey:@"BE"];
    [ndd_infos setObject:@"" forKey:@"BF"];
    [ndd_infos setObject:@"0" forKey:@"BG"];
    [ndd_infos setObject:@"" forKey:@"BH"];
    [ndd_infos setObject:@"" forKey:@"BI"];
    [ndd_infos setObject:@"" forKey:@"BJ"];
    [ndd_infos setObject:@"1" forKey:@"BM"];
    [ndd_infos setObject:@"0" forKey:@"BN"];
    [ndd_infos setObject:@"0" forKey:@"BO"];
    [ndd_infos setObject:@"0" forKey:@"BR"];
    [ndd_infos setObject:@"1" forKey:@"BS"];
    [ndd_infos setObject:@"" forKey:@"BT"];
    [ndd_infos setObject:@"" forKey:@"BW"];
    [ndd_infos setObject:@"8" forKey:@"BY"];
    [ndd_infos setObject:@"0" forKey:@"BZ"];
    [ndd_infos setObject:@"1" forKey:@"CA"];
    [ndd_infos setObject:@"" forKey:@"CD"];
    [ndd_infos setObject:@"" forKey:@"CF"];
    [ndd_infos setObject:@"0" forKey:@"CH"];
    [ndd_infos setObject:@"0" forKey:@"CI"];
    [ndd_infos setObject:@"0" forKey:@"CL"];
    [ndd_infos setObject:@"" forKey:@"CM"];
    [ndd_infos setObject:@"0" forKey:@"CN"];
    [ndd_infos setObject:@"09" forKey:@"CO"];
    [ndd_infos setObject:@"" forKey:@"CR"];
    [ndd_infos setObject:@"" forKey:@"CV"];
    [ndd_infos setObject:@"0" forKey:@"CX"];
    [ndd_infos setObject:@"" forKey:@"CY"];
    [ndd_infos setObject:@"" forKey:@"CZ"];
    [ndd_infos setObject:@"0" forKey:@"DE"];
    [ndd_infos setObject:@"" forKey:@"DJ"];
    [ndd_infos setObject:@"" forKey:@"DK"];
    [ndd_infos setObject:@"1" forKey:@"DO"];
    [ndd_infos setObject:@"7" forKey:@"DZ"];
    [ndd_infos setObject:@"0" forKey:@"EC"];
    [ndd_infos setObject:@"" forKey:@"EE"];
    [ndd_infos setObject:@"0" forKey:@"EG"];
    [ndd_infos setObject:@"0" forKey:@"EH"];
    [ndd_infos setObject:@"0" forKey:@"ER"];
    [ndd_infos setObject:@"0" forKey:@"ET"];
    [ndd_infos setObject:@"0" forKey:@"FI"];
    [ndd_infos setObject:@"" forKey:@"FJ"];
    [ndd_infos setObject:@"" forKey:@"FK"];
    [ndd_infos setObject:@"1" forKey:@"FM"];
    [ndd_infos setObject:@"" forKey:@"FO"];
    [ndd_infos setObject:@"" forKey:@"FR"];
    [ndd_infos setObject:@"" forKey:@"GA"];
    [ndd_infos setObject:@"0" forKey:@"GB"];
    [ndd_infos setObject:@"4" forKey:@"GD"];
    [ndd_infos setObject:@"" forKey:@"GF"];
    [ndd_infos setObject:@"" forKey:@"GH"];
    [ndd_infos setObject:@"" forKey:@"GI"];
    [ndd_infos setObject:@"" forKey:@"GL"];
    [ndd_infos setObject:@"" forKey:@"GM"];
    [ndd_infos setObject:@"" forKey:@"GP"];
    [ndd_infos setObject:@"" forKey:@"GQ"];
    [ndd_infos setObject:@"" forKey:@"GR"];
    [ndd_infos setObject:@"8" forKey:@"GS"];
    [ndd_infos setObject:@"" forKey:@"GT"];
    [ndd_infos setObject:@"" forKey:@"GW"];
    [ndd_infos setObject:@"" forKey:@"HK"];
    [ndd_infos setObject:@"0" forKey:@"HN"];
    [ndd_infos setObject:@"0" forKey:@"HR"];
    [ndd_infos setObject:@"0" forKey:@"HT"];
    [ndd_infos setObject:@"06" forKey:@"HU"];
    [ndd_infos setObject:@"0" forKey:@"ID"];
    [ndd_infos setObject:@"0" forKey:@"IE"];
    [ndd_infos setObject:@"0" forKey:@"IL"];
    [ndd_infos setObject:@"0" forKey:@"IN"];
    [ndd_infos setObject:@"0" forKey:@"IQ"];
    [ndd_infos setObject:@"0" forKey:@"IR"];
    [ndd_infos setObject:@"" forKey:@"IT"];
    [ndd_infos setObject:@"1" forKey:@"JM"];
    [ndd_infos setObject:@"0" forKey:@"JO"];
    [ndd_infos setObject:@"0" forKey:@"JP"];
    [ndd_infos setObject:@"0" forKey:@"KE"];
    [ndd_infos setObject:@"0" forKey:@"KG"];
    [ndd_infos setObject:@"0" forKey:@"KH"];
    [ndd_infos setObject:@"0" forKey:@"KI"];
    [ndd_infos setObject:@"" forKey:@"KM"];
    [ndd_infos setObject:@"1" forKey:@"KN"];
    [ndd_infos setObject:@"0" forKey:@"KP"];
    [ndd_infos setObject:@"0" forKey:@"KR"];
    [ndd_infos setObject:@"0" forKey:@"KW"];
    [ndd_infos setObject:@"1" forKey:@"KY"];
    [ndd_infos setObject:@"8" forKey:@"KZ"];
    [ndd_infos setObject:@"0" forKey:@"LB"];
    [ndd_infos setObject:@"1" forKey:@"LC"];
    [ndd_infos setObject:@"" forKey:@"LI"];
    [ndd_infos setObject:@"0" forKey:@"LK"];
    [ndd_infos setObject:@"22" forKey:@"LR"];
    [ndd_infos setObject:@"0" forKey:@"LS"];
    [ndd_infos setObject:@"8" forKey:@"LT"];
    [ndd_infos setObject:@"" forKey:@"LU"];
    [ndd_infos setObject:@"8" forKey:@"LV"];
    [ndd_infos setObject:@"0" forKey:@"LY"];
    [ndd_infos setObject:@"" forKey:@"MA"];
    [ndd_infos setObject:@"0" forKey:@"MC"];
    [ndd_infos setObject:@"0" forKey:@"MD"];
    [ndd_infos setObject:@"0" forKey:@"MG"];
    [ndd_infos setObject:@"1" forKey:@"MH"];
    [ndd_infos setObject:@"0" forKey:@"MK"];
    [ndd_infos setObject:@"0" forKey:@"MN"];
    [ndd_infos setObject:@"0" forKey:@"MO"];
    [ndd_infos setObject:@"1" forKey:@"MP"];
    [ndd_infos setObject:@"0" forKey:@"MQ"];
    [ndd_infos setObject:@"0" forKey:@"MR"];
    [ndd_infos setObject:@"1" forKey:@"MS"];
    [ndd_infos setObject:@"0" forKey:@"MU"];
    [ndd_infos setObject:@"0" forKey:@"MV"];
    [ndd_infos setObject:@"" forKey:@"MW"];
    [ndd_infos setObject:@"01" forKey:@"MX"];
    [ndd_infos setObject:@"0" forKey:@"MY"];
    [ndd_infos setObject:@"0" forKey:@"MZ"];
    [ndd_infos setObject:@"0" forKey:@"NA"];
    [ndd_infos setObject:@"0" forKey:@"NC"];
    [ndd_infos setObject:@"0" forKey:@"NE"];
    [ndd_infos setObject:@"" forKey:@"NF"];
    [ndd_infos setObject:@"0" forKey:@"NG"];
    [ndd_infos setObject:@"0" forKey:@"NI"];
    [ndd_infos setObject:@"0" forKey:@"NL"];
    [ndd_infos setObject:@"" forKey:@"NO"];
    [ndd_infos setObject:@"0" forKey:@"NR"];
    [ndd_infos setObject:@"0" forKey:@"NU"];
    [ndd_infos setObject:@"0" forKey:@"NZ"];
    [ndd_infos setObject:@"0" forKey:@"PA"];
    [ndd_infos setObject:@"0" forKey:@"PE"];
    [ndd_infos setObject:@"" forKey:@"PF"];
    [ndd_infos setObject:@"" forKey:@"PG"];
    [ndd_infos setObject:@"0" forKey:@"PH"];
    [ndd_infos setObject:@"0" forKey:@"PK"];
    [ndd_infos setObject:@"0" forKey:@"PL"];
    [ndd_infos setObject:@"0" forKey:@"PM"];
    [ndd_infos setObject:@"1" forKey:@"PR"];
    [ndd_infos setObject:@"0" forKey:@"PS"];
    [ndd_infos setObject:@"" forKey:@"PT"];
    [ndd_infos setObject:@"" forKey:@"PW"];
    [ndd_infos setObject:@"0" forKey:@"PY"];
    [ndd_infos setObject:@"0" forKey:@"QA"];
    [ndd_infos setObject:@"0" forKey:@"RE"];
    [ndd_infos setObject:@"0" forKey:@"RO"];
    [ndd_infos setObject:@"0" forKey:@"RS"];
    [ndd_infos setObject:@"0" forKey:@"RW"];
    [ndd_infos setObject:@"0" forKey:@"SA"];
    [ndd_infos setObject:@"" forKey:@"SB"];
    [ndd_infos setObject:@"0" forKey:@"SC"];
    [ndd_infos setObject:@"0" forKey:@"SD"];
    [ndd_infos setObject:@"0" forKey:@"SE"];
    [ndd_infos setObject:@"" forKey:@"SG"];
    [ndd_infos setObject:@"" forKey:@"SH"];
    [ndd_infos setObject:@"0" forKey:@"SI"];
    [ndd_infos setObject:@"0" forKey:@"SK"];
    [ndd_infos setObject:@"0" forKey:@"SL"];
    [ndd_infos setObject:@"0" forKey:@"SM"];
    [ndd_infos setObject:@"0" forKey:@"SN"];
    [ndd_infos setObject:@"" forKey:@"SO"];
    [ndd_infos setObject:@"" forKey:@"SR"];
    [ndd_infos setObject:@"0" forKey:@"ST"];
    [ndd_infos setObject:@"" forKey:@"SV"];
    [ndd_infos setObject:@"" forKey:@"SZ"];
    [ndd_infos setObject:@"1" forKey:@"TC"];
    [ndd_infos setObject:@"" forKey:@"TD"];
    [ndd_infos setObject:@"" forKey:@"TG"];
    [ndd_infos setObject:@"0" forKey:@"TH"];
    [ndd_infos setObject:@"8" forKey:@"TJ"];
    [ndd_infos setObject:@"" forKey:@"TK"];
    [ndd_infos setObject:@"" forKey:@"TL"];
    [ndd_infos setObject:@"8" forKey:@"TM"];
    [ndd_infos setObject:@"0" forKey:@"TN"];
    [ndd_infos setObject:@"0" forKey:@"TR"];
    [ndd_infos setObject:@"1" forKey:@"TT"];
    [ndd_infos setObject:@"" forKey:@"TV"];
    [ndd_infos setObject:@"" forKey:@"TW"];
    [ndd_infos setObject:@"0" forKey:@"TZ"];
    [ndd_infos setObject:@"8" forKey:@"UA"];
    [ndd_infos setObject:@"0" forKey:@"UG"];
    [ndd_infos setObject:@"1" forKey:@"US"];
    [ndd_infos setObject:@"0" forKey:@"UY"];
    [ndd_infos setObject:@"8" forKey:@"UZ"];
    [ndd_infos setObject:@"" forKey:@"VA"];
    [ndd_infos setObject:@"1" forKey:@"VC"];
    [ndd_infos setObject:@"0" forKey:@"VE"];
    [ndd_infos setObject:@"1" forKey:@"VG"];
    [ndd_infos setObject:@"1" forKey:@"VI"];
    [ndd_infos setObject:@"0" forKey:@"VN"];
    [ndd_infos setObject:@"" forKey:@"VU"];
    [ndd_infos setObject:@"" forKey:@"WF"];
    [ndd_infos setObject:@"0" forKey:@"WS"];
    [ndd_infos setObject:@"0" forKey:@"YE"];
    [ndd_infos setObject:@"" forKey:@"YT"];
    [ndd_infos setObject:@"0" forKey:@"ZA"];
    [ndd_infos setObject:@"0" forKey:@"ZW"];
}

@end
