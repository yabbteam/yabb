//
//  SettingsManager.h
//  Yabb
//
//  Created by denebtechsaplin on 10/11/13.
//
//

#import <Foundation/Foundation.h>

#define DID_CHANGE_SETTINGS_NOTIFICATION @"DID_CHANGE_SETTINGS"

@interface SettingsManager : NSObject

@property (assign, nonatomic) BOOL enableSounds;
@property (assign, nonatomic) BOOL enableVibration;
@property (assign, nonatomic) BOOL showLastSeenTimestamp;
@property (assign, nonatomic) BOOL saveToGallery;
@property (assign, nonatomic) BOOL enableSendSeen;

+ (instancetype)sharedInstance;

@end
