//
//  ChatBoardTable.h
//  speedsip
//
//  Created by denebtech on 18.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatBoardDelegate;

#define CHAT_BOARD_CELL     0
#define CHAT_IMAGE_CELL     1
#define CHAT_ANIMATION_CELL 2
#define CHAT_EDIT_CELL      3
#define CHAT_DATE_TIME_CELL 4

#define INITIAL_SHOW_COUNT 25;

@interface ChatBoardTable : UITableView <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate> {
    
    id<ChatBoardDelegate> __weak _chat_board_delegate;
    
    NSString* chat_id;
    NSMutableDictionary * contacts; // Cache for contact names
    NSString* server_id;
    id __weak _chat_board_controller;
    
    NSMutableArray * time_sections;
    
    BOOL _edit_mode;
    BOOL _at_screen;
    int num_of_selected;
    
    NSMutableArray * selected_cells;
    
    UIImageView*    userPhotoView;
    
    BOOL    _is_private;
    int     _messageTotalCount;
    
    NSMutableDictionary*    array_seen_messages;
}

@property (nonatomic, weak) IBOutlet id<ChatBoardDelegate> chat_board_delegate;
@property (nonatomic, weak) id chat_board_controller;
@property (nonatomic, assign) BOOL at_screen;
@property (nonatomic, assign) BOOL edit_mode;
@property (nonatomic, assign) int showingCount;

- (void) UpdateMessagesWithGroups:(NSArray *) groups;
- (void) Update_Messages:(NSArray *)groups scroll:(BOOL)isScroll;
- (void) Scroll_To_Bottom:(BOOL)animated;
- (void) setChatId:(NSString*)value;
- (void) edit;
- (void) deleteSelectedCells;
- (void) deleteCell:(NSString*) sid;
- (void) checkShowCells;
- (void) startObserveSeen;
- (void) stopObserveSeen;

@end


@protocol ChatBoardDelegate <NSObject>

@optional
- (void) tapOnTable;
- (void) editMode:(BOOL)edit;
- (void) selectCell:(int)num_of_selected;
- (void) showContactInfo:(NSString*)chat_id;
- (void) Button_Settings;

@end
