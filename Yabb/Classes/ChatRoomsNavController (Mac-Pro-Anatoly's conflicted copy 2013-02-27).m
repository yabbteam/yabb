//
//  ChatNavController.m
//  speedsip
//
//  Created by Anatoly T on 14.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatRoomsNavController.h"
//#import "ChatRooms.h"
#import "ChatRoomCell.h"
#import "ConnectionQueue.h"
#import "MainsipphoneAppDelegate.h"
#import "ChatStorage.h"
#import "AddressCollector.h"
#import "CustomBadge.h"
#import "ChatStatusViewController.h"
#import "ChatRoomStatusCell.h"
#import "Friends.h"
#import "PeopleMultiselectorViewController.h"
#import "InvitedFriends.h"

#define BROADCAST_IDX       0
#define STATUS_IDX          1
#define CHAT_ROOM_IDX       2
#define LAST_IDX            CHAT_ROOM_IDX

@interface ChatRoomsNavController ()

@end

@implementation ChatRoomsNavController

@synthesize chat_board = _chat_board, chat_rooms = _chat_rooms;

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        self->table_editing = NO;
        self->room_phones = [[NSMutableArray alloc] init];
        self->room_name = [[NSMutableString alloc] initWithString:@""];
    }
    
    return self;
}

- (void)viewDidLoad {
    
    _chat_board = [[ChatBoardViewController alloc] initWithNibName:@"ChatBoardViewController" bundle:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) importRooms {
    
    if (chat_room_list)
        [chat_room_list release], chat_room_list = nil;
    
    NSArray * array = [[ChatStorage sharedStorage] getChatRooms];
    if (array)
        chat_room_list = [[NSArray alloc] initWithArray:array];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [self importRooms];
    
    [self UpdateBadgeNumbers];
}



- (void) viewWillDisappear:(BOOL)animated {

    if (table_editing)
        [self Edit_Rooms:nil];
    
    if (chat_room_list)
        [chat_room_list release], chat_room_list = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction) Edit_Rooms:(id)sender {

    table_editing = table_editing?NO:YES;
    [_chat_rooms setEditing:table_editing animated:YES];
    
    if (table_editing) {
//        [chat_rooms insertSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationRight];
        [edit_button setTitle:@"Done"];
        [top_navigation setRightBarButtonItem:nil animated:YES];
    }
    else {
//        [chat_rooms deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationRight];
        [edit_button setTitle:@"Edit"];
        
        UIBarButtonItem * right_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(Compose_Button:)];
        [top_navigation setRightBarButtonItem:right_button animated:YES];
    }
}

- (IBAction) Compose_Button:(id)sender {
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
    PeopleMultiselectorViewController * nav = [[PeopleMultiselectorViewController alloc] initWithRootViewController:people_picker];

    [people_picker setDelegate:nav];
    [nav setRoot_controller:self];
    
    [self presentModalViewController:nav animated:YES];

//    [people_picker Enable_Selectors];

    [people_picker release];
    [nav release];

    /*
    PeoplePickerViewController * people_selector = [PeoplePickerViewController alloc] initWit
    
    PeopleMultiselectorViewController * people_selector = [[PeopleMultiselectorViewController alloc] init];
    
    [self presentModalViewController:people_selector animated:YES];
    
    [people_selector release];*/
}

- (void) Flashing_Badge:(NSNumber *)first_time {
    
    static BOOL working = NO;
    static int counter = 0;
    
    bool need_restart = NO;
    
    if ([first_time boolValue] == YES) {
        
        counter = 0;
        if (working == YES)
            return;
    }
    
    working = YES;
    
    counter++;
    if (counter < 10)
        need_restart = YES;
    else
        counter = 0;
    
    int messages = [[ChatStorage sharedStorage] countNewMessages];
    UITabBarItem * item = [self.tabBarController.tabBar.items objectAtIndex:1];
    
    if (item.badgeValue == nil || need_restart == NO) {
        [item setBadgeValue:(messages?[NSString stringWithFormat:@"%d", messages]:nil) ];
        
        if (need_restart)
            [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:NO] afterDelay:1.0];
        else
            working = NO;
    }
    else {
        [item setBadgeValue:nil];
        [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.5];
    }
}

- (void) UpdateBadgeNumbers {
    
    int prev_numbers = [[UIApplication sharedApplication] applicationIconBadgeNumber];
    
    int messages = [[ChatStorage sharedStorage] countNewMessages];
    NSString * nums = [NSString stringWithFormat:@"%d", messages];
    
//    NSLog(@"Bage nums: %@", nums);
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:messages];
    
    [self importRooms];
    [_chat_rooms reloadData];
    
    if (prev_numbers < messages)
        [self performSelector:@selector(Flashing_Badge:) withObject:[NSNumber numberWithBool:YES]];
    else
        [[self.tabBarController.tabBar.items objectAtIndex:1] setBadgeValue:([nums isEqualToString:@"0"]?nil:nums)];
}

- (NSDictionary *) getPostData {
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    
    [params setObject:@"post" forKey:@"cmd"];
    [params setObject:phone forKey:@"phone"];
    
    
    if ([ChatStorage sharedStorage].typing) {
        [params setObject:[NSString stringWithFormat:@"%lld", [ChatStorage sharedStorage].typing] forKey:@"typing"];
        [ChatStorage sharedStorage].typing = NO;
    }
    else
        [params setObject:@"" forKey:@"typing"];
    
//    NSLog(@"Typing: %lld", [ChatStorage sharedStorage].typing);
    
    // Compose new local messages
    NSArray * messages_array = [[ChatStorage sharedStorage] getLocalMessages];
    
    if (messages_array != nil && [messages_array count] != 0) {
        
        int tr = 0;
        if ([messages_array count] >= 2) {
            tr++;
        }
        
        int order = 0;
        for (NSDictionary * message in messages_array) {
            
            NSNumber * chat_id = [message objectForKey:@"room_id"];
            NSString * local_id = [message objectForKey:@"local_id"];
            NSString * key_chat_id = [NSString stringWithFormat:@"t[%@][chatid]", local_id];
            NSString * key_order = [NSString stringWithFormat:@"t[%@][order]", local_id];
            int type = [[message objectForKey:@"message_type"] intValue];
            
            if (type == 0) {
                NSString * key_message = [NSString stringWithFormat:@"t[%@][message]", local_id];
                NSString * encoded_message = [message objectForKey:@"original_message"];
                [params setObject:encoded_message forKey:key_message];
            }
            else {
                NSString * key_media_type = [NSString stringWithFormat:@"t[%@][mtype]", local_id];
                NSString * key_thumb = [NSString stringWithFormat:@"t[%@][mthumb]", local_id];
                NSString * thumb = [message objectForKey:@"media_thumb"];
                
                [params setObject:@"PNG" forKey:key_media_type];
                [params setObject:thumb forKey:key_thumb];
            }
            
            [params setObject:chat_id forKey:key_chat_id];
            [params setObject:[NSString stringWithFormat:@"%d", order] forKey:key_order];
            order++;
        }    
    }
    
    // Compose unconfirmed messages
    NSArray * unconfirmed_array = [[ChatStorage sharedStorage] getUnconfirmedMessages];
    if (unconfirmed_array != nil && [unconfirmed_array count] != 0) {
        NSMutableString * rcvkeys = [NSMutableString string];
        
        for (NSDictionary * dict in unconfirmed_array) {
            
            NSString * message_id = [NSString stringWithFormat:@"%llu", [[dict objectForKey:@"server_id"] longLongValue]];
            
            if ([rcvkeys isEqualToString:@""])
                [rcvkeys appendString:message_id];
            else
                [rcvkeys appendString:[NSString stringWithFormat:@",%@", message_id]];
            
        }
        
        [params setObject:rcvkeys forKey:@"rcvkeys"];
    }
    
    
    NSArray * delivered_array = [[ChatStorage sharedStorage] getDeliveredMessages];
    [[ChatStorage sharedStorage] resetDeliveredMessages];
    if (delivered_array != nil && [delivered_array count] != 0) {
        NSMutableString * rcvdrs = [NSMutableString string];
        
        for (NSDictionary * dict in delivered_array) {
            
            NSString * message_id = [NSString stringWithFormat:@"%llu", [[dict objectForKey:@"server_id"] longLongValue]];
            
            if ([rcvdrs isEqualToString:@""])
                [rcvdrs appendString:message_id];
            else
                [rcvdrs appendString:[NSString stringWithFormat:@",%@", message_id]];
            
        }
        
        [params setObject:rcvdrs forKey:@"rcvdrs"];
    }
    
    return params;
}

- (void) Move_To_Chat_Board:(NSDictionary *)room_dict showKeyboard:(BOOL)keyboard  {

    if (room_dict == nil)
        return;
    
    long long chat_id = [[room_dict objectForKey:@"room_id"] longLongValue];
    NSString * chat_name = [room_dict objectForKey:@"room_name"];
    
    [_chat_board setChatId:chat_id];
    [_chat_board setTitle:chat_name];
    
    if (self.visibleViewController != _chat_board) {

        // Does not push twice!
        NSArray * controllers = self.viewControllers;
        BOOL has_chat_controller = NO;
        for (id obj in controllers) {
            if ([obj isKindOfClass:[ChatBoardViewController class]]) {
                [self popToViewController:obj animated:keyboard?NO:YES];
                has_chat_controller = YES;
            }
        }

        // Not pushed? Do it!
        if (has_chat_controller == NO)
            [self pushViewController:_chat_board animated:keyboard?NO:YES];
    }
    else
        [_chat_board viewWillAppear:NO];
    
    [_chat_board.board_table Scroll_To_Bottom:NO];
    if (keyboard)
        [_chat_board performSelector:@selector(Show_Keyboard) withObject:nil afterDelay:0.01];
}

- (void) Send_Private_Message:(ABRecordRef)person {
    
    NSArray * phones = [[AddressCollector sharedCollector] Get_Phones_For_Person:person withPlus:YES];
    
    if (phones == nil)
        return;

    NSLog(@"Send message!!!");

    for (NSString * phone in phones) {
        
        long long chat_id = [[ChatStorage sharedStorage] getPrivateRoomByPhone:phone];
        if (chat_id != 0) {
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLongLong:chat_id], @"room_id", @"Chat", @"name", nil];
            [self Move_To_Chat_Board:dict showKeyboard:YES];

            return;
        }
    }

    // Chat does not exist (need create)
    NSDictionary * dictionary = [NSDictionary dictionaryWithObjectsAndKeys:room_name, @"person_name", phones, @"phones", nil];
    
    [self didCreateNewRoom:[[AddressCollector sharedCollector] Get_Name_For_Person:person] withPersons:[NSArray arrayWithObject:dictionary]];
}

#pragma mark -
#pragma mark Chat Room Table Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == STATUS_IDX) {
        return 58;
    }
    else if (indexPath.section == BROADCAST_IDX) {
        return 44;
    }
    
    NSDictionary * room_dict = [chat_room_list objectAtIndex:indexPath.row];
    BOOL private = [[room_dict objectForKey:@"private"] boolValue];

    return private?58:72;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (table_editing)
        [self Edit_Rooms:nil];    
    
    if (indexPath.section == STATUS_IDX) {
        ChatStatusViewController * status_controller = [[ChatStatusViewController alloc] init];
        [self pushViewController:status_controller animated:YES];
        [status_controller release];
    }
    else if (indexPath.section == CHAT_ROOM_IDX) {
        [self Move_To_Chat_Board:[chat_room_list objectAtIndex:indexPath.row] showKeyboard:NO];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == CHAT_ROOM_IDX)
        return YES;
    
    return NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == CHAT_ROOM_IDX) {
        int num_of_rooms = chat_room_list?[chat_room_list count]:0;
        
//        NSLog(@"Num of rooms: %d", num_of_rooms);
        return num_of_rooms;
    }
    
    return 1;    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSDictionary * chat = [chat_room_list objectAtIndex:indexPath.row];
        
        [[ChatStorage sharedStorage] deleteChatRoom:[[chat objectForKey:@"room_id"] longLongValue]];
        [self importRooms];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        [self UpdateBadgeNumbers];
    }
}

- (void) TapBroadcast {
    
    PeoplePickerViewController * people_picker = [[PeoplePickerViewController alloc] init];
    PeopleMultiselectorViewController * nav = [[PeopleMultiselectorViewController alloc] initWithRootViewController:people_picker];
    
    [people_picker setDelegate:nav];
    [nav setRoot_controller:self];
    [nav setMultiselector_delegate:self];
    
    [self presentModalViewController:nav animated:YES];
    
    [people_picker release];
    [nav release];
    
    NSLog(@"Broadcast");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    int row = indexPath.row;
    
    if (section == BROADCAST_IDX) {
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BroadcastCellId"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomCell" owner:self options:nil];
            cell = (ChatRoomCell *)[nib objectAtIndex:1];
            
            UIButton * button = (UIButton *)[cell viewWithTag:10];
            
            [button addTarget:self action:@selector(TapBroadcast) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    
    else if (section == STATUS_IDX) {

        ChatRoomStatusCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRoomStatusId"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomStatusCell" owner:self options:nil];
            cell = (ChatRoomStatusCell *)[nib objectAtIndex:0];
        }
        
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        [cell updateStatus];
        
        return cell;
    }
    else if (section == CHAT_ROOM_IDX) {
        
        ChatRoomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRoomCellId"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatRoomCell" owner:self options:nil];
            cell = (ChatRoomCell *)[nib objectAtIndex:0];
        }
        
        if (row < [chat_room_list count]) {
            NSDictionary * room_dict = [chat_room_list objectAtIndex:row];
            [cell setRoomName:room_dict?[room_dict objectForKey:@"room_name"]:@""];

            long long chat_id = [[room_dict objectForKey:@"room_id"] longLongValue];
            BOOL private = [[room_dict objectForKey:@"private"] boolValue];
            NSString * badge_str = [NSString stringWithFormat:@"%d", [[ChatStorage sharedStorage] countRoomNewMessages:chat_id]];
            
            [cell setLastMessage:chat_id];
            [cell setRoomType:private];
            
//            CustomBadge * people_badge = (CustomBadge *)[cell viewWithTag:10];

            NSString * phones_str = [room_dict objectForKey:@"phones"];
            NSArray * phones = [phones_str componentsSeparatedByString:@":"];
            [cell setPhoto:phones];

            /*
            if (private) {
                if (people_badge)
                    [people_badge removeFromSuperview];
                
            }
            else {
                
                if (people_badge == nil) {
                    people_badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"1"]];
                    [people_badge setTag:10];
                    [people_badge setBadgeInsetColor:[UIColor darkGrayColor]];
                    [cell addSubview:people_badge];
                }

                [people_badge setBadgeText:[NSString stringWithFormat:@"%d", [phones count]]];
                
                CGRect rect = people_badge.frame;
                rect.origin.x = -2;
                rect.origin.y = -2;
                [people_badge setFrame:rect];
                
            }*/
            
            CustomBadge * prev_badge = (CustomBadge *)[cell viewWithTag:20];
            if (prev_badge)
                [prev_badge removeFromSuperview];
 
            if ([badge_str isEqualToString:@"0"] == NO) {

                CustomBadge * badge = [CustomBadge customBadgeWithString:badge_str];
                [cell addSubview:badge];
                [badge setTag:20];

                CGRect rect = [badge frame];
                
                rect.origin.x = 290 - rect.size.width;
                rect.origin.y = (cell.frame.size.height - rect.size.height)/2;
                
                [badge setFrame:rect];
            }
        }
        
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];

        return cell;
    }
    
    return nil;
}

#pragma mark -
#pragma mark People Picker Delegate

- (void) didDoneButton:(NSString *)abook {
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    if (phone == nil)
        return;

    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    [params setObject:@"addchat" forKey:@"cmd"];
    [params setObject:phone forKey:@"phone"];
    
    int counter = 0;
    for (NSDictionary * dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        NSString * person = [dict objectForKey:@"phone"];
        NSString * name = [dict objectForKey:@"name"];
        if (phone == nil || name == nil)
            continue;
        
        
        NSString * name_key = [NSString stringWithFormat:@"chats[chat_%d][name]", counter];
        NSString * abook_key = [NSString stringWithFormat:@"chats[chat_%d][abook]", counter];
        
        [params setObject:[[ChatStorage sharedStorage] encodeString:name] forKey:name_key];
        [params setObject:[[AddressCollector sharedCollector] packPhoneNumber:person] forKey:abook_key];
        
        counter++;
    }
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"addchats" silent:NO];

    NSLog(@"Add new chat: %@", params);
    
    return;
}

#pragma mark -
#pragma Create New Room Delegate

- (void) didCreateNewRoom:(NSString *)room withPersons:(NSArray *)invated_persons {
    
    [room_name setString:room];
    
    NSLog(@"Create room: %@", room_name);

//    NSString * pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    
    if (phone == nil)
        return;
    
    [room_phones removeAllObjects];
    
    NSMutableString * abook = [[NSMutableString alloc] init];
    for (NSDictionary * person in invated_persons) {
        
        NSArray * phones = [person objectForKey:@"phones"];
        for (NSString * phone in phones) {
            
            if ([[Friends SharedFriends] isFriendPhone:phone]) {
//            if ([[AddressCollector sharedCollector] isPhoneFriend:phone]) {
            
//                long long v = [phone longLongValue];
                //[abook appendFormat:@"%013qx", v];
            
                [abook appendString:[[AddressCollector sharedCollector] packPhoneNumber:phone]];

                
                [room_phones addObject:phone];
                
                break;
            }
        }
    }
    
    if ([abook isEqualToString:@""])
        return;
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone", 
                             [[ChatStorage sharedStorage] encodeString:room_name], @"name", abook, @"abook", nil];
    
    NSLog(@"Add new chat: %@", params);
    
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"create_new_room" silent:NO];

    [abook release];
    
    [app.acc showActivityViewer:@"Creating..."];
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {

    if ([connection_name isEqualToString:@"set_user_status"]) {
        [_chat_rooms reloadData];
    }
    else if ([connection_name isEqualToString:@"create_new_room"]) {     // Create new room
    
        [app.acc hideActivityViewer];

        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1)
            return;
        
        NSNumber * number_id = [[dict objectForKey:@"data"] objectForKey:@"chatid"];
        if (number_id == nil)
            return;
        
        long chat_id = [number_id longLongValue];    
        
        // Add new room
        NSString * created_name = [[ChatStorage sharedStorage] addChatRoom:room_name withChatId:chat_id withPhones:room_phones];
        
        if (created_name == nil) {
            [self importRooms];
            [_chat_rooms reloadData];
            [self popViewControllerAnimated:NO];
        }

        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLongLong:chat_id], @"room_id", room_name, @"room_name", nil];
        
        [self Move_To_Chat_Board:dict showKeyboard:NO];
        
    }
    else if ([connection_name isEqualToString:@"addchats"]) {
     
        
        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1)
            return;

        NSDictionary * data = [dict objectForKey:@"data"];
        
        for (NSString * key in data) {
            
            
            NSArray * array = [data objectForKey:key];
            
            NSDictionary * chat = [array objectAtIndex:2];
            
            if (chat == nil || [chat objectForKey:@"chatid"] == nil)
                continue;
            
            long long chat_id = [[chat objectForKey:@"chatid"] longLongValue];
            
            NSLog(@"Chat id: %llu", chat_id);
            
            [[ChatStorage sharedStorage] addMessage:@"Broadcast message!" withChatId:chat_id];
            
        }
        

        return;
        
    }
    else if ([connection_name isEqualToString:@"lazy_chat_connection"]) {
        
        NSDictionary * data = [dict objectForKey:@"data"];
        
        NSArray * updated = [[ChatStorage sharedStorage] syncMessages:data];
        
        if (updated) {
            [_chat_board.board_table Update_Messages:updated];
            
            [self UpdateBadgeNumbers];
        }

        [self importRooms];
        
        if (self.visibleViewController == _chat_board)
            [[ChatStorage sharedStorage] markRoomRead:[_chat_board getChatId]];
    }
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"create_new_room"])
        [app.acc hideActivityViewer];
}

@end












