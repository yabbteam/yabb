//
//  ReconnectingView.m
//  Yabb
//
//  Created by denebtech
//
//

#import "ReconnectingView.h"

@interface ReconnectingView ()

@end

@implementation ReconnectingView {
    
    UILabel *_titleLabel;
    UIActivityIndicatorView *_indicator;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.indicator];
    }
    return self;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width*0.16, 0, self.bounds.size.width*0.84, self.bounds.size.height)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.shadowColor = [UIColor darkGrayColor];
        _titleLabel.shadowOffset = CGSizeMake(0, -1);
        _titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = NSLocalizedString(@"Waiting for internet", @"Waiting for internet");
    }
    return _titleLabel;
}

- (UIActivityIndicatorView *)indicator
{
    if (!_indicator) {
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _indicator.center = CGPointMake(self.bounds.size.width*0.08, self.bounds.size.height*0.5);
        [_indicator startAnimating];
    }
    return _indicator;
}

@end
