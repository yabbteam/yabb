//
//  ChatNameViewController.h
//  Baycall
//
//  Created by denebtech on 23.12.12.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ChatNameDelegate;

@interface ChatNameViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource> {
    
    
    id <ChatNameDelegate> __weak _chat_name_delegate;
    
    UITextField * text_field;
}

@property (nonatomic, strong) IBOutlet UITableView * table_view;
@property (nonatomic, weak) IBOutlet id <ChatNameDelegate> chat_name_delegate;

@end

@protocol ChatNameDelegate <NSObject>

- (NSString *) getRoomName;
- (void) renameRoom:(NSString*)name;

@end
