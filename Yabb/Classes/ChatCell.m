//
//  ChatCell.m
//  speedsip
//
//  Created by denebtech on 22.08.12.
//
//

#import "ChatCell.h"
#import "ChatStorage.h"
#import "ChatBoardViewController.h"
#import "AddressCollector.h"
#import "JabberLayer.h"
#import "CommonUtil.h"
#import "AvatarManager.h"

@implementation ChatCell

@synthesize is_owner = _is_owner, is_private = _is_private, server_id = _server_id, user_selection = _user_selection, is_marked_seen = _is_marked_seen;
@synthesize parent = _parent;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [selector setImage:[UIImage imageNamed:[self isSelected]?@"IsSelected.png":@"NotSelected.png"]];

    // Configure the view for the selected state
}

- (void) setPerson:(NSString *)name {
    
    [person_name setText:name?name:@""];
    
    if (_is_owner) {
        [person_name setTextAlignment:UITextAlignmentRight];
        [person_name setHidden:YES];
    }
    else {
        [person_name setTextAlignment:UITextAlignmentLeft];
        [person_name setHidden:_is_private];
    }
    
    [person_photo.layer setMasksToBounds:YES];
    [person_photo.layer setCornerRadius:4.0];

    self.multipleTouchEnabled = NO;
}

- (void) setMessageStatus:(int)status delivered:(BOOL)delivered {
    
    if (delivered)
        [msg_status setText:NSLocalizedString(@"✓Seen", @"✓Seen")];
    else if (status == MSG_STATUS_PENDING || status == MSG_STATUS_IMAGE_UPLOADING)
        [msg_status setText:NSLocalizedString(@"Sending...", @"Sending...")];
    else if (status == MSG_STATUS_SENT)
        [msg_status setText:NSLocalizedString(@"✓Sent", @"✓Sent")];
    else if (status == MSG_STATUS_CONFIRMED)
        [msg_status setText:NSLocalizedString(@"✓Delivered", @"✓Delivered")];
    else if (status == MSG_STATUS_SEND_FAILED)
        [msg_status setText:NSLocalizedString(@"Failed", @"Failed")];
    else if (status == MSG_STATUS_SEND_CANCELLED)
        [msg_status setText:NSLocalizedString(@"Cancelled", @"Cancelled")];
    
    if (status == MSG_STATUS_SEND_FAILED) {
        [msg_status setTextColor:[UIColor redColor]];
    }
    else {
        [msg_status setTextColor:[UIColor darkGrayColor]];
    }

    [msg_status setHidden:!_is_owner];
}

- (void) setTime:(NSDate *)date {
    
    [msg_time setText:[[ChatStorage sharedStorage] dateToStrTime:date]];

    [msg_time setTextAlignment:_is_owner?UITextAlignmentRight:UITextAlignmentLeft];
}

- (void) enableIndicator:(BOOL)enable {
    
    [selector setFrame:CGRectMake(2, selector.frame.origin.y, selector.frame.size.width, selector.frame.size.height)];
    
    [selector setHidden:!enable];
    [delimiter setHidden:!enable];
    
    [selector setImage:[UIImage imageNamed:[self isSelected]?@"IsSelected.png":@"NotSelected.png"]];
}

- (BOOL) isUserSelected {
    
    return _user_selection;
}

- (BOOL) selectCell {
    
    _user_selection = !_user_selection;

    return [self isSelected];
}

- (void) deleteMe {
    [self.parent deleteCell:self.server_id];
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}

- (void) buildLongTimeClick {
    enabledLongClick = YES;
}

- (void) notifyForwardClicked {
    ChatBoardViewController* chatViewController = (ChatBoardViewController*)self.parent.chat_board_controller;
    [chatViewController.dictForward setObject:_server_id forKey:@"message"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"forward_clicked" object:self.server_id];
}

- (void) notifyCopyClicked {
    ChatBoardViewController* chatViewController = (ChatBoardViewController*)self.parent.chat_board_controller;
    [chatViewController.dictForward setObject:_server_id forKey:@"message"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"copy_clicked" object:self.server_id];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    touched = YES;
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self];
    touchedLocation = location;
    
    [super touchesBegan:touches withEvent:event];  //let the tableview handle cell selection
    [self.nextResponder touchesBegan:touches withEvent:event]; // give the controller a chance for handling touch events
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(callLongClick) object:nil];
    if (enabledLongClick)
        [self performSelector:@selector(callLongClick) withObject:nil afterDelay:0.5f];
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    touched = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(callLongClick) object:nil];
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self];
    touchedLocation = location;
    CGRect rtPhoto = person_photo.frame;
    
    if (CGRectContainsPoint(rtPhoto, location)) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"person_photo_clicked" object:@{@"msgid":_server_id}];
    }
    
    [super touchesEnded:touches withEvent:event];
    [self.nextResponder touchesEnded:touches withEvent:event]; // give the controller a chance for handling touch events
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    touched = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(callLongClick) object:nil];
    
    [super touchesCancelled:touches withEvent:event];
    [self.nextResponder touchesCancelled:touches withEvent:event]; // give the controller a chance for handling touch events
}

- (void) callLongClick {
    
    if  ([self respondsToSelector:@selector(onLongClick:)]) {
        [self performSelector:@selector(onLongClick:) withObject:[NSValue valueWithCGPoint:touchedLocation]];
    }
}

- (UIImage*) getMyPhoto {
    
    UIImage* myImage = [[AvatarManager sharedInstance] getMyAvatar];
    if (myImage != nil)
        return myImage;
    else
        return [CommonUtil getNoAvatarImage];
}

@end
