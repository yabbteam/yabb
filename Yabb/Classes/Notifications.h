//
//  Notifications.h
//  speedsip
//
//  Created by denebtech on 30.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notifications : NSObject

@property (nonatomic, weak) id notif_delegate;

+ (Notifications *) sharedNotifications;
- (void) Register;
- (NSString *) getToken;

- (void) didRegistered:(NSData *)deviceToken;
- (void) didFailed:(NSError *)error;

@end
