//
//  InviteScrollView.m
//  Baycall
//
//  Created by denebtech on 29.10.12.
//
//

#import "InviteScrollView.h"
#import "InvitedFriends.h"
#import "AddressCollector.h"
#import "CommonUtil.h"

@implementation InviteScrollView

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        _next_position = CGPointMake(4, 10);
        is_active = NO;
    }
    
    return self;
}

- (void) updateCounter {
    
    [counter_label setText:[NSString stringWithFormat:@"%d/%d", [[InvitedFriends SharedFriends] Count], MAX_RECIPIENTS]];
}

- (void) Set_Invite_Height {

    int height = [InvitedFriends SharedFriends].height;
    
    height += 16;
    [self setContentSize:CGSizeMake(320, height)];
    
    if (height > 16+28*3)
        height = 16+28*3;
    
    [self setFrame:CGRectMake(0, 0, 320, height)];
    
    CGPoint bottomOffset = CGPointMake(0, [self contentSize].height - self.frame.size.height);
    
    if (bottomOffset.y > 0)
        [self setContentOffset: bottomOffset animated: NO];
    
    UITableView * table_view = [[[self superview] subviews] objectAtIndex:2];
    
    [table_view setFrame:CGRectMake(0, height, 320, [self superview].frame.size.height - height)];
    //    [table_view scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewRowAnimationTop animated:NO];
    
    NSLog(@"Add Height: %d", [InvitedFriends SharedFriends].height);
    NSLog(@"%d, %d", (int)_next_position.x, (int)_next_position.y);
    NSLog(@"Field height: %d", (int)text_field.frame.size.height);
    NSLog(@"Content size: %.02f, %.02f", self.frame.size.width, self.frame.size.height);

    
}

- (void) Update_Height {
    
    int height = (_next_position.y + text_field.frame.size.height);
    
    
    
    
//    if ([text_field isFirstResponder])
//        height += text_field.frame.origin.y - 13;
    
    [InvitedFriends SharedFriends].height = height;
    
}

- (void) Rebuild_Persons {
    
    _next_position = CGPointMake(4, 10);
    
    for (NSMutableDictionary * m_dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        RecipientView * r_view = [m_dict objectForKey:@"r_view"];
        NSString * name = [m_dict objectForKey:@"name"];
        
        if ([r_view superview])
            [r_view removeFromSuperview];
        
        int width = [r_view setText:name atPosition:_next_position ];
        if (width + _next_position.x > 270) {
            _next_position.x = 4;
            _next_position.y += 30;
            
            width = [r_view setText:name atPosition:_next_position];
        }
        
        [self addSubview:r_view];
        
        [m_dict setObject:[NSNumber numberWithInt:_next_position.x] forKey:@"x"];
        [m_dict setObject:[NSNumber numberWithInt:_next_position.y] forKey:@"y"];
        
        _next_position = CGPointMake(_next_position.x + RECIPIENT_SPACE + width, _next_position.y);
    }
    
    [self updateCounter];
    
    [invite_view setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    NSLog(@"Rebuild person");
    
    if ([[InvitedFriends SharedFriends] Count] || [text_field isFirstResponder])
        [hint_label setHidden:YES];
//    else if ([text_field.text isEqualToString:@" "] || [text_field.text isEqualToString:@""])
//        [hint_label setHidden:NO];
    else
        [hint_label setHidden:NO];
    
    
}

- (BOOL) Remove_Selected {
    
    for (NSMutableDictionary * m_dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        RecipientView * r_view = [m_dict objectForKey:@"r_view"];
        if (r_view.selected) {
            
            [[InvitedFriends SharedFriends] remove:m_dict];
            [self Rebuild_Persons];
            [self Update_Height];
            [self Set_Invite_Height];
            
            return YES;
        }
    }
    
    return NO;
}

- (void) Add_New_Person:(NSString *)name withIndex:(int)index withPhone:(NSString *)phone {
    
    RecipientView * recipient = [[RecipientView alloc] init];
    
    [recipient setRecipient_delegate:self];
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:recipient, @"r_view",
                                  [NSNumber numberWithInt:index], @"index",
                                  name, @"name", phone, @"phone", nil];
    
    [[InvitedFriends SharedFriends] add:dict];
}

- (void) didTouch {
    
    [self resetActive];
}

- (void) Remove_Person:(int)index {
    
    for (NSDictionary * dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        int i = [[dict objectForKey:@"index"] intValue];
        
        if (index == i) {
            RecipientView * r_view = [dict objectForKey:@"r_view"];
            
            [r_view removeFromSuperview];
            
            [[InvitedFriends SharedFriends] remove:dict];
            break;
        }
    }
    
}

- (void) Deselect_Persons {
    
    for (NSDictionary * dict in [[InvitedFriends SharedFriends] InvitedArray]) {
        
        RecipientView * r_view = [dict objectForKey:@"r_view"];
        if (r_view && r_view.selected) {
            
            [r_view didActive];
        }
    }
}

- (void) resetActive {
    
    [invite_view setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

    [self Text_Field_Hide];
    
    if ([[InvitedFriends SharedFriends] Count] || [text_field isFirstResponder])
        [hint_label setHidden:YES];
    else
        [hint_label setHidden:NO];
    
    NSLog(@"Reset Active!");
}

#pragma mark -
#pragma mark Text Field Delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    [hint_label setHidden:YES];
    
    if ([textField.text isEqualToString:@""])
        [textField setText:@" "];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    NSLog(@"text: [%@]", textField.text);
    
    if (range.location == 0 && range.length == 1) {     // No characters to delete
        
        int num_of_invited = [[InvitedFriends SharedFriends] Count];
        
        // Get selected
        if (num_of_invited) {
            
            if ([self Remove_Selected]) { // Already Selected
                [self Rebuild_Persons];
                [self Update_Height];
                [self Text_Field_Position];
                [self Set_Invite_Height];
                
                UITableView * table = [[[self superview] subviews] objectAtIndex:2];
                
                [table reloadData];
                return NO;
            }
            
            NSDictionary * dict = [[[InvitedFriends SharedFriends] InvitedArray] objectAtIndex:(num_of_invited -1)];
            RecipientView * r_view = [dict objectForKey:@"r_view"];
            
            // Delete last if selected
            if (r_view.selected) {
                [[InvitedFriends SharedFriends] remove:dict];
                [r_view removeFromSuperview];
                [self Rebuild_Persons];
                [self Text_Field_Position];
                [self Set_Invite_Height];

                UITableView * table = [[[self superview] subviews] objectAtIndex:2];
                [table reloadData];                
            }
            // Select last if nothing selected
            else {
                [self Text_Field_Hide];
                [r_view didActive];
                [self resetActive];
            }
        }
        
        return NO;
    }
    
    return YES;
}

- (void) Text_Field_Position {
    
    NSString * search_str = [text_field.text isEqualToString:@""]?text_field.text:[text_field.text substringFromIndex:1];
    
    // x=14, width=254
    CGRect text_rect = text_field.frame;
    
    CGSize expected = [CommonUtil getTextSize:search_str font:text_field.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, text_rect.size.height) lineBreakMode:UILineBreakModeWordWrap];
    
    // May be at first row
    if (_next_position.x + expected.width < 272) {
        text_rect.origin.x = _next_position.x;
        text_rect.origin.y = _next_position.y + 3;
    }
    else {
        text_rect.origin.x = 4;
        text_rect.origin.y = _next_position.y + 30;
    }
    
    [text_field setFrame:text_rect];
    
    int height = text_rect.origin.y + text_rect.size.height;
    
    [InvitedFriends SharedFriends].height = height;
    NSLog(@"Text rect: %d, %d", (int)text_rect.origin.y, (int)text_rect.size.height);
    NSLog(@"Height: %d", [InvitedFriends SharedFriends].height);    
}

- (void) Text_Field_Hide {
    
    CGRect text_rect = text_field.frame;
    text_rect.origin.y = -40;
    [text_field setFrame:text_rect];
}

- (void) Keyboard_Hide {
    
    if ([text_field isFirstResponder])
        [text_field resignFirstResponder];
    
    [self resetActive];
}

#pragma mark -
#pragma mark Invite View Delegate

- (void) didActive {
    
    [invite_view setFrame:CGRectMake(0, 480, 1, 1)];    // hide overlay view
    
    [text_field becomeFirstResponder];
    [self Text_Field_Position];
    [self Deselect_Persons];
    [self Set_Invite_Height];
    
    NSLog(@"Active!");
}



@end
