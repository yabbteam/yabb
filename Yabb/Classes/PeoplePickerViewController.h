//
//  PeoplePickerViewController.h
//  speedsip
//
//  Created by denebtech on 06.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contacts.h"
#import "InviteScrollView.h"
#import "BaseViewController.h"

@protocol PeoplePickerDelegate;

@interface PeoplePickerViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
    
    Contacts * __weak _contacts;
    BOOL _short_version;
    BOOL _friends_only;
    BOOL _invite_mode;
    UISegmentedControl * segmentedControl;
    
    id<PeoplePickerDelegate> __weak _delegate;
}

@property (nonatomic, weak) Contacts * contacts;
@property (nonatomic, assign) BOOL short_version;
@property (nonatomic, assign) BOOL friends_only;
@property (nonatomic, strong) IBOutlet UITableView * table_view;
@property (nonatomic, strong) IBOutlet UITableView * table_view_without_search;
@property (nonatomic, weak) IBOutlet id<PeoplePickerDelegate> delegate;
@property (nonatomic, assign) BOOL invite_mode;
@property (nonatomic, strong) IBOutlet InviteScrollView *invite_scroll_view;

//- (void) Enable_Selectors;

@end

@protocol PeoplePickerDelegate <NSObject> 

@optional

- (BOOL) isPreselectedCell:(NSString *)phone;
- (void) didSelectPerson:(int)index fromViewController:(UIViewController *)controller;
- (void) didSelectCell:(NSIndexPath *)indexPath withIndex:(int)index withName:(NSString *)name withPhone:(NSString *)phone selected:(BOOL)selected;
- (void) didCancelButton;
- (void) updateDoneButton;

@end
