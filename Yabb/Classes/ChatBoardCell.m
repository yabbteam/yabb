//
//  ChatBoardViewCell.m
//  speedsip
//
//  Created by denebtech on 19.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChatBoardCell.h"
#import "ChatStorage.h"
#import "CommonUtil.h"

enum {
    tagEmoticon = 3456,
};

@implementation ChatBoardCell

@synthesize bubble, message;//, text_view;

- (int) getTextRows:(NSString *)str maxWidth:(int)max_width {
    
    UIFont * font = [UIFont systemFontOfSize:CHAT_FONT_SIZE];
    CGSize theSize = [CommonUtil getAttributedLabelSize:str font:font constrainedToSize:CGSizeMake(max_width, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];

    return theSize.height / (CHAT_FONT_SIZE + 4);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self->emotions = nil;
    }
    return self;
}

/*
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}*/

- (int) Label_Width:(UILabel *)label {
    
    CGSize theSize = [CommonUtil getAttributedLabelSize:label.text font:label.font constrainedToSize:CGSizeMake(FLT_MAX, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    return theSize.width;
}

- (void) setText:(NSDictionary *)parsed_message editMode:(BOOL)edit {
    
    if (parsed_message == nil)
        return;
    
    NSString * text = [parsed_message objectForKey:@"parsed_message"];
    NSArray * smiles = [parsed_message objectForKey:@"smiles"];
    
    UIFont * font = [UIFont systemFontOfSize:CHAT_FONT_SIZE];
    CGSize theSize = [CommonUtil getAttributedLabelSize:text font:font constrainedToSize:CGSizeMake(CHAT_BUBBLE_WIDTH, FLT_MAX) lineBreakMode:UILineBreakModeWordWrap];
//    int num_of_lines = theSize.height / CHAT_FONT_SIZE;
    
    int photo_width = person_photo.frame.size.width + 4;

    int message_x = 21 + (edit?40:0) + photo_width;
    int message_y = _is_private?2:12;
    
    message.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    message.delegate = self;
    message.font = font;
    
//    [message setNumberOfLines:num_of_lines];
//    [message setText:text];
//    [message setText:@"Test"];
    
    [message setText:text];
//    [message setBackgroundColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.4]];
//    [message setTextColor:[UIColor greenColor]];

//    [text_view setContentInset:UIEdgeInsetsMake(-8, -8, -8, -8)];

    if (_is_owner) {
        
        if (theSize.width < 50)
            theSize.width = 50;
        
        message_x = self.parent.frame.size.width - theSize.width - (CHAT_FONT_SIZE + 4.0)-photo_width;
        [bubble setFrame:CGRectMake(self.parent.frame.size.width-theSize.width-34-photo_width, 1, theSize.width+32, theSize.height + 21)];
        [bubble setImage:[[UIImage imageNamed:@"ChatBubbleGreen.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:8]];
        
        [msg_time setFrame:CGRectMake(self.parent.frame.size.width-theSize.width-34 - msg_time.frame.size.width-photo_width, (theSize.height)/2.0 + 0, msg_time.frame.size.width, msg_time.frame.size.height)];
        [selector setFrame:CGRectMake(selector.frame.origin.x-photo_width, (theSize.height)/2.0 - 4, selector.frame.size.width, selector.frame.size.height)];

        [message setFrame:CGRectMake(message_x, 4, theSize.width, theSize.height)];
        [message setFrame:CGRectMake(message_x, 4, theSize.width/* + 16*/, theSize.height)];
        
        [msg_status setFrame:CGRectMake(message_x - 2, 5 + theSize.height,
                                        52, 11)];

        //person photo
        [person_photo setFrame:CGRectMake(self.parent.frame.size.width - person_photo.frame.size.width - 2,
                                          bubble.frame.origin.y+bubble.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        UIImage* myImage = [self getMyPhoto];
        if (myImage != nil)
            [person_photo setImage:myImage];
    }
    else {
        
        int label_width = [self Label_Width:person_name];
        
        if (label_width > 230) {
            
            CGRect person_name_frame = person_name.frame;
            person_name_frame.size.width = 230+photo_width;
            [person_name setFrame:person_name_frame];
            
            label_width = 230;
        }
        
        if (!person_name.hidden && label_width > theSize.width) {
            theSize.width = label_width;
        }
        
        if (theSize.width < 130)
            theSize.width = 130;
        
        if (_is_private)
            [bubble setFrame:CGRectMake(photo_width+(edit?40:0), message_y, theSize.width+34, theSize.height + 10)];
        else
            [bubble setFrame:CGRectMake(photo_width+(edit?40:0), message_y - 14, theSize.width+34, theSize.height + 24)];
        
        [bubble setImage:[[UIImage imageNamed:@"ChatBubbleGray.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:8]];

        [msg_time setFrame:CGRectMake(theSize.width+36+(edit?40:0)+photo_width, (theSize.height)/2.0 + message_y - (_is_private?6:14), msg_time.frame.size.width, msg_time.frame.size.height)];
        
        CGRect rect = person_name.frame;
        rect.origin.x  = photo_width+(edit?61:21);
        [person_name setFrame:rect];

        [selector setFrame:CGRectMake(selector.frame.origin.x+photo_width, (theSize.height)/2.0 + message_y - 9, selector.frame.size.width, selector.frame.size.height)];

        [message setFrame:CGRectMake(message_x, message_y + 3, theSize.width, theSize.height)];
        [message setFrame:CGRectMake(message_x, message_y + 3, theSize.width/* + 16*/, theSize.height)];

        [msg_status setFrame:CGRectMake(message_x - 2, message_y + 3 + theSize.height, 52, 11)];
        
        //person photo
        [person_photo setFrame:CGRectMake((edit?40:0)+2,
                                          bubble.frame.origin.y+bubble.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        
        UIImage* photo = self.personPhoto;
        if (photo != nil)
            [person_photo setImage:photo];
    }
    
    [message sizeToFit];

    [self removeImageViewRecursively:self];
    
    for (NSDictionary * dict in smiles) {
        
        int pos_x = [[dict objectForKey:@"pos_x"] intValue] + 3;
        int pos_y = [[dict objectForKey:@"pos_y"] intValue] - 1 - ( (_is_owner || _is_private)?10:0);
        //int width = [[dict objectForKey:@"width"] intValue] + 2;
        //int height = [[dict objectForKey:@"height"] intValue] + 2;
     
        int width = 18;
        int height = 18;
        
        UIImageView * view = [[UIImageView alloc] initWithFrame:CGRectMake(message_x + pos_x, 14+pos_y, width, height)];
        [view setImage:[UIImage imageNamed:[dict objectForKey:@"file_name"]]];
        view.tag = tagEmoticon;
        
        [self addSubview:view];
        [self bringSubviewToFront:view];
    }

    [self buildLongTimeClick];
}

- (void) removeImageViewRecursively:(UIView*) parent {
    for (UIView * view in [parent subviews]) {
        [self removeImageViewRecursively:view];
        if ([view isKindOfClass:[UIImageView class]] && view.tag == tagEmoticon) {
            [view removeFromSuperview];
        }
    }
}

#pragma mark -
#pragma mark long click functions

- (void) copyProcess {
    [self notifyCopyClicked];
}

- (void) forward_process {
    [self notifyForwardClicked];
}

- (BOOL) canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(copyProcess)) {
        return YES;
    }
    
    if (action == @selector(deleteMe)) {
        return YES;
    }
    
    if (action == @selector(forward_process)) {
        return YES;
    }
    
    return NO;
}

- (void) onLongClick:(NSValue*)value {
    if (self.parent.edit_mode)
        return;
    
    CGPoint location = [value CGPointValue];
    
    CGRect bubbleRect = bubble.frame;
    bubbleRect.origin.y = 0;
    bubbleRect.size.height = self.frame.size.height;
    if (!CGRectContainsPoint(bubbleRect, location))
        return;
    
    [self becomeFirstResponder];
    
    UIMenuItem* item1 = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Copy", @"Copy") action:@selector(copyProcess)];
    UIMenuItem* item2 = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Delete", @"Delete") action:@selector(deleteMe)];
    UIMenuItem* item3 = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Forward", @"Forward") action:@selector(forward_process)];
    
    UIMenuController* controller = [UIMenuController sharedMenuController];
    [controller setMenuItems:[NSArray arrayWithObjects:item1, item2,item3, nil]];

    CGRect targetRect = CGRectMake(location.x, location.y, 1, 1);
    [controller setTargetRect:targetRect inView:self];
    [controller setMenuVisible:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willHideMenu:) name:UIMenuControllerWillHideMenuNotification object:nil];
    
    bubble.alpha = 0.3f;
}

- (void)willHideMenu:(id)sender {
    bubble.alpha = 1.0f;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -
#pragma mark TTTAttributedLabelDelegate methods

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    if (url != nil)
        [[UIApplication sharedApplication] openURL:url];
}

@end
