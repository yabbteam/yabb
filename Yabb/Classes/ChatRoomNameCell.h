//
//  ChatRoomNameCell.h
//  speedsip
//
//  Created by denebtech on 05.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRoomNameCell : UITableViewCell {

    IBOutlet UITextField * chat_name_field;
}

@property (nonatomic, strong) UITextField * chat_name_field;

@end
