//
//  AboutViewController.h
//  Baycall
//
//  Created by admin on 07.05.13.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AboutViewController : BaseViewController <UIWebViewDelegate> {
    
    
    IBOutlet UIImageView * back_image;
    IBOutlet UILabel * version_label;
    IBOutlet UIWebView * web_view;
    IBOutlet UILabel* myPhone;
    
}


@end
