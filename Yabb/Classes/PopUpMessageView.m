//
//  PopUpMessageView.m
//  Yabb
//
//  Created by denebtech on 3/8/14.
//
//

#import "PopUpMessageView.h"
#import "JabberLayer.h"

@implementation PopUpMessageView


- (id) initDefault:(id)delegate {

    CGRect frame = CGRectZero;
    id ret = [self initWithFrame:frame];
    _delegate = delegate;
    return ret;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"PopUpMessageView" owner:self options:nil];
        self = [nib objectAtIndex:0];        // Initialization code
        labelName.font = [UIFont boldSystemFontOfSize:17.0f];
        
        CALayer * l = [photoView layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:4.0];
    }
    return self;
}

- (void) show {
    
    self.alpha = 1.0f;
    [UIView beginAnimations:@"hide" context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect rt = self.frame;
    rt.origin.y = 20.0f;

    self.frame = rt;
    [UIView commitAnimations];
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideWithAnimation) object:nil];
    [self performSelector:@selector(hideWithAnimation) withObject:nil afterDelay:3.5f];
}

- (void) hideWithAnimation {
    
    CGFloat offset = 20.0f;
    
    [UIView beginAnimations:@"hide" context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect rt = self.frame;
    rt.origin.y = rt.origin.y - rt.size.height - offset;
    self.frame = rt;
    self.alpha = 0.0f;
    [UIView commitAnimations];
}

- (void) setInfo:(UIImage*) photo name:(NSString*) name text:(NSString*)text chatid:(NSString*)chatid {
    
    if (photo == nil)
        photo = [UIImage imageNamed:@"placeholderPerson_52.png"];
    photoView.image = photo;
    labelName.text = name;
    labelText.text = text;
    
    _chatid = chatid;
    [self show];
}

-(IBAction) onCancelClicked:(id)sender {

    [self hideWithAnimation];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    [self onSelected];
    [super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesCancelled:touches withEvent:event];
}

- (void) onSelected {

    [self hide];
    if ([_delegate respondsToSelector:@selector(gotoOtherRoom:)]) {
        [_delegate performSelector:@selector(gotoOtherRoom:) withObject:_chatid];
    }
}

- (void) setText:(NSString*) text {
    
    labelText.text = text;
    [self show];
}

- (void) hide {
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideWithAnimation) object:nil];
    
    CGFloat offset = 20.0f;
    
    CGRect rt = self.frame;
    rt.origin.y = rt.origin.y - rt.size.height - offset;
    
    self.frame = rt;
}

@end
