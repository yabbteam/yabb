//
//  PeoplePickerViewController.m
//  speedsip
//
//  Created by denebtech on 06.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PeoplePickerViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AddressCollector.h"
#import "Friends.h"
#import "MyPersonViewController.h"
#import "AppDelegate.h"
#import "PickerCell.h"
//#import "InviteCell.h"
#import "InvitedFriends.h"
#import "LafUtil.h"
#import "Settings.h"
#import "ServerStateView.h"
#import "AvatarManager.h"

@interface PeoplePickerViewController ()

@end

@implementation PeoplePickerViewController

@synthesize delegate = _delegate;
@synthesize table_view, table_view_without_search;
@synthesize short_version = _short_version;
@synthesize friends_only = _friends_only;
@synthesize invite_mode = _invite_mode;
@synthesize contacts = _contacts;

- (id) init {
    
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    _friends_only = YES;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"All", @"All"), @"Yabb", nil]];
    [segmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    [segmentedControl setSelectedSegmentIndex:1];
    
    self.navigationItem.titleView = segmentedControl;
    
    [segmentedControl addTarget:self action:@selector(changeSegment:) forControlEvents:UIControlEventValueChanged];
    
    if (self.presentingViewController == nil) {
        ServerStateView* serverStateView = [[ServerStateView alloc] init];
        UIBarButtonItem *serveState = [[UIBarButtonItem alloc] initWithCustomView:serverStateView];
        NSArray* arrayItem = [NSArray arrayWithObjects:serveState, nil];
        
        [self.navigationItem setRightBarButtonItems:arrayItem];
    }

    
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    //[self.navigationController.navigationBar setTintColor:[UIColor darkGrayColor]];
    
    //[self.table_view setBackgroundColor:[UIColor clearColor]];
    //[self.table_view applyDefaultAppearance];
    
    if ([self.table_view respondsToSelector:@selector(setSectionIndexBackgroundColor:)])
        [self.table_view setSectionIndexBackgroundColor:[UIColor clearColor]];
    if ([self.table_view_without_search respondsToSelector:@selector(setSectionIndexBackgroundColor:)])
        [self.table_view_without_search setSectionIndexBackgroundColor:[UIColor clearColor]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedContactList:) name:@"update_contactlist" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(friendsUpdated:) name:@"update_friends" object:nil];
}

- (void) viewWillAppear:(BOOL)animated {
    
    _contacts = app->contacts;
    
    [segmentedControl setHidden:_short_version?YES:NO];

    if (_invite_mode) {
        [table_view setHidden:YES];
        [table_view_without_search setHidden:NO];
        [self.invite_scroll_view setHidden:NO];
//        [_invite_scroll_view setFrame:CGRectMake(0, 0, 320, 44)];
    }
    else {
        [table_view setHidden:NO];
        [table_view_without_search setHidden:YES];
        [self.invite_scroll_view setHidden:YES];
//        [_invite_scroll_view setFrame:CGRectMake(0, 0, 0, 0)];
    }

    [table_view reloadData];
}

- (void) viewDidDisappear:(BOOL)animated {
    
    if (_invite_mode)
        [[InvitedFriends SharedFriends] Clear];
    
    [table_view_without_search setFrame:CGRectMake(0, 0, 320, 416)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void) changeSegment:(id)sender {
    
    if (segmentedControl.selectedSegmentIndex == 1)
        _friends_only = YES;
    else
        _friends_only = NO;
    
    [table_view reloadData];
    [table_view_without_search reloadData];
    
    NSLog(@"Selected segment: %d", segmentedControl.selectedSegmentIndex);
}

#pragma mark -
#pragma mark Table Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([_delegate respondsToSelector:@selector(didSelectCell:withIndex:withName:withPhone:selected:)]) {

        NSArray * array;

        if (_contacts.has_filter)   // Filtered all contacts
            array = _contacts.filtered_contacts;
        else                        // Friends
            array = [_contacts.grouped_friends objectForKey:[_contacts.friends_letters objectAtIndex:indexPath.section]];
    
        NSDictionary * contact = [array objectAtIndex:indexPath.row];

        if (_delegate && [_delegate respondsToSelector:@selector(isPreselectedCell:)] && [_delegate isPreselectedCell:[contact objectForKey:@"phone"]])
            return;

        NSUInteger index = [[contact objectForKey:@"index"] intValue];
        NSString * phone = [contact objectForKey:@"phone"];
        
        if ([[[InvitedFriends SharedFriends] Selected] containsIndex:index])
            [[[InvitedFriends SharedFriends] Selected] removeIndex:index];
        else
            [[[InvitedFriends SharedFriends] Selected] addIndex:index];
        
        [_delegate didSelectCell:indexPath withIndex:index withName:[contact objectForKey:@"name"] withPhone:(NSString *)phone selected:[[[InvitedFriends SharedFriends] Selected] containsIndex:index] ];
        
        // Clear filter
        [self.invite_scroll_view->text_field setText:@" "];
        
        [_contacts Sort_By_Filter:@"" friendsOnly:_friends_only];

        [tableView reloadData];

        [self.invite_scroll_view resetActive];
        
    }
    if ([_delegate respondsToSelector:@selector(didSelectPerson:fromViewController:)]) {
        
        NSArray * array;
        
        if (_contacts.has_filter)   // Filtered all contacts
            array = _contacts.filtered_contacts;
        else if (_friends_only)     // Friends
            array = [_contacts.grouped_friends objectForKey:[_contacts.friends_letters objectAtIndex:indexPath.section]];
        else                       // Grouped all contacts
            array = [_contacts.grouped_contacts objectForKey:[_contacts.letters objectAtIndex:indexPath.section]];

        NSDictionary * contact = [array objectAtIndex:indexPath.row];

        if (_delegate && [_delegate respondsToSelector:@selector(isPreselectedCell:)] && [_delegate isPreselectedCell:[contact objectForKey:@"phone"]])
            return;

        [_delegate didSelectPerson:[[contact objectForKey:@"index"] intValue] fromViewController:self];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
/*    if (_invite_mode && indexPath.section == 0) {
        return ([InvitedFriends SharedFriends].height + 13);
    }
  */
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)index {
    
    if (_contacts.has_filter)    // Filtered contacts
        return @" ";
    else if (_friends_only)      // Friends
        return [_contacts.friends_letters objectAtIndex:index];
//        return @" ";
    else                        // Grouped all contacts
        return [_contacts.letters objectAtIndex:index];
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (_contacts.has_filter)   // Filtered contacts
        return [NSArray arrayWithObject:@""];
    else if (_friends_only)     // Friends
        return _contacts.friends_letters;
//        return [NSArray arrayWithObject:@""];
    else                       // Grouped all contacts
        return _contacts.letters;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    
    if (_delegate && [_delegate respondsToSelector:@selector(updateDoneButton)])
        [_delegate updateDoneButton];
    
    if (_contacts.has_filter)   // Filtered contacts
        return 1;
    else if (_friends_only) {     // Friends
        return [_contacts.friends_letters count];
//        return 1;
    }
    else                       // Grouped all contacts
        return [_contacts.letters count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    if (_contacts.has_filter) {    // Filtered contacts
        
        if ([_contacts.filtered_contacts count] == 0)
            [tableView setSeparatorColor:[UIColor clearColor]];
        else
            [tableView setSeparatorColor:[UIColor lightGrayColor]];
        
        return [_contacts.filtered_contacts count];
    }
    else if (_friends_only) {      // Friends

        [tableView setSeparatorColor:[UIColor lightGrayColor]];

        NSArray * array = [_contacts.grouped_friends objectForKey:[_contacts.friends_letters objectAtIndex:section]];
        return [array count];

//        return [_contacts.friend_contacts count];
    }
    else {                      // Grouped all contacts
        NSArray * array = [_contacts.grouped_contacts objectForKey:[_contacts.letters objectAtIndex:section]];
        return [array count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int row = indexPath.row, section = indexPath.section;

    PickerCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PickerCellID"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PickerCell" owner:self options:nil];
        cell = (PickerCell *)[nib objectAtIndex:0];
    }
    
    NSArray * array;
    if (_contacts.has_filter)        // Filtered contacts
        array = _contacts.filtered_contacts;
    else if (_friends_only) {          // Friends
        array = [_contacts.grouped_friends objectForKey:[_contacts.friends_letters objectAtIndex:section]];
//        array = _contacts.friend_contacts;
    }
    else                            // Grouped all contacts
        array = [_contacts.grouped_contacts objectForKey:[_contacts.letters objectAtIndex:section]];

    // Set name
    NSDictionary * contact = [array objectAtIndex:row];    

    int index = [[contact objectForKey:@"index"] intValue];

    [cell.person_label setText:[contact objectForKey:@"name"]];
    cell.person_label.adjustsFontSizeToFitWidth = YES;
    
    // Set online status
    if ( [[contact objectForKey:@"is_friend"] boolValue] ) {
        BOOL online = [[Friends SharedFriends] isPersonOnlineIndex:index];
        [cell.user_status setImage:[UIImage imageNamed:online?@"user_online.png":@"user_offline.png"]];
        [cell.user_status setHidden:NO];
        
        [cell setOriginText:[[Friends SharedFriends] originByIndex:index]];
        
    }
    else {
        [cell.user_status setHidden:YES];
        
        [cell resetOriginText];        
    }
    
    //user photo
    UIImage* photo = [[AvatarManager sharedInstance] getAvatarWithAddressCollecterIndex:index];
    if (photo != nil) {
        [cell.person_photo setImage:photo];
    }
    else {
        [cell.person_photo setImage:[UIImage imageNamed:@"placeholderPerson_52.png"]];
    }
    
    if (_invite_mode) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(isPreselectedCell:)] && [_delegate isPreselectedCell:[contact objectForKey:@"phone"]])
            [cell enableSelector:YES];
        else
            [cell enableSelector:[[[InvitedFriends SharedFriends] Selected] containsIndex:index]];
        
    }

    for(UIView *view in [tableView subviews])
    {
        if([[[view class] description] isEqualToString:@"UITableViewIndex"])
        {
            [view setBackgroundColor:[UIColor clearColor]];
        }
    }
    
    return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self.invite_scroll_view Keyboard_Hide];
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    for (UIView *subView in searchBar.subviews){
        if ([subView isKindOfClass:[UITextField class]])
        {
            UITextField* searchBarTextField = (UITextField *)subView;
            [searchBarTextField setTextColor:[UIColor blackColor]];
            if (searchBarTextField.text.length == 0)
                searchBarTextField.text = @"";
        }
        for (UIView *subView2 in subView.subviews){
            if ([subView2 isKindOfClass:[UITextField class]])
            {
                UITextField* searchBarTextField = (UITextField *)subView2;
                [searchBarTextField setTextColor:[UIColor blackColor]];
                if (searchBarTextField.text.length == 0)
                    searchBarTextField.text = @"";
                //break;
            }
        }
    }
}


- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar setText:@""];
    [_contacts Sort_By_Filter:@"" friendsOnly:_friends_only];

    [table_view reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    [controller.searchResultsTableView setBackgroundColor:DEFAULT_BACK_COLOR];
    [controller.searchResultsTableView setSeparatorColor:[UIColor lightGrayColor]];

    [_contacts Sort_By_Filter:searchString friendsOnly:_friends_only];
    return YES;
}

- (IBAction) Button_Cancel:(id)sender {
    
    if (_delegate && [_delegate respondsToSelector:@selector(didCancelButton)])
        [_delegate didCancelButton];
}

- (void)friendsUpdated:(NSNotification*) notification
{
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(friendsUpdateProcess) object:nil];
    
    [self performSelector:@selector(friendsUpdateProcess) withObject:nil afterDelay:1.0f];
    
}

- (void) changedContactList:(NSNotification*) notification {

    if (self.searchDisplayController.active) {
        [self.searchDisplayController setActive:NO animated:NO];
    }
    [self.table_view reloadData];
}

- (void) friendsUpdateProcess {
    
    [self.table_view reloadData];
}


@end
