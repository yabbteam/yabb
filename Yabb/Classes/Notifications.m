//
//  Notifications.m
//  speedsip
//
//  Created by denebtech on 30.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Notifications.h"

static Notifications * notifications = nil;

@implementation Notifications

+ (Notifications *)sharedNotifications {
    
    if (notifications == nil) {
        notifications = [Notifications alloc];
        //self.notif_delegate = nil;
    }
    
    return notifications;
}


- (void) setToken:(NSString *)token {
    
    if (token)
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *) getToken {
    
    NSString * token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    return (token?token:@"");
}

- (void) Finish {
    
    if (_notif_delegate && [_notif_delegate respondsToSelector:@selector(didNotificationDone)])
        [_notif_delegate performSelector:@selector(didNotificationDone)];
    
}

- (void) Register {
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}

- (void) didRegistered:(NSData *)deviceToken {
    
    NSString * token_str = [[[[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"--- Notifications TOKEN: %@ ---", token_str);
    [self setToken:token_str];
    [self Finish];
}

- (void) didFailed:(NSError *)error {
    
    
#if TARGET_IPHONE_SIMULATOR
    [self didRegistered:[@"__simulator-push-token__" dataUsingEncoding:NSUTF8StringEncoding]];
    
    return;
#endif
    
    NSLog(@"--- Notification error: %@ ---", error.description);
    
    [self setToken:nil];
    [self Finish];
}

@end
