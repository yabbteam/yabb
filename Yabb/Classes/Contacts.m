//
//  Contacts.m
//  speedsip
//
//  Created by denebtech on 15.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Contacts.h"
#import "AddressCollector.h"
#import "Friends.h"

@implementation Contacts

- (id) init {
    
    self = [super init];
    if (self) {
    
        _contacts = [[NSMutableArray alloc] init];
        _filtered_contacts = [[NSMutableArray alloc] init];
        _grouped_contacts = [[NSMutableDictionary alloc] init];
        _letters = [[NSMutableArray alloc] init];
        _friend_contacts = [[NSMutableArray alloc] init];
        _friends_letters = [[NSMutableArray alloc] init];
        _grouped_friends = [[NSMutableDictionary alloc] init];
        
        _has_filter = NO;
        _friends_only = NO;
    }
    
    return self;
}


- (void) Sort_By_Filter:(NSString *)filter friendsOnly:(BOOL)friends_only {
    
    
    NSTimeInterval start = [[NSDate date] timeIntervalSince1970];
    
    [_filtered_contacts removeAllObjects];
    
    float diff = [[NSDate date] timeIntervalSince1970] - start;
    
    NSLog(@"Diff 1: %.04f", diff);
    
    if (filter == nil || [filter isEqualToString:@""]) {
        _has_filter = NO;
        return;
    }
    else
        _has_filter = YES;
    
    NSString * filter_for_phone = [filter stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSArray * array = friends_only?_friend_contacts:_contacts;
    
    for (NSMutableDictionary * contact in array) {
    
        NSString * name = [contact objectForKey:@"name"];
        
        NSComparisonResult result = NSOrderedAscending;
        if (name != nil && filter != nil && [filter isEqualToString:@""] == NO) {
            
            // Check name
            NSArray * split = [name componentsSeparatedByString:@" "];
            
            for (NSString * str in split) {
                
                result = [str compare:filter options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [filter length])];
                if (result == NSOrderedSame) {
                    break;
                }
            }
            
            NSString * ph = [contact objectForKey:@"phone"];
            NSArray * phones;
            if (ph) {
                phones = [NSArray arrayWithObject:ph];
            }
            else {
                phones = [contact objectForKey:@"phones"];
            }
            if (phones) {
    
                for (NSString * phone in phones) {
                    
                    NSString * c_phone = [[AddressCollector sharedCollector] clearedPhoneNumber:phone withPlus:NO];
                    NSRange range = [c_phone rangeOfString:filter_for_phone];
                    
                    if ([filter isEqualToString:filter_for_phone] == NO && range.location != 0) {
                        range.length = 0;
                    }
                    
                    if (range.length || [filter_for_phone isEqualToString:@""]) {
                        result = NSOrderedSame;
                    }
                }
            }
        }

        if (result == NSOrderedSame) {
            [_filtered_contacts addObject:contact];        
        }
    }
    diff = [[NSDate date] timeIntervalSince1970] - start;
    NSLog(@"Diff 2: %.04f", diff);
}

- (void) Make_Contacts {
    
    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];
    
    [_contacts removeAllObjects];
    [_grouped_contacts removeAllObjects];
    [_letters removeAllObjects];

    int num_of_contacts = [[AddressCollector sharedCollector] Num_Of_Contacts];
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Count contacts [%.02f]", timer);
    timer = [[NSDate date] timeIntervalSince1970];

    ABPersonSortOrdering sort_order = ABPersonGetSortOrdering();
    for (int i = 0; i < num_of_contacts; i++) {
        
        bool is_friend = [[Friends SharedFriends] isFriendIndex:i];
        
        NSString * name = [[AddressCollector sharedCollector] Get_Name_By_Index:i];
        NSDictionary * sort = [[AddressCollector sharedCollector] Get_Sorting_Name_By_Index:i withSort:sort_order];
        if (sort == nil)
            continue;
        
        NSString * sort_char = [sort objectForKey:@"sort_char"];
        NSString * sort_name = [sort objectForKey:@"sort_name"];
        
        NSMutableDictionary * m_dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:is_friend], @"is_friend", name, @"name", sort_name, @"sort_name", sort_char, @"sort_char", [NSNumber numberWithInt:i], @"index", [[AddressCollector sharedCollector] Get_Phones_By_Index:i], @"phones", nil];
        [_contacts addObject:m_dict];
    }
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Make mutable contacts [%.02f]", timer);
    timer = [[NSDate date] timeIntervalSince1970];
    
    
    NSArray * array = [NSArray arrayWithArray:_contacts];
    
    [_contacts removeAllObjects];
    
    [_contacts addObjectsFromArray:[array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        NSString * first = [a objectForKey:@"sort_name"];
        NSString * second = [b objectForKey:@"sort_name"];
        
        return [first localizedCaseInsensitiveCompare:second];
    }]];


    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Sort contacts [%.02f]", timer);
    timer = [[NSDate date] timeIntervalSince1970];

    
    // Group contacts
    for (NSDictionary * contact in _contacts) {
        
        NSString * first_letter = [contact objectForKey:@"sort_char"];
        
        NSMutableArray * letter_dict = [_grouped_contacts objectForKey:first_letter];
        if (letter_dict == nil) {
            letter_dict = [NSMutableArray arrayWithObject:contact];
			[_grouped_contacts setObject:letter_dict forKey:first_letter];
            [_letters addObject:first_letter];
		}
		else {
			[letter_dict addObject:contact];
		}
    }
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Group contacts [%.02f]", timer);
    timer = [[NSDate date] timeIntervalSince1970];

}

- (void) Make_Friends {
    
    NSTimeInterval timer = [[NSDate date] timeIntervalSince1970];

    [_friend_contacts removeAllObjects];
    [_grouped_friends removeAllObjects];
    [_friends_letters removeAllObjects];
    
    if (_contacts == nil || [_contacts count] == 0)
        return;
    
    ABPersonSortOrdering sort_order = ABPersonGetSortOrdering();
    int num_of_contacts = [[AddressCollector sharedCollector] Num_Of_Contacts];
    for (int i = 0; i < num_of_contacts; i++) {

        
        NSString * friend_phone = [[Friends SharedFriends] friendPhoneByIndex:i];
        
        if (friend_phone == nil)
            continue;
        
//        bool is_friend = [[Friends SharedFriends] isFriendIndex:i];
//        if (is_friend == NO)
//            continue;
        
        NSString * name = [[AddressCollector sharedCollector] Get_Name_By_Index:i];
        NSDictionary * sort = [[AddressCollector sharedCollector] Get_Sorting_Name_By_Index:i withSort:sort_order];
        if (sort == nil)
            continue;
        
        NSString * sort_char = [sort objectForKey:@"sort_char"];
        NSString * sort_name = [sort objectForKey:@"sort_name"];
        
        NSMutableDictionary * m_dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"is_friend", name, @"name", sort_name, @"sort_name", sort_char, @"sort_char", [NSNumber numberWithInt:i], @"index", [[AddressCollector sharedCollector] clearedPhoneNumber:friend_phone withPlus:YES], @"phone", nil];
        [_friend_contacts addObject:m_dict];
            
    }
    
    if (_friend_contacts) {
    
        NSArray * array = [NSArray arrayWithArray:_friend_contacts];
        
        [_friend_contacts removeAllObjects];
        
        [_friend_contacts addObjectsFromArray:[array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            
            NSString * first = [a objectForKey:@"sort_name"];
            NSString * second = [b objectForKey:@"sort_name"];
            
            return [first localizedCaseInsensitiveCompare:second];
        }]];
    }
    
    
    timer = [[NSDate date] timeIntervalSince1970] - timer;
    NSLog(@"Make friends [%.02f]", timer);
    timer = [[NSDate date] timeIntervalSince1970];

    
    // Group contacts
    for (NSDictionary * contact in _friend_contacts) {
        
        NSString * first_letter = [contact objectForKey:@"sort_char"];
        
        NSMutableArray * letter_dict = [_grouped_friends objectForKey:first_letter];
        if (letter_dict == nil) {
            letter_dict = [NSMutableArray arrayWithObject:contact];
			[_grouped_friends setObject:letter_dict forKey:first_letter];
            [_friends_letters addObject:first_letter];
		}
		else {
			[letter_dict addObject:contact];
		}
    }
}


@end
