//
//  AvatarManager.m
//  Yabb
//
//  Created by denebtech on 3/27/14.
//
//

#import "AvatarManager.h"
#import "AddressCollector.h"
#import "JabberLayer.h"

static AvatarManager * avatar_manager = nil;

@implementation AvatarManager {
    
    NSMutableDictionary* arrayInfo;
}

+ (AvatarManager *) sharedInstance {
    
    if (avatar_manager == nil) {
        
        avatar_manager = [AvatarManager alloc];
        [avatar_manager initInfo];
        [avatar_manager load];
    }
    
    return avatar_manager;
}

- (void) initInfo {
    
    arrayInfo = [[NSMutableDictionary alloc] initWithCapacity:200];
}

- (void) clear {
    
    [arrayInfo removeAllObjects];
    [self save];
}

- (void) load {
    
    NSDictionary* dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar_manager_saved"];
    if (dict != nil)
        arrayInfo = [self makeImageDictionary:dict];
}

- (void) save {

    NSDictionary* dict = [self makeNSDataImageDictionary:arrayInfo];
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"avatar_manager_saved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    dict = nil;
}


- (void) setEjabberdAvatar:(UIImage*) image hash:(NSString*)hash phone:(NSString*)phone {
    
    NSMutableDictionary* dict = [arrayInfo objectForKey:phone];
    if (dict == nil) {
        dict = [[NSMutableDictionary alloc] init];
        [arrayInfo setObject:dict forKey:phone];
    }

    if (image == nil) {
        [dict removeObjectForKey:@"ejabberd"];
        [dict removeObjectForKey:@"ejabberd_hash"];
        return;
    }
    
    [dict setObject:image forKey:@"ejabberd"];
    if (hash != nil)
        [dict setObject:hash forKey:@"ejabberd_hash"];
    
    [self save];
}

- (void) setUserAvatar:(UIImage*) image phone:(NSString*)phone {
    
    NSMutableDictionary* dict = [arrayInfo objectForKey:phone];
    if (dict == nil) {
        dict = [[NSMutableDictionary alloc] init];
        [arrayInfo setObject:dict forKey:phone];
    }
    
    if (image == nil) {
        [dict removeObjectForKey:@"user"];
        return;
    }
    
    [dict setObject:image forKey:@"user"];

    [self save];
}

- (UIImage*) getAvatarWithPhone:(NSString*)phone {
    
    NSDictionary* dict = [arrayInfo objectForKey:phone];
    if (dict == nil)
        return nil;
    
    if ([dict objectForKey:@"ejabberd"])
        return [dict objectForKey:@"ejabberd"];

    int index = [[AddressCollector sharedCollector] Get_Index_By_Phone:phone];
    if (index >= 0) {
        UIImage* image = [[AddressCollector sharedCollector] Get_Image_By_Index:index];
        if (image != nil)
            return image;
    }
    
//    if ([dict objectForKey:@"user"])
//        return [dict objectForKey:@"user"];
    
    return nil;
}

- (NSString*) getEjabberdAvatarHash:(NSString*)phone {
    
    NSDictionary* dict = [arrayInfo objectForKey:phone];
    if (dict == nil)
        return nil;

    return [dict objectForKey:@"ejabberd_hash"];
}

- (UIImage*) getMyAvatar {
    
    return [self getAvatarWithPhone:@"me"];
}

- (void) setMyAvatar:(UIImage*) avatar hash:(NSString*)hash {
    
    [self setEjabberdAvatar:avatar hash:hash phone:@"me"];
}

- (NSString*) getMyEjabberdAvatarHash:(NSString*)phone {
    
    return [self getEjabberdAvatarHash:@"me"];
}

- (void) removeMyAvatar {
    
    [arrayInfo removeObjectForKey:@"me"];
    [self save];
}

- (NSDictionary *)makeNSDataImageDictionary:(NSDictionary*) parent
{
    NSMutableDictionary * ret = [[NSMutableDictionary alloc]
                                 initWithCapacity:[parent count]];
    
    for (id key in [parent allKeys])
    {
        NSMutableDictionary* new_dict = [NSMutableDictionary dictionary];
        
        NSMutableDictionary* dict = [parent objectForKey:key];
        
        for (id sub_key in [dict allKeys]) {
            if ([[dict objectForKey:sub_key] isKindOfClass:[UIImage class]]) {
                UIImage* image = [dict objectForKey:sub_key];
                NSData* data = UIImageJPEGRepresentation(image, 0.8f);
                [new_dict setObject:data forKey:sub_key];
            }
            else {
                [new_dict setObject:[[dict objectForKey:sub_key] copy] forKey:sub_key];
            }
        }
        
        [ret setObject:new_dict forKey:key];
    }
    
    return ret;
}

- (NSMutableDictionary*) makeImageDictionary:(NSDictionary*) parent {
    
    NSMutableDictionary * ret = [[NSMutableDictionary alloc]
                                 initWithCapacity:[parent count]];
    
    for (id key in [parent allKeys])
    {
        NSMutableDictionary* new_dict = [NSMutableDictionary dictionary];
        
        NSMutableDictionary* dict = [parent objectForKey:key];
        
        for (id sub_key in [dict allKeys]) {
            if ([[dict objectForKey:sub_key] isKindOfClass:[NSData class]]) {
                NSData* imageData = [dict objectForKey:sub_key];
                UIImage* image = [UIImage imageWithData:imageData];
                [new_dict setObject:image forKey:sub_key];
            }
            else {
                [new_dict setObject:[[dict objectForKey:sub_key] copy] forKey:sub_key];
            }
        }
        
        [ret setObject:new_dict forKey:key];
    }
    
    return ret;
}

- (UIImage*) getAvatarWithAddressCollecterIndex:(int)index {
    
    NSArray *phones = [[AddressCollector sharedCollector] Get_Phones_By_Index:index];
    for (NSString* phone in phones) {
        UIImage* image = [self getAvatarWithPhone:phone];
        if (image != nil)
            return image;
    }
    
    return nil;
}

@end
