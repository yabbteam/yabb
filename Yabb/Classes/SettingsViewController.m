//
//  SettingsViewController.m
//  Yabb
//
//  Created by denebtechsaplin on 10/10/13.
//
//

#import <TPKeyboardAvoidingTableView.h>

#import "SettingsViewController.h"
#import "SettingsManager.h"
#import "ConnectionQueue.h"
#import "AppDelegate.h"

@interface SettingsViewController ()

@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *lastSeenCell;
@property (strong, nonatomic) IBOutlet UISwitch *lastSeenSwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *inAppVibrateCell;
@property (strong, nonatomic) IBOutlet UISwitch *inAppVibrateSwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *inAppSoundsCell;
@property (strong, nonatomic) IBOutlet UISwitch *inAppSoundsSwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *saveGalleryCell;
@property (strong, nonatomic) IBOutlet UISwitch *saveGallerySwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *sendSeenCell;
@property (strong, nonatomic) IBOutlet UISwitch *sendSeenSwitch;

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"Settings", @"Settings");
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;

    [self customizeAppearance];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadData];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self saveData];

    [app.chatRoomsViewController sendSoundSettingToServer];
}

- (void)customizeAppearance
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
    } else {
    }
}

- (void)loadData
{
    self.inAppSoundsSwitch.on = [[SettingsManager sharedInstance] enableSounds];
    self.inAppVibrateSwitch.on = [[SettingsManager sharedInstance] enableVibration];
    self.lastSeenSwitch.on = [[SettingsManager sharedInstance] showLastSeenTimestamp];
    self.saveGallerySwitch.on = [[SettingsManager sharedInstance] saveToGallery];
    self.sendSeenSwitch.on = [[SettingsManager sharedInstance] enableSendSeen];
}

- (void) saveData
{
    [[SettingsManager sharedInstance] setEnableSounds:self.inAppSoundsSwitch.on];
    [[SettingsManager sharedInstance] setEnableVibration:self.inAppVibrateSwitch.on];
    [[SettingsManager sharedInstance] setShowLastSeenTimestamp:self.lastSeenSwitch.on];
    [[SettingsManager sharedInstance] setSaveToGallery:self.saveGallerySwitch.on];
    [[SettingsManager sharedInstance] setEnableSendSeen:self.sendSeenSwitch.on];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: return 2;
        case 1: return 1;
        case 2: return 1;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0: return NSLocalizedString(@"Sound & Vibration (All Chats)", @"Sound & Vibration (All Chats)");
        case 1: return NSLocalizedString(@"Media", @"Media");
        case 2: return NSLocalizedString(@"Message", @"Message");
    }
    return nil;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0: return self.inAppSoundsCell;
                case 1: return self.inAppVibrateCell;
            }
        case 1:
            switch (indexPath.row) {
                case 0: return self.saveGalleryCell;
            }
        case 2:
            switch (indexPath.row) {
                case 0: return self.sendSeenCell;
            }
    }
    return nil;
}

- (IBAction)onChangeSoundValue:(id)sender {
    [self saveData];
}

- (IBAction)onChangeVibrationValue:(id)sender {
    [self saveData];
}

@end
