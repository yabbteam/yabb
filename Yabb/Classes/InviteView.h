//
//  InviteView.h
//  Baycall
//
//  Created by denebtech on 09.10.12.
//
//

#import <UIKit/UIKit.h>

@protocol InviteViewDelegate;

@interface InviteView : UIView {

    id<InviteViewDelegate> __weak _invite_delegate;
}

@property (nonatomic, weak) IBOutlet id<InviteViewDelegate> invite_delegate;

@end

@protocol InviteViewDelegate <NSObject>

@optional
    -(void) didActive;

@end
