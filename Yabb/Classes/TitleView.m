//
//  ChatTitleView.m
//  Yabb
//
//  Created by denebtechsaplin on 10/8/13.
//
//

#import "TitleView.h"

@interface TitleView ()

@end

@implementation TitleView {
    
    UILabel *_titleLabel;
    UILabel *_subtitleLabel;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.subtitleLabel];
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.subtitleLabel.text = subtitle;
    if (subtitle) {
        self.titleLabel.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height*0.7);
        self.subtitleLabel.frame = CGRectMake(0, self.bounds.size.height*0.7, self.bounds.size.width, self.bounds.size.height*0.3);
        self.subtitleLabel.hidden = NO;
    } else {
        self.titleLabel.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        self.subtitleLabel.hidden = YES;
    }
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height*0.7)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.shadowColor = [UIColor darkGrayColor];
        _titleLabel.shadowOffset = CGSizeMake(0, -1);
        _titleLabel.font = [UIFont boldSystemFontOfSize:21.0f];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UILabel *)subtitleLabel
{
    if (!_subtitleLabel) {
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height*0.7, self.bounds.size.width, self.bounds.size.height*0.3)];
        _subtitleLabel.backgroundColor = [UIColor clearColor];
        _subtitleLabel.textColor = [UIColor lightGrayColor];
        _subtitleLabel.font = [UIFont boldSystemFontOfSize:10.0f];
        _subtitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _subtitleLabel;
}

@end
