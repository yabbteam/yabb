//
//  ChatStorage.h
//  speedsip
//
//  Created by denebtech on 30.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AssetsLibrary/AssetsLibrary.h"

#define CHAT_BUBBLE_WIDTH   200
#define EMOTION_CELL_HEIGHT 110
#define CHAT_FONT_SIZE      16.0
#define IMAGE_COMMENT_FONT_SIZE      12.0
#define SMILE_REPLACE       @"11 "
#define IS_IPHONE_5   ( [ [ UIScreen mainScreen ] bounds ].size.height == 568 )

#define SAVE_TEMP_TEXT_FORMAT @"tempText_%@"

enum {
    MSG_STATUS_PENDING = 0,
    MSG_STATUS_SENT = 1,
    MSG_STATUS_CONFIRMED = 2,
    MSG_STATUS_IMAGE_UPLOADING = 3,
    MSG_STATUS_SEND_FAILED = 4,
    MSG_STATUS_SEND_CANCELLED = 5,
};

@interface ChatStorage : NSObject  {
    
    NSDateFormatter *formatter;
    NSString*        _typing;
}

@property (nonatomic, strong) NSString* typing;

+ (ChatStorage *) sharedStorage;

+ (CGSize) getThumbSize:(CGSize)size;

- (NSString *) getTimeStamp:(NSTimeInterval)time;
- (NSTimeInterval) getTime:(NSString *)time_stamp;
- (NSString *) generateRandom:(int)length;
- (NSString *)encodeString:(NSString *)str;
- (NSString *)decodeString:(NSString *)str;
- (NSDictionary *)decodeStringToDictionary:(NSString *)str;

// Messages
- (int) countGroupMessages:(NSString*)chat_id forTimeGroup:(long long)time_group;
- (int) countNewMessages;
- (int) countRoomNewMessages:(NSString*)chat_id;
- (int)countNewMessagesWithEnableSound;
- (int)countNewMessagesWithEnableVibrate;

- (void) markRoomRead:(NSString*) chat_id;

- (long long) addMessage:(NSString *)message withChatId:(NSString*)chat_id;
- (long long) addImageMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withAsset:(NSString *)asset withName:(NSString *)media_name comment:(NSString*)comment;
- (long long) addVideoMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withAsset:(NSString *)asset withName:(NSString *)media_name comment:(NSString*)comment;
- (long long) addLocationMessage:(UIImage *)thumb withLocalId:(NSString *)local_id withChatId:(NSString*)chat_id withLocation:(NSString *)location comment:(NSString*)comment;
- (NSDictionary *)getRecord:(NSString*)chat_id withIndex:(int)index forTimeGroup:(long long)time_group;
- (NSArray *)getRecords:(NSString*)chat_id forTimeGroup:(long long)time_group;
- (NSArray *) getAllRecords:(NSString*)chat_id;
- (NSDictionary *) getMessage:(NSString*)chat_id withIndex:(int)idx forTimeGroup:(long long)time_group;
- (NSArray *)getLocalMessages;
- (NSArray *)getUnconfirmedMessages;
- (NSArray *)getDeliveredMessages;
- (NSDictionary *) getLastMessage:(NSString*) chat_id;
- (void) resetDeliveredMessages;
- (NSString*) getLastIncomingMessage:(NSString*) chat_id;

- (NSString *) dateToString:(NSDate *)date format:(NSString *)format;
- (NSString *) dateToStrTime:(NSDate *)date;
- (NSArray *) getTimeGroups:(NSString*)chat_id count:(int)count;
- (NSDictionary *)getRecordByServerId:(NSString*)server_id;

- (NSArray *) syncMessages:(NSDictionary *)dict;

- (void) deleteMessage:(NSString*)server_id;
- (void) deleteAllMessages:(NSString*)room_id;

- (void) resetAccount;
- (void) clearAccount;
- (void) setMessageReadMark:(NSString*) server_id;
- (void) setMessageWinkMark:(NSString*) server_id;

- (void) setMessageStatusByLocalId:(NSString*) localid status:(int)status;
- (void) initImageMessageState;
- (BOOL) isOwnMessage:(NSString*)server_id;
- (BOOL) isRecordVideoByServerId:(NSString*)server_id;

- (NSDictionary*) getLocationInfo:(NSString*)server_id;
- (NSDictionary*) getLocationInfoFromLocationString:(NSString*)location;

// Chats
- (NSString *) addChatRoom:(NSString *)chat_name withChatId:(NSString*)chat_id withPhones:(NSArray *)phones;
- (NSString *) getRoomName:(NSString*)chat_id;
- (void) renameRoom:(NSString*)chat_id withName:(NSString *)name;
- (NSString *) getRoomPhone:(NSString*)chat_id;
- (BOOL) getRoomPrivate:(NSString*)chat_id;
- (NSArray *) getChatRooms;
- (NSDictionary *) getChatRoom:(NSString*)chat_id;
- (NSString*) getPrivateRoomByPhone:(NSString *)phone;
- (void) deleteChatRoom:(NSString*)chat_id;

- (int) getMessageHeight:(NSDictionary *)msg_dict;

- (void) setRoomBackground:(NSString *)image_name forRoom:(NSString*)chat_id;
- (NSString *) getRoomBackground:(NSString*)chat_id;

- (void) appendNewPhoneNumberToGroupChat:(NSString*)chat_id phone:(NSString*)phone;
- (void) removePhoneNumberFromGroupChat:(NSString*)chat_id phone:(NSString*)phone;

- (int) getMessageCount:(NSString*)chat_id;

- (BOOL) getChatSoundSetting:(NSString*)chat_id;
- (BOOL) getChatVibrateSetting:(NSString*)chat_id;
- (void) setChatSoundSetting:(NSString*)chat_id setting:(BOOL)enabled;
- (void) setChatVibrateSetting:(NSString*)chat_id setting:(BOOL)enabled;

// Images
- (void) getImage:(NSString*)server_id target:(id)target done:(SEL)done_callback failed:(SEL)failed_callback;
- (void) getVideo:(NSString*)server_id target:(id)target done:(SEL)done_callback failed:(SEL)failed_callback;
- (void) setImageAsset:(NSString *)asset forServerId:(NSString*)server_id;
- (void) setMediaName:(NSString *)name forServerId:(NSString*)server_id;
- (BOOL) Save_Image_Data:(NSData *)data withName:(NSString *)name;
- (NSString*) getMediaSavePath:(NSString*)name;
- (NSString*) Save_Video_Data:(ALAsset*) asset withName:(NSString*)name;
- (NSString*) Save_Video_Data_From_File:(NSString*) videoPath withName:(NSString*)name;
- (void) changeMediaNameWithLocalId:(NSString*)msgid name:(NSString*)name;
- (int) Get_Media_Size:(NSString *)name;
- (BOOL) Delete_Media:(NSString*)name;
- (BOOL) Media_Exist:(NSString*)name;
- (UIImage *) Load_Image_Data:(NSString *)name;
- (NSData *) Load_Video_Data:(NSString *)name;
- (NSArray*) getImagesServerId:(NSString*) chatId;

- (NSString *) getAnimationName:(NSString *) str;
- (int) getAnimationIndex:(NSString *) str ;
- (BOOL) connectedToNetwork;
- (void) appDidLaunch;

@end
