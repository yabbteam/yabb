//
//  Constants.m
//  MarkaVIP
//
//  Created by denebtech on 7/25/13.
//  Copyright (c) 2013 MarkaVIP. All rights reserved.
//

#import "Constants.h"

#ifdef DEVELOPMENT
    NSString * const API_ENDPOINT = @"http://j.yabb.com/api.php";
    NSString * const XMPP_HOSTNAME = @"j.yabb.com";
    NSString * const FILE_ENDPOINT = @"http://j.yabb.com:5280";
#elif DISTRIBUTION
    NSString * const API_ENDPOINT = @"http://live.j.yabb.com/api.php";
    NSString * const XMPP_HOSTNAME = @"live.j.yabb.com";
    NSString * const FILE_ENDPOINT = @"http://live.j.yabb.com:5280";
#endif
