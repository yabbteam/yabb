//
//  DeleteButton.m
//  Baycall
//
//  Created by denebtech on 21.09.12.
//
//

#import "DeleteButton.h"

@implementation DeleteButton


- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        
        UIImage * image = [[UIImage imageNamed:@"deleteBtn.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:2];
        [self setBackgroundImage:image forState:UIControlStateNormal];
        
        trash_image = [[UIImageView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, 4, 14, 18)];
        [trash_image setImage:[UIImage imageNamed:@"deleteImg.png"]];
        [self addSubview:trash_image];
        
        self.enabled = NO;
    }
    
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImage * image = [[UIImage imageNamed:@"deleteBtn.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:4];
        [self setBackgroundImage:image forState:UIControlStateNormal];
        
        
        self.enabled = YES;
        
    }
    
    return self;
}


- (void) setTitle:(NSString *)title forState:(UIControlState)state {
    
    [super setTitle:title forState:state];

    if (trash_image)
        [trash_image setFrame:CGRectMake(self.titleLabel.frame.origin.x, 4, 14, 18)];
    
    
}

@end
