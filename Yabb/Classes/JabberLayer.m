
//
//  JabberLayer.m
//  Yabb
//
//  Created by denebtech on 1/23/14.
//
//

#import "JabberLayer.h"
#import "AddressCollector.h"
#import "ChatStatusViewController.h"
#import "ChatStorage.h"
#import "XMPPMessage+XEP0045.h"
#import "NSData+Base64.h"
#import "Friends.h"
#import "XMPPvCardTemp.h"
#import "TimeBomb.h"
#import "AvatarManager.h"
#import "ConnectionQueue.h"
#import "ASIHTTPRequest.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "MyXMPPRoom.h"
#import "LocationViewController.h"
#import "AppDelegate.h"
#import "Constants.h"

#if DV_FLOATING_WINDOW_ENABLE
    #undef XMPPLogInfo
    #define XMPPLogInfo NSLog
#endif

static const int xmppLogLevel = XMPP_LOG_LEVEL_INFO;
#define UPLOAD_IMAGE_TIME_OUT 300

#define OUR_VIDEO_EXTENSION @".mp4"

enum {
    LOG_STATE_NONE,
    LOG_STATE_CONNECTING,
    LOG_STATE_LOGGING,
    LOG_STATE_RECONNECTING,
    LOG_STATE_LOGGED,
    LOG_STATE_DEACTIVATING
};

#define YABB_GROUP_CHAT @"yabb_group_chat"

void runOnMainQueueWithoutDeadlocking(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

@implementation JabberLayer

static JabberLayer * jabberlayer = nil;

+ (JabberLayer *) SharedInstance {
    
    if (jabberlayer == nil) {
        
        jabberlayer = [[JabberLayer alloc] init];
    }
    
    return jabberlayer;
}

- (id) init {
    
    self = [super init];
    if (self) {
        xmppRooms = [[NSMutableDictionary alloc] init];
        workingQueue = dispatch_get_main_queue();//dispatch_queue_create("jabber_layer_thread_pool", NULL);
        hashCmdClass = [[NSMutableDictionary alloc] init];
        op_queue = [[NSOperationQueue alloc] init];
        op_queue.maxConcurrentOperationCount = 1;
        uploadFileInfos = [[NSMutableArray alloc] init];
        
        loggingState = LOG_STATE_NONE;
        
        [self setupStream];
    }
    
    return self;
}

- (BOOL) login:(NSString*) userid password:(NSString*)password {
    
    if (userid == nil || password == nil)
        return NO;
    
    loggingState = LOG_STATE_CONNECTING;
    
    XMPPLogInfo(@"start connecting...!");
    
    currentUserJid = [XMPPJID jidWithUser:userid domain:@"yabb.com" resource:nil];
    [xmppStream setMyJID:currentUserJid];
    
    user_password = password;
    
    if (xmppStream && [xmppStream isConnected]) {
        [xmppStream disconnect];
    }
    
    sessionToken = @"";
    
    NSError *error = nil;
    if (![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error]) {
        [self bk_performBlock:^(id sender){
            if (login_delegate != nil && [login_delegate respondsToSelector:@selector(loginCompleted:needRegister:)])
                [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@NO withObject:@NO];
        } afterDelay:0.5f];
    }
    
    return YES;
}

- (BOOL) isActive {
    
    return xmppStream != nil && [xmppStream isAuthenticated];
}

- (BOOL) isConnectedServer {
    
    return [xmppStream isConnected] && _isConnectedServer && (loggingState != LOG_STATE_RECONNECTING);
}


- (void) setupStream {
    
    xmppStream = [[XMPPStream alloc] init];
    
#if !TARGET_IPHONE_SIMULATOR
	{
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        xmppStream.enableBackgroundingOnSocket = YES;
	}
#endif
    
    [xmppStream addDelegate:self delegateQueue:workingQueue];
    
    xmppStream.hostName = XMPP_HOSTNAME;
    xmppStream.hostPort = 5222;
    xmppStream.autoStartTLS = YES;
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    [xmppReconnect activate:xmppStream];
    [xmppReconnect addDelegate:self delegateQueue:workingQueue];
    xmppReconnect.autoReconnect = YES;
    
    xmppMessageDeliveryRecipts = [[XMPPMessageDeliveryReceipts alloc] initWithDispatchQueue:workingQueue];
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = NO;
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = YES;
    [xmppMessageDeliveryRecipts activate:xmppStream];
    
    xmppLastActivity = [[XMPPLastActivity alloc] init];
    xmppLastActivity.respondsToQueries = YES;
    [xmppLastActivity activate:xmppStream];
    [xmppLastActivity addDelegate:self delegateQueue:workingQueue];
    
    xmppMuc = [[XMPPMUC alloc] initWithDispatchQueue:workingQueue];
    [xmppMuc activate:xmppStream];
    [xmppMuc addDelegate:self delegateQueue:workingQueue];
    
    xmppAutoPing = [[XMPPAutoPing alloc] initWithDispatchQueue:workingQueue];
    [xmppAutoPing activate:xmppStream];
    [xmppAutoPing addDelegate:self delegateQueue:workingQueue];
    [xmppAutoPing setPingInterval:5.0f];
    [xmppAutoPing setPingTimeout:3.0f];
}

- (void) sayOffline {
    if (![xmppStream isAuthenticated])
        return;
    
	XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
	[xmppStream sendElement:presence];
}

- (void) sayAway {
    
    if (![xmppStream isAuthenticated])
        return;
    
	XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    
    NSXMLElement *show = [NSXMLElement elementWithName:@"show"];
    
    [show setStringValue:@"away"];
    [presence addChild:show];
    
	[xmppStream sendElement:presence];
}

- (void) sayOnline {
    
    if (![xmppStream isAuthenticated])
        return;
    
    NSString * user_status = [ChatStatusViewController getStatus];
    
	XMPPPresence *presence = [XMPPPresence presence];
    
    NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
    
    [status setStringValue:user_status];
    
    [presence addChild:status];
	[xmppStream sendElement:presence];
}

#pragma mark -
#pragma mark XMPP delegates

- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    
    XMPPLogInfo(@"start logging in...!");
    
    if (![sender isAuthenticated]) {
        if (loggingState == LOG_STATE_CONNECTING)
            loggingState = LOG_STATE_LOGGING;
        
        NSError *error = nil;
        [xmppStream authenticateWithPassword:user_password error:&error];
    }
    
    _isConnectedServer = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_CONNECTED object:nil];
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    
    if ([sender isAuthenticated]) {
        
        XMPPLogInfo(@"Authenticate successed!");
        [self sendGetSessionTokenRequest];
    }
    else {
        
        XMPPLogInfo(@"Authenticate failed!");
        currentUserJid = nil;
        if (login_delegate != nil && [login_delegate respondsToSelector:@selector(loginCompleted:needRegister:)])
            [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@NO withObject:@NO];
    }
}


- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    
    if ([iq elementForName:@"value"] != nil || [iq elementForName:@"query"] != nil )
        XMPPLogInfo(@"Jabber received IQ - %@", [iq description]);
    
    // get token for uplading/downloading on server
	//NSString *iqid = [[iq attributeForName:@"id"] stringValue];
	if ([iq attributeForName:@"type"] != nil && [[[iq attributeForName:@"type"] stringValue] isEqualToString:@"result"]) {
        NSString* token = [[iq elementForName:@"value"] stringValue];
        if (token != nil) {
            [self onSessionTokenReceived:token];
        }
    }
    
    //room member list
    NSArray *members = [self didReceivedMemberList:iq];
    NSString* currentid = [currentUserJid user];
    if (members != nil) {
        for (NSString* item in members) {
            NSString* userid = [[XMPPJID jidWithString:item] resource];
            NSString* phone = [self getPhoneNo:userid];
            
            if (![userid isEqualToString:currentid])
                [[ChatStorage sharedStorage] appendNewPhoneNumberToGroupChat:[iq fromStr] phone:phone];
        }
    }
    
    return NO;
}

- (void) onSessionTokenReceived:(NSString*)token {
    
    sessionToken = token;
    
    if (loggingState == LOG_STATE_LOGGING) {
        
        [[AddressCollector sharedCollector] GetPeopleWithNormal:YES];
        
        [[AddressCollector sharedCollector] setConnectionDoneDeleagate:self];
        [[AddressCollector sharedCollector] Upload_To_Server:YES];
    }
    else if (loggingState == LOG_STATE_RECONNECTING) {
        
        //re-connecting
        [self sayOnline];
        
        loggingState = LOG_STATE_LOGGED;
        [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_RECONNECTED object:nil];
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    
    XMPPLogInfo(@"Jabber received message - %@", [message description]);
    
    NSString *msg = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];
    NSString *messageid = [[message attributeForName:@"id"] stringValue];
    
    if ([self isGroupChatInvite:message]) {
        
        NSString *reason = [self getGroupChatInviteReason:message];
        if (reason != nil && [reason isEqualToString:YABB_GROUP_CHAT]) {
            [self onGroupChatInvited:from];
            return;
        }
    }
    
    if ([message isGroupChatMessageWithSubject]) {
        [self onGroupSubjectMessageReceived:message];
        return;
    }
    
    if (msg == nil) {
        
        [self processMessageEventNotification:message];
        return;
    }
    
    if (messageid == nil) {
        messageid = [self requestRandomMessageId];
    }
    
    if (![self isMessage:message]) {
        return;
    }
    
    NSString* chatid = nil;
    NSString* fromUser = nil;
    if ([self isGroupMessage:message]) {
        chatid = [[XMPPJID jidWithString:from] bare];
        fromUser = [[XMPPJID jidWithString:from] resource];
    }
    else {
        chatid = [[XMPPJID jidWithString:from] user];
        fromUser = [[XMPPJID jidWithString:from] user];
    }
    
    if ([fromUser isEqualToString:[currentUserJid user]])
        return;
    
    if (![self isChatExist:chatid]) {
        
        [self onChatCreated:[NSArray arrayWithObject:chatid]];
    }

    NSString* thumbUrl = [self getPropertyWithName:@"thumb" parent:message];
    if (thumbUrl != nil) {
        
        ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:thumbUrl ]];
        __weak ASIHTTPRequest *_request = request;
        
        [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
        
        [request setRequestMethod:@"GET"];
        
        [request setFailedBlock:^{
            NSError* error = _request.error;
            NSLog(@"Httperror:%@%lu", error.localizedDescription, error.code);
        }];
        
        [request setCompletionBlock:^{
            
            NSInteger responseCode = [_request responseStatusCode];
            if (responseCode == 200)
                [self onMessageReceived:message thumb:_request.responseData];
        }];
        
        [request startAsynchronous];
        
        return;
    }
    
    NSString* location = [self getPropertyWithName:@"location" parent:message];
    if (location != nil) {
        NSDictionary* dict_location = [[ChatStorage sharedStorage] getLocationInfoFromLocationString:location];
        float latitude = [[dict_location objectForKey:@"latitude"] floatValue];
        float longitude = [[dict_location objectForKey:@"longitude"] floatValue];
        
        CLLocationCoordinate2D pt;
        pt.latitude = latitude;
        pt.longitude = longitude;
        
        if (app.isForeground) {
            [LocationViewController getMapSnapShot:pt handler:^(UIImage* thumb) {
                NSData* data;
                if (thumb != nil)
                    data = UIImageJPEGRepresentation(thumb, 0.85);
                else
                    data = UIImageJPEGRepresentation([UIImage imageNamed:@"mark_location.png"], 0.85);
                
                [self onMessageReceived:message thumb:data];
            }];
        }
        else {
            NSData* data;
            data = UIImageJPEGRepresentation([UIImage imageNamed:@"mark_location.png"], 0.85);
            [self onMessageReceived:message thumb:data];
        }
        
        return;
    }
    
    [self onMessageReceived:message thumb:nil];
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(NSXMLElement *)error {
 
    NSLog(@"didReceiveError - %@", [error description]);

	NSString *elementName = [error name];
	
	if ([elementName isEqualToString:@"stream:error"] || [elementName isEqualToString:@"error"])
	{
		NSXMLElement *conflict = [error elementForName:@"conflict" xmlns:@"urn:ietf:params:xml:ns:xmpp-streams"];
		if (conflict)
		{
            if (loggingState != LOG_STATE_DEACTIVATING)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"conflict_account" object:nil];
		}
	}
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {

    XMPPLogInfo(@"xmppStreamDidDisconnect - %@", error==nil?@"":[error description]);
    [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_DISCONNECTED object:nil];
    
    if (login_delegate != nil && [login_delegate respondsToSelector:@selector(loginCompleted:needRegister:)]) {
        
        if (loggingState == LOG_STATE_LOGGING || loggingState == LOG_STATE_CONNECTING) {
            if (error != nil && error.code == 7) //confict error
                [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@NO withObject:@YES];
            else
                [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@NO withObject:@NO];
        }
    }

    _isConnectedServer = NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    
    XMPPLogInfo(@"Jabber received presence - %@", [presence description]);
    
    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate != nil) {
        
        NSString *presenceFromUser = [[presence from] user];
        
        NSString * userid = [currentUserJid user];
        if ([userid isEqualToString:presenceFromUser])
            return;
        
        if ([self isGroupChat:[[presence from] bare]])
            return;
        
        NSString *presenceType = [presence type];
        NSString* presenceShow = [presence show];
        if (presenceType == nil || ([presenceType isEqualToString:@"available"] && (presenceShow == nil || ![presenceShow isEqualToString:@"away"])))
            presenceType = @"1";
        else
            presenceType = @"0";
        
        NSString *presenceStatus = [presence status];
        if (presenceStatus == nil)
            presenceStatus = @"";
        
        NSMutableDictionary* d = [NSMutableDictionary dictionary];
        
        [d setObject:presenceFromUser forKey:@"userid"];
        [d setObject:[NSString stringWithFormat:@"+%@", presenceFromUser] forKey:@"phone"];
        [d setObject:presenceType forKey:@"online"];
        [d setObject:presenceStatus forKey:@"origin"];
        
        NSXMLElement *xElement = [presence elementForName:@"x" xmlns:@"vcard-temp:x:update"];
        NSXMLElement* elementPhoto = [xElement elementForName:@"photo"];
        if (elementPhoto != nil) {
            NSString* photoHash = [elementPhoto stringValue];
            if (photoHash != nil && [photoHash length] > 0) {
                NSData* photo_data = [xmppvCardAvatarModule photoDataForJID:[presence from]];
                if (photo_data != nil) {
                    UIImage *photo = [[UIImage alloc] initWithData:photo_data];
                    [d setObject:photo forKey:@"photo"];
                }
            }
            else {
                [xmppvCardTempModule fetchvCardTempForJID:[presence from] ignoreStorage:YES];
                [d setObject:@YES forKey:@"empty_photo"];
            }
        }
        
        NSArray* friends = [NSArray arrayWithObject:d];
        
        NSMutableDictionary* data = [NSMutableDictionary dictionary];
        [data setObject:@"" forKey:@"origin"];
        [data setObject:friends forKey:@"friends"];
        
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        [dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
        [dict setObject:@"" forKey:@"error"];
        [dict setObject:data forKey:@"data"];
        
        if (![self isGroupChat:[[presence from] full]]) {
            [xmppLastActivity sendLastActivityQueryToJID:[presence from]];
        }

        runOnMainQueueWithoutDeadlocking(^{
            if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                [delegate performSelector:@selector(didConnectionDone:withName:) withObject:dict withObject:@"lazy_chat_connection"];
            }
        });
    }
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    
    //[self onSentMessage:message];
}

- (XMPPIQ *)xmppStream:(XMPPStream *)sender willSendIQ:(XMPPIQ *)iq {
    
    if ([iq elementForName:@"ping"] == nil)
        XMPPLogInfo(@"Jabber sent IQ - %@", [iq description]);
    
    return iq;
}

- (XMPPMessage *)xmppStream:(XMPPStream *)sender willSendMessage:(XMPPMessage *)message {
    
    XMPPLogInfo(@"Jabber sent message - %@", [message description]);
    return message;
}

- (XMPPPresence *)xmppStream:(XMPPStream *)sender willSendPresence:(XMPPPresence *)presence {
    
    XMPPLogInfo(@"Jabber sent  presence - %@", [presence description]);
    return presence;
}

- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence {
 
}

#pragma mark -

- (void) uploadedABook:(NSNumber*) successed {
    
    if ([successed boolValue]) {

        loggingState = LOG_STATE_LOGGED;
        
        [self sayOnline];
        if (abookBlock != nil)
            dispatch_async(workingQueue, abookBlock);
    }
    else {
        
        loggingState = LOG_STATE_NONE;
        
        XMPPLogInfo(@"uploadAbook failed!");
        if (login_delegate != nil && [login_delegate respondsToSelector:@selector(loginCompleted:needRegister:)])
            [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@NO withObject:@NO];
    }
}

- (void) setLoggedinDelegate:(id)delegate {
    
    login_delegate = delegate;
}

- (NSMutableArray *)makeFriendsListFromRoster:(XMPPRosterMemoryStorage*)storage {
    
    NSMutableArray* friends = [[NSMutableArray alloc] init];
    NSArray* array = [storage unsortedUsers];
    
    for (XMPPUserMemoryStorageObject* object in array) {
        NSMutableDictionary* d = [NSMutableDictionary dictionary];
        NSString* userid = object.jid.user;
        NSString* username = object.nickname;
        if ([arrayNicknames objectForKey:object.jid.full])
            username = [arrayNicknames objectForKey:object.jid.full];
        
        [d setObject:userid forKey:@"userid"];
        [d setObject:[NSString stringWithFormat:@"+%@", userid] forKey:@"phone"];
        if ([object isOnline])
            [d setObject:@"1" forKey:@"online"];
        else
            [d setObject:@"0" forKey:@"online"];
        [d setObject:@"" forKey:@"origin"];
        if (username != nil)
            [d setObject:username forKey:@"name"];
        
        NSData* data = [xmppvCardAvatarModule photoDataForJID:object.jid];
        if (data != nil) {
            UIImage *photo = [[UIImage alloc] initWithData:data];
            [d setObject:photo forKey:@"photo"];
        }
        
        [friends addObject:d];
        
        [xmppLastActivity sendLastActivityQueryToJID:object.jid];
    }
    return friends;
}

- (NSDictionary*) makeImportResponse {
    
    NSMutableArray *friends;
    friends = [self makeFriendsListFromRoster:xmppRosterMemStorage];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@"" forKey:@"origin"];
    [data setObject:friends forKey:@"friends"];
    
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [dict setObject:@"" forKey:@"error"];
    [dict setObject:data forKey:@"data"];
    
    return dict;
}

- (BOOL) processThis:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate silent:(BOOL)silent {
    
    NSString* cmd = [dict objectForKey:@"cmd"];
    [hashCmdClass setObject:delegate forKey:cmd];

    BOOL ret = NO;
    
    __weak JabberLayer* weakSelf = self;
    if ([cmd isEqualToString:@"import"]) {
        ret = NO;
        abookBlock = ^{
            [weakSelf processImport:dict name:name delegate:delegate];
        };
    }
    else if ([cmd isEqualToString:@"profile"]) {
        if ([dict objectForKey: @"origin"] != nil) {
            ret = YES;
            [self processProfile:dict name:name delegate:delegate];
        }
    }
    else if ([cmd isEqualToString:@"reset"]) {
        ret = YES;
        loggingState = LOG_STATE_DEACTIVATING;
        [[ConnectionQueue sharedQueue] Add_Connection_To_REST:self withParams:dict withName:name silent:silent];
    }
    else if ([cmd isEqualToString:@"addchat"]) {
        ret = YES;
        [self processAddchat:dict name:name delegate:delegate];
    }
    else if ([cmd isEqualToString:@"chatin"]) {
        ret = YES;
        [self processChatin:dict name:name delegate:delegate];
    }
    else if ([cmd isEqualToString:@"chatname"]) {
        ret = YES;
        [self processChatName:dict name:name delegate:delegate];
    }
    else if ([cmd isEqualToString:@"chatout"]) {
        ret = YES;
        [self processChatOut:dict name:name delegate:delegate];
    }
    else if ([cmd isEqualToString:@"getmedia"]) {
        ret = YES;
        [self processDownloadMedia:dict name:name delegate:delegate];
    }
    
    return ret;
}

- (void) processImport:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    XMPPLogInfo(@"starting fetchRoster...");
    
    if (xmppvCardAvatarModule != nil) {
        [xmppvCardAvatarModule deactivate];
        xmppvCardAvatarModule = nil;
    }
    
    if (xmppvCardTempModule != nil) {
        [xmppvCardTempModule deactivate];
        xmppvCardTempModule = nil;
    }
    
    if (xmppRoster != nil) {
        [xmppRoster deactivate];
        xmppRoster = nil;
    }
    
    if (xmppRosterMemStorage != nil)
        xmppRosterMemStorage = nil;
    
    xmppRosterMemStorage = [[XMPPRosterMemoryStorage alloc] init];
    xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterMemStorage dispatchQueue:workingQueue];
    [xmppRoster addDelegate:self delegateQueue:workingQueue];
	xmppRoster.autoFetchRoster = NO;
	xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    [xmppRoster activate:xmppStream];
    
    xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:[XMPPvCardCoreDataStorage sharedInstance] dispatchQueue:workingQueue];
    [xmppvCardTempModule activate:xmppStream];
    [xmppvCardTempModule addDelegate:self delegateQueue:workingQueue];

    xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:xmppvCardTempModule dispatchQueue:workingQueue];
    [xmppvCardAvatarModule activate:xmppStream];
    [xmppvCardAvatarModule addDelegate:self delegateQueue:workingQueue];
    
    [xmppRoster fetchRoster];
}

- (void) processProfile:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    dispatch_async(workingQueue, ^{
        
        NSString* origin = [dict objectForKey:@"origin"];
        [self sayChangeStatus:origin];
        
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        [dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
        [dict setObject:@"" forKey:@"error"];
        
        runOnMainQueueWithoutDeadlocking(^{
            if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                [delegate performSelector:@selector(didConnectionDone:withName:) withObject:dict withObject:name];
            }
        });
        
    });
}

- (void) processReset:(NSDictionary*)argDict name:(NSString*)name delegate:(id)delegate {
    
    [self clear];
    
    [xmppStream disconnect];
    
    loggingState = LOG_STATE_NONE;
    
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [dict setObject:@"" forKey:@"error"];
    
    if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
        [delegate performSelector:@selector(didConnectionDone:withName:) withObject:dict withObject:name];
    }
}

- (void) registerConnectionDelegate:(id)delegate name:(NSString*)name {
    
    [hashCmdClass setObject:delegate forKey:name];
}

- (void) sayChangeStatus:(NSString*)statusText {
    
    if (![xmppStream isAuthenticated])
        return;
    
	XMPPPresence *presence = [XMPPPresence presence];
    
    NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
    
    [status setStringValue:statusText];
    
    [presence addChild:status];
	[xmppStream sendElement:presence];
}

- (void) onChatCreated:(NSArray*) chatids {
    
    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate == nil)
        return;
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    for (NSString* chatid in chatids) {
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        
        [dict setObject:@"1" forKey:@"private"];
        
        [dict setObject:chatid forKey:@"chatid"];
        
        [dict setObject:[self getPhoneNo:chatid] forKey:@"phone"];
        
        [ma addObject:dict];
    }
    
    [data_dict setObject:ma forKey:@"chats"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
        }
    });
}

- (BOOL) isChatExist:(NSString*)chatid {
    
    return [[ChatStorage sharedStorage] getChatRoom:chatid] != nil;
}

- (NSString*) getPhoneNoFromJidString:(NSString*)jid {
    
    return [NSString stringWithFormat:@"+%@", [[XMPPJID jidWithString:jid] user]];
}

- (NSString*) getPhoneNo:(NSString*)userid {
    
    return [NSString stringWithFormat:@"+%@", userid];
}

- (NSString*) getUserId:(NSString*)phone {
    
    return [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
}

- (NSString*) makeJid:(NSString*)user {
    
    return [NSString stringWithFormat:@"%@@yabb.com", [user stringByReplacingOccurrencesOfString:@"+" withString:@""]];
}

- (void) onMessageReceived:(XMPPMessage*) message thumb:(NSData*)thumb{
    
    [self processOnline:message];
    
    NSString *msg = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];
    NSString *messageid = [[message attributeForName:@"id"] stringValue];
    NSString* dateStr = [self getPropertyWithName:@"date" parent:message];
    NSString* messageTimeout = [self getPropertyWithName:@"timebomb" parent:message];
    NSString* locationInfo = [self getPropertyWithName:@"location" parent:message];
    
    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate == nil)
        return;
    
    if (messageid == nil)
        return;
    
    if (messageTimeout != nil) {
        [[TimeBomb sharedInstance] setMessageTimeout:messageid value:[messageTimeout intValue]];
        thumb = UIImageJPEGRepresentation([UIImage imageNamed:@"eye_opened.png"], 0.85);
    }
    
    NSString* groupChatAvatarUrl = [self getGroupChatAvatarUrl:message];
    if (groupChatAvatarUrl != nil) {
        [self downloadGroupChatAvatar:groupChatAvatarUrl from:[[XMPPJID jidWithString:from] bare]];
        return;
    }
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];

    if (msg != nil)
        [dict setObject:[[ChatStorage sharedStorage] encodeString:msg] forKey:@"message"];
    
    if (locationInfo != nil) {
        [dict setObject:locationInfo forKey:@"location"];
        [dict setObject:[thumb base64EncodedString] forKey:@"mthumb"];
    }
    else if (thumb != nil) {
        [dict setObject:[thumb base64EncodedString] forKey:@"mthumb"];
        NSString* mediaUrl = [self getPropertyWithName:@"media" parent:message];
        [dict setObject:mediaUrl forKey:@"url"];
        
        NSString* mediaSize = [self getPropertyWithName:@"media_size" parent:message];
        if (mediaSize != nil)
            [dict setObject:mediaSize forKey:@"media_size"];
    }
    
    NSString* chatid;
    NSString* phone;
    if ([self isGroupMessage:message]) {
        chatid = [[XMPPJID jidWithString:from] bare];
        phone = [self getPhoneNo:[[XMPPJID jidWithString:from] resource]];
    }
    else {
        chatid = [[XMPPJID jidWithString:from] user];
        phone = [self getPhoneNoFromJidString:from];
    }
    
    [dict setObject:chatid forKey:@"chatid"];
    [dict setObject:messageid forKey:@"messageid"];
    [dict setObject:phone forKey:@"phone"];
    [dict setObject:@"0" forKey:@"sread"];
    [dict setObject:@"0" forKey:@"sdelivered"];
    
    if (dateStr == nil)
        dateStr = [NSString stringWithFormat:@"%lu", (long)[[NSDate date] timeIntervalSince1970]];
    [dict setObject:dateStr forKey:@"date"];
    
    [ma addObject:dict];
    
    [data_dict setObject:ma forKey:@"new"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
        }
    });
}

- (void) processAddchat:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
//    NSString  *roomName = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"name"]];
    NSArray* phones = [dict objectForKey:@"abook"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    
    if (phones != nil && [phones count] == 1) {
        
        NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
        [data_dict setObject:[self getUserId:[phones objectAtIndex:0]] forKey:@"chatid"];
        [ret_dict setObject:data_dict forKey:@"data"];
    }
    else {
        
        phones = [dict objectForKey:@"abooks"];
        NSMutableArray* ma = [[NSMutableArray alloc] init];
        if (phones != nil && [phones count] > 0) {
            
            for (NSString* phone in phones) {
                [ma addObject:[phone stringByReplacingOccurrencesOfString:@"+" withString:@""]];
            }
            [ret_dict setObject:ma forKey:@"data"];
        }
        else {
            phones = [dict objectForKey:@"abookg"];
            BOOL isPublic = [[dict objectForKey:@"is_public"] boolValue];
            NSString* subject = [dict objectForKey:@"subject"];
            UIImage*  photo = [dict objectForKey:@"photo"];
            
            if (subject == nil || subject.length == 0)
                subject = NSLocalizedString(@"Group Chat", @"Group Chat");

            NSString *roomid = [self createMUCRoom:phones isPublic:isPublic photo:photo subject:subject];
            
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            
            [data_dict setObject:roomid forKey:@"chatid"];
            [data_dict setObject:subject forKey:@"room_name"];
            [data_dict setObject:phones forKey:@"phones"];
            
            NSMutableDictionary *ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            [ret_dict setObject:data_dict forKey:@"data"];
            
            createMucBlock = ^{
                if (photo != nil)
                    [self sendGroupChatAvatarMessage:roomid photo:photo room:[xmppRooms objectForKey:roomid]];
                
                if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                    [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
                }
            };
            
            return;
        }
    }
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
        }
    });
}

- (void) processChatin:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    NSString* chatid = [dict objectForKey:@"chatid"];
    XMPPRoom *room = [xmppRooms objectForKey:chatid];
    if (room == nil)
        return;
    
    NSString* roomname = [dict objectForKey:@"roomname"];
    
    for (NSString* phone in [dict objectForKey:@"abook"]) {
        XMPPJID* jid = [XMPPJID jidWithString:[self makeJid:phone]];
        [room inviteUser:jid withMessage:YABB_GROUP_CHAT];
    }

    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    [data_dict setObject:chatid forKey:@"chatid"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
        }
    });
}

- (void) processChatName:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    NSString* chatid = [dict objectForKey:@"chatid"];
    XMPPRoom *room = [xmppRooms objectForKey:chatid];
    if (room == nil)
        return;
    
    NSString* roomname = [[ChatStorage sharedStorage] decodeString:[dict objectForKey:@"name"]];
    [self changeRoomSubject:room subject:roomname];

    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    [data_dict setObject:chatid forKey:@"chatid"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
        }
    });
}

- (void) processChatOut:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    NSString* chatid = [dict objectForKey:@"chatid"];
    [self removeGroupChat:chatid];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    [data_dict setObject:chatid forKey:@"chatid"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
        }
    });
}

- (void) processDownloadMedia:(NSDictionary*)dict name:(NSString*)name delegate:(id)delegate {
    
    NSString* msgid = [dict objectForKey:@"mid"];
    NSDictionary* record = [[ChatStorage sharedStorage] getRecordByServerId:msgid];
    NSString* url = [record objectForKey:@"media_asset"];
    if (url == nil) {
        runOnMainQueueWithoutDeadlocking(^{
            if (delegate && [delegate respondsToSelector:@selector(didConnectionFailed:)]) {
                [delegate performSelector:@selector(didConnectionFailed:) withObject:name];
            }
        });
        return;
    }
    
    url = [[ChatStorage sharedStorage] decodeString:url];
    NSString* dest_name = [dict objectForKey:@"dest_name"];
    dest_name = [NSString stringWithFormat:@"%@.%@", dest_name ,[url pathExtension]];

    BOOL isVideo = NO;
    if ([dict objectForKey:@"isVideo"])
        isVideo = [[dict objectForKey:@"isVideo"] boolValue];
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"GET"];
    
    [request setFailedBlock:^{
        NSError* error = _request.error;
        NSLog(@"Httperror:%@%lu", error.localizedDescription,error.code);
        
        runOnMainQueueWithoutDeadlocking(^{
            if (delegate && [delegate respondsToSelector:@selector(didConnectionFailed:)]) {
                [delegate performSelector:@selector(didConnectionFailed:) withObject:name];
            }
        });
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 200) {
            NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            [data_dict setObject:dest_name forKey:@"dest_name"];
            [ret_dict setObject:data_dict forKey:@"data"];
            
            runOnMainQueueWithoutDeadlocking(^{
                if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                    [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:name];
                }
            });
        }
    }];
    
    if (isVideo) {
        [request setBytesReceivedBlock:^(unsigned long long size, unsigned long long total) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"download_progress_updated"
                                                                object:@{@"size":[NSNumber numberWithUnsignedLongLong:size],
                                                                         @"total":[NSNumber numberWithUnsignedLongLong:total]}];
        }];
    }
    
    NSString* path = [[ChatStorage sharedStorage] getMediaSavePath:dest_name];
    request.downloadDestinationPath = path;
    
    [request startAsynchronous];
    
    return;
}

- (void) sendMessage:(NSDictionary*) message {
    
    NSString* chat_id = [message objectForKey:@"room_id"];
    NSString* toId = [self makeJid:chat_id];
    NSString * local_id = [message objectForKey:@"local_id"];
    int type = [[message objectForKey:@"message_type"] intValue];
    NSString* msgText = [[ChatStorage sharedStorage] decodeString:[message objectForKey:@"original_message"]];
    NSString *messageID = local_id;
    NSString* media_name = [message objectForKey:@"media_name"];
    
    if (msgText == nil && media_name == nil)
        return;
    
    NSXMLElement *xmppMessage = [NSXMLElement elementWithName:@"message"];
    [xmppMessage addAttributeWithName:@"type" stringValue:@"chat"];
    [xmppMessage addAttributeWithName:@"from" stringValue:[currentUserJid full]];
    [xmppMessage addAttributeWithName:@"id" stringValue:messageID];
    NSString* dateStr = [NSString stringWithFormat:@"%lu", (long)[[NSDate date] timeIntervalSince1970]];
    [self makePropertyWithName:@"date" type:@"long" value:dateStr parent:xmppMessage];
    [xmppMessage addAttributeWithName:@"temp_id" stringValue:local_id];

    int messageTimeout = [[TimeBomb sharedInstance] getMessageTimeout:messageID];
    BOOL isPhotoWink = messageTimeout > 0;
    if (isPhotoWink) {
        [self makePropertyWithName:@"timebomb" type:@"string" value:[NSString stringWithFormat:@"%d", messageTimeout] parent:xmppMessage];
    }
    
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    if (type != 0 || msgText == nil) {
        //image
        [body setStringValue:msgText];
        
        if (type == 1 || type == 2) { //photo / video
            if (type == 2) { //video
                NSString* otherVideoName = [media_name stringByAppendingString:OUR_VIDEO_EXTENSION];
                if ([[ChatStorage sharedStorage] Media_Exist:otherVideoName]) {
                    media_name = otherVideoName;
                }
            }
            [self makePropertyWithName:@"media" type:@"string" value:[self getFileUrl:media_name] parent:xmppMessage];
            int fileSize = [[ChatStorage sharedStorage] Get_Media_Size:media_name];
            [self makePropertyWithName:@"media_size" type:@"string" value:[NSString stringWithFormat:@"%d", fileSize] parent:xmppMessage];
            if (!isPhotoWink) {
                [self makePropertyWithName:@"thumb" type:@"string" value:[self getImageThumbUrl:media_name] parent:xmppMessage];
            }
        }
        else if (type == 3) {
            [self makePropertyWithName:@"location" type:@"string" value:[message objectForKey:@"media_name"] parent:xmppMessage];
        }
    }
    else {
        //text
        [body setStringValue:msgText];
    }
    [xmppMessage addChild:body];
    
    XMPPRoom* room = [xmppRooms objectForKey:chat_id];
    
    if (room == nil) {
        [xmppMessage addAttributeWithName:@"to" stringValue:toId];
        [xmppStream sendElement:xmppMessage];
        [self notifyPosted:messageID tempId:local_id];
    }
    else {
        if ([room isJoined]) {
            [room sendMessage:[XMPPMessage messageFromElement:xmppMessage]];
            [self notifyPosted:messageID tempId:local_id];
        }
    }
}

- (NSString*) requestRandomMessageId {
    return [[xmppStream generateUUID] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

- (void) notifyPosted:(NSString*) messageId tempId:(NSString*)tempId {
    //in case of message sent
    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate == nil)
        return;
    
    NSString *messageID = messageId;
    NSString *local_id = tempId;
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    [dict setObject:local_id forKey:@"key"];
    [dict setObject:messageID forKey:@"messageid"];
    NSString* dateStr = [NSString stringWithFormat:@"%lu", (long)[[NSDate date] timeIntervalSince1970]];
    [dict setObject:dateStr forKey:@"date"];
    
    [ma addObject:dict];
    
    [data_dict setObject:ma forKey:@"posted"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
        }
    });
}

- (void)onSentMessage:(XMPPMessage*) message {

    if (![self isMessage:message] || [message isGroupChatMessageWithSubject]) {
        return;
    }
    
    //in case of message sent
    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate == nil)
        return;
    
    NSString *messageID = [[message attributeForName:@"id"] stringValue];
    NSString *local_id = [[message attributeForName:@"temp_id"] stringValue];
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    [dict setObject:local_id forKey:@"key"];
    [dict setObject:messageID forKey:@"messageid"];
    NSString* dateStr = [NSString stringWithFormat:@"%lu", (long)[[NSDate date] timeIntervalSince1970]];
    [dict setObject:dateStr forKey:@"date"];
    
    [ma addObject:dict];
    
    [data_dict setObject:ma forKey:@"posted"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
        }
    });
}

- (void)sendMessageEvent:(NSString *)message_id eventName:(NSString*)eventName {
    /*
     <message
     from='romeo@montague.net/orchard'
     to='juliet@capulet.com/balcony'>
     <x xmlns='jabber:x:event'>
     <displayed/>
     <id>message22</id>
     </x>
     </message>
     */
    
    NSDictionary* messageInfo = [[ChatStorage sharedStorage] getRecordByServerId:message_id];
    NSString* chatId = [messageInfo objectForKey:@"room_id"];
    if ([self isGroupChat:chatId])
        return;
    
	NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:event"];
    if (eventName != nil) {
        NSXMLElement *displayed = [NSXMLElement elementWithName:eventName];
        [x addChild:displayed];
    }
    [x addChild:[NSXMLElement elementWithName:@"id" stringValue:message_id]];
	
	NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    
    NSString* to = [self makeJid:[messageInfo objectForKey:@"phone"]];
	[message addAttributeWithName:@"to" stringValue:to];
    [message addAttributeWithName:@"from" stringValue:[currentUserJid full]];
	
	[message addChild:x];
	
	[xmppStream sendElement:message];
}

- (void) sendMessageDelivered:(NSString*) message_id {
    
    [self sendMessageEvent:message_id eventName:@"delivered"];
}

- (void) sendMessageSeen:(NSString*) message_id {
    
    [self sendMessageEvent:message_id eventName:@"displayed"];
}

- (void) sendMessageComposingStarted:(NSString*) message_id {
    
    [self sendMessageEvent:message_id eventName:@"composing"];
}

- (void) sendMessageComposingCanceled:(NSString*) message_id {
    
    [self sendMessageEvent:message_id eventName:nil];
}

- (void) processMessageEventNotification:(XMPPMessage*) message {
    
    NSXMLElement* elementX = [message elementForName:@"x"];
    NSXMLElement* elementError = [message elementForName:@"error"];
    if (elementError != nil)
        return;
    
    if (elementX != nil) {
        
        NSMutableDictionary* ret_dict = nil;
        id delegate = [hashCmdClass objectForKey:@"post"];
        if (delegate == nil)
            return;
        
        if ([elementX elementForName:@"displayed"] != nil ) {
            
            //displayed notification
            NSString* messageId = [[elementX elementForName:@"id"] stringValue];
            
            //in case of message sent
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            
            NSMutableArray* ma = [[NSMutableArray alloc] init];
            [ma addObject:messageId];
            
            [data_dict setObject:ma forKey:@"drs"];
            
            ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            [ret_dict setObject:data_dict forKey:@"data"];
            
        }
        else if ([elementX elementForName:@"delivered"] != nil ) {
            
            //displayed notification
            NSString* messageId = [[elementX elementForName:@"id"] stringValue];
            
            //in case of message sent
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            
            NSMutableArray* ma = [[NSMutableArray alloc] init];
            [ma addObject:messageId];
            
            [data_dict setObject:ma forKey:@"rcvs"];
            
            ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            [ret_dict setObject:data_dict forKey:@"data"];
            
        }
        else if ([elementX elementForName:@"composing"] != nil ) {
            //composing notification
            
            NSString* messageId = [[elementX elementForName:@"id"] stringValue];
            NSDictionary* record = [[ChatStorage sharedStorage] getRecordByServerId:messageId];
            if (record == nil)
                return;
            
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            
            NSMutableArray* ma = [[NSMutableArray alloc] init];
            
            [ma addObject:[NSDictionary dictionaryWithObject:[record objectForKey:@"room_id"] forKey:@"chatid"]];
            
            [data_dict setObject:ma forKey:@"typing"];
            
            ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            [ret_dict setObject:data_dict forKey:@"data"];
        }
        else {
            //user cancelled typing
            NSString* messageId = [[elementX elementForName:@"id"] stringValue];
            NSDictionary* record = [[ChatStorage sharedStorage] getRecordByServerId:messageId];
            if (record == nil)
                return;
            
            NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
            
            NSMutableArray* ma = [[NSMutableArray alloc] init];
            
            [ma addObject:[NSDictionary dictionaryWithObject:@"" forKey:@"chatId"]];
            
            [data_dict setObject:ma forKey:@"typing"];
            
            ret_dict = [NSMutableDictionary dictionary];
            [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
            [ret_dict setObject:@"" forKey:@"error"];
            [ret_dict setObject:data_dict forKey:@"data"];
        }
        
        if (ret_dict == nil)
            return;
        
        runOnMainQueueWithoutDeadlocking(^{
            
            if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
            }
        });
    }
}

- (NSUInteger)numberOfIdleTimeSecondsForXMPPLastActivity:(XMPPLastActivity *)sender queryIQ:(XMPPIQ *)iq currentIdleTimeSeconds:(NSUInteger)idleSeconds {

    return idleSeconds;
}

- (XMPPJID*) makeMUC_id:(NSArray*) phones isPublic:(BOOL) isPublic {
    
    
    NSString *random = [xmppStream generateUUID];
    random = [random stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString * roomid = nil;
    if (isPublic) {
        roomid = [NSString stringWithFormat:@"+%@-%@", [currentUserJid user], random];
    }
    else {
        roomid = [NSString stringWithFormat:@"-%@-%@", [currentUserJid user], random];
    }
    
    return [XMPPJID jidWithUser:roomid domain:GROUP_CHAT_SUFFIX resource:nil];
}

- (NSString*) createMUCRoom:(NSArray*) phones isPublic:(BOOL)isPublic photo:(UIImage*)photo subject:(NSString*)subject {
    
    XMPPJID* muc_jid = [self makeMUC_id:phones isPublic:isPublic];
    
    XMPPRoom* room = [[MyXMPPRoom alloc] initWithRoomStorage:self jid:muc_jid dispatchQueue:workingQueue];
    [room activate:xmppStream];
    
    [room addDelegate:self delegateQueue:workingQueue];
    
    NSXMLElement* config = nil;
    
    [room joinRoomUsingNickname:[currentUserJid user] history:nil];
    [self changeRoomSubject:room subject:subject];
    
    for (NSString* phone in phones) {
        
        NSString* jid = [self makeJid:phone];
        [room inviteUser:[XMPPJID jidWithString:jid] withMessage:YABB_GROUP_CHAT];
    }
    
    [xmppRooms setObject:room forKey:[muc_jid bare]];
    
    if (isPublic) {
        [room fetchConfigurationForm];
        
        config = [self makePublicGroupChatRoomSetting];
    }
    
    [room configureRoomUsingOptions:config];
    
    return [muc_jid bare];
}

- (NSString*) fetchRoomMemberList:(XMPPJID*) groupChatId {
/*
    <iq from='hag66@shakespeare.lit/pda'
    id='kl2fax27'
    to='coven@chat.shakespeare.lit'
    type='get'>
    <query xmlns='http://jabber.org/protocol/disco#items'/>
    </iq>
 */
    
    NSString *fetchID = [xmppStream generateUUID];
    
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" to:groupChatId elementID:fetchID child:query];
    
    [xmppStream sendElement:iq];
    
    return fetchID;
}

- (NSArray*) didReceivedMemberList:(XMPPIQ*) iq {
    
	NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    if (query == nil)
        return nil;
    
	NSArray *items = [query elementsForName:@"item"];
	
	NSMutableArray* jids = [[NSMutableArray alloc] initWithCapacity:[items count]];
	
	NSUInteger i;
	for(i = 0; i < [items count]; i++)
	{
		NSString *itemJidStr = [[[items objectAtIndex:i] attributeForName:@"jid"] stringValue];
        [jids addObject:itemJidStr];
	}
    
    return jids;
}

#pragma mark -
#pragma mark XMPPRoomStorage delegate

- (BOOL)configureWithParent:(XMPPRoom *)aParent queue:(dispatch_queue_t)queue {
    return YES;
}

- (void)handlePresence:(XMPPPresence *)presence room:(XMPPRoom *)room {
    NSLog(@"handlePresence");
}

- (void)handleIncomingMessage:(XMPPMessage *)message room:(XMPPRoom *)room {
    NSLog(@"handleIncomingMessage");
}

- (void)handleOutgoingMessage:(XMPPMessage *)message room:(XMPPRoom *)room {
    NSLog(@"handleOutgoingMessage");
}

- (void)handleDidLeaveRoom:(XMPPRoom *)room {
    NSLog(@"handleDidLeaveRoom");
}

#pragma mark -
#pragma mark XMPPRoomDelegate

- (void)xmppRoom:(XMPPRoom *)sender didConfigure:(XMPPIQ *)iqResult {
    NSLog(@"didConfigure - %@", [iqResult description]);
}

- (void)xmppRoom:(XMPPRoom *)sender didNotConfigure:(XMPPIQ *)iqResult {
    NSLog(@"didNotConfigure - %@", [iqResult description]);
}

- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
    
    XMPPLogInfo(@"xmppRoomDidCreate");
    if (createMucBlock)
        runOnMainQueueWithoutDeadlocking(createMucBlock);
    createMucBlock = nil;
}

- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
    
    XMPPLogInfo(@"xmppRoomDidJoin");
    if (createMucBlock != nil)
        runOnMainQueueWithoutDeadlocking(createMucBlock);
    createMucBlock = nil;
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchConfigurationForm:(NSXMLElement *)configForm {
 
    /*
    NSString *from = [[configForm attributeForName:@"from"] stringValue];
    if ([self isPublicGroupChat:from]) {
        NSXMLElement* config = [self makePublicGroupChatRoomSetting];
        [sender configureRoomUsingOptions:config];
        
        NSXMLElement *newConfig = [configForm copy];
        NSArray* fields = [newConfig elementsForName:@"field"];
        for (NSXMLElement *field in fields) {
            NSString *var = [field attributeStringValueForName:@"var"];
            if ([var isEqualToString:@"muc#roomconfig_allowinvites"]) {
                [field removeChildAtIndex:0];
                [field addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
            }
            if ([var isEqualToString:@"muc#roomconfig_changesubject"]) {
                [field removeChildAtIndex:0];
                [field addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
            }
        }
        [sender configureRoomUsingOptions:newConfig];
    }
     */
}

- (void)xmppRoomDidLeave:(XMPPRoom *)sender {
    
    XMPPLogInfo(@"xmppRoomDidLeave");
}

- (void)xmppRoomDidDestroy:(XMPPRoom *)sender {
    
    XMPPLogInfo(@"xmppRoomDidDestroy");
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
    XMPPLogInfo(@"occupantDidJoin");
    NSString* chat_id = [[sender roomJID] bare];
    NSString* user_id = [occupantJID resource];
    NSString* phone = [self getPhoneNo:user_id];
    
    if ([user_id isEqualToString:[currentUserJid user]])
        return;
    
    [[ChatStorage sharedStorage] appendNewPhoneNumberToGroupChat:chat_id phone:phone];
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
    XMPPLogInfo(@"occupantDidLeave");
    NSString* chat_id = [[sender roomJID] bare];
    NSString* user_id = [occupantJID resource];
    NSString* phone = [self getPhoneNo:user_id];
    
    if ([user_id isEqualToString:[currentUserJid user]])
        return;
    
    [[ChatStorage sharedStorage] removePhoneNumberFromGroupChat:chat_id phone:phone];
}

- (void)xmppRoom:(XMPPRoom *)sender occupantDidUpdate:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
    XMPPLogInfo(@"occupantDidUpdate - %@", [presence description]);
}

- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID {
    NSLog(@"didReceiveMessage");
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchMembersList:(NSArray *)items {
    NSLog(@"didFetchMembersList");
}

- (void)xmppRoom:(XMPPRoom *)sender didNotFetchMembersList:(XMPPIQ *)iqError {
    NSLog(@"didNotFetchMembersList = %@", [iqError description]);
}

- (void)xmppMUC:(XMPPMUC *)sender roomJID:(XMPPJID *) roomJID didReceiveInvitation:(XMPPMessage *)message {
    
    NSLog(@"didReceiveInvitation");
}

#pragma mark - XMPPAutoPingDelegate

- (void)xmppAutoPingDidSendPing:(XMPPAutoPing *)sender {
    
}

- (void)xmppAutoPingDidReceivePong:(XMPPAutoPing *)sender {
    
    if (!_isConnectedServer) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_CONNECTED object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
    }
    _isConnectedServer = YES;
}

- (void)xmppAutoPingDidTimeout:(XMPPAutoPing *)sender {
    
    if (_isConnectedServer) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_DISCONNECTED object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
    }
    _isConnectedServer = NO;
    
    NSString* ip = [self getIPAddress];
    if (ip == nil || [ip isEqualToString:@"error"]) {
        loggingState = LOG_STATE_RECONNECTING;
        [[NSNotificationCenter defaultCenter] postNotificationName:SERVER_STATE_RECONNECTING object:nil];
        
        [xmppStream disconnect];
        [xmppReconnect manualStart];
    }
}

#pragma mark - XMPPReconnectDelegate

- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkConnectionFlags)connectionFlags {
    
    XMPPLogInfo(@"didDetectAccidentalDisconnect");
}

- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkConnectionFlags)connectionFlags {

    XMPPLogInfo(@"shouldAttemptAutoReconnect");
    
    return YES;
}


#pragma mark -

- (void)xmppLastActivity:(XMPPLastActivity *)sender didReceiveResponse:(XMPPIQ *)response {
    
    XMPPLogInfo(@"didReceiveResponse - %@", [response description]);
    if ([response isErrorIQ])
        return;
    
    NSString* strType = response.type;
    if (![strType isEqualToString:@"result"])
        return;
    
    NSXMLElement* element = [response elementForName:@"query" xmlns:XMPPLastActivityNamespace];
    if (element == nil)
        return;
    
    XMPPJID* from = [response from];
    if (from == nil)
        return;
    
    NSString* seconds = [[element attributeForName:@"seconds"] stringValue];
    if (seconds == nil)
        return;
    
    NSTimeInterval seen = [[NSDate date] timeIntervalSince1970] - [seconds doubleValue];
    [[Friends SharedFriends] updateLastSeenByPhone:[self getPhoneNo:[from user]] idle:seen];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
}

- (void)xmppLastActivity:(XMPPLastActivity *)sender didNotReceiveResponse:(NSString *)queryID dueToTimeout:(NSTimeInterval)timeout {
    
    XMPPLogInfo(@"didNotReceiveResponse - %@", queryID);
}

#pragma mark -

- (void)xmppRosterDidBeginPopulating:(XMPPRoster *)sender {

    if (arrayNicknames == nil)
        arrayNicknames = [[NSMutableDictionary alloc] initWithCapacity:20];
    else
        [arrayNicknames removeAllObjects];
}

- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(NSXMLElement *)item {
    
    DDXMLNode *jidElement = [item attributeForName:@"jid"];
    if (jidElement == nil)
        return;
    
    NSString *jidStr = [jidElement stringValue];
    NSXMLElement* customElement = [item elementForName:@"custom"];
    if (customElement == nil)
        return;
    
    DDXMLNode *nickElement = [customElement attributeForName:@"nickname"];
    if (nickElement == nil)
        return;
    
    NSString* nickName = [nickElement stringValue];
    [arrayNicknames setObject:nickName forKey:jidStr];
}

- (void)xmppRosterDidPopulate:(XMPPRosterMemoryStorage *)sender {
    
    XMPPLogInfo(@"fetching roster completed!");
    NSDictionary* response = [self makeImportResponse];
    id delegate = [hashCmdClass objectForKey:@"import"];
    if (delegate != nil) {
        runOnMainQueueWithoutDeadlocking(^{
            if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
                [delegate performSelector:@selector(didConnectionDone:withName:) withObject:response withObject:nil];
            }
            
            if (login_delegate != nil && [login_delegate respondsToSelector:@selector(loginCompleted:needRegister:)])
                [login_delegate performSelector:@selector(loginCompleted:needRegister:) withObject:@YES withObject:@NO];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
        });
    }
    
    [arrayNicknames removeAllObjects];

    if (uploadAvatar != nil) {
        uploadAvatar();
        uploadAvatar = nil;
    }
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didAddUser:(XMPPUserMemoryStorageObject *)user {

    NSLog(@"didAddUser");
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didUpdateUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didUpdateUser");
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender didRemoveUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didRemoveUser");
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender
    didAddResource:(XMPPResourceMemoryStorageObject *)resource
          withUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didAddResource - %@", [resource.presence description]);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender
 didUpdateResource:(XMPPResourceMemoryStorageObject *)resource
          withUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didUpdateResource - %@", [resource.presence description]);
}

- (void)xmppRoster:(XMPPRosterMemoryStorage *)sender
 didRemoveResource:(XMPPResourceMemoryStorageObject *)resource
          withUser:(XMPPUserMemoryStorageObject *)user {
    
    NSLog(@"didRemoveResource - %@", [resource.presence description]);
}


#pragma mark -

- (BOOL)isGroupChatInvite:(XMPPMessage*) message
{
    NSXMLElement * x = [message elementForName:@"x" xmlns:XMPPMUCUserNamespace];
	NSXMLElement * invite  = [x elementForName:@"invite"];
    NSXMLElement * directInvite = [message elementForName:@"x" xmlns:@"jabber:x:conference"];
	
	if (invite || directInvite)
    {
		return YES;
    }
    else {
        return NO;
    }
    
}

- (NSString*) getGroupChatInviteReason:(XMPPMessage*) message {
    
    NSXMLElement * x = [message elementForName:@"x" xmlns:XMPPMUCUserNamespace];
	NSXMLElement * invite  = [x elementForName:@"invite"];
    
    if (invite)
        return [[invite elementForName:@"reason"] stringValue];
    
    return nil;
}

- (void) onGroupChatInvited:(NSString*)from {
    
    XMPPJID* muc_jid = [[XMPPJID jidWithString:from] bareJID];
    
    XMPPRoom* room = [[MyXMPPRoom alloc] initWithRoomStorage:self jid:muc_jid dispatchQueue:workingQueue];
    [room activate:xmppStream];
    [room addDelegate:self delegateQueue:workingQueue];
    
    [room configureRoomUsingOptions:nil];
    
    [room joinRoomUsingNickname:[currentUserJid user] history:nil];
    
    [self fetchRoomMemberList:muc_jid];//[room fetchMembersList];
    
    [xmppRooms setObject:room forKey:[muc_jid bare]];

    id delegate = [hashCmdClass objectForKey:@"post"];
    if (delegate == nil)
        return;
    
    NSMutableDictionary* data_dict = [NSMutableDictionary dictionary];
    
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    [dict setObject:@"0" forKey:@"private"];
    
    NSString* chatid = [muc_jid bare];
    [dict setObject:chatid forKey:@"chatid"];
    
    NSString* subject = room.roomSubject;
    if (subject == nil)
        subject = NSLocalizedString(@"Group Chat", @"Group Chat");
    [dict setObject:[[ChatStorage sharedStorage] encodeString:subject] forKey:@"name"];
    
    [dict setObject:[NSArray arrayWithObject:@""] forKey:@"phones"];
    
    [ma addObject:dict];
    
    [data_dict setObject:ma forKey:@"chats"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:[NSNumber numberWithInt:1] forKey:@"errorcode"];
    [ret_dict setObject:@"" forKey:@"error"];
    [ret_dict setObject:data_dict forKey:@"data"];
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret_dict withObject:@"lazy_chat_connection"];
        }
    });
}

- (BOOL) isMessage:(XMPPMessage*) message {
    
    if ([message isChatMessage])
        return YES;
    
    if ([message isChatMessageWithBody])
        return YES;
    
    if ([message isGroupChatMessage])
        return YES;
    
    if ([message isGroupChatMessageWithBody])
        return YES;
    
    return NO;
}

- (BOOL) isGroupMessage:(XMPPMessage*) message {
    
    if ([message isGroupChatMessage])
        return YES;
    
    if ([message isGroupChatMessageWithBody])
        return YES;
    
    return NO;
}

- (void) notifyEnteredChatBoard:(NSString*) chat_id {
    
    XMPPRoom* room = [xmppRooms objectForKey:chat_id];
    if (room == nil) {
        room = [[MyXMPPRoom alloc] initWithRoomStorage:self jid:[XMPPJID jidWithString:chat_id] dispatchQueue:workingQueue];
        [room activate:xmppStream];
        [room addDelegate:self delegateQueue:workingQueue];
        
        [room configureRoomUsingOptions:nil];
        
        [xmppRooms setObject:room forKey:chat_id];
    }
    
    if (![room isJoined]) {
        [room joinRoomUsingNickname:[currentUserJid user] history:nil];
    }
}

- (void) removeGroupChat:(NSString*) chat_id {
    
    XMPPRoom *room = [xmppRooms objectForKey:chat_id];
    if (room == nil)
        return;
    
    [room leaveRoom];
    [xmppRooms removeObjectForKey:chat_id];
}

- (BOOL) isGroupChat:(NSString*) chat_id {
    
    NSRange range = [chat_id rangeOfString:GROUP_CHAT_SUFFIX];
    if (range.location == NSNotFound || range.length == 0)
        return NO;
    
    return YES;
}

- (void) changeRoomSubject:(XMPPRoom*)room subject:(NSString*)subject {

/*
    <message
    from='coven@chat.shakespeare.lit/secondwitch'
    id='F437C672-D438-4BD3-9BFF-091050D32EE2'
    to='crone1@shakespeare.lit/desktop'
    type='groupchat'>
    <subject>Fire Burn and Cauldron Bubble!</subject>
    </message>
*/
    if (subject == nil || [subject isEqualToString:@""])
        return;
    
    NSXMLElement *subjectElement = [NSXMLElement elementWithName:@"subject"];
    [subjectElement setStringValue:subject];
    
    NSXMLElement *xmppMessage = [NSXMLElement elementWithName:@"message"];
    [xmppMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    [xmppMessage addAttributeWithName:@"to" stringValue:[[room roomJID] full]];
    [xmppMessage addAttributeWithName:@"id" stringValue:[xmppStream generateUUID]];
    [xmppMessage addChild:subjectElement];
    
    [room sendMessage:[XMPPMessage messageFromElement:xmppMessage]];
}

- (void) onGroupSubjectMessageReceived:(XMPPMessage*) message {
    
    NSString *subject = [[message elementForName:@"subject"] stringValue];
    if ([subject isEqualToString:@""])
        return;
    
    runOnMainQueueWithoutDeadlocking(^{
        NSString *from = [[message attributeForName:@"from"] stringValue];
        NSString* chat_id = [[XMPPJID jidWithString:from] bare];
        [[ChatStorage sharedStorage] renameRoom:chat_id withName:subject];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"chataid":chat_id} ];
    });
}

- (void) sendGetSessionTokenRequest {
    
	NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"urn:tmp:token"];
	
	NSString *uuid = [xmppStream generateUUID];
	XMPPIQ *iq = [XMPPIQ iqWithType:@"get" to:nil elementID:uuid child:query];
	
	[xmppStream sendElement:iq];
}

- (void) uploadFile:(NSData*) data thumb:(NSData*)thumb name:(NSString*)name target:(id)delegate local_id:(NSString*)local_id {

    NSMutableDictionary* dict;
    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:name, @"name", delegate, @"target", local_id, @"local_id", thumb, @"thumb", [NSNumber numberWithUnsignedLongLong:0], @"upload_size", nil];
    if (data != nil)
        [dict setObject:data forKey:@"data"];
    
    [uploadFileInfos addObject:dict];

    if ([uploadFileInfos count] == 1) {
        [self uploadFileProcess:data thumb:thumb name:name target:delegate local_id:local_id info:dict];
    }
}

- (void) uploadFileProcess:(NSData*) data thumb:(NSData*)thumb name:(NSString*)name target:(id)delegate local_id:(NSString*)local_id info:(NSDictionary*)info {
    
    if (data == nil) {
        if ([self isVideoAlreadyCompressed:name]) {
            NSData* data = [[ChatStorage sharedStorage] Load_Video_Data:name];
            [self uploadFileProcess:data thumb:thumb name:name target:delegate local_id:local_id info:info];
            return;
        }
        
        NSString* raw_name = name;
        
        name = [name stringByAppendingString:OUR_VIDEO_EXTENSION];
        NSString* videoPath = [[ChatStorage sharedStorage] getMediaSavePath:name];
        NSString* rawVideoPath = [[ChatStorage sharedStorage] getMediaSavePath:raw_name];
        
        NSURL *videoURL = [NSURL fileURLWithPath:rawVideoPath];
        NSURL *outputURL = [NSURL fileURLWithPath:videoPath];
        [self convertVideoToLowQuailtyWithInputURL:videoURL outputURL:outputURL handler:^(AVAssetExportSession *exportSession)
         {
             if (exportSession.status == AVAssetExportSessionStatusCompleted)
             {
                 NSData* data = [[ChatStorage sharedStorage] Load_Video_Data:name];
                 [self uploadFileProcess:data thumb:thumb name:name target:delegate local_id:local_id info:info];
             }
             else
             {
                 NSData* data = [[ChatStorage sharedStorage] Load_Video_Data:raw_name];
                 [self uploadFileProcess:data thumb:thumb name:raw_name target:delegate local_id:local_id info:info];
             }
         }];
        
        return;
    }
    
    NSString* url = [self getImageThumbUrl:name];
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"GET"];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:local_id forKey:@"local_id"];
    
    [request setFailedBlock:^{
        [self doUpload:data thumb:thumb name:name target:delegate local_id:local_id info:info];
    }];
    
    [request setCompletionBlock:^{
        
        if ([info objectForKey:@"cancelled"]) {
            [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
            return;
        }
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 200) {
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
        }
        else {
            [self doUpload:data thumb:thumb name:name target:delegate local_id:local_id info:info];
        }
    }];
    
    [request startAsynchronous];
}

- (NSString*) getRESTAuthorizationHeaderField {
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [currentUserJid user], sessionToken];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authValue = [authData base64Encoding];
    return [NSString stringWithFormat:@"Basic %@", authValue];
}

- (void) doUpload:(NSData*) data thumb:(NSData*)thumb name:(NSString*)name target:(id)delegate local_id:(NSString*)local_id info:(NSDictionary*)info {
    
    NSString * url = [self getFileUrl:name];
    
    NSMutableDictionary* ret_dict = [NSMutableDictionary dictionary];
    [ret_dict setObject:url forKey:@"media_url"];
    [ret_dict setObject:local_id forKey:@"local_id"];
    
    if ([info objectForKey:@"cancelled"]) {
        [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
        [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
        return;
    }
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"PUT"];
    [request setPostBody:[NSMutableData dataWithData:data]];
    
    [request setFailedBlock:^{
        NSError* error = [_request error];
        NSLog(@"Httperror:%@%lu", error.localizedDescription,error.code);
        [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
    }];
    
    [request setCompletionBlock:^{
        
        NSLog(@"CompletionBlock");
        if ([info objectForKey:@"cancelled"]) {
            [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
            return;
        }
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 201) {
            
            if ([info objectForKey:@"cancelled"]) {
                [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
                [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
                return;
            }
            
            if (thumb != nil) {
                [self uploadThumb:thumb name:name target:delegate local_id:local_id ret:ret_dict info:info];
            }
            else {
                [self sendUdidsPostForImageName:name target:delegate local_id:local_id ret:ret_dict info:info ];
            }
        }
        else {
            [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
        }
    }];
    
    [request setBytesSentBlock:^(unsigned long long size, unsigned long long total) {
        
        unsigned long long upload_size = [[JabberLayer SharedInstance] updateUploadingSize:local_id additional:size];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"upload_progress_updated"
                                                            object:@{@"progress":[NSNumber numberWithFloat:(float)upload_size/(float)total],
                                                                     @"local_id":local_id}];
    }];
    
    [self setRequestInfoToUploadArray:local_id request:request];
    [request startAsynchronous];
}

- (void) uploadThumb:(NSData*) thumb name:(NSString*)name target:(id)delegate local_id:(NSString*)local_id ret:(NSMutableDictionary*)ret_dict info:(NSDictionary*)info {
    
    if ([info objectForKey:@"cancelled"]) {
        [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
        [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
        return;
    }
    
    NSString * url = [self getImageThumbUrl:name];
    [ret_dict setObject:url forKey:@"thumb_url"];
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"PUT"];
    [request setPostBody:[NSMutableData dataWithData:thumb]];
    
    [request setFailedBlock:^{
        NSError* error = [_request error];
        NSLog(@"Httperror:%@%lu", error.localizedDescription,error.code);
        [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
    }];
    
    [request setCompletionBlock:^{
        if ([info objectForKey:@"cancelled"]) {
            [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
            return;
        }
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 201) {
            [self sendUdidsPostForImageName:name target:delegate local_id:local_id ret:ret_dict info:info ];
        }
        else {
            [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
        }
    }];
    
    [self setRequestInfoToUploadArray:local_id request:request];
    [request startAsynchronous];
}

- (void) sendUdidsPostForImageName:(NSString*)name target:(id)delegate local_id:(NSString*)local_id ret:ret_dict info:(NSDictionary*)info {

    NSString * url = [self getFileUrl:name];
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"POST"];
    
    NSArray* phones = [self getPhoneNumbersByMsgId:local_id];
    NSMutableString* body = [[NSMutableString alloc] init];
    [body appendString:@"udids="];
    
    for (NSString* phone_number in phones) {
        NSString* jid = [self makeJid:phone_number];
        [body appendString:jid];
        [body appendString:@","];
    }
    [body deleteCharactersInRange:NSMakeRange(body.length-1, 1)];

    [request setPostBody:[NSMutableData dataWithData:[body dataUsingEncoding:NSASCIIStringEncoding]]];
    
    [request setFailedBlock:^{
        NSError* error = [_request error];
        NSLog(@"Httperror:%@%lu", error.localizedDescription,error.code);
        [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
    }];
    
    [request setCompletionBlock:^{
        if ([info objectForKey:@"cancelled"]) {
            [ret_dict setObject:@"cancelled" forKey:@"cancelled"];
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
            return;
        }
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 200) {
            [self callMediaUploadingSuccessDelegate:delegate ret:ret_dict];
        }
        else {
            [self callMediaUploadingFailedDelegate:delegate ret:ret_dict];
        }
    }];
    
    [self setRequestInfoToUploadArray:local_id request:request];
    [request startAsynchronous];
}

- (NSArray*) getPhoneNumbersByMsgId:(NSString*) msgid {

    NSString* chatid = [[[ChatStorage sharedStorage] getRecordByServerId:msgid] objectForKey:@"room_id"];
    NSString* phones = [[ChatStorage sharedStorage] getRoomPhone:chatid];
    return  [phones componentsSeparatedByString:@":"];
}

- (void) uploadNextFileInfo {
    
    if ([uploadFileInfos count] <= 0)
        return;
    
    [uploadFileInfos removeObjectAtIndex:0];
    if ([uploadFileInfos count] <= 0)
        return;
    
    NSDictionary* info = [uploadFileInfos objectAtIndex:0];
    
    [self uploadFileProcess:[info objectForKey:@"data"] thumb:[info objectForKey:@"thumb"] name:[info objectForKey:@"name"]
                     target:[info objectForKey:@"target"] local_id:[info objectForKey:@"local_id"] info:info];
}

- (void) callMediaUploadingSuccessDelegate:(id)delegate ret:(NSDictionary*)ret {
    
    NSString* msgid = [ret objectForKey:@"local_id"];
    if (msgid != nil) {
        [self setRequestInfoToUploadArray:msgid request:nil];
    }
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionDone:withName:)]) {
            [delegate performSelector:@selector(didConnectionDone:withName:) withObject:ret withObject:@"upload_image"];
        }
        [self uploadNextFileInfo];
    });

}

- (void) callMediaUploadingFailedDelegate:(id)delegate ret:(NSDictionary*)ret {
    
    NSString* msgid = [ret objectForKey:@"local_id"];
    if (msgid != nil) {
        [self setRequestInfoToUploadArray:msgid request:nil];
    }
    
    runOnMainQueueWithoutDeadlocking(^{
        if (delegate && [delegate respondsToSelector:@selector(didConnectionFailed:)]) {
            NSString* local_id = [ret objectForKey:@"local_id"];
            [delegate performSelector:@selector(didConnectionFailed:) withObject:local_id];
        }
        [self uploadNextFileInfo];
    });
}

- (NSString*) getFileUrl:(NSString*)fileName {
    
    NSString * userid = [currentUserJid user];
    return [NSString stringWithFormat:@"%@/files/%@/%@", FILE_ENDPOINT, userid, fileName];
}

- (NSString*) getImageThumbUrl:(NSString*)fileName {
    
    NSString * userid = [currentUserJid user];
    return [NSString stringWithFormat:@"%@/thumbs/%@/%@", FILE_ENDPOINT, userid, fileName];
}

- (void) makePropertyWithName:(NSString*)name type:(NSString*)type value:(NSString*)value parent:(NSXMLElement*) parent {
    
    /*
     <message xmlns="jabber:client" from="8615643329200@yabb.com/Smack" to="8615526762316@yabb.com/764155651391699754289608" id="H14yp-12" type="chat”>
     <body>yggg</body>
     <thread>PILU10</thread>
     <x xmlns="jabber:x:event"><displayed/><composing/></x>
     
     <properties xmlns="http://www.jivesoftware.com/xmlns/xmpp/properties”>
     
     <property>
     <name>date</name>
     <value type="long">1391700154434</value>
     </property>
     
     </properties>
     
     </message>
     
     */
    
    NSXMLElement* properties = [parent elementForName:@"properties"];
    if (properties == nil) {
        properties = [NSXMLElement elementWithName:@"properties" xmlns:@"http://www.jivesoftware.com/xmlns/xmpp/properties"];
        [parent addChild:properties];
    }
    
    NSXMLElement* property = [NSXMLElement elementWithName:@"property"];
    
    NSXMLElement* e_name = [NSXMLElement elementWithName:@"name" stringValue:name];
    NSXMLElement* e_value = [NSXMLElement elementWithName:@"value" stringValue:value];
    [e_value addAttributeWithName:@"type" stringValue:type];
    [property addChild:e_name];
    [property addChild:e_value];
    
    [properties addChild:property];
}

- (NSString*) getPropertyWithName:(NSString*)name parent:(NSXMLElement*) parent {
    
    NSXMLElement* properties = [parent elementForName:@"properties"];
    if (properties == nil) {
        return nil;
    }
    
    NSArray* array = [properties elementsForName:@"property"];
    for (NSXMLElement* property in array) {
        if ([property elementForName:@"name"] == nil)
            continue;
        
        NSString* str = [[property elementForName:@"name"] stringValue];
        if ([str isEqualToString:name]) {
            if ([property elementForName:@"value"] == nil)
                continue;
            
            return [[property elementForName:@"value"] stringValue];
        }
    }
    
    return nil;
}

- (void) clear {
    
    [hashCmdClass removeAllObjects];
    [op_queue cancelAllOperations];
    [uploadFileInfos removeAllObjects];
}

- (void) cancelUpload:(NSString*)local_id {
    
    [[ChatStorage sharedStorage] setMessageStatusByLocalId:local_id status:MSG_STATUS_SEND_CANCELLED]; //mark cancelled
    
    NSMutableDictionary* finded = nil;
    for (NSMutableDictionary* d in uploadFileInfos) {
        NSString* lid = [d objectForKey:@"local_id"];
        if ([lid isEqualToString:local_id]) {
            finded = d;
            break;
        }
    }
    
    if (finded != nil) {
        [finded setObject:@"cancelled" forKey:@"cancelled"];
        [finded setObject:[NSNumber numberWithUnsignedLongLong:0] forKey:@"upload_size"];
        ASIHTTPRequest* request = [finded objectForKey:@"request"];
        if (request != nil)
            [request cancel];
    }
}

- (void) setRequestInfoToUploadArray:(NSString*) msgid request:(ASIHTTPRequest*)request {
    
    NSMutableDictionary* finded = nil;
    for (NSMutableDictionary* d in uploadFileInfos) {
        NSString* lid = [d objectForKey:@"local_id"];
        if ([lid isEqualToString:msgid]) {
            finded = d;
            break;
        }
    }
    
    if (finded != nil) {
        if (request != nil)
            [finded setObject:request forKey:@"request"];
        else
            [finded removeObjectForKey:@"request"];
    }
}

- (unsigned long long) updateUploadingSize:(NSString*) msgid additional:(unsigned long long) add_size {
    
    unsigned long long size = 0;
    NSMutableDictionary* finded = nil;
    for (NSMutableDictionary* d in uploadFileInfos) {
        NSString* lid = [d objectForKey:@"local_id"];
        if ([lid isEqualToString:msgid]) {
            finded = d;
            break;
        }
    }
    
    if (finded != nil) {
        size = [[finded objectForKey:@"upload_size"] unsignedLongLongValue];
        size += add_size;
        [finded setObject:[NSNumber numberWithUnsignedLongLong:size] forKey:@"upload_size"];
    }
    
    return size;
}

- (NSString*) getMyPhoneNo {
    
    if (currentUserJid == nil)
        return @"";
    
    return [self getPhoneNo:currentUserJid.user];
}

- (void) updateAvatar:(UIImage *)avatar nickname:(NSString*)nickname {

    [[AvatarManager sharedInstance] setMyAvatar:avatar hash:nil];

    [self doUpdateAvatar:avatar removeAvatar:avatar==nil nickname:nickname];
}

- (void) doUpdateAvatar:(UIImage *)avatar removeAvatar:(BOOL) isRemoveAvatar nickname:(NSString*)nickname {
    
    [[AvatarManager sharedInstance] setMyAvatar:avatar hash:nil];
    
    XMPPvCardTemp * vcard = [XMPPvCardTemp vCardTemp];
    if (avatar != nil) {
        [vcard setPhoto:UIImageJPEGRepresentation(avatar, 0.8)];
    }
    else {
        if (isRemoveAvatar) {
            NSXMLElement *photo = [vcard elementForName:@"PHOTO"];
            if(photo)
                [vcard removeChildAtIndex:[[vcard children] indexOfObject:photo]];
            
            photo = [NSXMLElement elementWithName:@"PHOTO"];
            [vcard addChild:photo];
            
            avatar = nil;
        }
    }
    
    if (nickname != nil) {
        [vcard setNickname:nickname];
    }
    
    [xmppvCardAvatarModule.xmppvCardTempModule updateMyvCardTemp:vcard];
}

- (void) updateAvatarAfter:(UIImage *)avatar nickname:(NSString*)nickname {
    
    __weak JabberLayer* weakSelf = self;
    uploadAvatar = ^{
        NSLog(@"updateAvatar - after - %@, %@", avatar, nickname);
        [weakSelf doUpdateAvatar:avatar removeAvatar:NO nickname:nickname];
    };
}

#pragma mark - XMPPvCardAvatarDelegate

- (void)xmppvCardAvatarModule:(XMPPvCardAvatarModule *)vCardTempModule
              didReceivePhoto:(UIImage *)photo
                       forJID:(XMPPJID *)jid {
    
    NSLog(@"received avatar module from %@", [jid description]);
    
    if (![self isActive])
        return;
    
    if ([[jid user] isEqualToString:[currentUserJid user]]) {
        [[AvatarManager sharedInstance] setMyAvatar:photo hash:nil];
    }
    else {
        NSString* phone = [self getPhoneNoFromJidString:[jid full]];
        [[AvatarManager sharedInstance] setEjabberdAvatar:photo hash:nil phone:phone];
    }
}

- (NSString*) getRoomIdFromJID:(NSString*) chatId {

    XMPPJID* jid = [XMPPJID jidWithString:chatId];
    if ([self isGroupChat:chatId]) {
        return [jid bare];
    }
    else {
        return [jid user];
    }
}

#include <ifaddrs.h>
#include <arpa/inet.h>

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

- (NSString*) getServerChatID:(NSString*) room_id phone:(NSString*) phone {

    if ([self isGroupChat:room_id]) {
        return [NSString stringWithFormat:@"%@/%@", room_id, [phone stringByReplacingOccurrencesOfString:@"+" withString:@""]];
    }
    
    return [self makeJid:phone];
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"reset"]) {
        NSLog(@"*** Connection of Reset is OK ***");
        id delegate = [hashCmdClass objectForKey:@"reset"];
        if (delegate != nil) {
            [self processReset:dict name:connection_name delegate:delegate];
        }
    }
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"reset"]) {
        NSLog(@"*** Connection of Reset is Failed ***");
        id delegate = [hashCmdClass objectForKey:@"reset"];
        if (delegate != nil) {
            runOnMainQueueWithoutDeadlocking(^{
                if ([delegate respondsToSelector:@selector(didConnectionFailed:)]) {
                    [delegate performSelector:@selector(didConnectionFailed:) withObject:connection_name];
                }
            });
            return;
        }
    }
}

- (NSString*) getMyNickName {
    
    XMPPvCardTemp* vcard =[[XMPPvCardCoreDataStorage sharedInstance] myvCardTempForXMPPStream:xmppStream];
    
    if (vcard == nil)
        return nil;
    
    return vcard.nickname;
}

- (NSString*) getNickNameByPhone:(NSString*) phoneNo {
    
    NSString* jidString = [self makeJid:phoneNo];
    
    XMPPvCardTemp* vcard =[[XMPPvCardCoreDataStorage sharedInstance] vCardTempForJID:[XMPPJID jidWithString:jidString] xmppStream:xmppStream];

    return vcard.nickname;
}

- (NSXMLElement*) makePublicGroupChatRoomSetting {

//     <x xmlns='jabber:x:data' type='submit'>
//       <field var='FORM_TYPE'>
//         <value>http://jabber.org/protocol/muc#roomconfig</value>
//       </field>
//       <field var='muc#roomconfig_roomname'>
//         <value>A Dark Cave</value>
//       </field>
//       <field var='muc#roomconfig_enablelogging'>
//         <value>0</value>
//       </field>
//       ...
//     </x>
    
	NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"jabber:x:data"];
    
    {
        NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
        [field addAttributeWithName:@"var" stringValue:@"FORM_TYPE"];
        
        NSXMLElement * value = [NSXMLElement elementWithName:@"value" stringValue:@"http://jabber.org/protocol/muc#roomconfig"];
        [field addChild:value];
        
        [x addChild:field];
    }
    
    {
        NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
        [field addAttributeWithName:@"var" stringValue:@"muc#roomconfig_allowinvites"];
        
        NSXMLElement * value = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
        [field addChild:value];
        
        [x addChild:field];
    }
    
    {
        NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
        [field addAttributeWithName:@"var" stringValue:@"muc#roomconfig_changesubject"];
        
        NSXMLElement * value = [NSXMLElement elementWithName:@"value" stringValue:@"1"];
        [field addChild:value];
        
        [x addChild:field];
    }
    
    return x;
}

- (NSString*) getKindStringOfGroupChat:(NSString*)chatid {
    
    if (![self isGroupChat:chatid])
        return nil;
    
    if ([chatid characterAtIndex:0] == '+') {
        return NSLocalizedString(@"Public", @"Public");
    }
    
    if ([chatid characterAtIndex:0] == '-') {
        return NSLocalizedString(@"Private", @"Private");
    }
    
    return nil;
}

- (NSString*) getAdminOfGroupChat:(NSString*) chatid {
    
    if (![self isGroupChat:chatid])
        return nil;
    
    if ([chatid characterAtIndex:0] == '+') {
        return nil;
    }

    NSString *temp = [chatid substringWithRange:NSMakeRange(1, [chatid length]-1)];
    
    NSRange range = [temp rangeOfString:@"-"];
    if (range.length == 0)
        return nil;
    
    return [temp substringToIndex:range.location];
}

- (void) processOnline:(XMPPElement*)element {

    if ([self isLoggingProgress] || [self isReconnecting])
        return;
    
    NSString *from = [[element attributeForName:@"from"] stringValue];
    if ([self isGroupChat:from])
        return;
    
    NSString* resource = [[XMPPJID jidWithString:from] resource];
    NSString* userid = [[XMPPJID jidWithString:from] user];
    NSString* phone = [@"+" stringByAppendingString:userid];
    
    if (resource == nil || resource.length == 0) {
        
        //offline user
    }
    else {
        
        //online user
        BOOL changed = [[Friends SharedFriends] updateOnlineWithPhone:phone online:YES];
        if (changed) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
        }
    }
}

- (BOOL) isLoggingProgress {
    
    return loggingState == LOG_STATE_CONNECTING || loggingState == LOG_STATE_LOGGING;
}

- (BOOL) isReconnecting {
    
    return loggingState == LOG_STATE_RECONNECTING;
}

- (BOOL) isLoggingInitState {
    
    return loggingState == LOG_STATE_NONE;
}

- (void) sendGroupChatAvatarMessage:(NSString*)chatid photo:(UIImage*)image room:(XMPPRoom*) room{

    [[AvatarManager sharedInstance] setEjabberdAvatar:image hash:nil phone:chatid];

    NSData* thumb = UIImageJPEGRepresentation(image, 0.85);
    
    NSString* image_name = [[XMPPJID jidWithString:chatid] user];
    NSString * url = [self getImageThumbUrl:image_name];
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"PUT"];
    [request setPostBody:[NSMutableData dataWithData:thumb]];
    
    [request setFailedBlock:^{
        NSLog(@"failed of uploading group chatting avatar!");
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 201) {
            ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
            __weak ASIHTTPRequest *_request = request;
            
            [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
            
            [request setRequestMethod:@"POST"];
            
            NSMutableString* body = [[NSMutableString alloc] init];
            [body appendString:@"udids="];
            [body appendString:chatid];
            
            [request setPostBody:[NSMutableData dataWithData:[body dataUsingEncoding:NSASCIIStringEncoding]]];
            
            [request setFailedBlock:^{
                NSLog(@"failed of sending permission info of uploading group chatting avatar!");
            }];
            
            [request setCompletionBlock:^{
                
                NSInteger responseCode = [_request responseStatusCode];
                if (responseCode == 200) {
                    //success
                    NSXMLElement *xmppMessage = [NSXMLElement elementWithName:@"message"];
                    
                    [xmppMessage addAttributeWithName:@"id" stringValue:[xmppStream generateUUID ]];
                    [self makePropertyWithName:@"group_chat_avatar" type:@"string" value:url parent:xmppMessage];

                    NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:@""];
                    [xmppMessage addChild:body];
                    
                    [room sendMessage:[XMPPMessage messageFromElement:xmppMessage]];
                }
                else {
                    NSLog(@"failed of sending permission info of uploading group chatting avatar!");
                }
            }];
            
            [request startAsynchronous];
        }
        else {

        }
    }];
    
    [request startAsynchronous];
}

- (NSString*) getGroupChatAvatarUrl:(XMPPMessage*) message {
    
    NSString* avatarUrl = [self getPropertyWithName:@"group_chat_avatar" parent:message];
    if (avatarUrl == nil || avatarUrl.length == 0)
        return nil;
    
    return avatarUrl;
}

- (void) downloadGroupChatAvatar:(NSString*) url from:(NSString*)from {
    
    ASIHTTPRequest* request = [[ASIHTTPRequest alloc] initWithURL:[ NSURL URLWithString:url ]];
    __weak ASIHTTPRequest *_request = request;
    
    [request addRequestHeader:@"Authorization" value:[self getRESTAuthorizationHeaderField]];
    
    [request setRequestMethod:@"GET"];
    
    [request setFailedBlock:^{
        NSLog(@"failed of downloading the group chat avatar");
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger responseCode = [_request responseStatusCode];
        if (responseCode == 200) {
            UIImage* image = [UIImage imageWithData:_request.responseData];
            [[AvatarManager sharedInstance] setEjabberdAvatar:image hash:nil phone:from];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_friends" object:nil];
        }
    }];
    
    [request startAsynchronous];
    
    return;
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(AVAssetExportSession*))handler
{
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetLowQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         handler(exportSession);
     }];
}

- (BOOL) isVideoAlreadyCompressed:(NSString*)name {
    
    NSArray* array = [name componentsSeparatedByString:@"."];
    if (array == nil || [array count] <= 2)
        return NO;
    
    return YES;
}

- (NSString*) getRawVideoName:(NSString*)name {
    
    NSArray* array = [name componentsSeparatedByString:@"."];
    if (array == nil || [array count] <= 2)
        return name;

    if (![name hasSuffix:OUR_VIDEO_EXTENSION])
        return name;
    
    return [name substringToIndex:[name length]-[OUR_VIDEO_EXTENSION length]];
}

@end

