//
//  ChatNavController.h
//  speedsip
//
//  Created by Anatoly T on 14.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

#import "ChatBoardViewController.h"
#import "CreateChatRoomViewController.h"
#import "PeopleMultiselectorViewController.h"

@interface ChatRoomsNavController : UINavigationController <UITableViewDataSource, UITableViewDelegate, CreateChatDelegate, MultiselectorDelegate> {
    
    ChatBoardViewController * _chat_board;
    
    UITableView * _chat_rooms;

    NSMutableString * room_name;
    NSMutableArray * room_phones;
    
    NSArray * chat_room_list;
    BOOL table_editing;
    
    IBOutlet UINavigationItem * top_navigation;
    
    IBOutlet UIBarButtonItem * edit_button;
    IBOutlet UIBarButtonItem * write_button;
}

@property (nonatomic, assign) ChatBoardViewController * chat_board;
@property (nonatomic, assign) IBOutlet UITableView * chat_rooms;

- (void) Move_To_Chat_Board:(NSDictionary *)room_dict showKeyboard:(BOOL)keyboard;
- (void) importRooms;
- (void) UpdateBadgeNumbers;
- (void) Send_Private_Message:(ABRecordRef)person;

@end
