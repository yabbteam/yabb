//
//  RecipientView.m
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import "RecipientView.h"
#import <QuartzCore/QuartzCore.h>
#import "CommonUtil.h"

@implementation RecipientView

@synthesize selected = _selected;
@synthesize recipient_delegate = _recipient_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _selected = NO;
    }
    return self;
}


- (int) setText:(NSString *) text atPosition:(CGPoint)position {
    
    UIFont * font = [UIFont systemFontOfSize:15.0];

    UIImage * img = [[UIImage imageNamed:@"recipientBubble.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:12];

    CGSize size = [CommonUtil getTextSize:text font:font constrainedToSize:CGSizeMake(FLT_MAX, self.frame.size.height) lineBreakMode:UILineBreakModeWordWrap];

    CGRect rect = self.frame;
    
    rect.origin.x = position.x;
    rect.origin.y = position.y;
    rect.size.width = size.width + 16;
    rect.size.height = img.size.height;
    
    if (rect.size.width > 264)
        rect.size.width = 264;
    
    [self setFrame:rect];
    
    // Back Image
    UIImageView * back = (UIImageView *)[self viewWithTag:10];
    if (back == nil) {
        UIImageView * back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, img.size.height)];
        [back setTag:10];
        [self addSubview:back];
        [back setImage:img];
        [back setUserInteractionEnabled:NO];
        
    }
        
    // Text label
    UILabel * label = (UILabel *)[self viewWithTag:20];
    if (label == nil) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width - 10, self.frame.size.height)];
        [label setFont:font];
        [label setTextColor:[UIColor blackColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:label];
        [label setTag:20];
        [label setText:text];
        [label setUserInteractionEnabled:NO];
        
    }
    
    return self.frame.size.width;
}

- (void) didActive {
    
    _selected = !_selected;
    
    UIImageView * back = (UIImageView *)[self viewWithTag:10];
    UILabel * label = (UILabel *)[self viewWithTag:20];
    if (back == nil || label == nil)
        return;
    
    if (_selected) {
        
        [back setImage:nil];
        [self setBackgroundColor:[UIColor colorWithRed:0.5 green:0.5 blue:1.0 alpha:1.0]];
        
        [label setTextColor:[UIColor whiteColor]];
        
        [[self layer] setMasksToBounds:YES];
        [[self layer] setCornerRadius:12.0];
        
    }
    else {
        [self setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor blackColor]];
        
        [back setImage:[[UIImage imageNamed:@"recipientBubble.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:12] ];
    }
    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_recipient_delegate && [_recipient_delegate respondsToSelector:@selector(didTouch)])
        [_recipient_delegate didTouch];
    
    [self didActive];
    
}

@end
