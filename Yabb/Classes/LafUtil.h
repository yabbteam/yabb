//
//  LafUtil.h
//  Yabb
//
//  Created by denebtech on 10/16/13.
//
//

#import <Foundation/Foundation.h>

@interface UITableView (Laf)

- (void)applyDefaultAppearance;

@end

@interface UINavigationItem (Laf)

- (void)applyDefaultAppearance;

@end

@interface LafUtil : NSObject

@end
