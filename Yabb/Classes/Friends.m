//
//  Friends.m
//  Baycall
//
//  Created by denebtech on 23.09.12.
//
//

#import "Friends.h"
#import "ChatStorage.h"
#import "AddressCollector.h"
#import "AppDelegate.h"
#import "JabberLayer.h"
#import "AvatarManager.h"

@implementation Friends

static Friends * friends = nil;

+ (Friends *) SharedFriends {
    
    if (friends == nil) {
        
        friends = [Friends alloc];
        friends->all_friends = [[NSMutableArray alloc] init];
        //friends->all_friends = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"all_friends"]];
    }
    
    return friends;
}

- (void) save {
    
//    [[NSUserDefaults standardUserDefaults] setObject:all_friends forKey:@"all_friends"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (int) numberOfFriends {
    
    if (friends->all_friends)
        return [friends->all_friends count];
    return 0;
}

- (NSMutableDictionary *) friendWithPhone:(NSString *)phone {
    
    for (NSMutableDictionary * user in all_friends) {
        
        NSString * friend_phone = [user objectForKey:@"phone"];
        
        if (friend_phone == nil || [friend_phone isEqualToString:@""])
            continue;
        
        if ([friend_phone isEqualToString:phone])
            return user;
    }
    
    return nil;
}

- (BOOL) isFriendPhone:(NSString *)phone {
    
    for (NSDictionary * friend in all_friends) {

        NSString * friend_phone = [friend objectForKey:@"phone"];
        if (friend_phone && [friend_phone isEqualToString:phone])
            return YES;
    }
    
    return NO;
}

- (BOOL) isFriendIndex:(int)index {
    
    NSArray * phones = [[AddressCollector sharedCollector] Get_Phones_By_Index:index];
    if (phones == nil)
        return NO;
    
    for (NSString * phone in phones)
        if ([self isFriendPhone:phone])
            return YES;
    
    return NO;
}

- (NSString *) friendPhoneByIndex:(int)index {

    NSArray * phones = [[AddressCollector sharedCollector] Get_Phones_By_Index:index];
    if (phones == nil)
        return NO;
    
    for (NSString * phone in phones)
        if ([self isFriendPhone:phone])
            return phone;
    
    return nil;
}

- (BOOL) isPersonOnlineIndex:(int)index {
    
    if (![[JabberLayer SharedInstance] isConnectedServer])
        return NO;
    
    for (NSDictionary * friend in all_friends) {
        
        int friend_index = [[friend objectForKey:@"index"] intValue];
        BOOL online = [[friend objectForKey:@"online"] boolValue];
        
        if (friend_index == index && online)
            return YES;
        
    }
    
    return NO;
}

- (void) setOnline:(NSArray *)array {
    
    [all_friends removeAllObjects];
    
    [self updateOnline:array];
}

- (void)updateOnline:(NSArray *)array
{
    
    if (array == nil || [array count] == 0)
        return;
    
    BOOL needRemake = NO;
    
    for (NSMutableDictionary * user in array) {
        
        NSString * phone = [user objectForKey:@"phone"];
        if (phone == nil)
            continue;
        
        int index = [[AddressCollector sharedCollector] Get_Index_By_Phone:phone];
        
        NSMutableDictionary * current_user = [self friendWithPhone:phone];
        if (current_user == nil) {
            current_user = [NSMutableDictionary dictionary];
            [all_friends addObject:current_user];
            needRemake = YES;
        }
        
        NSString * origin = [user objectForKey:@"origin"];
        
        [current_user setObject:[user objectForKey:@"online"] forKey:@"online"];
        if ([user objectForKey:@"lstdate"] != nil)
            [current_user setObject:[user objectForKey:@"lstdate"] forKey:@"lastSeenAt"];
        [current_user setObject:origin forKey:@"origin"];
        [current_user setObject:phone forKey:@"phone"];
        [current_user setObject:[NSNumber numberWithInt:index] forKey:@"index"];
        if ([user objectForKey:@"name"] != nil) {
            [current_user setObject:[user objectForKey:@"name"] forKey:@"name"];
        }
        
        if ([user objectForKey:@"photo"] != nil) {
            [[AvatarManager sharedInstance] setEjabberdAvatar:[user objectForKey:@"photo"] hash:nil phone:phone];
        }
        
        if ([user objectForKey:@"empty_photo"] != nil) {
            [[AvatarManager sharedInstance] setEjabberdAvatar:nil hash:nil phone:phone];
        }
    }
    
    [self save];
    
    if (needRemake)
        [[AddressCollector sharedCollector] remakeAllPeopleByFriends:all_friends];
}

- (NSString *) originByPhone:(NSString *) phone withDefault:(BOOL)def {
    
    NSString * def_str = def?DEFAULT_ORIGIN:@"";
    
    if (phone == nil || [phone isEqualToString:@""])
        return def_str;
    
    for (NSMutableDictionary * user in all_friends) {
        
        NSString * friend_phone = [user objectForKey:@"phone"];
        if (friend_phone && phone && [friend_phone isEqualToString:phone])
            return ([user objectForKey:@"origin"]?[user objectForKey:@"origin"]:def_str);
    }
    
    return def_str;
}

- (NSString *) originByIndex:(int)index {
    
    return [self originByPhone:[self friendPhoneByIndex:index] withDefault:YES];
}

- (NSString *)nameByPhone:(NSString *)phone {
    
    if (phone == nil)
        return nil;
    
    NSDictionary * dict = [self friendWithPhone:phone];
    if (dict == nil)
        return phone;
    
    int index = [[dict objectForKey:@"index"] intValue];
    return [[AddressCollector sharedCollector] Get_Name_By_Index:index];
    
}

- (void) updateLastSeenByPhone:(NSString*)phone idle:(long long) idleTime {
    
    NSMutableDictionary * current_user = [self friendWithPhone:phone];
    if (current_user == nil)
        return;
    
    [current_user setObject:[NSNumber numberWithLongLong:idleTime] forKey:@"lastSeenAt"];
}

- (void) updatePhonebookRelation {

    for (NSMutableDictionary * user in all_friends) {
        NSString* phone = [user objectForKey:@"phone"];
        int index = [[AddressCollector sharedCollector] Get_Index_By_Phone:phone];
        [user setObject:[NSNumber numberWithInt:index] forKey:@"index"];
    }
}

- (BOOL) updateOnlineWithPhone:(NSString*)phone online:(BOOL)isOnline {
    
    NSMutableDictionary * current_user = [self friendWithPhone:phone];
    if (current_user == nil)
        return NO;
    
    NSString* rawValue = [current_user objectForKey:@"online"];
    if (rawValue == nil || [rawValue intValue] != isOnline) {
        [current_user setObject:isOnline?@"1":@"0" forKey:@"online"];
        return YES;
    }
    
    return NO;
}

@end
