//
//  CreateGroupChatViewController.h
//  Yabb
//
//  Created by denebtech on 5/15/14.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "VPImageCropperViewController.h"

@protocol CreateGroupChatDelegate <NSObject>

@required
- (void) didCreateNewRoom:(NSString *)room_subject withRoomPhoto:(UIImage*) room_photo isPublic:(BOOL)isPublic;

@end

@interface CreateGroupChatViewController : BaseViewController<UIActionSheetDelegate, VPImageCropperDelegate> {
    
    IBOutlet    UIImageView*    avatarView;
    IBOutlet    UISegmentedControl* segRoomKind;
    IBOutlet    UITextField     *textSubject;
    
    __strong UIImage     *selectedPhoto;
}

@property (nonatomic, strong) id<CreateGroupChatDelegate> delegate;

@end


