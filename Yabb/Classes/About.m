//
//  About.m
//  speedsip
//
//  Created by denebtech on 20/01/11.
//  Modified by denebtech, 10.aug.11
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MFMailComposeViewController.h>
#import <FontasticIcons.h>
#import <FIFontAwesomeIcon.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

#import "About.h"
#import "ChatStorage.h"
#import "AppDelegate.h"
#import "ConnectionQueue.h"
#import "Settings.h"
#import "AboutViewController.h"
#import "FBWebDialogs.h"
#import "InvitedFriends.h"
#import "SettingsViewController.h"
#import "AddressCollector.h"
#import "ServerStateView.h"
#import "PhotoEditViewController.h"

@interface About ()
{
    UIColor* _colorRawNavigationBar;
}

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation About


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self setTitle:NSLocalizedString(@"More", @"More")];
    //[self.view setBackgroundColor:DEFAULT_BACK_COLOR];
    
    [self.view addSubview:self.tableView];
    [self.tableView setSeparatorColor:[UIColor lightGrayColor]];
    
    
    /*
    UITextView *textField = [UITextView initWithFrame:CGRectMake(10, 200, 300, 40)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:15];
    textField.placeholder = @"enter text";
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;    
    //textField.delegate = self;
    [self.view addSubview:textField];
    [textField release];
     */
    
    ServerStateView* serverStateView = [[ServerStateView alloc] init];
    UIBarButtonItem *serveState = [[UIBarButtonItem alloc] initWithCustomView:serverStateView];
    NSArray* arrayItem = [NSArray arrayWithObjects:serveState, nil];
    
    [self.navigationItem setRightBarButtonItems:arrayItem];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

/////-----------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
#if APP_TARGET_Yabb_Dialer
    if (indexPath.row == 4)
        return 66;
    
    return 44;
#endif

    return 44;
}


// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
#if APP_TARGET_Yabb_Dialer
    return 7;
#endif

    return 7;

}



// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *path = nil;
    NSString *CellIdentifier = [ NSString stringWithFormat: @"%d:%d", indexPath.row, indexPath.section];
    UITableViewCell *cell = [ tableView dequeueReusableCellWithIdentifier: CellIdentifier];
	
	if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    
        
        int row = indexPath.row;

#if APP_TARGET_Yabb
        if (row >= 4) {
            row++;
        }
#endif

        switch (row) {
            case 0:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon questionSignIcon] imageWithBounds:CGRectMake(0, 0, 32, 32) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"About", @"About");
            }
                break;
                
            case 1:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon groupIcon] imageWithBounds:CGRectMake(0, 0, 26, 26) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"Invite by email", @"Invite by email");
            }
            break;
    
            case 2:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon groupIcon] imageWithBounds:CGRectMake(0, 0, 26, 26) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"Invite by SMS", @"Invite by SMS");
            }
            break;
                
            case 3:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon facebookSignIcon] imageWithBounds:CGRectMake(0, 0, 32, 32) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"Share on Facebook", @"Share on Facebook");
            }
            break;
                
            case 4:
            {
                [[cell imageView] setImage:[UIImage imageNamed:@"more_recharge.png"]];
                cell.textLabel.text = NSLocalizedString(@"Recharge", @"Recharge");
            }
            break;

            case 5:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon cogIcon] imageWithBounds:CGRectMake(0, 0, 32, 32) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"Settings", @"Settings");
            }
            break;

            case 6:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon userIcon] imageWithBounds:CGRectMake(0, 0, 32, 32) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"My Profile", @"My Profile");
            }
                break;
                
            case 7:
            {
                [[cell imageView] setImage:[[FIFontAwesomeIcon removeCircleIcon] imageWithBounds:CGRectMake(0, 0, 32, 32) color:[UIColor blackColor]]];
                cell.textLabel.text = NSLocalizedString(@"Deactivate", @"Deactivate");
            }
            break;

                /*
                 case 5:
                 {
                 path = @"more_about.png";
                 cell.textLabel.text = @"Log-out";
                 }
                 break;*/
                
            default:
                break;
        }
	}
    
	return cell;
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (void) Invite_Facebook {
    
   NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"243465279150608", @"app_id", @"Yabb Messenger", @"name", NSLocalizedString(@"Invitation to Yabb Messenger", @"Invitation to Yabb Messenger"), @"caption", NSLocalizedString(@"I just downloaded Yabb Messenger. It is a new way to chat with your friends for free. Yabb is free to download for phone and Android at http://www.yabb.com/download-app", @"I just downloaded Yabb Messenger"), @"description", @"http://www.yabb.com/download-app", @"link", @"www.yabb.com/skins/skin1_yabb/images/yabb-share-on-facebook-logo-small.png", @"picture", nil];
    
    // Invoke the dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or publishing a story.
             NSLog(@"Error publishing story.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled story publishing.");
             } else {
                 // Handle the publish feed callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"post_id"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled story publishing.");
                 } else {
                     // User clicked the Share button
                     NSString *msg = [NSString stringWithFormat:
                                      @"Posted story, id: %@",
                                      [urlParams valueForKey:@"post_id"]];
                     NSLog(@"%@", msg);
                     // Show the result in an alert
//                     [[[UIAlertView alloc] initWithTitle:@"Result"
//                                                 message:msg
//                                                delegate:nil
//                                       cancelButtonTitle:@"OK!"
//                                       otherButtonTitles:nil]
//                      show];
                 }
             }
         }
     }];

}

- (void)shareOnFacebook
{
    /*if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])*/
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        NSString *shareText = NSLocalizedString(@"I just downloaded Yabb Messenger. It is a new way to chat and send photos to your friends on your mobile for free. Yabb is free to download at http://www.yabb.com/install", @"I just downloaded Yabb Messenger.");
        [mySLComposerSheet setInitialText:shareText];
        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.yabb.com"]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                default:
                    break;
            }
        }];
        [app.tabBarController presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    //NSLog(@"Country=%@",indexPath);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int row = indexPath.row;
    
#if APP_TARGET_Yabb
    if (row >= 3) {
        row++;
    }
#endif
    
    switch (row) {
        case 0:
        {
            AboutViewController *controller  = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
            controller.title = NSLocalizedString(@"About", @"About");
/*
            UIWebView *textView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 4, 320, 360)];
            // why so, and what's this - I do not know... it's just a copy-paste from StackOverflow solution
            for (int x=0;x<10;x++) { [[[[[textView subviews] objectAtIndex:0] subviews] objectAtIndex:x] setHidden:YES]; }
            [textView setBackgroundColor:[UIColor clearColor]];
            [textView setOpaque:NO];
            [textView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"]]]];
            [controller.view addSubview:textView];
            */
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case 1:
        {
            NSString *subj = NSLocalizedString(@"Invitation to Yabb Messenger", @"Invitation to Yabb Messenger");
            NSString *body = NSLocalizedString(@"Hi,<br><br>\n\nI just downloaded Yabb Messenger on my phone. It is a brand new way to keep in touch with your friends for free. It works exactly like how you send SMS and photos on your phone but it does not cost anything!  I would like to start chatting and sending photos to you so please download the app now. You can do this by clicking or going to the link below:\n\n<br><br><a href=\"http://www.yabb.com/download-app\">http://www.yabb.com/download-app</a>\n\n<br><br>Please forward this email to your friends too so that that everyone can keep in touch for free!\n\n<br><br>Chat soon!", @"Hi,<br><br>");
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:?subject=%@&body=%@",
//                                                        [subj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
//                                                        [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setSubject:subj];
            [controller setMessageBody:body isHTML:YES];
            if (controller)
                [self presentViewController:controller animated:YES completion:^{}];
            return;
        }
        break;
            
        case 2:
        {
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                _colorRawNavigationBar = [UINavigationBar appearance].barTintColor;
                [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.0]];
            }
            
            [InvitedFriends sendInAppSMS:@"" controller:self delegate:self];
        }
        break;
            
        case 3:
        {
            [self Invite_Facebook];
        }
        break;
            
        case 4:
        {
            [self shareOnFacebook];
        }
        break;
            /*
        
        case 4: {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Reset account" message:@"Do you want to delete all chat boards, messages and settings?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert setTag:100];
            [alert show];
            [alert release];
        }
        break;*/

        case 5: {
            UIViewController *controller = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
            [self.navigationController pushViewController:controller animated:YES];
        }
        break;
            
        case 6: {
            [self edit:self];
        }
        break;

        case 7: {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete account", @"Delete account") message:NSLocalizedString(@"Do you want to delete account and wipe data?", @"Do you want to delete account and wipe data?") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Ok", @"Ok"), nil];
            [alert setTag:200];
            [alert show];
        }
            break;
            
        default:
            break;
    }

}

#pragma mark -
#pragma mark message delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UINavigationBar appearance] setBarTintColor:_colorRawNavigationBar];
    }
    
    [controller dismissViewControllerAnimated:YES completion:^{}];
    return;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller  
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[UINavigationBar appearance] setBarTintColor:_colorRawNavigationBar];
    }
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {

        NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
        
        NSDictionary * params;
        NSString * connection_name;
        
        if (alertView.tag == 100) {
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"droidunreg", @"cmd", phone, @"phone", nil];
            NSLog(@"Unregister: %@", params);
            connection_name = @"unregister";
        }
        else {
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"reset", @"cmd", phone, @"phone", nil];
            NSLog(@"Reset: %@", params);
            connection_name = @"reset";
        }

        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:connection_name silent:NO];
        
        [app.acc showActivityViewer:@"Delete account..."];
    }
}


#pragma marl Connection Delegate
- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {

    [app.acc hideActivityViewer];

    if ([[dict objectForKey:@"errorcode"] intValue] == -1 ) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[dict objectForKey:@"error"] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    [[ChatStorage sharedStorage] resetAccount];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"force":@YES}];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"request_register" object:nil];
}

- (void) didConnectionFailed:(NSString *)connection_name {
    
    [app.acc hideActivityViewer];
}

- (void) edit:(id)sender {
    
    PhotoEditViewController * photo_view_controller = [[PhotoEditViewController alloc] init];
    [self.navigationController pushViewController:photo_view_controller animated:YES];
}

@end
