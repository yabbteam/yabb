//
//  DateTimeCell.m
//  Baycall
//
//  Created by denebtech on 19.09.12.
//
//

#import "DateTimeCell.h"

@implementation DateTimeCell

@synthesize date_time_label;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
