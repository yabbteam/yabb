//
//  ChatSettingsViewController.h
//  Baycall
//
//  Created by denebtech on 23.12.12.
//
//

#import <UIKit/UIKit.h>
#import "ChatNameViewController.h"
#import "PeoplePickerViewController.h"
#import "PeopleMultiselectorViewController.h"
#import "BaseViewController.h"
#import "CreateGroupChatViewController.h"

@interface ChatSettingsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ChatNameDelegate, MultiselectorDelegate, UITextFieldDelegate, CreateGroupChatDelegate> {
    
//    NSNumber * _chat_id;
    
    
    PeoplePickerViewController * people_picker;

    UITableView * __weak _table_view;
    
    BOOL isInvitable;
    
    NSMutableArray* selected_group_abook;
    __weak UITableViewCell*    roomCell;
    
    __strong NSDictionary*      groupParam;
}

@property (nonatomic, weak) IBOutlet UITableView * table_view;
@property (nonatomic, strong) NSString * chat_id;
@property (nonatomic, strong) NSDictionary * room_dict;
@property (nonatomic, strong) UIViewController * parent_controller;

@end
