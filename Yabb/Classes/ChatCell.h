//
//  ChatCell.h
//  speedsip
//
//  Created by denebtech on 22.08.12.
//
//

#import <UIKit/UIKit.h>
#import "ChatBoardTable.h"

@interface ChatCell : UITableViewCell {
    
    IBOutlet UILabel * msg_status;
    IBOutlet UILabel * person_name;
    IBOutlet UILabel * msg_time;
    IBOutlet UIImageView * selector;
    IBOutlet UIImageView * delimiter;
    IBOutlet UIButton*  btnOperation;
    IBOutlet UIImageView*  person_photo;
    
    BOOL _is_private;
    BOOL _is_owner;
    int _is_marked_seen;
    NSString * _server_id;
    BOOL _user_selection;
    
    ChatBoardTable * _parent;
    
    BOOL   enabledLongClick;
    BOOL    touched;
    CGPoint    touchedLocation;
    
}

@property (nonatomic, assign) BOOL is_private;
@property (nonatomic, assign) BOOL is_owner;
@property (nonatomic, assign) int is_marked_seen;
@property (nonatomic, strong) NSString * server_id;
@property (nonatomic, assign) BOOL user_selection;
@property (nonatomic, strong) ChatBoardTable * parent;
@property (nonatomic, strong) UIImage * personPhoto;

- (void) setPerson:(NSString *)name;
- (void) setMessageStatus:(int)status delivered:(BOOL)delivered;
- (void) setTime:(NSDate *)date;
- (void) enableIndicator:(BOOL)enable;
- (BOOL) selectCell;
- (UIImage*) getMyPhoto;

- (void) deleteMe;
- (void) buildLongTimeClick;
- (void) notifyForwardClicked;
- (void) notifyCopyClicked;

@end
