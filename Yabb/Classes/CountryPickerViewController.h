//
//  CountriesViewController.h
//  speedsip
//
//  Created by denebtech on 06.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@protocol CountryPickerViewControllerDelegate <NSObject>

- (void) Select_Country:(NSDictionary *)dict;

@end

@interface CountryPickerViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView * country_table;

@property (nonatomic, strong) NSArray * country_list;
@property (nonatomic, strong) NSMutableDictionary * grouped_countries;
@property (nonatomic, strong) NSMutableArray * letters;
@property (nonatomic, weak) id<CountryPickerViewControllerDelegate> delegate;
@property (nonatomic, assign) int selected_section;
@property (nonatomic, assign) int selected_index;

- (void) Get_Default_Country_Code;

@end
