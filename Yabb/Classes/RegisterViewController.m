//
//  RegisterViewController.m
//  Yabb
//
//  Created by denebtechsaplin
//  Copyright (c) 2013 Baycall. All rights reserved.
//

#import "RegisterViewController.h"
#import "AddressCollector.h"
#import "ConnectionQueue.h"
#import "Alert.h"
#import "AppDelegate.h"
#import "ChatStatusViewController.h"
#import "RegisterSecondController.h"

@interface RegisterViewController ()

@property (nonatomic, strong) CountryPickerViewController *countryPickerViewController;

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        selected_country = -1;
        phone_number_full = [[NSMutableString alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    country_button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    country_button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    phone_activity.hidden = YES;
    [country_button setTitle:@"" forState:UIControlStateNormal];
    
    phone_text.delegate = self;

    keyboardHeight = 0.0f;
    prevOffset = 0.0f;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([UIScreen mainScreen].bounds.size.height > 480.0f)
    {
        [phone_text becomeFirstResponder];
    }

    [self.countryPickerViewController Get_Default_Country_Code];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

+ (bool) isActivated {

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"])
        return YES;
    
    return NO;
}

- (void) clearSelection {
    
    [phone_text setText:@""];
    country_code = nil;
    country_abbr = nil;
}

- (void) Select_Country:(NSDictionary *)dict {
    
    selected_country = [[dict objectForKey:@"country_index"] intValue];
    selected_section = [[dict objectForKey:@"country_section"] intValue];
    country_code = [dict objectForKey:@"country_code"];
    country_name = [dict objectForKey:@"country_name"];
    country_abbr = [dict objectForKey:@"country_abbr"];
    
    if (country_code && country_name) {
        [labelCountry setText:country_name];
        [labelCCode setText:[NSString stringWithFormat:@"+%@", country_code]];
    }
    else
        [labelCountry setText:NSLocalizedString(@"Country code", @"Country code")];

}

- (IBAction) Button_Country:(id)sender
{
    [self.countryPickerViewController setSelected_index:selected_country];
    [self.countryPickerViewController setSelected_section:selected_section];
    [self presentViewController:self.countryPickerViewController animated:YES completion:nil];
}

- (IBAction) Button_Register:(id)sender
{
    NSString * phone = phone_text.text;
    
    if (phone == nil || [phone length] < PHONE_DIGITS_MIN || [phone length] > PHONE_DIGITS_MAX || country_code == nil)
        return;

    [phone_number_full setString:[NSString stringWithFormat:@"%@%@", country_code, phone]];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Number confirmation", @"Number confirmation") message:[NSString stringWithFormat:NSLocalizedString(@"We will text the verification code to phone number +%@", @"We will text the verification code to phone number +%@"), phone_number_full] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Confirm", @"Confirm"), nil];
    [alert show];
}

- (IBAction) Button_Call_Me_Baby_May_Be:(id)sender {
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"callmeback", @"cmd", phone_number_full, @"phone", nil];
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"call_me_back" silent:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"register", @"cmd", phone_number_full, @"phone", country_abbr, @"ccode", nil];
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"register_new_user" silent:NO];
        [self Enable_Phone:NO];
    }
    
}

- (void) Enable_Phone:(BOOL)enabled {
    
    [phone_text setEnabled:enabled];
    [phone_button setEnabled:enabled];
    
    [phone_text setAlpha:enabled?1.0:0.5];
    [phone_button setAlpha:enabled?1.0:0.5];
    
    if (enabled) {
        [phone_button setTitle:NSLocalizedString(@"Register", @"Register") forState:UIControlStateNormal];
        [phone_activity stopAnimating];
        phone_activity.hidden = YES;
    }
    else {
        [phone_button setTitle:@"" forState:UIControlStateNormal];
        phone_activity.hidden = NO;
        [phone_activity startAnimating];
    }
}

- (void) Connection_Done_Register {
    
    RegisterSecondController * second_controller = [[RegisterSecondController alloc] initWithNibName:@"RegisterSecondController" bundle:nil];
    
    [second_controller setTitle:NSLocalizedString(@"Verification", @"Verification")];
    [second_controller setPhone_number_full:phone_number_full];
    [second_controller setPhone_country_abbr:country_abbr];
    
    [[NSUserDefaults standardUserDefaults] setObject:country_abbr forKey:@"country_abbr"];
    [[NSUserDefaults standardUserDefaults] setObject:country_code forKey:@"ccode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController pushViewController:second_controller animated:YES];
}


- (void) didNotificationDone {
    
    [[ConnectionQueue sharedQueue] startConnectionCycle:app.chatRoomsViewController withPeriod:1.0];
}

#pragma mark -
#pragma Connection Delegates

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {

    NSLog(@"Register connection done");

    int error_code = [[dict objectForKey:@"errorcode"] intValue];
//    NSDictionary * data = [dict objectForKey:@"data"];

    [self Enable_Phone:YES];
//    [self Enable_Pin:YES];

    if (error_code != 1) {
        
        [Alert show:@"Error" withMessage:[dict objectForKey:@"error"]];
        
        return;
    }

    if ([connection_name isEqualToString:@"register_new_user"])
        [self Connection_Done_Register];
    
}

- (void) didConnectionFailed:(NSString *)connection_name
{
    NSLog(@"Register connection failed");
    [self Enable_Phone:YES];
}

- (CountryPickerViewController *)countryPickerViewController
{
    if (!_countryPickerViewController) {
        _countryPickerViewController = [[CountryPickerViewController alloc] initWithNibName:@"CountryPickerViewController" bundle:nil];
        [_countryPickerViewController setDelegate:self];
    }
    return _countryPickerViewController;
}

#pragma mark -
#pragma mark Keyboard Delegates

- (void) keyboardWillShow:(NSNotification*)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue* keyboardFrameEndValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    keyboardHeight = [keyboardFrameEndValue CGRectValue].size.height;
    
    if ([UIScreen mainScreen].bounds.size.height <= 480.0f) {
        CGRect rt = contentview.frame;
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            prevOffset = -10;
        }
        else {
            prevOffset = -30;
        }
        rt.origin.y = prevOffset;
        
        contentview.frame = rt;
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else {
        prevOffset = 0.0f;
    }

    NSLog(@"Keyboard show");
}

- (void) keyboardWillHide:(NSNotification*)notification {
    
    if (prevOffset == 0.0f)
        return;
    
    CGRect rt = contentview.frame;
    rt.origin.y += -prevOffset;
    contentview.frame = rt;
    
    prevOffset = 0.0f;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSLog(@"Keyboard hide");
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > PHONE_DIGITS_MAX) ? NO : YES;
}

@end
