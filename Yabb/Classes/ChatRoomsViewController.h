//
//  ChatRoomsViewController.h
//  Baycall
//
//  Created by denebtech on 14.03.13.
//
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

#import "ChatBoardViewController.h"
#import "CreateChatRoomViewController.h"
#import "PeopleMultiselectorViewController.h"
#import "BaseViewController.h"
#import "CreateGroupChatViewController.h"

@protocol ChatRoomsViewControllerDelegate;

@interface ChatRoomsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, CreateChatDelegate, MultiselectorDelegate, PeoplePickerDelegate, UISearchBarDelegate, UISearchDisplayDelegate, UIAlertViewDelegate, CreateGroupChatDelegate> {
    
    NSMutableDictionary*    popups;
    BOOL        isAppear;
}

@property (nonatomic, strong) NSMutableString * room_name;
@property (nonatomic, strong) NSMutableArray * room_phones;

@property (nonatomic, strong) NSArray * chat_room_list;
@property (nonatomic, strong) NSMutableArray * chat_room_sorted;
@property (nonatomic, assign) BOOL has_filter;

@property (nonatomic, assign) BOOL table_editing;

@property (nonatomic, strong) PeopleMultiselectorViewController * multiselector_controller;

@property (nonatomic, strong) IBOutlet UISearchBar * search_bar;

@property (nonatomic, strong) ChatBoardViewController * chat_board;
@property (nonatomic, weak) IBOutlet UITableView * chat_rooms;
@property (nonatomic, strong) id <ChatRoomsViewControllerDelegate> chat_rooms_delegate;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subtitle;

@property (nonatomic, strong) NSString *needGoToRoom;
@property (nonatomic, assign) BOOL  isHaveAppeared;

- (void) Move_To_Chat_Board:(NSDictionary *)room_dict showKeyboard:(BOOL)keyboard anim:(BOOL) isAnim;
- (void) importRooms;
- (NSString*) Send_Private_Message:(int)person_index withKeyboard:(BOOL)keyboard;
- (void) gotoOtherRoomOnChatBoard:(NSString*)room_id withKeyboard:(BOOL)keyboard;
- (void)sendSoundSettingToServer;
- (BOOL) becomeActiveProcess;

@end

@protocol ChatRoomsViewControllerDelegate <NSObject>

@optional
- (void) didCreateRoom:(NSString*)room_id;

@end
