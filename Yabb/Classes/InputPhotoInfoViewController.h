//
//  InputPhotoInfoViewController.h
//  Yabb
//
//  Created by denebtech on 3/15/14.
//
//

#import <UIKit/UIKit.h>

@class UIPlaceHolderTextView;

@protocol InputPhotoInfoViewControllerDelegate <NSObject>
@optional

- (void) onSelectedComment:(UIImage*) image comment:(NSString*) text other:(id)other;

@end



@interface InputPhotoInfoViewController : UIViewController {
    
}

@property (nonatomic, strong) IBOutlet    UIImage* image;
@property (nonatomic, strong) IBOutlet    UIImageView*    imageVideoPlay;
@property (nonatomic, strong) IBOutlet    UIImageView*    photoView;
@property (nonatomic, strong) IBOutlet    UIPlaceHolderTextView *textView;
@property (nonatomic, strong) id<InputPhotoInfoViewControllerDelegate>    delegate;
@property (nonatomic, strong)  id otherParam;
@property (nonatomic, assign)  BOOL isVideo;

@end
