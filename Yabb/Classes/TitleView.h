//
//  ChatTitleView.h
//  Yabb
//
//  Created by denebtechsaplin on 10/8/13.
//
//

#import <UIKit/UIKit.h>

@interface TitleView : UIView

@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) UILabel *subtitleLabel;

@property (nonatomic, weak) NSString *title;
@property (nonatomic, weak) NSString *subtitle;

@end
