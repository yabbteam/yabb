
//
//  AnimationCell.m
//  speedsip
//
//  Created by denebtech on 21.08.12.
//
//

#import "AnimationCell.h"
#import "EmotionsView.h"

@implementation AnimationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) setAnimation:(NSString *) animation_name editMode:(BOOL)edit {
    
    NSString * first_image_name = [NSString stringWithFormat:@"%@1.png", animation_name];
    NSString * second_image_name = [NSString stringWithFormat:@"%@2.png", animation_name];
    
    UIImage * first_image = [UIImage imageNamed:first_image_name];
    UIImage * second_image = [UIImage imageNamed:second_image_name];
    
    image_view.animationImages = [NSArray arrayWithObjects:first_image, second_image, first_image, first_image, first_image, nil];
    image_view.animationDuration = 1.6;
    image_view.animationRepeatCount = 0;
    [image_view startAnimating];

    int message_y = _is_private?0:10;
    
//    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(202, 1, 112, 82)];
//    [view setBackgroundColor:[UIColor redColor]];

    int photo_width = person_photo.frame.size.width + 4;
    if (_is_owner) {
        
 //       [self addSubview:view];
        [back_view setImage:[UIImage imageNamed:@"bubble_right.png"]];
        [back_view setFrame:CGRectMake(self.parent.frame.size.width-120-photo_width, 1, 120, 82)];
        [image_view setFrame:CGRectMake(self.parent.frame.size.width-120+1-photo_width, 1, 112, 82)];
        [msg_status setFrame:CGRectMake(self.parent.frame.size.width - 118-photo_width, 85, 52, 11)];
        [msg_time setFrame:CGRectMake(self.parent.frame.size.width-174-photo_width, 34, 51, 20)];
        
        //set person photo
        [person_photo setFrame:CGRectMake((edit?40:0)+self.parent.frame.size.width - person_photo.frame.size.width - 2,
                                          image_view.frame.origin.y+image_view.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        UIImage* myImage = [self getMyPhoto];
        if (myImage != nil)
            [person_photo setImage:myImage];
    }
    else {
        [back_view setImage:[UIImage imageNamed:@"bubble_left.png"]];
        [back_view setFrame:CGRectMake((edit?40:0)+photo_width, message_y+1, 120, 82)];
        [image_view setFrame:CGRectMake(8 + (edit?40:0)+photo_width, message_y + 1, 112, 82)];
        [msg_time setFrame:CGRectMake(124 + (edit?40:0)+photo_width, message_y + 32, 51, 20)];
        
        CGRect rect = person_name.frame;
        rect.origin.x  = edit?40:0;
        [person_name setFrame:rect];

        //person photo
        [person_photo setFrame:CGRectMake((edit?40:0)+2,
                                          image_view.frame.origin.y+image_view.frame.size.height - person_photo.frame.size.height - 2,
                                          person_photo.frame.size.width, person_photo.frame.size.height)];
        UIImage* photo = self.personPhoto;
        if (photo != nil)
            [person_photo setImage:photo];
    }
}

@end
