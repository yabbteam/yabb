//
//  CreateGroupChatViewController.m
//  Yabb
//
//  Created by denebtech on 5/15/14.
//
//

#import "CreateGroupChatViewController.h"
#import "CommonUtil.h"
#import "ImagePickerViewController.h"
#import "PhotoEditViewController.h"

@interface CreateGroupChatViewController ()

@end

@implementation CreateGroupChatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Cancel Button
        UIBarButtonItem * left_button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didCancelButton:)];
        [self.navigationItem setLeftBarButtonItem:left_button];
        
        // Done Button
        UIBarButtonItem * done_button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", @"Next") style:UIBarButtonItemStylePlain target:self action:@selector(didDoneButton:)];
        [self.navigationItem setRightBarButtonItem:done_button];
        
        [self.navigationItem setTitle:NSLocalizedString(@"New Group", @"New Group")];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [avatarView setImage:[CommonUtil getNoAvatarImage]];
    selectedPhoto = nil;

    CALayer * l = [avatarView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
    
    [textSubject becomeFirstResponder];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        segRoomKind.tintColor = [UIColor darkGrayColor];
        [segRoomKind setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateNormal];
    }

    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapPhoto:)];
    [avatarView addGestureRecognizer:recognizer];
    avatarView.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didCancelButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didDoneButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate didCreateNewRoom:textSubject.text withRoomPhoto:selectedPhoto isPublic:segRoomKind.selectedSegmentIndex==0];
    }];
}

- (IBAction) didTapPhoto:(id)sender {
    UIActionSheet* actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Take Picture",@"Take Picture")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Choose From Gallery", @"Choose From Gallery")];
    
    int cancel_index = (int)[actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:cancel_index];
    
    [actSheet showInView:self.view];
}

- (IBAction) onClickTakeCamera:(id)sender {
    
    if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        
        ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypeCamera];
        [image_picker setImage_picker_delegate:self];
        
        [self presentViewController:image_picker animated:YES completion:^{}];
    }
}

- (IBAction) onClickPhotoGallery:(id)sender {
    
    ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    [image_picker setImage_picker_delegate:self];
    
    [self presentViewController:image_picker animated:YES completion:^{}];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self onClickTakeCamera:self];
    }
    else if (buttonIndex == 1) {
        [self onClickPhotoGallery:self];
    }
}

- (void) Pick_Image:(UIImage *)image assetReference:(NSString *)asset {
    
    if (image == nil)
        return;
    
    UIImage* pickResult = [CommonUtil fixOrientation:image];
    
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:pickResult cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
    
    imgCropperVC.delegate = self;
    [self presentViewController:imgCropperVC animated:YES completion:^{
    }];
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
    selectedPhoto = [CommonUtil imageByScalingToSize:CGSizeMake(AVATAR_SIZE, AVATAR_SIZE) cgimage:editedImage.CGImage orientation:editedImage.imageOrientation];
    avatarView.image = selectedPhoto;
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
