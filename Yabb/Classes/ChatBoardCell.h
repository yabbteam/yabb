//
//  ChatBoardViewCell.h
//  speedsip
//
//  Created by denebtech on 19.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"
#import "TTTAttributedLabel.h"

@interface ChatBoardCell : ChatCell<TTTAttributedLabelDelegate> {
    
    IBOutlet UIImageView * bubble;
    IBOutlet TTTAttributedLabel * message;
//    IBOutlet UITextView * text_view;
    
    NSMutableArray * emotions;
    NSMutableArray * chat_rows;    
}

@property (nonatomic, strong) UIImageView * bubble;
@property (nonatomic, strong) TTTAttributedLabel * message;
//@property (nonatomic, retain) UITextView * text_view;

- (void) setText:(NSDictionary *)parsed_message  editMode:(BOOL)edit;


@end
