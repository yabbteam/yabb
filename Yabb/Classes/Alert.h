//
//  Alerts.h
//  speedsip
//
//  Created by Tolian T on 15.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Alert : NSObject

+ (void) show:(NSString *)title withMessage:(NSString *)message;

@end
