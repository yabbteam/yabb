//
//  ChatSettingsViewController.m
//  Baycall
//
//  Created by denebtech on 23.12.12.
//
//

#import "ChatSettingsViewController.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import "Friends.h"
#import "ChatStorage.h"
#import "Alert.h"
#import "ConnectionQueue.h"
#import "AddressCollector.h"
#import "AppDelegate.h"
#import "DeleteButton.h"
#import "JabberLayer.h"
#import "ChatSoundSettingsViewController.h"

@interface ChatSettingsViewController ()

@end

@implementation ChatSettingsViewController


@synthesize table_view = _table_view;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
        self.title = NSLocalizedString(@"Settings", @"Settings");
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    
    if ([[JabberLayer SharedInstance] isGroupChat:self.chat_id]) {
        NSString* kind = [[JabberLayer SharedInstance] getKindStringOfGroupChat:self.chat_id];
        if (kind) {
            self.title = [NSString stringWithFormat:NSLocalizedString(@"Group Chat (%@)", @"Group Chat (%@)"), kind];
        }
        
        NSString* admin = [[JabberLayer SharedInstance] getAdminOfGroupChat:self.chat_id];
        if (admin) {
            NSString * userid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
            if ([admin isEqualToString:userid])
                isInvitable = YES;
            else
                isInvitable = NO;
        }
        else {
            isInvitable = YES;
        }
    }
    else {
        self.title = [NSString stringWithFormat:@"Info [%@]", self.chat_id];
        isInvitable = YES;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Update_Friends) name:@"update_friends" object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"update_friends" object:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) Update_Friends {
    
    [self.table_view reloadData];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {     // Delete image
        UIImagePickerController * image_picker = [[UIImagePickerController alloc] init];
        image_picker.delegate = self;
        [self presentViewController:image_picker animated:YES completion:nil];
    }
    else if (buttonIndex == 1) {
        [[ChatStorage sharedStorage] setRoomBackground:@"" forRoom:self.chat_id ];
    }
}

#define MAX_DIMENSION   800.0f
#define RAD(a)          (a * M_PI/180.0f)

-(UIImage*)imageByScalingToSize:(CGSize)targetSize sourceImage:(UIImage *)sourceImage needRotate:(BOOL)rotate
{
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
    }
    
    if (rotate) {
        if (sourceImage.imageOrientation == UIImageOrientationLeft) {
            CGContextRotateCTM (bitmap, RAD(90));
            CGContextTranslateCTM (bitmap, 0, -targetHeight);
            
        } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
            CGContextRotateCTM (bitmap, RAD(-90));
            CGContextTranslateCTM (bitmap, -targetWidth, 0);
            
        } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
            // NOTHING
        } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
            CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
            CGContextRotateCTM (bitmap, RAD(-180.));
        }
    }
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown)
        CGContextDrawImage(bitmap, CGRectMake(0, 0, targetWidth, targetHeight), imageRef);
    else
        CGContextDrawImage(bitmap, CGRectMake(0, 0, targetHeight, targetWidth), imageRef);
    
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref scale:1.0 orientation:rotate?0:[sourceImage imageOrientation]];
    
    CGImageRelease(ref);
    CGContextRelease(bitmap);
    
    return newImage;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage * image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    NSURL * url = [info valueForKey:@"UIImagePickerControllerReferenceURL"];
    
    if (image == nil || url == nil)
        return;
    
    CGSize image_size = image.size;
    float image_factor = image_size.width / image_size.height;
    NSData * original_data = nil;
    
    // Need resize by width
    if (image_factor >= 1.0 && image_size.width > MAX_DIMENSION) {
        
        float scale_factor = MAX_DIMENSION / image_size.width;
        
        UIImage * resized_image = [self imageByScalingToSize:CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor) sourceImage:image needRotate:NO];
        original_data = UIImageJPEGRepresentation(resized_image, 0.7);
    }
    // Need resize by height
    else if (image_factor < 1.0 && image_size.height > MAX_DIMENSION) {
        float scale_factor = MAX_DIMENSION / image_size.height;
        
        UIImage * resized_image = [self imageByScalingToSize:CGSizeMake(image_size.width * scale_factor, image_size.height * scale_factor) sourceImage:image needRotate:NO];
        original_data = UIImageJPEGRepresentation(resized_image, 0.7);
    }
    // Original size
    else {
        original_data = UIImageJPEGRepresentation(image, 0.7);
    }
    
    // Save image to '/tmp' dir
    NSString * name = [NSString stringWithFormat:@"image_%@.jpg", [[ChatStorage sharedStorage] generateRandom:16]];
    if ([[ChatStorage sharedStorage] Save_Image_Data:original_data withName:name] == NO) {
        [Alert show:NSLocalizedString(@"Error", @"Error") withMessage:NSLocalizedString(@"Can not save image.", @"Can not save image.")];
        return;
    }
    
    [[ChatStorage sharedStorage] setRoomBackground:name forRoom:self.chat_id ];
    
    [picker dismissModalViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) didDoneButton:(NSArray *)abooks {
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    if (phone == nil)
        return;
    
    if ([[JabberLayer SharedInstance] isGroupChat:self.chat_id]) {
    
        [app.acc showActivityViewer:NSLocalizedString(@"New person", @"New person")];
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"chatin", @"cmd", phone, @"phone",
                                 self.chat_id, @"chatid", abooks, @"abook", [self getRoomName], @"roomname", nil];
        
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"chat_in" silent:NO];
    }
    else {
        [app.acc showActivityViewer:NSLocalizedString(@"Creating...", @"Creating...")];
        
        NSMutableArray* phones4Group = [[NSMutableArray alloc] initWithArray:[self getPhones]];
        [phones4Group addObjectsFromArray:abooks];
        
        UIImage* photo = [groupParam objectForKey:@"photo"];
        NSString* subject = [groupParam objectForKey:@"subject"];
        NSNumber* isPublic = [groupParam objectForKey:@"is_public"];
        
        NSDictionary * params;
        
        if (photo == nil) {
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone",
                      phones4Group, @"abookg", isPublic, @"is_public", subject, @"subject", nil];
        }
        else {
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"addchat", @"cmd", phone, @"phone",
                      phones4Group, @"abookg", isPublic, @"is_public", photo, @"photo", subject, @"subject", nil];
        }
        
        NSLog(@"Add new chat: %@", params);
        
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"create_new_room" silent:NO];
    }
    
    [people_picker dismissModalViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        [app.acc showActivityViewer:NSLocalizedString(@"Delete room", @"Delete room")];
        
        NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
        if (phone == nil)
            return;
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"chatout", @"cmd", phone, @"phone",
                                 self.chat_id, @"chatid", nil];
        
        
        [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"chat_out" silent:NO];
        
    }
    
}

- (void) Exit_And_Delete {
 
    
    UIAlertView * alert_view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Leave and delete", @"Leave and delete") message:NSLocalizedString(@"Are you sure you want to leave and delete this conversation?", @"Are you sure you want to leave and delete this conversation?") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Delete", @"Delete"), nil];
    
    [alert_view show];
    
}

#pragma mark -
#pragma mark Connection Delegate

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {

    if ([connection_name isEqualToString:@"chat_in"] || [connection_name isEqualToString:@"create_new_room"]) {     // Create new room

        [app.acc hideActivityViewer];

        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1) {
            

            return;
        }
        
        NSString * number_id = [[dict objectForKey:@"data"] objectForKey:@"chatid"];
        if (number_id == nil) {
            return;
        }
        
        if ([number_id isEqualToString:self.chat_id]) {     // Invate person to chat
         
            if (self.parent_controller)
                [self.navigationController popToViewController:self.parent_controller animated:YES];
        }
        else {  // Create new group chat (always group)

            NSString* chat_id = number_id;
            NSString * room_name = [[dict objectForKey:@"data"] objectForKey:@"room_name"];
            NSArray * room_phones = [[dict objectForKey:@"data"] objectForKey:@"phones"];
            
            // Add new room
            [[ChatStorage sharedStorage] addChatRoom:room_name withChatId:chat_id withPhones:room_phones];

            ChatRoomsViewController * room_controller = app.chatRoomsViewController;
            [room_controller importRooms];
            
            NSDictionary * chat_room = [[ChatStorage sharedStorage] getChatRoom:number_id];
            if (chat_room) {
                [self.navigationController popToRootViewControllerAnimated:NO];
                [app.chatRoomsViewController Move_To_Chat_Board:chat_room showKeyboard:NO anim:YES];
            }
        }
        
    }
    else if ([connection_name isEqualToString:@"chat_out"]) {   // Delete room
        
        [[ChatStorage sharedStorage] deleteChatRoom:self.chat_id];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:@{@"force":@YES}];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [app.acc hideActivityViewer];
        
        return;
    }
}


- (void) didConnectionFailed:(NSString *)connection_name {
  
    
    NSLog(@"Chat Failed");
    
    [app.acc hideActivityViewer];
}

#pragma mark -
#pragma mark Table Delegates

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    
    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        section++;
    
    if (section == 0) {
        
        if (roomCell != nil) {
            UITextField * editText = (UITextField *)[roomCell viewWithTag:400];
            if (editText != nil) {
                if (![editText isFirstResponder]){
                    [editText becomeFirstResponder];
                }
            }
        }

        /*
        ChatNameViewController * chat_name_controller = [[ChatNameViewController alloc] init];
        
        chat_name_controller.chat_name_delegate = self;
        [self.navigationController pushViewController:chat_name_controller animated:YES];
         */
    }
    else if (section == 1) {
        UIActionSheet * actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
        
        [actSheet addButtonWithTitle:NSLocalizedString(@"Select Background", @"Select Background")];
        [actSheet addButtonWithTitle:NSLocalizedString(@"Reset Background", @"Reset Background")];
        
        int index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
        [actSheet setCancelButtonIndex:index];
        [actSheet showInView:[self.view window]];
    }
    else if (section == 2) {
        ChatSoundSettingsViewController * sound_settings_controller = [[ChatSoundSettingsViewController alloc] initWithNibName:@"ChatSoundSettingsViewController" bundle:nil];
        
        sound_settings_controller.chat_id = self.chat_id;
        
        [self.navigationController pushViewController:sound_settings_controller animated:YES];
    }
    else if (section == 3) {
        
        if (indexPath.row > [self getNumOfPhones]) {
            
            if ([[JabberLayer SharedInstance] isReconnecting]) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                return;
            }
            
            if ([[JabberLayer SharedInstance] isGroupChat:self.chat_id]) {

                [self didCreateNewRoom:nil withRoomPhoto:nil isPublic:NO];
            }
            else {
                CreateGroupChatViewController * createRoomController = [[CreateGroupChatViewController alloc] initWithNibName:@"CreateGroupChatViewController" bundle:nil];
                createRoomController.delegate = self;
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:createRoomController];
                [self presentViewController:nav animated:YES completion:^{}];
            }
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [[ChatStorage sharedStorage] getRoomPrivate:self.chat_id]?3:4;
    
}

- (NSArray *) getPhones {

    NSString * phones_str = [self.room_dict objectForKey:@"phones"];
    NSMutableArray * phones = [NSMutableArray arrayWithArray:[phones_str componentsSeparatedByString:@":"]];
    
    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
//    [phones addObject:user_phone];
    
    int counter = [phones count];
    for (int i = 0; i < counter; i++) {
        NSString * phone = [phones objectAtIndex:i];
        
        if ( [[[AddressCollector sharedCollector] clearedPhoneNumber:phone withPlus:NO] isEqualToString:[[AddressCollector sharedCollector] clearedPhoneNumber:user_phone withPlus:NO]]) {
            
            [phones removeObjectAtIndex:i];
            i--;
            counter--;
        }
    }
    /*
    for (NSString * phone in phones) {
        
        if ( [[[AddressCollector sharedCollector] clearedPhoneNumber:phone withPlus:NO] isEqualToString:[[AddressCollector sharedCollector] clearedPhoneNumber:user_phone withPlus:NO]]) {
            
            [phones removeObject:phone];
        }
    }
    */
    
    return phones;
}

- (int) getNumOfPhones {
    
    NSArray * phones = [self getPhones];

    return phones?[phones count]:0;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        section++;
    
    if (section == 3) {
        int numOfPhones = [self getNumOfPhones];
        
        if (isInvitable)
            return numOfPhones + 2;
        else
            return numOfPhones + 1;
    }
    
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        section++;
    
    if (section == 0) {
        return NSLocalizedString(@"Room Name", @"Room Name");
    }

    if (section == 3) {
        int numOfPhones = [self getNumOfPhones] + 1;
        return [NSString stringWithFormat:NSLocalizedString(@"Participants (%d)", @"Participants (%d)"), numOfPhones];
    }
    
    return nil;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        return nil;
    
    if (section == 3) {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60)];
        [headerView setBackgroundColor:[UIColor clearColor]];
        
        DeleteButton * delete_button = [[DeleteButton alloc] initWithFrame:CGRectMake(10, 10, 300, 40)];        
        
        delete_button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20.0f];
        delete_button.titleLabel.shadowColor = [UIColor darkGrayColor];
        delete_button.titleLabel.shadowOffset = CGSizeMake(1, 1);
        
        [delete_button setTitle:NSLocalizedString(@"Exit and Delete", @"Exit and Delete") forState:UIControlStateNormal];
        [delete_button addTarget:self action:@selector(Exit_And_Delete) forControlEvents:UIControlEventTouchUpInside];
        
        [headerView addSubview:delete_button];
        
        return headerView;
        
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        return 0.0f;
    
    if (section == 3) {
        return 50.0f;
    }
    
    return 0.0f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int section = indexPath.section;
    
    if ([[ChatStorage sharedStorage] getRoomPrivate:self.chat_id])
        section++;
    
    if (section == 0) { //Group chat name cell
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Group_Chat_Name_Setting"];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Group_Chat_Name_Setting"];
        
        cell.textLabel.hidden = YES;
        
        UITextField * editText = (UITextField *)[cell viewWithTag:400];
        if (editText == nil) {
            editText = [[UITextField alloc] initWithFrame:CGRectMake(15, 10, 280, 32)];
            [editText setTag:400];
            
            [cell addSubview:editText];
            
            editText.backgroundColor = [UIColor clearColor];
            editText.borderStyle = UITextBorderStyleNone;
            editText.returnKeyType = UIReturnKeyDone;
            [editText setFont:[UIFont systemFontOfSize:18.0]];
            
            editText.delegate = self;
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tableview_item_indicator.png"]];
        }
        
        editText.text = [self getRoomName];
        
        roomCell = cell;
        return cell;
    }
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"CellID_%d", section]];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"CellID_%d", section]];
    
    /*
    UIImageView * status_view = (UIImageView *)[cell viewWithTag:100];
    if (status_view == nil) {

        status_view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_offline.png"]];
        
        CGRect rect = status_view.frame;
        rect.origin.x = 270, rect.origin.y = 8;
        [status_view setFrame:rect];
        
        [status_view setTag:100];

        [cell addSubview:status_view];
        
        [status_view release];
    }
    [status_view setHidden:YES];
    */
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];

    if (section == 0) {
    }
    else if (section == 1) {
        cell.textLabel.text = NSLocalizedString(@"Background Image", @"Background Image");
    }
    else if (section == 2) {
        cell.textLabel.text = NSLocalizedString(@"Sound & Vibration", @"Sound & Vibration");
    }
    else if (section == 3) {

        UILabel * label = (UILabel *)[cell viewWithTag:200];
        if (label == nil) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(20, 2, 250, 22)];
            [label setFont:[UIFont boldSystemFontOfSize:18.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            [label setTag:200];
            
            [cell addSubview:label];
        }
        
        UILabel * origin_label = (UILabel *)[cell viewWithTag:300];
        if (origin_label == nil) {
            origin_label = [[UILabel alloc] initWithFrame:CGRectMake(20, 22, 250, 22)];
            [origin_label setFont:[UIFont systemFontOfSize:14.0]];
            [origin_label setBackgroundColor:[UIColor clearColor]];
            [origin_label setTag:300];
            
            [cell addSubview:origin_label];
        }
        
        [label setFrame:CGRectMake(20, 0, 250, 44)];
        [label setTextAlignment:NSTextAlignmentLeft];
        
        [origin_label setText:@""];
        [origin_label setTextAlignment:NSTextAlignmentLeft];
        
        NSString* phone_number = nil;
        if (indexPath.row == 0) {
            
            label.text = NSLocalizedString(@"You", @"You");
            NSString * userid = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
            phone_number = [@"+" stringByAppendingString:userid];
        }
        else if (indexPath.row <= [self getNumOfPhones]) {
            
            NSString * phone = [[self getPhones] objectAtIndex:(indexPath.row - 1)];
            NSString * o = [[Friends SharedFriends] originByPhone:phone withDefault:NO];
            
            if ([o isEqualToString:@""] == NO) {
                [origin_label setText:o];
                [label setFrame:CGRectMake(20, 2, 250, 22)];
            }
            
//            [status_view setHidden:NO];
            
//            int index = [[AddressCollector sharedCollector] Get_Index_By_Phone:phone];
//            if (index > -1 && [[Friends SharedFriends] isPersonOnlineIndex:index]) {
//                [status_view setImage:[UIImage imageNamed:@"user_online.png"]];
//            }
//            else {
//                [status_view setImage:[UIImage imageNamed:@"user_offline.png"]];
//            }
            
            label.text = [[AddressCollector sharedCollector] Get_Name_By_Phone:phone withPlus:YES];
            
            NSLog(@"Cell: %.2f, %.2f", cell.textLabel.frame.size.width, cell.textLabel.frame.size.height);
            
            phone_number = phone;
        }
        else {
            label.text = NSLocalizedString(@"Invite friends", @"Invite friends");
            [label setTextAlignment:NSTextAlignmentCenter];
        }
        
        if (phone_number != nil) {
            NSString* admin = [[JabberLayer SharedInstance] getAdminOfGroupChat:self.chat_id];
            if (admin) {
                NSString* adminPhone = [@"+" stringByAppendingString:admin];
                if ([adminPhone isEqualToString:phone_number]) {
                    label.text = [label.text stringByAppendingString:NSLocalizedString(@" (Admin)", @" (Admin)")];
                }
            }
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark Chat Name Delegate

- (NSString *) getRoomName {
    
    return [[ChatStorage sharedStorage] getRoomName:self.chat_id];
}

- (void) renameRoom:(NSString *)name {

    NSString * user_phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    NSString * pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];
    NSString * chat_id = self.chat_id;
    
    [[ChatStorage sharedStorage] renameRoom:self.chat_id withName:name];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_chatroom_info" object:nil];
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@"chatname", @"cmd", user_phone, @"phone", pass, @"pass", chat_id, @"chatid", [[ChatStorage sharedStorage] encodeString:name], @"name", nil];
    
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"set_room_name" silent:NO];

    [self.table_view reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.text) {
        [self renameRoom:textField.text];
    }
    
    [textField resignFirstResponder];
    
    return NO;
}

- (void) didCreateNewRoom:(NSString *)room_subject withRoomPhoto:(UIImage*) room_photo isPublic:(BOOL)isPublic {
    
    people_picker = [[PeoplePickerViewController alloc] init];
    PeopleMultiselectorViewController * nav = [[PeopleMultiselectorViewController alloc] initWithRootViewController:people_picker rightButtonText:nil
                                               ];
    [nav setMultiselector_delegate:self];
    [nav setPreselected_phones:[self getPhones]];
    [people_picker setDelegate:nav];
    [nav setRoot_controller:self];
    
    if (room_subject == nil)
        room_subject = @"";
    
    if (room_photo == nil)
        groupParam = @{@"subject":room_subject, @"is_public":[NSNumber numberWithBool:isPublic]};
    else
        groupParam = @{@"subject":room_subject, @"photo":room_photo, @"is_public":[NSNumber numberWithBool:isPublic]};
    
    [self presentViewController:nav animated:YES completion:nil];
}


/*
*/
@end
