//
//  PhotoWinkViewController.m
//  Yabb
//
//  Created by denebtech on 3/13/14.
//
//

#import "PhotoWinkViewController.h"
#import "UIPlaceHolderTextView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ImagePickerViewController.h"
#import "CommonUtil.h"

@interface PhotoWinkViewController ()

@end

@implementation PhotoWinkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    textView.placeholder = NSLocalizedString(@"Tap to add a message", @"Tap to add a message");
    segTime.selectedSegmentIndex = 4;

    CALayer * l = [backView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
    
    photoView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTapPhoto:)];
    [photoView addGestureRecognizer:tapGestureRecognizer];
    
    scrollView.contentSize = CGSizeMake(280, 454);
    scrollView.showsHorizontalScrollIndicator = NO;

    photoView.contentMode = UIViewContentModeScaleAspectFit;
    
    photoBackView.frame = photoView.frame;
    
    sendButton.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) onCancelClicked:(id)sender {
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect rtScreen = [UIScreen mainScreen].bounds;
                         self.view.center = CGPointMake(self.view.center.x, self.view.center.y + rtScreen.size.height);
                         self.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             [self.view removeFromSuperview];
                         }
                     }];
    
    if (self.delegate)
        [self.delegate onSelectedPhotoWinkCancel];
    
}

- (void) userDidTapPhoto:(UITapGestureRecognizer*) recoginzer {
    
    if ([textView isFirstResponder]) {
        
        [textView resignFirstResponder];
        return;
    }

    UIActionSheet* actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Take Picture",@"Take Picture")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Choose From Gallery", @"Choose From Gallery")];
    
    int cancel_index = [actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:cancel_index];

    [actSheet showInView:self.view];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) { // take picture
        
        if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else if (buttonIndex == 1) { // from gallery
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage * image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage* pickResult = [CommonUtil fixOrientation:image];
    photoView.image = pickResult;
    sendButton.enabled = YES;
    
    photoBackView.text = @"";
    photoBackView.backgroundColor = [UIColor blackColor];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) onClickSend:(id)sender {

    int time = 0;
    switch(segTime.selectedSegmentIndex) {
        case 0:
            time = 2;
            break;
        case 1:
            time = 4;
            break;
        case 2:
            time = 6;
            break;
        case 3:
            time = 8;
            break;
        case 4:
            time = 10;
            break;
        case 5:
            time = 20;
            break;
    }
    
    [self.delegate onSelectedPhotoWink:photoView.image message:textView.text time:time];
    
    [self onCancelClicked:self];
}

@end
