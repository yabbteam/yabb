//
//  AddressCollector.h
//  speedsip
//
//  Created by denebtech on 03.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface AddressCollector : NSObject {
    
    NSMutableArray * allPeople;
    NSMutableArray * temp_allPeople;
    
    id connection_done_delegate;
    
    NSString*       uploadedAbook;
    
    NSString*       onceGotContactInfo;
    
    NSMutableDictionary*    cachePhone_Index;
    
    NSMutableDictionary*    hashModiInfo;
    NSMutableDictionary*    ndd_infos;
}

@property (nonatomic, strong) NSString * promo;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * ver;


+ (AddressCollector *) sharedCollector;
- (int) Num_Of_Contacts;
- (void) GetPeopleWithNormal:(BOOL) isNormal;
- (BOOL) isContactBookChanged;
- (void) GetPeopleSynchronize;
- (NSString *) Get_All_Contacts;

- (NSString *) Get_Name_By_Index:(int)index;
- (NSDictionary *) Get_Sorting_Name_By_Index:(int)index withSort:(ABPersonSortOrdering)sort;
- (NSString *) Get_Person_Name_1:(int)index;
- (NSString *) Get_Person_Name_2:(int)index;
- (UIImage *) Get_Image_By_Index:(int)index;
- (NSMutableArray *) Get_Localized_Phones:(int)index;
- (NSString *) Get_Email:(int)index;
- (NSMutableArray *) Get_Phones_By_Index:(int)index;

- (void) Set_Photo_By_Index:(int) index photo:(UIImage*) photo;


- (NSString *) Get_Name_By_Phone:(NSString *)phone_number  withPlus:(BOOL)plus;
- (int) Get_Index_By_Phone:(NSString *)phone;

- (NSString *) clearedPhoneNumberLeft:(NSString *) dirty_number;
- (NSString *) clearedPhoneNumber:(NSString *) dirty_number withPlus:(BOOL)plus;

- (void) remakeAllPeopleByFriends:(NSArray*) arrayFriends;

- (NSString *)packPhoneNumber:(NSString *)ph;
- (void) Upload_To_Server:(BOOL) isLoginProgress;

- (CGRect) rectForText:(NSString *)text inFrame:(CGRect)currentFrame withFont:(UIFont *)font;

- (void) setConnectionDoneDeleagate:(id)delegate;

- (void) clearAbookCache;
- (void) saveAbookCache;

- (BOOL) isNeedToUploadAbook;

- (ABRecordRef) addNewContactWithPhoneNumber:(NSString*)phone_number;
- (void) addPhoneNumberToContact:(ABRecordRef) person phone:(NSString*)phone_number;
- (void) addNewContactWithPhoneNumber:(NSString*)phone_number name:(NSString*) name; // for test

@end
