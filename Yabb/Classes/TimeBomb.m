//
//  TimeBomb.m
//  Yabb
//
//  Created by denebtech on 3/14/14.
//
//

#import "TimeBomb.h"
#import "ChatStorage.h"

static TimeBomb * timebomb = nil;

@implementation TimeBomb {
    
    NSMutableDictionary* arrayInfo;
}

+ (TimeBomb *) sharedInstance {
    
    if (timebomb == nil) {
        
        timebomb = [TimeBomb alloc];
        [timebomb initInfo];
        [timebomb load];
    }
    
    return timebomb;
}

- (void) initInfo {
    
    arrayInfo = [[NSMutableDictionary alloc] init];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(tick) userInfo:nil repeats:YES];
}

- (void) load {

    NSDictionary* dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"time_bomb_info"];
    if (dict != nil)
        arrayInfo = [[NSMutableDictionary alloc] initWithDictionary:dict];
}

- (void) save {
    
    [[NSUserDefaults standardUserDefaults] setObject:arrayInfo forKey:@"time_bomb_info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setMessageTimeout:(NSString*)msgid value:(int)seconds {
    
    if (msgid == nil)
        return;

    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInt:seconds] forKey:@"time"];
    [arrayInfo setObject:dict forKey:msgid];
    
    [self save];
}

- (int) getMessageTimeout:(NSString*)msgid {
    
    NSDictionary *dict = [arrayInfo objectForKey:msgid];
    if (dict == nil)
        return 0;
    
    if ([dict objectForKey:@"started"]) {
        
        int seconds = (int)[[dict objectForKey:@"time"] integerValue];
        NSTimeInterval value = [[dict objectForKey:@"since"] doubleValue];
        NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
        int ret = seconds - (current-value);
        if (ret < 0)
            return 0;
        return ret;
    }
    
    return (int)[[dict objectForKey:@"time"] integerValue];
}


- (BOOL) isTimeBombMessage:(NSString*)msgid {
    
    if ([self getMessageTimeout:msgid] > 0)
        return YES;
    
    NSDictionary* message = [[ChatStorage sharedStorage] getRecordByServerId:msgid];
    
    if ([[message objectForKey:@"message_type"] integerValue] == 0)
        return NO;
    
    if (![message objectForKey:@"phone"] || [message objectForKey:@"media_asset"])
        return NO;
    
    return YES;
}

- (void) start:(NSString*)msgid {
    
    NSMutableDictionary *dict = [arrayInfo objectForKey:msgid];
    if (dict == nil)
        return;
    
    if ([dict objectForKey:@"started"])
        return;
    
    [dict setObject:@YES forKey:@"started"];
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    [dict setObject:[NSNumber numberWithDouble:current] forKey:@"since"];

    [self save];
}

- (void) tick {

    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    for (id key in [arrayInfo allKeys]) {
        
        NSMutableDictionary* dict = [arrayInfo objectForKey:key];
        if ([dict objectForKey:@"started"] == nil)
            continue;
        
        int seconds = (int)[[dict objectForKey:@"time"] integerValue];
        NSTimeInterval value = [[dict objectForKey:@"since"] doubleValue];
        if ((current-value) >= seconds) {
            NSString* msgid = key;
            
//            NSDictionary* dict = [[ChatStorage sharedStorage] getRecordByServerId:msgid];
//            NSString* image_name = [dict objectForKey:@"media_name"];
//            [[ChatStorage sharedStorage] Delete_Image:image_name];
            
            [[ChatStorage sharedStorage] setMessageWinkMark:msgid];
            [[ChatStorage sharedStorage] setMessageReadMark:msgid];
            
            [arrayInfo removeObjectForKey:key];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"update_messages" object:nil];
            
            [self save];
        }
    }

}

- (void) clear {
    
    [arrayInfo removeAllObjects];
}

@end
