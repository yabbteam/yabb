//
//  speedsipAppDelegate.h
//  speedsip
//
//  Created by adore on 09/07/10.
//  Modified by denebtech, 10.aug.11
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Accounts.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "About.h"
#import "Constants.h"
#import "RegisterViewController.h"
#import "MyPeoplePicker.h"
#import "TabBarViewController.h"
#import "Contacts.h"
#import "ChatRoomsViewController.h"

#import "CommonUtil.h"

#define SEND_TYPING_PERIOD 3.0f
#define KEEP_SHOW_TYPING_THRESHOLD 5.0f

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@interface NavigationWrapperViewController : UIViewController {
    UINavigationController *navigationController;
}
@property (nonatomic) UINavigationController *navigationController;
@end;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


@interface AppDelegate : NSObject
    <UIApplicationDelegate,
    ABPeoplePickerNavigationControllerDelegate,
//    ABNewPersonViewControllerDelegate,
    UITabBarControllerDelegate> 
{
    
    NSString                *DID;
    NSString                *IDD;
	NSString                *contactno;
	NSString                *sip;
	NSString                *strname;
	UIWindow                *window;
    NSString				*logFile;


	NSString				*configFile;
	NSMutableDictionary 	*configList;
	
	TabBarViewController		*tabBarController;
	
    RegisterViewController * register_view_controller;
    
	UINavigationController  *recentNavController;
	MyPeoplePicker  *contactsNavController;
	Accounts *acc;
	NavigationWrapperViewController  *settingsNavController;
	
	NSMutableArray			*sipAccount;
		
	NSString				*recentFile;
    NSMutableArray			*recentCalls;
	
	NSString                *compare;
	BOOL					 isMute;
	BOOL					 isHold;
	int						 currentNetwork;
    NSTimer                 *vibrateTimer;

	About                   *ab;
    
    BOOL                    _need_promo;
    NSString*                _needGoRoomId;
    
    @public
    Contacts                * contacts;
}

@property (nonatomic, strong) NSString *DID;
@property (nonatomic, strong) NSString *IDD;

@property (nonatomic,strong) NSString *strname;
@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet TabBarViewController		*tabBarController;

@property (nonatomic,strong) NSString *contactno;
@property (nonatomic, strong) IBOutlet RegisterViewController * register_view_controller;
@property (nonatomic, strong) ChatRoomsViewController *chatRoomsViewController;

@property (nonatomic, strong) IBOutlet Accounts *acc;
@property (nonatomic, strong) IBOutlet About *ab;

@property (nonatomic, strong) IBOutlet UINavigationController	*recentNavController;
@property (nonatomic, strong) IBOutlet NavigationWrapperViewController	*settingsNavController;


@property (nonatomic, strong) IBOutlet MyPeoplePicker	*contactsNavController;

@property (nonatomic, copy)   NSString            *logFile;
@property (nonatomic, strong) NSString			  *configFile;
@property (nonatomic, strong) NSMutableDictionary *configList;


@property (nonatomic, strong) NSMutableArray     *sipAccount;

@property (nonatomic, copy)   NSString           *recentFile;
@property (nonatomic, strong) NSMutableArray     *recentCalls;

@property (readwrite, assign) BOOL				  isMute;
@property (readwrite, assign) BOOL				  isHold;


@property (readwrite, assign) int				  currentNetwork;
@property (nonatomic, strong) NSTimer			 *vibrateTimer;

@property (nonatomic, assign) BOOL                  need_promo;
@property (nonatomic, strong) NSString*             needGoRoomId;
@property (nonatomic, assign) BOOL                  gettingAvatarFromFacebook;

@property (nonatomic, strong) NSDate*             lastSyncedAbookTime;
@property (nonatomic, assign) BOOL              needToSyncAbook;
@property (nonatomic, assign) BOOL              isForeground;
@property (nonatomic, assign) BOOL              isTempAlive;
@property (nonatomic, assign) BOOL              isRegistering;


//- (void) makeTabBarHidden:(BOOL)hide;
- (int) initiated;
- (void) initContacts;
- (void) readConfig;
- (void) registerSipAccount: (NSDictionary *) dict;
- (void) unregisterSipAccount;
//- (BOOL) isNetworkTypeWiFi;
- (void) setNewMinuteTimer: (int)interval;

- (void) registerGsmAccount: (NSDictionary *) dict;
- (void) saveFavNum:(NSString*)num_  withAlert:(BOOL)alert;
- (void) saveFavs;

@end

extern AppDelegate *app;
