//
//  ChatSoundSettingsViewController.h
//  Yabb
//
//  Created by denebtech on 10/10/13.
//
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface ChatSoundSettingsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSString* chat_id;

@end
