//
//  PeopleMultiselectorViewController.h
//  Baycall
//
//  Created by denebtech on 30.09.12.
//
//

#import <UIKit/UIKit.h>
#import "PeoplePickerViewController.h"

@protocol MultiselectorDelegate;

@interface PeopleMultiselectorViewController : UINavigationController <PeoplePickerDelegate, UIActionSheetDelegate> {
    
    PeoplePickerViewController * people_picker;
    UIBarButtonItem * done_button;

    NSMutableArray * room_phones;
    NSMutableArray * selected_group_abook;

    id <MultiselectorDelegate> __weak _multiselector_delegate;
}

@property (nonatomic, strong) id root_controller;
@property (nonatomic, strong) NSArray * preselected_phones;
@property (nonatomic, weak) id <MultiselectorDelegate> multiselector_delegate;
@property (nonatomic, strong) NSDictionary*    groupParam;

- (id)initWithRootViewController:(UIViewController *)rootViewController rightButtonText:(NSString*) rtext;

@end


@protocol MultiselectorDelegate <NSObject>

@optional

- (void) didDoneButton:(NSString *)abook;

@end
