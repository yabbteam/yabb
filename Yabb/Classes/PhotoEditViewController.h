//
//  PhotoEditViewController.h
//  Yabb
//
//  Created by denebtech on 2/27/14.
//
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "VPImageCropperViewController.h"

#define AVATAR_SIZE 128

@interface PhotoEditViewController : BaseViewController<VPImageCropperDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
    
    IBOutlet    UIImageView*    photoView;
    IBOutlet    UITableView*    buttonTableView;
    IBOutlet    UITextField*    textNickName;
}

@property (nonatomic, strong) UIImage* resultImage;
@property (nonatomic, strong) UIViewController* parentController;


@end
