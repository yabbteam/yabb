//
//  EmotionsView.m
//  speedsip
//
//  Created by denebtech on 19.04.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmotionsView.h"


@implementation EmotionsView

@synthesize delegate = _delegate;
@synthesize isLandscape;

- (id) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self->current_type = EMOTIONS_VIEW;
        currentPage = -1;

    }
    
    return self;
}

#define ICON_NUM           35
#define ANIMATION_NUM      39

#define ICON_SIZE           26//53

#define ICON_DISTANCE_X       18
#define ICON_DISTANCE_Y       12

#define ANIMATION_WIDTH     100
#define ANIMATION_HEIGHT    69
#define ANIMATION_DIST_X    5
#define ANIMATION_DIST_Y    5

- (void) Tap_Emotion:(id)sender {
    
    NSString * emo_key = [sender titleForState:UIControlStateReserved];
    
    if ([_delegate respondsToSelector:@selector(didSelectEmotion:)])
        [_delegate didSelectEmotion:emo_key];

    NSLog(@"Emotion key: %@", emo_key);
}

- (void) Tap_Animation:(id)sender {
    
    NSString * anim_key = [sender titleForState:UIControlStateReserved];
    
    if ([_delegate respondsToSelector:@selector(didSelectAnimation:)])
        [_delegate didSelectAnimation:anim_key];
    
    NSLog(@"Animation key: %@", anim_key);
}

+ (NSDictionary *) getEmoDictionary {

    NSDictionary *emo = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"amazed.png",@"amazed",
                         @"angel.png",@"angel",
                         @"angry.png",@"angry",
                         @"beaten.png",@"beaten",
                         @"bored.png",@"bored",
                         @"clown.png",@"clown",
                         @"confused.png",@"confused",
                         @"cool.png",@"cool",
                         @"cry.png",@"cry",
                         @"devil.png",@"devil",
                         @"doubtful.png",@"doubtful",
                         @"emo.png",@"emo",
                         @"frozen.png",@"frozen",
                         @"grin.png",@"grin",
                         @"indian.png",@"indian",
                         @"karate.png",@"karate",
                         @"kiss.png",@"kiss",
                         @"laugh.png",@"laugh",
                         @"love.png",@"love",
                         @"millionaire.png",@"millionaire",
                         @"nerd.png",@"nerd",
                         @"ninja.png",@"ninja",
                         @"party.png",@"party",
                         @"pirate.png",@"pirate",
                         @"punk.png",@"punk",
                         @"sad.png",@"sad",
                         @"santa.png",@"santa",
                         @"shy.png",@"shy",
                         @"sick.png",@"sick",
                         @"smile.png",@"smile",
                         @"speechless.png",@"speechless",
                         @"sweating.png",@"sweating",
                         @"tongue.png",@"tongue",
                         @"vampire.png",@"vampire",
                         @"wacky.png",@"wacky",
                         @"wink.png",@"wink",
                         nil];
    
/*
    
    NSDictionary *emo = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"angry.png",@"angry",
                         @"baloons.png",@"baloons",
                         @"beatenheart.png",@"beatenheart",
                         @"beer.png",@"beer",
                         @"bitten.png",@"bitten",
                         @"bitya.png",@"bitya",
                         @"bomb2.png",@"bomb2",
                         @"bomb.png",@"bomb",
                         @"boyhug.png",@"boyhug",
                         @"cake.png",@"cake",
                         @"callingphone.png",@"callingphone",
                         @"cat.png",@"cat",
                         @"cloud.png",@"cloud",
                         @"coctail.png",@"coctail",
                         @"cold.png",@"cold",
                         @"cool.png",@"cool",
                         @"cry2.png",@"cry2",
                         @"cry3.png",@"cry3",
                         @"cry.png",@"cry",
                         @"cup.png",@"cup",
                         @"cute.png",@"cute",
                         @"deadflower.png",@"deadflower",
                         @"dead.png",@"dead",
                         @"depressed.png",@"depressed",
                         @"disasterfully.png",@"disasterfully",
                         @"dog.png",@"dog",
                         @"dream.png",@"dream",
                         @"evil2.png",@"evil2",
                         @"evil.png",@"evil",
                         @"falleninlove.png",@"falleninlove",
                         @"fist.png",@"fist",
                         @"flower.png",@"flower",
                         @"flykiss.png",@"flykiss",
                         @"foo.png",@"foo",
                         @"gift.png",@"gift",
                         @"girlhug.png",@"girlhug",
                         @"glad.png",@"glad",
                         @"good.png",@"good",
                         @"hahaha.png",@"hahaha",
                         @"haha.png",@"haha",
                         @"handshake.png",@"handshake",
                         @"hat.png",@"hat",
                         @"heart.png",@"heart",
                         @"hello.png",@"hello",
                         @"hey.png",@"hey",
                         @"hm.png",@"hm",
                         @"hoho.png",@"hoho",
                         @"ill.png",@"ill",
                         @"interesting.png",@"interesting",
                         @"lamp2.png",@"lamp2",
                         @"lamp.png",@"lamp",
                         @"lipkiss.png",@"lipkiss",
                         @"lips.png",@"lips",
                         @"murder.png",@"murder",
                         @"music.png",@"music",
                         @"night.png",@"night",
                         @"no.png",@"no",
                         @"offened.png",@"offened",
                         @"oh2.png",@"oh2",
                         @"ohshit.png",@"ohshit",
                         @"ok.png",@"ok",
                         @"oops.png",@"oops",
                         @"ovation.png",@"ovation",
                         @"phone.png",@"phone",
                         @"pig.png",@"pig",
                         @"putup.png",@"putup",
                         @"question.png",@"question",
                         @"rainbow.png",@"rainbow",
                         @"rain.png",@"rain",
                         @"rice.png",@"rice",
                         @"sad2.png",@"sad2",
                         @"sad.png",@"sad",
                         @"saynothing.png",@"saynothing",
                         @"scary.png",@"scary",
                         @"seethis.png",@"seethis",
                         @"shh.png",@"shh",
                         @"shoes.png",@"shoes",
                         @"shy.png",@"shy",
                         @"sleep.png",@"sleep",
                         @"sleepy.png",@"sleepy",
                         @"sly.png",@"sly",
                         @"smarty.png",@"smarty",
                         @"smile.png",@"smile",
                         @"snail.png",@"snail",
                         @"stars.png",@"stars",
                         @"strain.png",@"strain",
                         @"sun.png",@"sun",
                         @"support.png",@"support",
                         @"tease.png",@"tease",
                         @"tricky.png",@"tricky",
                         @"veryangry.png",@"veryangry",
                         @"verycold.png",@"verycold",
                         @"veryevil.png",@"veryevil",
                         @"victory.png",@"victory",
                         @"vomit.png",@"vomit",
                         @"what.png",@"what",
                         @"wine.png",@"wine",
                         @"wink.png",@"wink",
                         @"wow2.png",@"wow2",
                         @"wow3.png",@"wow3",
                         @"wow.png",@"wow",
                            nil];*/
    return emo;
    
}

+ (NSString *) getAnimationString:(int)index {
    
    NSArray * anim = [NSArray arrayWithObjects:
                      @"2_glass_of_wine_",
                      @"Best_",
                      @"Booo_",
                      @"broken_heart_",
                      @"Burger_and_cola_",
                      @"Call_me_",
                      @"Car_",
                      @"Chicken_and_beer_",
                      @"Cinema_",
                      @"Coffee_and_cake_new",
                      @"Congrats_",
                      @"CoolNew_",
                      @"GhostNew_",
                      @"Good_Bye_",
                      @"Happy_birthday_",
                      @"Hello_",
                      @"Hugs_",
                      @"Ice_Cream_",
                      @"ILU_",
                      @"Kiss_",
                      @"Large_2_stage_cake_",
                      @"MoneyNew_",
                      @"Ok_",
                      @"Pizza_new",
                      @"RainNew_",
                      @"Shopping_",
                      @"SiniakNew_",
                      @"Sleep_",
                      @"SmokingNew_",
                      @"SnowNew_",
                      @"Sorry_",
                      @"StudyNew_",
                      @"Sun_with_small_cloudNew_",
                      @"TeaNew_",
                      @"Thank_you_",
                      @"ThunderstormNew_",
                      @"Victory_",
                      @"What_",
                      @"xmas_tree_",
                      nil];

    
    if (index >= [anim count])
        return nil;
    
    
    return [anim objectAtIndex:index];
    
}

- (NSString *) getFileNameFromIndex:(int)index {
    
    NSDictionary * emo = [EmotionsView getEmoDictionary];
    
    int counter = 0;
    for (NSString * key in emo) {
        
        if (counter == index)
            return [emo objectForKey:key];
        
        counter++;
    }
    
    return nil;
}

- (NSString *) getKeyFromIndex:(int)index {
    
    NSDictionary * emo = [EmotionsView getEmoDictionary];
    
    int counter = 0;
    for (NSString * key in emo) {
        
        if (counter == index)
            return key;
        
        counter++;
    }
    
    return nil;
}

- (void) Tap:(id)sender {
    
    UITapGestureRecognizer * recognizer = sender;
    
    int tag = recognizer.view.tag;
    
    NSString * emo_key = [self getKeyFromIndex:tag];
    
    if ([_delegate respondsToSelector:@selector(didSelectEmotion:)])
        [_delegate didSelectEmotion:emo_key];
    
    NSLog(@"Emotion key: %@", emo_key);    
}

- (void) initIcons {

    // Init smiles
    [[emotion_scroll_view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    int iconCols = emotion_scroll_view.frame.size.width / ((ICON_SIZE + ICON_DISTANCE_X));
    [emotion_scroll_view setContentSize:CGSizeMake((ICON_SIZE + ICON_DISTANCE_X) * iconCols, 206)];

    BOOL isBreak = NO;
    float marginLeft = (emotion_scroll_view.frame.size.width - iconCols * (ICON_SIZE + ICON_DISTANCE_X)) / 2;
    
    for (int row = 0; isBreak != YES; row++) {
        for (int col = 0; col < iconCols; col++) {
            
            if ((row * iconCols + col) >= ICON_NUM) {
                isBreak = YES;
                break;
            }
            
            int x = col * ICON_SIZE + col * ICON_DISTANCE_X + marginLeft;
            int y = row * ICON_SIZE + row * ICON_DISTANCE_Y + 14;
            
            NSString * image_name = [self getFileNameFromIndex:(row * iconCols + col)];
//            NSString * image_key = [self getKeyFromIndex:(row * ICON_COLS + col)];
            UIImage * image = [UIImage imageNamed:image_name];
            
            if (image == nil)
                continue;

            UIImageView * button = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, ICON_SIZE, ICON_SIZE)];

            [button setUserInteractionEnabled:YES];
            [button setImage:image];
            
//            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(x, y, ICON_SIZE, ICON_SIZE)];
//            [button setBackgroundImage:image forState:UIControlStateNormal];
//            [button setTitle:image_key forState:UIControlStateReserved];
//            [button addTarget:self action:@selector(Tap_Emotion:) forControlEvents:UIControlEventTouchUpInside];
            
            [emotion_scroll_view addSubview:button];
            [button setTag:(row * iconCols + col)];
            
//            [button setUserInteractionEnabled:YES];
            
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Tap:)];
            [button addGestureRecognizer:recognizer];
            
        }
    }
    
    // Init animations
    [[animation_scroll_view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    int animationCols = animation_scroll_view.frame.size.width / (ANIMATION_WIDTH + ANIMATION_DIST_X);
    int rowCount = (ANIMATION_NUM + animationCols - 1) / animationCols;
    [animation_scroll_view setContentSize:CGSizeMake(animation_scroll_view.frame.size.width, (ANIMATION_HEIGHT + ANIMATION_DIST_Y) * rowCount + 7 + 24)];
    marginLeft = (animation_scroll_view.frame.size.width - animationCols * (ANIMATION_WIDTH + ANIMATION_DIST_X)) / 2;
    
    isBreak = NO;
    for (int row = 0; isBreak != YES; row++) {
        
        for (int col = 0; col < animationCols; col++) {
            
            if ((row * animationCols + col) >= ANIMATION_NUM) {
                isBreak = YES;
                break;
            }
            
            int x = col * (ANIMATION_WIDTH + ANIMATION_DIST_X) + marginLeft;
            int y = row * (ANIMATION_HEIGHT + ANIMATION_DIST_Y) + 32;
            int anim_index = row * animationCols + col;
            
            NSString * image_prefix = [EmotionsView getAnimationString:anim_index];
            
            if (image_prefix == nil)
                continue;
            
            NSString * image_name = [NSString stringWithFormat:@"%@1.png", image_prefix];
            NSString * image_key = [NSString stringWithFormat:@"ANIM-%@", image_prefix];
            UIImage * image = [UIImage imageNamed:image_name];
            
            if (image == nil)
                continue;
            
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(x, y, ANIMATION_WIDTH, ANIMATION_HEIGHT)];
            [button setBackgroundImage:image forState:UIControlStateNormal];
            [button setTitle:image_key forState:UIControlStateReserved];
            [button addTarget:self action:@selector(Tap_Animation:) forControlEvents:UIControlEventTouchUpInside];
            
            [animation_scroll_view addSubview:button];
        }
    }
    
    parent_scroll_view.delegate = self;
    float total_width = [self getWidthOfPage:0]+[self getWidthOfPage:1]+[self getWidthOfPage:2];
    float total_height = parent_scroll_view.frame.size.height;
    [parent_scroll_view setContentSize:CGSizeMake(total_width, total_height)];
    parent_scroll_view.pagingEnabled = NO;
    
    if (currentPage < 0)
        [self setPage:1];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    monitorVelocity = decelerate;
    currentTime = [[NSDate date] timeIntervalSince1970];
    prevTime = [[NSDate date] timeIntervalSince1970];
    currentPosition = parent_scroll_view.contentOffset.x;
    prevPosition = parent_scroll_view.contentOffset.x;
    
    if(!decelerate) {
        int nPage = [self getPageNo];
        [self setPage:nPage];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (monitorVelocity) {
        currentTime = [[NSDate date] timeIntervalSince1970];
        currentPosition = parent_scroll_view.contentOffset.x;
        float velocity = abs((currentPosition - prevPosition) / (currentTime - prevTime));
        prevTime = currentTime;
        prevPosition = currentPosition;
        
        if (velocity > 10) {
            monitorVelocity = NO;
            CGPoint pt = parent_scroll_view.contentOffset;
            CGFloat virtual_width = parent_scroll_view.contentSize.width / 3;
            int nPage = (int)((pt.x + virtual_width/2) / virtual_width);
            [self setPage:nPage];
        }
    }
}

- (CGFloat) getWidthOfPage:(int)page {
    if (page == 0)
        return animation_scroll_view.frame.size.width;
    else if (page == 1)
        return button_view.frame.size.width;
    else if (page == 2)
        return emotion_scroll_view.frame.size.width;
    return 0;
}

- (void) setPage:(int)nPage {
    CGPoint pt = parent_scroll_view.contentOffset;
    if (nPage == 0)
        pt.x = 0;
    else if (nPage == 1)
        pt.x = [self getWidthOfPage:0] - ([self getWidthOfPage:0] - [self getWidthOfPage:1]) / 2;
    else
        pt.x = (parent_scroll_view.contentSize.width-parent_scroll_view.bounds.size.width);
    
    [parent_scroll_view setContentOffset:pt animated:YES];
    currentPage = nPage;
}

- (IBAction) onPhotoGalleryClicked:(id)sender {
    if ([_delegate respondsToSelector:@selector(didSelectPhotoFromGallery)])
        [_delegate didSelectPhotoFromGallery];
}

- (IBAction) onTakePictureClicked:(id)sender {
    if ([_delegate respondsToSelector:@selector(didSelectTakePicture)])
        [_delegate didSelectTakePicture];
}

- (IBAction) onPhotoWinkClicked:(id)sender {
    
    if ([_delegate respondsToSelector:@selector(didSelectPhotoWink)])
        [_delegate didSelectPhotoWink];
}

- (IBAction) onLocationClicked:(id)sender {
    if ([_delegate respondsToSelector:@selector(didSelectLocation)])
        [_delegate didSelectLocation];
}

- (CGFloat) getViewHeight {
    if (_isLandscape)
        return 162.0f;
    else
        return 216.0f;
}

- (void) prepareRotate {
    currentPage = [self getPageNo];
}

- (void) setIsLandscape:(BOOL)value {
    _isLandscape = value;
    
    CGRect rt = self.frame;
    rt.size.height = [self getViewHeight];
    self.frame = rt;
    [self setNeedsLayout];
    
    CGSize size = parent_scroll_view.contentSize;
    size.height = [self getViewHeight];
    parent_scroll_view.contentSize = size;
    [parent_scroll_view setNeedsLayout];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if (_isLandscape)
        screenWidth = [UIScreen mainScreen].bounds.size.height;

    CGFloat x = 0;
    
    rt = animation_scroll_view.frame;
    rt.size.width = screenWidth;
    animation_scroll_view.frame = rt;
    x += rt.size.width;
    [animation_scroll_view setNeedsLayout];
    
    rt = button_view.frame;
    rt.origin.x = x;
    if (!_isLandscape)
        rt.size.width = screenWidth - 80.0f;
    else
        rt.size.width = screenWidth * 2.8/4;

    button_view.frame = rt;
    x += rt.size.width;
    [button_view setNeedsLayout];
    
    rt = emotion_scroll_view.frame;
    rt.origin.x = x;
    rt.size.width = screenWidth;
    emotion_scroll_view.frame = rt;
    [emotion_scroll_view setNeedsLayout];
    
    //button position
    
    if (!_isLandscape) {
        CGPoint pt;
        pt = takeButton.center;
        pt.x = (button_view.bounds.size.width - takeButton.bounds.size.width*2)/3 + takeButton.bounds.size.width/2;
        pt.y = (button_view.bounds.size.height - takeButton.bounds.size.height*2)/3 + takeButton.bounds.size.height/2;
        takeButton.center = pt;
        
        pt = galleryButton.center;
        pt.x = (button_view.bounds.size.width - galleryButton.bounds.size.width*2)*2/3 + galleryButton.bounds.size.width*3/2;
        pt.y = (button_view.bounds.size.height - takeButton.bounds.size.height*2)/3 + galleryButton.bounds.size.height/2;
        galleryButton.center = pt;
        
        pt = winkButton.center;
        pt.x = (button_view.bounds.size.width - winkButton.bounds.size.width*2)/3 + winkButton.bounds.size.width/2;
        pt.y = (button_view.bounds.size.height - winkButton.bounds.size.height*2)*2/3 + winkButton.bounds.size.height*3/2;
        winkButton.center = pt;
        
        pt = locationButton.center;
        pt.x = (button_view.bounds.size.width - locationButton.bounds.size.width*2)*2/3 + locationButton.bounds.size.width*3/2;
        pt.y = (button_view.bounds.size.height - winkButton.bounds.size.height*2)*2/3 + winkButton.bounds.size.height*3/2;
        locationButton.center = pt;
    }
    else {
        
        CGPoint pt;
        pt = takeButton.center;
        pt.x = (button_view.bounds.size.width - takeButton.bounds.size.width*4)/5 + takeButton.bounds.size.width/2;
        pt.y = button_view.bounds.size.height / 2;
        takeButton.center = pt;
        
        pt = galleryButton.center;
        pt.x = (button_view.bounds.size.width - galleryButton.bounds.size.width*4)*2/5 + galleryButton.bounds.size.width*3/2;
        pt.y = button_view.bounds.size.height / 2;
        galleryButton.center = pt;
        
        pt = winkButton.center;
        pt.x = (button_view.bounds.size.width - winkButton.bounds.size.width*4)*3/5 + winkButton.bounds.size.width*5/2;
        pt.y = button_view.bounds.size.height / 2;
        winkButton.center = pt;

        pt = locationButton.center;
        pt.x = (button_view.bounds.size.width - locationButton.bounds.size.width*4)*4/5 + locationButton.bounds.size.width*7/2;
        pt.y = button_view.bounds.size.height / 2;
        locationButton.center = pt;
    }
    
    [self initIcons];
    
    [self setPage:currentPage];
}

- (int) getPageNo {
    CGFloat virtual_width = parent_scroll_view.contentSize.width / 3;
    int page = (int)((parent_scroll_view.contentOffset.x + virtual_width/2) / virtual_width);
    return page;
}

@end
