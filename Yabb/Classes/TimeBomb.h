//
//  TimeBomb.h
//  Yabb
//
//  Created by denebtech on 3/14/14.
//
//

#import <Foundation/Foundation.h>

@interface TimeBomb : NSObject;

+ (TimeBomb *) sharedInstance;

- (void) clear;
- (void) setMessageTimeout:(NSString*)msgid value:(int)seconds;
- (void) start:(NSString*)msgid;
- (int) getMessageTimeout:(NSString*)msgid;
- (BOOL) isTimeBombMessage:(NSString*)msgid;

@end
