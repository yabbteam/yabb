//
//  Accounts.h
//  speedsip
//
//  Created by denebtech on 09/06/11.
//  Modified by denebtech, 10.aug.11
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Accounts : UITableViewController
    <UIAlertViewDelegate,
    UITextFieldDelegate,
    UITableViewDelegate, 
    UITableViewDataSource>
{	
	NSMutableDictionary         *accountdict;
    NSMutableData *receivedData;
    NSMutableArray			*accArray;
    UIView *activityView;
    int ssec, srow;
}

@property (nonatomic, strong) UIView *activityView;
@property(nonatomic,strong)NSMutableDictionary *accountdict;
@property(nonatomic,strong)NSMutableArray  *accArray;

@property(nonatomic)int ssec, srow;

-(void)initAndFindByPhone:(NSString *)phone;
-(IBAction)done:(id)sender;
-(IBAction)cancel:(id)sender;

-(void)mybalance_;
-(int)check;
-(void)update:(int)i;
-(void)save;
-(void)saveLast:(NSString *)ph;
-(void)exit_;
-(void)keepcity;
-(void)loaddef;
-(BOOL)isPINless:(NSString *)txt;
-(BOOL)isPIN:(NSString *)pin;

-(void)showActivityViewer:(NSString *)title;
-(BOOL) isShowingActivityViewer;
-(void)hideActivityViewer;

@end
