//
//  Accounts.m
//  speedsip
//
//  Created by denebtech on 09/06/11.
//  Modified by denebtech, 10.aug.11
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Accounts.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ChatStorage.h"


@implementation Accounts
@synthesize accountdict;
@synthesize accArray;
@synthesize ssec, srow;
@synthesize activityView;


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/



-(int)check
{
//    NSLog(@"Country=%@",[[app.acc accountdict] objectForKey:GSM_COUNTRY_KEY]);
//    NSLog(@"City=%@",[[app.acc accountdict] objectForKey:GSM_CITY_KEY]);
//    NSLog(@"Phone=%@",[[app.acc accountdict] objectForKey:GSM_CITYPH_KEY]);
//    NSLog(@"PIN=%@",[accountdict objectForKey:SIP_USERNAME_KEY]);
    
    if([[accountdict objectForKey:SIP_USERNAME_KEY] isEqual:@""] ||
       [[accountdict objectForKey:GSM_CITYPH_KEY] isEqual:@""] ||
       ![self isPIN:[accountdict objectForKey:SIP_USERNAME_KEY]])
    {
//        NSLog(@"check:NOK");
        return 0;
    }
    
//    NSLog(@"check:OK");

    return 1;
}



-(IBAction)cancel:(id)sender
{
    [app readConfig]; // get saved data from the file...
    [self loaddef];
    NSLog(@"acc.after-read-config %@",accountdict);
    [self exit_];
}

-(void)exit_
{
//    [app makeTabBarHidden:NO];
}


-(void)loaddef
{
	if ([[app sipAccount] count])
    {
        accountdict = [[app sipAccount] objectAtIndex: 0];
        NSLog(@"load.def.1");
    }
	else 
    {
        
        
        NSString * pin = [[NSUserDefaults standardUserDefaults] objectForKey:@"pin"];
        
        if (pin == nil)
            pin = @"";
        
		accountdict =  [[NSMutableDictionary  alloc] initWithObjectsAndKeys :
						@"0", SIP_REGISTER_KEY,
						pin, SIP_USERNAME_KEY,
						@"", SIP_PASSWORD_KEY,
						@"", GSM_LAST_NUM_KEY, 
                        @"", GSM_PINLESS_KEY,
                        @"", GSM_CITY_KEY,
                        @"", GSM_IDD_KEY,
                        @"", GSM_COUNTRY_KEY,
                        @"", GSM_CITYPH_KEY,
                        @"", PIN_PREV_BAL_KEY,
                        @"", PIN_PREV_TIM_KEY,
						@"", PIN_CARD_KEY,
						@"", PIN_SERIAL_KEY,
						@"", PIN_DOMAIN_KEY,
						nil];
        NSLog(@"load.def.2");
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewWillAppear:(BOOL)animated
{  
    [super viewWillAppear:animated];

    NSLog(@"WillApp.acc");
    [self update:1];
}


- (void)viewDidLoad
{
    
    accArray = [[NSMutableArray alloc] init];
    
    NSLog(@"DidLoad.acc");
    [self loaddef];
    [self setTitle:@"Settings"];
    [self update:1];
}

- (void) viewDidUnload {
    [super viewDidUnload];

}


- (void)initAndFindByPhone:(NSString *)phone
{
    int c,r;
        
    if(![accArray count])
    {
        NSString *path;

        // Look in Documents
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"accessnums.plist"];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:path])
        {
            path= [[NSBundle mainBundle] pathForResource:@"accessnumbs" ofType:@"plist"];
            NSLog(@"accnum from BUNDLE: %@",path);
            if(![[NSFileManager defaultManager] fileExistsAtPath:path])
            {
                NSLog(@"File Not Found");
                exit(99);
            }
        }
        else
            NSLog(@"accnum from FILE: %@",path);

        [accArray addObjectsFromArray:[NSArray arrayWithContentsOfFile:path]];
        
        // tolian: why it crashes when release?
        // tolian!: POTOMU 4to alloca netu :)
        // [path release];
    }
        
    if(![phone isEqual:@""])
    {
//        NSLog(@"looking for city with ph# (%@)",phone);
        for(c=0;c<[accArray count];c++)
            for(r=0;r<[[[accArray objectAtIndex:c] objectForKey:@"Cities"] count];r++)
                if([[[[[accArray objectAtIndex:c] objectForKey:@"Cities"] objectAtIndex:r] objectForKey:@"Phone"] isEqual:phone])
                {
//                    NSLog(@"My FOUND select is %d x %d",c,r);
                    app.acc.srow = r;
                    app.acc.ssec = c;
                    return;
                }
    }

    app.acc.srow = -1;
    app.acc.ssec = -1;
}

-(BOOL)isPINless:(NSString *)txt
{
    return [txt isEqual:@"AU"];
}

-(BOOL)isPIN:(NSString *)pin
{
    // all ten symbols should be digits

    if(pin.length!=10 && pin.length!=11)
        return 0;
    
    unichar c;
    int e,i;
    
    for (e=0,i=0; i<[pin length]; i++ )
    {
        c = [pin characterAtIndex:i];        
        if (c>='0' && c<='9')
            e++;
    }

//    NSLog(@"isPIN(%@)=%d", pin, e);
    
    return e==pin.length;
}


-(void)update:(int)i
{
 /*   UIBarButtonItem *but;
    
    
    if([self check] && i!=2)
    {
        but = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
        self.navigationItem.rightBarButtonItem = but;
        [but release];
    }
    else
        self.navigationItem.rightBarButtonItem = nil;
        
    if([app initiated])
    {
        but = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
        self.navigationItem.leftBarButtonItem = but;
        [but release];
    }
    else
        self.navigationItem.leftBarButtonItem = nil;

        
    if(i!=2)
*/
    
    /*
    if([self check])
	{        
        [self save];
        [app registerSipAccount:accountdict];
    }
    else {
        
        [[app sipAccount] removeAllObjects];
        [app unregisterSipAccount];
        
        [[app sipAccount] writeToFile:[app configFile] atomically:YES];

    }*/

    [self.tableView reloadData];
}



-(void)saveLast:(NSString *)ph
{
    if(accountdict==nil)
        [self loaddef];
    [accountdict setValue:ph forKey:GSM_LAST_NUM_KEY];
    [self save];
}



-(void)save
{
    if(![self isPINless:[accountdict objectForKey:GSM_COUNTRY_KEY]])
        [accountdict setValue:@"" forKey:GSM_PINLESS_KEY];
	
    if (![[app sipAccount] count])
		[[app sipAccount] addObject:accountdict];
//    else
//        [[app sipAccount] replaceObjectAtIndex:0 withObject:accountdict];

    NSLog(@"Saved settings: %@",accountdict);

	[[app sipAccount] writeToFile:[app configFile] atomically:YES];
}



-(IBAction)done:(id)sender
{
    if([self check])
	{        
        [self save];
        [app registerSipAccount:accountdict];
        [self exit_];
    }
}


-(void)keepcity
{
    [accountdict setValue:[[accArray objectAtIndex:ssec] objectForKey:@"IDD"] forKey:GSM_IDD_KEY];
    [accountdict setValue:[[accArray objectAtIndex:ssec] objectForKey:@"Code"] forKey:GSM_COUNTRY_KEY];
    [accountdict setValue:[[[[accArray objectAtIndex:ssec] objectForKey:@"Cities"] objectAtIndex:srow] objectForKey:@"City"] forKey:GSM_CITY_KEY];    
    [accountdict setValue:[[[[accArray objectAtIndex:ssec] objectForKey:@"Cities"] objectAtIndex:srow] objectForKey:@"Phone"] forKey:GSM_CITYPH_KEY];
    
//    NSLog(@"keepcity.IDD=%@",[accountdict objectForKey:GSM_IDD_KEY]);    
//    NSLog(@"keepcity.Country=%@",[accountdict objectForKey:GSM_COUNTRY_KEY]);
//    NSLog(@"keepcity.City=%@",[accountdict objectForKey:GSM_CITY_KEY]);
//    NSLog(@"keepcity.Phone=%@",[accountdict objectForKey:GSM_CITYPH_KEY]);
    
    [self update:1];
    
    [self save];
    [app registerSipAccount:accountdict];
//    [self exit_];

}





// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{    
    int i = 1;
    
    if([self isPIN:[accountdict objectForKey:SIP_USERNAME_KEY]])
    {
        i++;
        if (![[accountdict objectForKey:PIN_CARD_KEY] isEqual:@""])
            i++;
    }
        
    return  i;
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==1)
        return NSLocalizedString(@"Balance", @"Balance");

    if(section==2)
        return NSLocalizedString(@"Phone Card", @"Phone Card");

    return @""; // 1st of any other section
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    // in case we already updated the balance..
    if(section==1 && ![[accountdict objectForKey:PIN_PREV_TIM_KEY] isEqual:@""])
        return NSLocalizedString(@"Tap on Balance to update", @"Tap on Balance to update");

    // in case we entered wrong PIN..
    if(section==0)
    {
        if(![self isPIN:[accountdict objectForKey:SIP_USERNAME_KEY]])
            return NSLocalizedString(@"Please enter 10 digit PIN", @"Please enter 10 digit PIN");
        
    }

    return @""; // 1st of any other section
}


#pragma mark UITableView datasource methods


// tell our table how many rows it will have, in our case the size of our menuList
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int i=0;

    if(section==0)
    {
        i++;
        if([self isPIN:[accountdict objectForKey:SIP_USERNAME_KEY]])
            i++;
        if([self isPINless:[accountdict objectForKey:GSM_COUNTRY_KEY]])
            i++;
    }
    else if(section==1)
    {
        i++;
        if(![[accountdict objectForKey:PIN_PREV_TIM_KEY] isEqual:@""])
            i++;
    }
    else if(section==2)
    {
        i+=3;
    }

    return i;
}

/// what's this? :(((
#pragma mark ControlEventTarget Actions

- (void) switchChanged:(id)sender {
    UISwitch* switchControl = sender;
    // PIN code
    if(switchControl.tag==2)
        [accountdict setValue:(switchControl.on?@"ON":@"") forKey:GSM_PINLESS_KEY];
    
    [self update:0];
}


#pragma mark <UITextFieldDelegate> Methods


// tolian: how to make "onBlur on the editing text-field?
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    NSLog(@"textFieldShouldBeginEditing: sender = %d, %@",  [textField tag], [textField text]);
//    [self update:2];
//    return YES;
//}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self textFieldShouldReturn:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // PIN code
    if(textField.tag==0)
    {
        if(![[accountdict objectForKey:SIP_USERNAME_KEY] isEqual:textField.text])
        {
            if(![[accountdict objectForKey:SIP_USERNAME_KEY] isEqual:@""])
            {
                [accountdict setValue:@"" forKey:PIN_PREV_BAL_KEY];
                [accountdict setValue:@"" forKey:PIN_PREV_TIM_KEY];
                [accountdict setValue:@"" forKey:PIN_CARD_KEY];
                [accountdict setValue:@"" forKey:PIN_SERIAL_KEY];
                [accountdict setValue:@"" forKey:PIN_DOMAIN_KEY];
            }
            [accountdict setValue:textField.text forKey:SIP_USERNAME_KEY];
            [self update:1];
        }
    }
    [textField resignFirstResponder];
    return YES;
}


// tell our table what kind of cell to use and its title for the given row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"hello: %d,%d",indexPath.row,indexPath.section);
    
    NSString *CellIdentifier = [ NSString stringWithFormat: @"%d:%d", [ indexPath indexAtPosition: 0 ], [ indexPath indexAtPosition:1 ]];
    
    UITableViewCell *cell = nil; //[ tableView dequeueReusableCellWithIdentifier: CellIdentifier];

    // tolian: is it ok to redraw so small table? :)
    if (1 || cell == nil) {
//        cell = [ [ [ UITableViewCell alloc ] initWithFrame: CGRectZero reuseIdentifier: CellIdentifier] autorelease ];
        cell = [ [ UITableViewCell alloc ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        switch (indexPath.section) {
            case 0: // pin + pinless + city
                //
                switch (indexPath.row) {
                    case 0: // pin
                    {
                        UITextField *fld = [ [ UITextField alloc ] initWithFrame: CGRectMake(100, 10, 185, 38) ];
                        fld.text = [accountdict objectForKey:SIP_USERNAME_KEY];
                        fld.textAlignment = NSTextAlignmentRight;
                        fld.clearButtonMode = UITextFieldViewModeNever;
                        fld.secureTextEntry = YES;
                        [cell addSubview:fld];
                        
                        [fld setEnabled:YES];
                        fld.tag = 0;
                        fld.delegate = self;
                        cell.textLabel.text = NSLocalizedString(@"PIN", @"PIN");
                    }
                        break;
                    case 1: // city
                    {
                        UITextField *fld = [ [ UITextField alloc ] initWithFrame: CGRectMake(100, 10, 185, 38) ];
                        if([[accountdict objectForKey:GSM_CITY_KEY] isEqual:@""])
                            fld.text = @"";
                        else
                            fld.text = [[NSString alloc] initWithFormat:@"%@, %@",[accountdict objectForKey:GSM_CITY_KEY],[accountdict objectForKey:GSM_COUNTRY_KEY]];
                        
                        fld.textAlignment = UITextAlignmentRight;
                        [cell addSubview:fld];
                        
                        [fld setEnabled:NO];
                        fld.tag = 1;
                        fld.delegate = self;
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; // sign > at the right side of cell
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue; // change color on tap
                        cell.textLabel.text = [[accountdict objectForKey:GSM_CITY_KEY] isEqual:@""] ? NSLocalizedString(@"Select city", @"Select city") : NSLocalizedString(@"City", @"City");
                    }
                        break;                    
                    case 2: // pinless
                    {
                        UISwitch *fld = [ [ UISwitch alloc ] initWithFrame: CGRectMake(200, 10, 0, 0) ];
                        fld.on = [[accountdict objectForKey:GSM_PINLESS_KEY] isEqual:@"ON"];
                        fld.tag = 2;
                        [fld addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                        [cell addSubview: fld];
                        cell.textLabel.text = NSLocalizedString(@"PINless", @"PINless"); // OS3
                    }   
                        break;
                    default:
                        break;
                }
                //
                break;
            case 1: // balance + balance check date
                //
                switch (indexPath.row) {
                    case 0: // balance
                    {
                        UITextField *fld = [ [ UITextField alloc ] initWithFrame: CGRectMake(100, 10, 185, 38) ];
                        fld.text = [accountdict objectForKey:PIN_PREV_BAL_KEY];
                        fld.textAlignment = UITextAlignmentRight;
                        [cell addSubview:fld];
                        
                        [fld setEnabled:NO];
                        fld.tag = 10;
                        fld.delegate = self;
                        
                        if([[ChatStorage sharedStorage] connectedToNetwork])
                            cell.selectionStyle = UITableViewCellSelectionStyleBlue; // change color on tap
                        
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator; // sign > at the right side of cell                        
                        cell.textLabel.text = [[accountdict objectForKey:PIN_PREV_BAL_KEY] isEqual:@""] ? NSLocalizedString(@"Get info", @"Get info") : NSLocalizedString(@"Balance", @"Balance") ;
                    }
                        break;
                    case 1: // balance date
                    {
                        UITextField *fld = [ [ UITextField alloc ] initWithFrame: CGRectMake(100, 10, 185, 38) ];
                        fld.text = [accountdict objectForKey:PIN_PREV_TIM_KEY];
                        fld.textAlignment = UITextAlignmentRight;
                        [cell addSubview:fld];
                        
                        [fld setEnabled:NO];
                        fld.tag = 10+indexPath.row;
                        cell.textLabel.text = NSLocalizedString(@"Checked", @"Checked");
                    }
                        break;

                    default:
                        break;
                }
                //
                break;
            case 2: // phone card info
            {

                UITextField *fld = [ [ UITextField alloc ] initWithFrame: CGRectMake(100, 10, 185, 38) ];

                switch (indexPath.row) {
                    case 0: // product
                    {
                        fld.text = [accountdict objectForKey:PIN_CARD_KEY];
                        cell.textLabel.text = NSLocalizedString(@"Title", @"Title");
                    }
                        break;
                    case 1: // serial
                    {
                        fld.text = [accountdict objectForKey:PIN_SERIAL_KEY];
                        cell.textLabel.text = NSLocalizedString(@"Serial#", @"Serial#");
                    }
                        break;
                    case 2: // domain
                    {
                        fld.text = [accountdict objectForKey:PIN_DOMAIN_KEY];
                        cell.textLabel.text = @" ";
                    }
                        break;                        
                    default:
                        break;
                }
                
                // tolian: condition is looked ugly. is it ok? ok!
                if ( ![cell.textLabel.text isEqual:@""])
                {
                    fld.textAlignment = UITextAlignmentRight;
                    [fld setEnabled:NO];
                    fld.tag = 20+indexPath.row;
                    [cell addSubview:fld];
                }
                

            }
                   //
                break;

            default:
                break;
        }
}
    
    return cell;
}


//  Invoked by the table view when user taps on a row.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // city selection
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// tolian: what's abt the collator???
/*
 <Warning>: dowload url... http://www.phonecardpoint.com.au/t2.php?pin=61280144594
 <Warning>: Succeeded! Received 15 bytes of data
 <Warning>: tmpTOC: [Balance|A$-1.15]
 <Warning>: tmpArr: [Balance -- A$-1.15]
 <Warning>: WARNING! Creating precompiled collator because collator is out of date. This is expensive!
 */



#pragma mark Bal.Act.Ind

-(void)hideActivityViewer
{
    [[[activityView subviews] objectAtIndex:2] stopAnimating];
    [activityView removeFromSuperview];
    activityView = nil;
}

-(BOOL) isShowingActivityViewer {
    
    return activityView != nil;
}

-(void)showActivityViewer:(NSString *)title
{
    CGRect rect = [[UIScreen mainScreen] bounds];
    activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, rect.size.width, rect.size.height)];
    [activityView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.4]];
    
    UIFont* font = [UIFont systemFontOfSize:15];
    CGSize expected = [CommonUtil getTextSize:title font:font constrainedToSize:CGSizeMake(140, 500) lineBreakMode:UILineBreakModeWordWrap];
    
    UIButton *btnBg = [[UIButton alloc] initWithFrame: CGRectMake(rect.size.width / 2 - 70, rect.size.height / 2 - 40, 140, 60+expected.height)];
    [btnBg setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.6]];
    
    
    [[btnBg layer] setCornerRadius:8.0f];
//    [[btnBg layer] setMasksToBounds:YES];
//    [[btnBg layer] setBorderWidth:1.0f];
//    [[btnBg layer] setBackgroundColor:[[UIColor redColor] CGColor]];

    [activityView addSubview:btnBg];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(rect.size.width / 2 - 70, rect.size.height / 2 + 15, 140, expected.height)];
    [lbl setFont:font];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setText:title];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setNumberOfLines:3];
    [activityView addSubview:lbl];
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(rect.size.width / 2 - 19, rect.size.height / 2 - 19 - 10, 38, 38)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [activityView addSubview:activityWheel];
    
    
    
    [app.window addSubview: activityView];
    
    [[[activityView subviews] objectAtIndex:2] startAnimating];
}


-(void)mybalance_
{
    if (![self isPIN:[accountdict objectForKey:SIP_USERNAME_KEY]])
        return;
    
    if (![[ChatStorage sharedStorage] connectedToNetwork])
    {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No connection", @"No connection")
                                                      message:NSLocalizedString(@"You Are Not Connected To Any Network", @"You Are Not Connected To Any Network")
                                                     delegate:nil 
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
		[alert show];
        return;
	}
    

    NSString *url = [[NSString alloc] initWithFormat:@"http://www.phonecardpoint.com.au/t2.php?pin=%@",[accountdict objectForKey:SIP_USERNAME_KEY]];
    
    NSLog(@"download url... %@",url);
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString: url]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if (theConnection)
    {
///
/*
        modalDialogViewController *modalController = [[modalDialogViewController alloc]
                                                      initWithNibName:@"modalDialogView" bundle:nil];  
        [self presentModalViewController:modalController animated:YES];
        [modalController release];
*/
///
        [self showActivityViewer:NSLocalizedString(@"Checking...", @"Checking...")];
        receivedData = [NSMutableData data];
    }
    else
    {
        UIAlertView * alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connection Failure", @"Connection Failure")
                                                       message:NSLocalizedString(@"Unable to get your balance", @"Unable to get your balance")
                                                      delegate:nil 
                                             cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                             otherButtonTitles:nil];
        [alert show];
    }  
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{    
    [receivedData setLength:0];
    return;
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideActivityViewer];
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self hideActivityViewer];
    
    NSString *tmpTOC = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSArray *tmpArr = [tmpTOC componentsSeparatedByString:@"|"];

    NSLog(@"tmpTOC: [%@] = %@",tmpTOC,tmpArr);    
    
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    NSString *time = [outputFormatter stringFromDate:[NSDate date]];

    [accountdict setValue:[NSString stringWithFormat:@"%@",time] forKey:PIN_PREV_TIM_KEY];   
    [accountdict setValue:[NSString stringWithString:[tmpArr objectAtIndex:1]] forKey:PIN_PREV_BAL_KEY];
    [accountdict setValue:[NSString stringWithString:[tmpArr objectAtIndex:2]] forKey:PIN_CARD_KEY];   
    [accountdict setValue:[NSString stringWithString:[tmpArr objectAtIndex:3]] forKey:PIN_SERIAL_KEY];   
    [accountdict setValue:[NSString stringWithString:[tmpArr objectAtIndex:4]] forKey:PIN_DOMAIN_KEY];   
    
    [self update:10];

    UIAlertView * alert=[[UIAlertView alloc] initWithTitle:(NSString *)[tmpArr objectAtIndex:0] 
                                                   message:(NSString *)[tmpArr objectAtIndex:1]
                                                  delegate:nil 
                                         cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                                         otherButtonTitles:nil];
    [alert show];
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


@end
