//
//  MyPeoplePicker.h
//  speedsip
//
//  Created by denebtech on 04.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeoplePickerViewController.h"

@interface MyPeoplePicker : UINavigationController <PeoplePickerDelegate>

@property (nonatomic, strong) IBOutlet PeoplePickerViewController * people_picker;

@end
