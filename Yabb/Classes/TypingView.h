//
//  TypingView.h
//  Baycall
//
//  Created by denebtech on 09.11.12.
//
//

#import <UIKit/UIKit.h>

#define TYPING_VIEW_HEIGHT  15

@interface TypingView : UIView {
    
    IBOutlet UILabel * text_label;
    IBOutlet UIImageView * pencil_image;
    BOOL _typing;
}

@property (nonatomic, assign) BOOL typing;

- (void) clearText;
- (void) setText:(NSString *)str atPosition:(CGPoint)position;
- (void) update:(CGPoint) position;

@end
