//
//  ImageDownloadViewController.h
//  speedsip
//
//  Created by Tolian T on 06.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareImage.h"
#import "ChatRoomsViewController.h"
#import "BaseViewController.h"
#import "PZPhotoView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ImageDownloadViewController : BaseViewController <UIActionSheetDelegate, ShareImageDelegate, PeoplePickerDelegate, ChatRoomsViewControllerDelegate, PZPhotoViewDelegate> {
    
    IBOutlet UIActivityIndicatorView * activity;
//    IBOutlet UIView * main_view;
    
    IBOutlet UIToolbar * tool_bar;
    IBOutlet UIPageControl* page_control;
    IBOutlet UIBarButtonItem*   itemAction;
    IBOutlet UIBarButtonItem*   itemDelete;
    IBOutlet PZPhotoView*       source_view;
    IBOutlet UIButton*          buttonVideoPlay;
    IBOutlet UILabel*          labelProgress;
    
    unsigned long long          sizeDownloaded;
    
    NSString * _server_id;
    NSString * _chat_id;
    BOOL        _is_video;
    
    __strong UIImage*    currentImageThumb;
    __strong NSURL*    currentMediaURL;
    __strong MPMoviePlayerViewController *moviePlayController;
    
    NSArray*    array_server_ids;
    
    int         nTimerCount;
    
    BOOL        isTaped;
    NSInteger   _orientation;
    CGRect      _myRect;
    
    BOOL        disableRotation;
    
    BOOL        isPlayingVideo;
}

@property (nonatomic, copy) NSString * server_id;
@property (nonatomic, copy) NSString * chat_id;
@property (nonatomic, strong) UIViewController * source_controller;
@property (nonatomic, assign) BOOL  is_owner;
@property (nonatomic, strong) UILabel* labelCountDown;

@end
