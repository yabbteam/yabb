    //
//  ConnectionQueue.m
//  speedsip
//
//  Created by denebtech on 09.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConnectionQueue.h"
#import "Alert.h"
#import <mach/mach_time.h>
#import "OpenUDID.h"
#import "KeychainItemWrapper.h"
#import <Security/SecItem.h>
#import "JabberLayer.h"

#import <TestFlight.h>


#define RANDOM_SEED()		srandom((unsigned)(mach_absolute_time() & 0xFFFFFFFF))
#define RANDOM_FLOAT		((float)random() / (float)INT32_MAX)

@implementation ConnectionQueue

static ConnectionQueue * connection_queue;

+ (ConnectionQueue *) sharedQueue {
    
    if (connection_queue == nil) {

        connection_queue = [ConnectionQueue alloc];
        
        // Init
        connection_queue->connections = [[NSMutableArray alloc] init];
        connection_queue->cycle_period = 5.0;
        connection_queue->cycle_delegate = nil;
        connection_queue->cycle_time = 0;
        connection_queue->cycle_params = nil;
        connection_queue->cycle_forced = NO;
        connection_queue->cycle_silent = YES;
        connection_queue->busy = NO;
        connection_queue->current_connection = nil;
        
        [connection_queue performSelector:@selector(Process) withObject:nil afterDelay:1.0];
        
        TFLog(@"API endpoint: %@", API_ENDPOINT);
    }
    
    return connection_queue;
}


- (void) forceCycle {
    
    cycle_forced = YES;
    [self performSelector:@selector(Process)];

}

- (void) setCycleSilent:(BOOL)silent {
    cycle_silent = silent;
}

- (void) setBusy:(BOOL)b {
    
    busy = b;
}

- (void) stop {
    
    
    if (busy)
        [active_connection cancel];
    
    [connections removeAllObjects];

    if (data)
        data = nil;

    cycle_delegate = nil;
    
    busy = NO;
}

- (void) startConnectionCycle:(id)delegate withPeriod:(float)period { // Set up "Lazy" connection
    
    cycle_period = period;
    cycle_delegate = delegate;
    [[JabberLayer SharedInstance] registerConnectionDelegate:delegate name:@"post"];
    
    if (cycle_params)
        cycle_params = nil;
}


- (void) Add_Connection:(id)target withParams:(NSDictionary *)params withName:(NSString *)name silent:(BOOL)silent {
    
    if ([[JabberLayer SharedInstance] processThis:params name:name delegate:target silent:silent]) {
        return;
    }

    [self Add_Connection_To_REST:target withParams:params withName:name silent:silent];
}

- (void) Add_Connection_To_REST:(id)target withParams:(NSDictionary *)params withName:(NSString *)name silent:(BOOL)silent {
    
    NSDictionary * connection_dict = [NSDictionary dictionaryWithObjectsAndKeys:params, @"params", target, @"target", name, @"connection_name", [NSNumber numberWithBool:silent], @"silent", nil];
    [connections addObject:connection_dict];
}


- (void) Process {
    
    if (busy == NO) { // No active connections

        if ([connections count]) {  // Has connection in queue (first priority)
        
            NSDictionary * connection = [connections objectAtIndex:0];

            if (current_connection)
                current_connection = nil;
            
            current_connection = [[NSDictionary alloc] initWithDictionary:connection];
            
            [self Open_URL];

            [connections removeObjectAtIndex:0];
        }
        else if (cycle_delegate != nil) {   // Lazy chat connection
            
            if ([[NSDate date] timeIntervalSince1970] - cycle_time > cycle_period || cycle_forced) {

                cycle_forced = NO;
                
                if (current_connection)
                    current_connection = nil;

                if ([cycle_delegate respondsToSelector:@selector(processPostData)])
                    [cycle_delegate performSelector:@selector(processPostData)];
                
                cycle_time = [[NSDate date] timeIntervalSince1970];
                cycle_silent = YES;
            }
            
        }
        
    }
    [self performSelector:@selector(Process) withObject:nil afterDelay:0.2];
}


- (NSString *) urlEncode:(NSString *)str {
    
    
    return [[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
}

#pragma mark -
#pragma mark Connection

- (NSString*) urlEncodedString:(NSDictionary*)dict {
	
    if ([dict count] == 0)
        return nil;
    
	NSMutableArray *parts = [NSMutableArray array];
    
	for (NSString * key in dict) {
		NSString * value = [dict objectForKey: key];
		NSString * part = [NSString stringWithFormat: @"%@=%@", [self urlEncode:key], [self urlEncode:value]];
		[parts addObject: part];
	}
    
	return [parts componentsJoinedByString: @"&"];
}

- (NSString *) CR_string:(NSString *)str
{
    RANDOM_SEED();
    
	NSMutableString * crypted = [[NSMutableString alloc] init];
    NSMutableString * key = [[NSMutableString alloc] init];
    
	if (str == NULL)
        return nil;
    else {
        for (int i = 0; i < [str length]; i+=2) {
            unsigned short rand = RANDOM_FLOAT * 0xEF + 0x10;
            
            [key appendFormat:@"%02x", rand];
            
            NSString * byte = [str substringWithRange:NSMakeRange(i, 2)];
            unsigned short	chr;
            sscanf([byte cStringUsingEncoding:NSUTF8StringEncoding], "%hx", &chr);

            chr ^= rand;
            [crypted appendString:[NSString stringWithFormat:@"%02x", chr]];
        }
	}
    
    [crypted appendString:key];
    
	return [crypted uppercaseString];
}

- (NSString *)secureUDID {
    
    
    
//    NSString * udid = [[UIDevice currentDevice].uniqueIdentifier stringByReplacingOccurrencesOfString:@"-" withString:@""];

//    return [self CR_string:[OpenUDID value]];
    
    return [self CR_string:[self udidUsingCFUUID]];
}

- (NSString *)securePass {
    
    NSString * user_pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_pass"];

    return [self CR_string:user_pass];
}

- (void) Open_URL {
    
    //NSLog(@"Sending data...");
    
    NSString * strParams = nil;
	NSData * myRequestData = nil;
    id target = [current_connection objectForKey:@"target"];
    NSDictionary * params = [current_connection objectForKey:@"params"];
    
    if (target == nil || params == nil)
        return;
    
//    NSLog(@"Open url: %@", HOST);
    
    if (params) {
        strParams = [self urlEncodedString:params];
        myRequestData = [ NSData dataWithBytes: [ strParams UTF8String ] length: [ strParams length ] ];
	}
	
	if (active_connection) {
		active_connection = NULL;
	}
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[ NSURL URLWithString:API_ENDPOINT ] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:300.0];

    NSString * sUDID = [self secureUDID];
    NSString * sPass = [self securePass];
    
	[ request setHTTPMethod: @"POST" ];
	[ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type" ];
    
    if (sUDID)
        [ request setValue:sUDID forHTTPHeaderField:@"X-Model"];
    if (sPass)
        [ request setValue:sPass forHTTPHeaderField:@"X-Powered-By"];
    
    if (myRequestData)
        [ request setHTTPBody: myRequestData ];
    
    busy = YES;
	active_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
}

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData
{
    if (data == NULL)
		data = [[NSMutableData alloc] initWithCapacity:BUFFER_SIZE_KB * 1024];
    
    [data appendData:incrementalData];
}

- (void) Connection_Failed {

    id target = [current_connection objectForKey:@"target"];
    NSString * connection_name = [current_connection objectForKey:@"connection_name"];

    if (target && [target respondsToSelector:@selector(didConnectionFailed:)])
        [target performSelector:@selector(didConnectionFailed:) withObject:connection_name];

    if (data)
        data = nil;

}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {

    //NSLog(@"Response received");
    
	busy = FALSE;
    
    id target = [current_connection objectForKey:@"target"];
    NSString * connection_name = [current_connection objectForKey:@"connection_name"];

//    NSLog(@"Connection Done: %@", connection_name);

    if (data == nil) {        
        [Alert show:NSLocalizedString(@"Connection Error", @"Connection Error") withMessage:NSLocalizedString(@"Empty request.", @"Empty request.")];
        [self Connection_Failed];
        return;
        
    }
    
    // All data in XML
    NSString *errorStr = nil;
    NSPropertyListFormat format; 
    
    NSDictionary *response = [NSPropertyListSerialization propertyListFromData: data
                                                          mutabilityOption: NSPropertyListImmutable
                                                          format: &format
                                                          errorDescription: &errorStr];
    
    if (errorStr) {
//        [Alert show:@"Parse Error" withMessage:@""];
        [self Connection_Failed];
        return;
    }
    
    if (format != kCFPropertyListXMLFormat_v1_0) {
//        [Alert show:@"Parse Error" withMessage:@"Unknown data format."];
        [self Connection_Failed];
        return;
    }

    NSNumber * error_code = [response objectForKey:@"errorcode"];
    if (error_code == nil) {
        [Alert show:@"Parse Error" withMessage:@"Can't get error code."];
        [self Connection_Failed];
        return;
    }
    
    if (target && [target respondsToSelector:@selector(didConnectionDone:withName:)])
        [target performSelector:@selector(didConnectionDone:withName:) withObject:response withObject:connection_name];
    else
        NSLog(@"Required 'didConnectionDone:withName:' for connection delegate");
    
    if (data)
        data = nil;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    NSLog(@"Connection failed");

	busy = NO;
    
    [connection cancel];

    BOOL silent = [[current_connection objectForKey:@"silent"] boolValue];
    
    if (silent == NO) {
        [Alert show:NSLocalizedString(@"Error", @"Error") withMessage:NSLocalizedString(@"Registration failed. Check Internet connection and try again",
                                                                                                              @"Registration failed. Check Internet connection and try again")];
    }
    
    [self Connection_Failed];
    
}

- (NSString*) udidUsingCFUUID
{
    // initialize keychaing item for saving UUID.
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UUID"
                                                                       accessGroup:nil];
    
    NSString *uuid = [wrapper objectForKey:(__bridge id)kSecAttrAccount];
    if( uuid == nil || uuid.length == 0)
    {
        
        // if there is not UUID in keychain, make UUID and save it.
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
        CFRelease(uuidRef);
        uuid = [NSString stringWithString:(__bridge NSString *) uuidStringRef];
        CFRelease(uuidStringRef);
        
        // save UUID in keychain
        [wrapper setObject:uuid forKey:(__bridge id)kSecAttrAccount];
    }
    
    //return uuid;
    return [[uuid stringByReplacingOccurrencesOfString:@"-" withString:@""] lowercaseString];
}
@end
