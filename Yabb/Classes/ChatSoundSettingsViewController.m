//
//  SettingsViewController.m
//  Yabb
//
//  Created by denebtechsaplin on 10/10/13.
//
//

#import <TPKeyboardAvoidingTableView.h>

#import "ChatSoundSettingsViewController.h"
#import "SettingsManager.h"
#import "ConnectionQueue.h"
#import "AppDelegate.h"
#import "ChatStorage.h"

@interface ChatSoundSettingsViewController ()

@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (strong, nonatomic) IBOutlet UITableViewCell *inAppVibrateCell;
@property (strong, nonatomic) IBOutlet UISwitch *inAppVibrateSwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *inAppSoundsCell;
@property (strong, nonatomic) IBOutlet UISwitch *inAppSoundsSwitch;

@end

@implementation ChatSoundSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"Sound & Vibration", @"Sound & Vibration");
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;

    [self customizeAppearance];

    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadData];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self saveData];

    [app.chatRoomsViewController sendSoundSettingToServer];
}

- (void)customizeAppearance
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
    } else {
    }
}

- (void)loadData
{
    if (![[SettingsManager sharedInstance] enableSounds]) {
        self.inAppSoundsSwitch.on = NO;
        self.inAppSoundsSwitch.enabled = NO;
    }
    else {
        self.inAppSoundsSwitch.on = [[ChatStorage sharedStorage] getChatSoundSetting:self.chat_id];
    }
    
    if (![[SettingsManager sharedInstance] enableVibration]) {
        self.inAppVibrateSwitch.on = NO;
        self.inAppVibrateSwitch.enabled = NO;
    }
    else {
        self.inAppVibrateSwitch.on = [[ChatStorage sharedStorage] getChatVibrateSetting:self.chat_id];
    }
}

- (void) saveData
{
    if (self.inAppSoundsSwitch.enabled)
        [[ChatStorage sharedStorage] setChatSoundSetting:self.chat_id setting:self.inAppSoundsSwitch.on];
    if (self.inAppVibrateSwitch.enabled)
        [[ChatStorage sharedStorage] setChatVibrateSetting:self.chat_id setting:self.inAppVibrateSwitch.on];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: return 2;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if (self.inAppSoundsSwitch.enabled && self.inAppVibrateSwitch.enabled)
        return nil;
    
    return @"To use these settings please Enable Sound & Vibration (All Chats) in More -> Settings";
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0: return self.inAppSoundsCell;
                case 1: return self.inAppVibrateCell;
            }
    }
    return nil;
}

- (IBAction)onChangeSoundValue:(id)sender {
    [self saveData];
}

- (IBAction)onChangeVibrationValue:(id)sender {
    [self saveData];
}

@end
