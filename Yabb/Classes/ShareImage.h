//
//  ShareImage.h
//  speedsip
//
//  Created by denebtech on 04.09.12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Facebook.h>
#import "PeoplePickerViewController.h"

@protocol ShareImageDelegate;

@interface ShareImage : NSObject <UIActionSheetDelegate, FBSessionDelegate, FBRequestDelegate, FBDialogDelegate, MFMailComposeViewControllerDelegate> {
    
    UIActionSheet * actSheet;
    UIViewController * _root_controller;
    UIImage * _image;
    NSString* _message_id;
    NSString* _chat_id;
    
    Facebook * facebook;
    BOOL isConnected;
    
    id <ShareImageDelegate> __weak _share_delegate;
    
    int currentAPICall;
}

@property (nonatomic, strong) UIViewController * root_controller;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) NSString* message_id;
@property (nonatomic, strong) NSString* chat_id;
@property (strong, nonatomic) Facebook *facebook;
@property (nonatomic) BOOL isConnected;
@property (nonatomic, weak) id <ShareImageDelegate> share_delegate;
@property (nonatomic) BOOL isCanSaveGallery;

@property (strong, nonatomic) FBSession *session;

+ (ShareImage *) SharedImage;
- (void) showInView:(UIView *) view;

@end

@protocol ShareImageDelegate <NSObject>

@required
- (void) ForwardToFriend;
- (void) SelectActionSheet;

@end


