//
//  SeverStateView.m
//  Yabb
//
//  Created by denebtech on 2/17/14.
//
//

#import "ServerStateView.h"
#import "JabberLayer.h"

@implementation ServerStateView

- (id) init {
    return [self initWithFrame:CGRectMake(0, 0, 30, 30)];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_online.png"]];
        imageView.frame = frame;//CGRectMake(frame.size.width*1/5, 0, frame.size.width*3/5, frame.size.height*3/5);
        [self addSubview:imageView];
        stateIcon = imageView;
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height*3.5/5, frame.size.width, frame.size.height*2/5)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica" size:10];
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];
        stateLabel = label;
        stateLabel.hidden = YES;
        
        [self setState:[[JabberLayer SharedInstance] isConnectedServer]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serverConnected) name:SERVER_STATE_CONNECTED object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serverDisconnected) name:SERVER_STATE_DISCONNECTED object:nil];
        
        checkTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(checkTimerTick) userInfo:nil repeats:YES];
    }
    return self;
}

- (void) dealloc {
    
    [checkTimer invalidate];
    checkTimer = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) setState:(BOOL)active {
    if (active) {
        stateLabel.text = NSLocalizedString(@"Online", @"Online");
        stateIcon.image = [UIImage imageNamed:@"user_online.png"];
    }
    else {
        stateLabel.text = NSLocalizedString(@"Offline", @"Offline");
        stateIcon.image = [UIImage imageNamed:@"user_offline.png"];
    }
    [stateIcon sizeToFit];
}

- (void) serverConnected {
    if ([[JabberLayer SharedInstance] isConnectedServer])
        [self setState:YES];
    else
        [self setState:NO];
}

- (void) serverDisconnected {
    if ([[JabberLayer SharedInstance] isConnectedServer])
        [self setState:YES];
    else
        [self setState:NO];
}

- (void) checkTimerTick {
    
    if ([[JabberLayer SharedInstance] isConnectedServer])
        [self setState:YES];
    else
        [self setState:NO];
}

@end
