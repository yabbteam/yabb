//
//  InvitedFriends.m
//  Baycall
//
//  Created by denebtech on 02.10.12.
//
//

#import "InvitedFriends.h"

static InvitedFriends * friends = nil;

@implementation InvitedFriends

@synthesize height = _height;

+ (InvitedFriends *) SharedFriends {
    
    if (friends == nil) {
        friends = [InvitedFriends alloc];
        
        friends->invited_persons = [[NSMutableArray alloc] init];
        friends->m_selected = [[NSMutableIndexSet alloc] init];

        friends->_height = 31;
    }
    
    return friends;
}


- (NSMutableIndexSet *) Selected {
    
    return m_selected;
}

- (void) add:(NSDictionary *)dict {
    
    [invited_persons addObject:dict];
}

- (void) remove:(NSDictionary *)dict {
    
    int index = [[dict objectForKey:@"index"] intValue];
    UIView * view = [dict objectForKey:@"r_view"];

    [view removeFromSuperview];
    [invited_persons removeObject:dict];
    
    [m_selected removeIndex:index];
}

- (void) removeAll {
    
    for (NSDictionary * dict in invited_persons) {
        
        UIView * view = [dict objectForKey:@"r_view"];
        if (view)
            [view removeFromSuperview];
    }
    
    [m_selected removeAllIndexes];
    [invited_persons removeAllObjects];
    
    _height = 31;
}

- (int) Count {
    
    return [invited_persons count];
}

- (void) Clear {
    [invited_persons removeAllObjects];
}

- (NSMutableArray *) InvitedArray {
    
    return invited_persons;
}

+ (void)sendInAppSMS:(NSString *)phone controller:(UIViewController *)parentController delegate:(id<MFMessageComposeViewControllerDelegate>)delegate
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
	if([MFMessageComposeViewController canSendText])
	{
//		controller.body = @"Hello, you need to try Yabb application right now! It is free and fun. :)";
		controller.body = NSLocalizedString(@"I am using Yabb messenger. To chat with me for free please install Yabb on your phone at http://www.yabb.com/install.", @"I am using Yabb messenger. To chat with me for free please install Yabb on your phone at http://www.yabb.com/install.");
		controller.recipients = [NSArray arrayWithObject:phone];
		controller.messageComposeDelegate = delegate;
		[parentController presentViewController:controller animated:YES completion:^{}];
	}
}

@end
