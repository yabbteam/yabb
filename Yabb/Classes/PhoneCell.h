//
//  PhoneCell.h
//  speedsip
//
//  Created by denebtech on 29.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * phone_name;
@property (nonatomic, strong) IBOutlet UILabel * phone_value;

@end
