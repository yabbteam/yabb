//
//  BroadcastViewController.m
//  Baycall
//
//  Created by denebtech on 26.02.13.
//
//

#import "BroadcastViewController.h"
#import "InvitedFriends.h"
#import "ChatStorage.h"
#import "AddressCollector.h"
#import "ConnectionQueue.h"

@interface BroadcastViewController ()

@end

@implementation BroadcastViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [text_view becomeFirstResponder];

    UIBarButtonItem * send_button = [[UIBarButtonItem alloc]
                                     initWithTitle: NSLocalizedString(@"Send", @"Send")
                                     style: UIBarButtonItemStyleDone
                                     target: self action: @selector(Send)];
    
    [self.navigationItem setRightBarButtonItem:send_button];
    

}

- (void) Send {
    
    if (text_view.text == nil || [text_view.text isEqualToString:@""])
        return;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    NSString * phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_phone"];
    if (phone == nil)
        return;
    
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    [params setObject:@"addchat" forKey:@"cmd"];
    [params setObject:phone forKey:@"phone"];
    
//    int counter = 0;
    NSMutableArray* ma = [[NSMutableArray alloc] init];
    for (NSDictionary * dict in self.array) {
        
        NSString * person = [dict objectForKey:@"phone"];
        NSString * name = [dict objectForKey:@"name"];
        if (phone == nil || name == nil)
            continue;
        
        [ma addObject:person];
/*
        NSString * name_key = [NSString stringWithFormat:@"chats[chat_%d][name]", counter];
        NSString * abook_key = [NSString stringWithFormat:@"chats[chat_%d][abook]", counter];
        
        [params setObject:[[ChatStorage sharedStorage] encodeString:name] forKey:name_key];
        [params setObject:[[AddressCollector sharedCollector] packPhoneNumber:person] forKey:abook_key];
        
        counter++;
*/
    }
    [params setObject:ma forKey:@"abooks"];
    
    [[ConnectionQueue sharedQueue] Add_Connection:self withParams:params withName:@"addchats" silent:NO];
    
    NSLog(@"Add new chat: %@", params);
}

- (void) didConnectionDone:(NSDictionary *)dict withName:(NSString *)connection_name {
    
    if ([connection_name isEqualToString:@"addchats"]) {
        
        
        int error_code = [[dict objectForKey:@"errorcode"] intValue];
        if (error_code != 1)
            return;
        
/*
        NSDictionary * data = [dict objectForKey:@"data"];
        
        for (NSString * key in data) {
            
            
            NSArray * array = [data objectForKey:key];
            
            NSDictionary * chat;
            if ([array isKindOfClass:[NSDictionary class]]) {
                chat = [(NSDictionary*)array objectForKey:@"data"];
            }
            else {
                chat = [array objectAtIndex:2];
            }
            
            
            if (chat == nil || [chat objectForKey:@"chatid"] == nil)
                continue;
            
            NSString* chat_id = [chat objectForKey:@"chatid"];
            
            NSLog(@"Chat id: %@", chat_id);
            
            [[ChatStorage sharedStorage] addMessage:text_view.text withChatId:chat_id];
            
        }
 */
        NSArray * data = [dict objectForKey:@"data"];
        for (NSString* chat_id in data) {
            [[ChatStorage sharedStorage] addMessage:text_view.text withChatId:chat_id];
        }
        
    }
}

@end
