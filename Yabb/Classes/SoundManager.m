//
//  SoundManager.m
//  Yabb
//
//  Created by denebtechsaplin on 10/10/13.
//
//

#import <AVFoundation/AVFoundation.h>

#import "SoundManager.h"
#import "SettingsManager.h"

@interface SoundManager ()

@property (nonatomic, strong) AVAudioPlayer *messageReceivedSound;
@property (nonatomic, strong) AVAudioPlayer *messageSentSound;

@end

@implementation SoundManager

+ (instancetype)sharedInstance
{
    static SoundManager *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [self new];
    }
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.messageReceivedSound = [self playerWithResourceNamed:@"message_received"];
        self.messageSentSound = [self playerWithResourceNamed:@"message_sent"];
    }
    return self;
}

- (AVAudioPlayer *)playerWithResourceNamed:(NSString *)name
{
    NSError *error = nil;
    NSString *path =[[NSBundle mainBundle] pathForResource:name ofType:@"caf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    return [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
}

- (void)playMessageReceivedSound
{
    if ([[SettingsManager sharedInstance] enableSounds]) {
        if (!self.messageReceivedSound) {
            self.messageReceivedSound = [self playerWithResourceNamed:@"message_received"];
        }
        [self.messageReceivedSound play];
    }
}

- (void)playMessageReceivedVibrate
{
    if ([[SettingsManager sharedInstance] enableVibration]) {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
}

- (void)playMessageSentSound
{
    if (![[SettingsManager sharedInstance] enableSounds]) {
        return;
    }
    if (!self.messageSentSound) {
        self.messageSentSound = [self playerWithResourceNamed:@"message_sent"];
    }
    [self.messageSentSound play];
}

@end
