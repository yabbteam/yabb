//
//  Contacts.h
//  speedsip
//
//  Created by denebtech on 15.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contacts : NSObject {
    
    NSMutableArray * _contacts;
    NSMutableArray * _filtered_contacts;

    NSMutableArray * _friend_contacts;
    
    NSMutableDictionary * _grouped_contacts;
    NSMutableDictionary * _grouped_letters;
    NSMutableArray * _letters;
    NSMutableArray * _friends_letters;
    NSMutableDictionary * _grouped_friends;
    
    BOOL _has_filter;
    BOOL _friends_only;
}

@property (nonatomic) NSMutableArray * contacts;
@property (nonatomic, strong) NSMutableArray * filtered_contacts;

@property (nonatomic) NSMutableArray * friend_contacts;

@property (nonatomic) NSMutableDictionary * grouped_contacts;
@property (nonatomic) NSMutableDictionary * grouped_friends;
@property (nonatomic) NSMutableArray * letters;
@property (nonatomic) NSMutableArray * friends_letters;


@property (nonatomic, assign) BOOL has_filter;
@property (nonatomic, assign) BOOL friends_only;

- (void) Sort_By_Filter:(NSString *)filter friendsOnly:(BOOL)friends_only;
- (void) Make_Contacts;
- (void) Make_Friends;

@end
