//
//  AvatarManager.h
//  Yabb
//
//  Created by denebtech on 3/27/14.
//
//

#import <Foundation/Foundation.h>

@interface AvatarManager : NSObject

+ (AvatarManager *) sharedInstance;
- (void) clear;
- (void) save;

- (void) setEjabberdAvatar:(UIImage*) image hash:(NSString*)hash phone:(NSString*)phone;
//- (void) setUserAvatar:(UIImage*) image phone:(NSString*)phone;
- (UIImage*) getAvatarWithPhone:(NSString*)phone;
- (UIImage*) getAvatarWithAddressCollecterIndex:(int)index;
- (UIImage*) getMyAvatar;
- (void) setMyAvatar:(UIImage*) avatar hash:(NSString*)hash;
- (void) removeMyAvatar;
- (NSString*) getEjabberdAvatarHash:(NSString*)phone;
- (NSString*) getMyEjabberdAvatarHash:(NSString*)phone;

@end

