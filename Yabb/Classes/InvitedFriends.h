//
//  InvitedFriends.h
//  Baycall
//
//  Created by denebtech on 02.10.12.
//
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface InvitedFriends : NSObject {
    
    NSMutableArray * invited_persons;
    NSMutableIndexSet * m_selected;

    int _height;
}

@property (nonatomic, assign) int height;

+ (InvitedFriends *) SharedFriends;

- (NSMutableIndexSet *) Selected;
- (void) add:(NSDictionary *)dict;
- (void) remove:(NSDictionary *)dict;
- (void) removeAll;
- (int) Count;
- (void) Clear;
- (NSMutableArray *) InvitedArray;

+ (void)sendInAppSMS:(NSString *)phone controller:(UIViewController *)parentController delegate:(id<MFMessageComposeViewControllerDelegate>)delegate;

@end
