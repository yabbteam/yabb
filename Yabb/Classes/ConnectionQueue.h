//
//  ConnectionQueue.h
//  speedsip
//
//  Created by denebtech on 09.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BUFFER_SIZE_KB	1024

@interface ConnectionQueue : NSObject {
    
    NSMutableArray * connections;
    int busy;
    NSURLConnection * active_connection;
    NSMutableData * data;
    NSDictionary * current_connection;
    
    // Cycle
    NSTimeInterval cycle_time;
    int cycle_period;
    id cycle_delegate;
    NSDictionary * cycle_params;
    BOOL cycle_forced; // will send request immediately
    BOOL cycle_silent; // will show an alert in case of a falure
}

+ (ConnectionQueue *) sharedQueue;
- (void) startConnectionCycle:(id)delegate withPeriod:(float)period;
- (void) forceCycle;
- (void) setCycleSilent:(BOOL)silent;
- (void) setBusy:(BOOL)b;
- (void) stop;
- (void) Add_Connection:(id)target withParams:(NSDictionary *)params withName:(NSString *)name silent:(BOOL)silent;
- (void) Add_Connection_To_REST:(id)target withParams:(NSDictionary *)params withName:(NSString *)name silent:(BOOL)silent;

@end
