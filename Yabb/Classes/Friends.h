//
//  Friends.h
//  Baycall
//
//  Created by denebtech on 23.09.12.
//
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

#define DEFAULT_ORIGIN NSLocalizedString(@"I am using Yabb",@"I am using Yabb")

@interface Friends : NSObject {
    
    NSMutableArray * all_friends;
}

+ (Friends *) SharedFriends;

- (int) numberOfFriends;

- (NSMutableDictionary *) friendWithPhone:(NSString *)phone;

- (BOOL) isFriendPhone:(NSString *)phone;
- (BOOL) isFriendIndex:(int)index;
- (NSString *) friendPhoneByIndex:(int)index;

- (BOOL) isPersonOnlineIndex:(int)index;

- (void) setOnline:(NSArray *)array;
- (void) updateOnline:(NSArray *)array;
- (BOOL) updateOnlineWithPhone:(NSString*)phone online:(BOOL)isOnline;
- (NSString *) originByPhone:(NSString *) phone withDefault:(BOOL)def;
- (NSString *) originByIndex:(int)index;
- (NSString *) nameByPhone:(NSString *)phone;
- (void) updateLastSeenByPhone:(NSString*)phone idle:(long long) idleTime;
- (void) updatePhonebookRelation;

@end
