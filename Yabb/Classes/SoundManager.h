//
//  SoundManager.h
//  Yabb
//
//  Created by denebtech on 10/10/13.
//
//

#import <Foundation/Foundation.h>

@interface SoundManager : NSObject

+ (instancetype)sharedInstance;
- (void)playMessageReceivedSound;
- (void)playMessageReceivedVibrate;
- (void)playMessageSentSound;

@end
