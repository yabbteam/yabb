//
//  Alerts.m
//  speedsip
//
//  Created by Tolian T on 15.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Alert.h"

@implementation Alert

+ (void) show:(NSString *)title withMessage:(NSString *)message {
    
    UIAlertView * alert_view = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil];
    [alert_view show];
}

@end
