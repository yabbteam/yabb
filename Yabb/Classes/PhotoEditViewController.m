//
//  PhotoEditViewController.m
//  Yabb
//
//  Created by denebtech on 2/27/14.
//
//

#import "PhotoEditViewController.h"
#import "ImagePickerViewController.h"
#import "CommonUtil.h"
#import <FacebookSDK.h>
#import "JabberLayer.h"
#import "AppDelegate.h"
#import "AvatarManager.h"

@interface PhotoEditViewController ()

@end

@implementation PhotoEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.parentController = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationItem setTitle:NSLocalizedString(@"Select your photo", @"Select your photo")];
    
    UIBarButtonItem* but = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done") style:UIBarButtonItemStylePlain target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = but;
    
    photoView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickAddPhoto:)];
    [photoView addGestureRecognizer:tapGestureRecognizer];
    
    self.resultImage = nil;
    if (self.parentController != nil) {
        [[AvatarManager sharedInstance] removeMyAvatar];
        photoView.image = [UIImage imageNamed:@"placeholderPerson_52.png"];
    }
    else {
        textNickName.text = [[JabberLayer SharedInstance] getMyNickName];
        if ([[AvatarManager sharedInstance] getMyAvatar] != nil) {
            photoView.image = [[AvatarManager sharedInstance] getMyAvatar];
            self.resultImage = photoView.image;
        }
        else {
            photoView.image = [UIImage imageNamed:@"placeholderPerson_52.png"];
        }
    }
    
    CALayer * l = [photoView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:4.0];
    
    buttonTableView.scrollEnabled = NO;
    
    if (self.parentController != nil) {
        self.navigationItem.hidesBackButton = YES;
        textNickName.placeholder = NSLocalizedString(@"Required", @"Required");
    }
    
    [textNickName becomeFirstResponder];
    textNickName.delegate = self;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) done:(id) sender {
    
    if (!self.parentController) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
        [[JabberLayer SharedInstance] updateAvatar:self.resultImage nickname:textNickName.text];
    }
    else {
        
        if (textNickName.text.length <= 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                message:NSLocalizedString(@"Please fill in name", @"Please fill in name")
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return;
        }
        
        [[JabberLayer SharedInstance] updateAvatarAfter:self.resultImage nickname:textNickName.text];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:NO];
            app.isRegistering = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"request_login" object:nil];
        }];
    }
}

- (IBAction) onClickAddPhoto:(id)sender {
    UIActionSheet* actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    [actSheet addButtonWithTitle:NSLocalizedString(@"Take Picture",@"Take Picture")];
    [actSheet addButtonWithTitle:NSLocalizedString(@"Choose From Gallery", @"Choose From Gallery")];
    
    int cancel_index = (int)[actSheet addButtonWithTitle:NSLocalizedString(@"Cancel", @"Cancel")];
    [actSheet setCancelButtonIndex:cancel_index];
    
    if (self.parentController != nil)
        [actSheet showInView:self.view];
    else
        [actSheet showInView:self.tabBarController.view];
}

- (IBAction) onClickTakeCamera:(id)sender {
    
    if ( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        
        ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypeCamera];
        [image_picker setImage_picker_delegate:self];

        [self presentViewController:image_picker animated:YES completion:^{}];
    }
}

- (IBAction) onClickPhotoGallery:(id)sender {
    
    ImagePickerViewController * image_picker = [[ImagePickerViewController alloc] initWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    [image_picker setImage_picker_delegate:self];
    
    [self presentViewController:image_picker animated:YES completion:^{}];
}

- (IBAction) onClickFacebookGallery:(id)sender {
    
    [app.acc showActivityViewer:NSLocalizedString(@"Getting Photo...", @"Getting Photo...")];
    
    if (!FBSession.activeSession.isOpen) {
        app.gettingAvatarFromFacebook = YES;
        
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"user_photos", nil];
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              [app.acc hideActivityViewer];
                                              
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:error.localizedDescription
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                              [alertView show];
                                          } else if (session.isOpen) {
                                              [self loadFaceBookPhotos];
                                          }
                                      }];
        return;
    }
    
    [self loadFaceBookPhotos];
}

-(void) loadFaceBookPhotos {
    
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *aUser, NSError *error) {
         if (!error) {
             
             NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture", [aUser objectForKey:@"id"]];
             NSMutableURLRequest *urlRequest =
             [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                                 timeoutInterval:60.0f];
             
             // Run request asynchronously
             NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest
                                                                              delegate:self];
             if (!urlConnection)
                 NSLog(@"Failed to download picture");
             
             NSLog(@"User id %@",[aUser objectForKey:@"id"]);
         }
         else {
             [app.acc hideActivityViewer];
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                 message:error.localizedDescription
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
             [alertView show];
         }
     }];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [app.acc hideActivityViewer];
    
    if (data != nil) {
        UIImage* image = [[UIImage alloc] initWithData:data];
        photoView.image = image;
        self.resultImage = image;
        
        [buttonTableView reloadData];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Error"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    [app.acc hideActivityViewer];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (IBAction) onClickRemove:(id)sender {
 
    self.resultImage = nil;
    photoView.image = [UIImage imageNamed:@"placeholderPerson_52.png"];
    
    [buttonTableView reloadData];
}

- (void) Pick_Image:(UIImage *)image assetReference:(NSString *)asset {
    
    if (image == nil)
        return;
    
    UIImage* pickResult = [CommonUtil fixOrientation:image];
    
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:pickResult cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
    
    imgCropperVC.delegate = self;
    [self presentViewController:imgCropperVC animated:YES completion:^{
    }];
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    
    photoView.image = [CommonUtil imageByScalingToSize:CGSizeMake(AVATAR_SIZE, AVATAR_SIZE) cgimage:editedImage.CGImage orientation:editedImage.imageOrientation];
    self.resultImage = photoView.image;
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];

    [buttonTableView reloadData];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    
    [cropperViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) pickImageProcess {
    
}

#pragma mark -
#pragma mark Table Delegates

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            [self onClickAddPhoto:self];
            break;
        case 1:
            [self onClickFacebookGallery:self];
            break;
        case 2:
            if (self.parentController == nil) {
                [self onClickRemove:self];
            }
            else {
                //skip
                [self dismissViewControllerAnimated:YES completion:^{
                    [self.navigationController popViewControllerAnimated:NO];
                    app.isRegistering = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"request_login" object:nil];
                    
                }];
            }
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.parentController != nil)
        return 2;
    
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return nil;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 0.0f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
    
    UILabel * label = (UILabel *)[cell viewWithTag:200];
    if (label == nil) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(20, 2, 280, 22)];
        [label setFont:[UIFont boldSystemFontOfSize:18.0]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTag:200];
        
        [cell addSubview:label];
        [label setTextAlignment:NSTextAlignmentCenter];
    }
    
    
    [label setFrame:CGRectMake(20, 0, 280, 44)];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    int section = indexPath.section;
    
    [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
    
    if (section == 0) {
        if (indexPath.row == 0) {
            if (self.resultImage == nil)
                label.text = NSLocalizedString(@"Add Photo", @"Add Photo");
            else
                label.text = NSLocalizedString(@"Change Photo", @"Change Photo");
        }
        else if (indexPath.row == 1)
            label.text = NSLocalizedString(@"From Facebook", @"From Facebook");
        else if (indexPath.row == 2) {
            if (self.parentController == nil)
                label.text = NSLocalizedString(@"Remove Photo", @"Remove Photo");
            else
                label.text = NSLocalizedString(@"Skip", @"Skip");
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self onClickTakeCamera:self];
    }
    else if (buttonIndex == 1) {
        [self onClickPhotoGallery:self];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > 15) ? NO : YES;
}

@end
